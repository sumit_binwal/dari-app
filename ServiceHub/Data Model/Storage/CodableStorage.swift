//
//  CodableStorage.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/5/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

class CodableStorage {
   private let storage: DiskStorage
   private let decoder: JSONDecoder
   private let encoder: JSONEncoder
   
   init(storage: DiskStorage, decoder: JSONDecoder = .init(), encoder: JSONEncoder = .init()) {
      self.storage = storage
      self.decoder = decoder
      self.encoder = encoder
   }
   
   convenience init(path: URL, decoder: JSONDecoder = .init(), encoder: JSONEncoder = .init()) {
      self.init(storage: DiskStorage(path: path), decoder: decoder, encoder: encoder)
   }
   
   convenience init() {
      guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
         fatalError("[CodableStorage] Failed to get document directory.")
      }
      self.init(storage: DiskStorage(path: url))
   }
   
   func load<T: Decodable>(for key: String) throws -> T {
      let data = try storage.fetchValue(for: key)
      return try decoder.decode(T.self, from: data)
   }
   
   func save<T: Encodable>(_ value: T, for key: String) throws {
      let data = try encoder.encode(value)
      try storage.save(value: data, for: key)
   }
   
   func deleteData(for key: String) throws {
      try self.storage.deleteData(for: key)
   }
}
