//
//  ImageWrapper.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/23/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

struct ImageWrapper: Codable {
    var image: UIImage
    
    enum CodingKeys: String, CodingKey {
        case imageData
    }
    
    init?(_ image: UIImage?) {
        guard let image = image else { return nil }
        self.image = image
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let imageData = try container.decode(Data.self, forKey: .imageData)
        guard let image = UIImage(data: imageData) else {
            fatalError("[ImageWrapper] Failed to get image from data.")
        }
        self.image = image
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        guard let imageData = image.pngData() else {
            fatalError("[ImageWrapper] Failed to get data from image.")
        }
        try container.encode(imageData, forKey: .imageData)
    }
}
