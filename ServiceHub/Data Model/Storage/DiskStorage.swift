//
//  DiskStorage.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/5/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

enum StorageError: Error {
   case notFound
   case cantWrite(Error)
}

class DiskStorage {
   private let queue: DispatchQueue
   private let fileManager: FileManager
   private let path: URL
   
   init(
      path: URL,
      queue: DispatchQueue = .init(label: "DiskCache.Queue"),
      fileManager: FileManager = .default
   ) {
      self.path = path
      self.queue = queue
      self.fileManager = fileManager
   }
}

extension DiskStorage: WritableStorage {
   func save(value: Data, for key: String) throws {
      let url = path.appendingPathComponent(key)
      do {
         try createFolders(in: url)
         try value.write(to: url, options: .atomic)
      } catch {
         throw StorageError.cantWrite(error)
      }
   }
   
   func save(value: Data, for key: String, handler: @escaping Handler<Data>) {
      queue.async {
         do {
            try self.save(value: value, for: key)
            handler(.success(value))
         } catch {
            handler(.failure(error))
         }
      }
   }
   
   private func createFolders(in url: URL) throws {
      let folderURL = url.deletingLastPathComponent()
      if !fileManager.fileExists(atPath: folderURL.path) {
         try fileManager.createDirectory(
            at: folderURL,
            withIntermediateDirectories: true,
            attributes: nil
         )
      }
   }
   
   func deleteData(for key: String) throws {
      let url = self.path.appendingPathComponent(key)
      guard self.fileManager.fileExists(atPath: url.path) else { return }
      try self.fileManager.removeItem(at: url)
   }
}

extension DiskStorage: ReadableStorage {
   func fetchValue(for key: String) throws -> Data {
      let url = path.appendingPathComponent(key)
      guard let data = fileManager.contents(atPath: url.path) else {
         throw StorageError.notFound
      }
      return data
   }
   
   func fetchValue(for key: String, handler: @escaping Handler<Data>) {
      queue.async {
         handler(Result { try self.fetchValue(for: key) })
      }
   }
}
