//
//  Service.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/9/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

class Service: Codable {
   static var manager: Manager { return Manager.default }
   // MARK: - Properties
   let id: Int
   let englishName: String
   let arabicName: String
   /// Occurrences per week.
   let frequency: Int
   /// Number of weeks
   let periodCount: Int
   let itemConstraints: [Item.Constraints]
   let rateType: RateType

   private let englishWeekdaySelectionInstruction: String
   private let arabicWeekdaySelectionInstruction: String
   private let englishDateSelectionInstruction: String
   private let arabicDateSelectionInstruction: String
   private let englishTimeSelectionInstruction: String
   private let arabicTimeSelectionInstruction: String
   private let englishNoteInstruction: String
   private let arabicNoteInstruction: String
   
   let parentID: Int?
   private let englishTitleAsVariant: String
   private let arabicTitleAsVariant: String
   private let englishVariantSelectionInstruction: String
   private let arabicVariantSelectionInstruction: String
   let rank: Int
   
   // MARK: - Codable
   private enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case frequency
      case periodCount = "period_count"
      case itemConstraints = "item_constraints"
      // Do not change rate type without changing its corresponding coding key in Services
      case rateType = "rate_type"
      
      case englishWeekdaySelectionInstruction = "weekday_selection_instruction_english"
      case arabicWeekdaySelectionInstruction = "weekday_selection_instruction_arabic"
      case englishDateSelectionInstruction = "date_selection_instruction_english"
      case arabicDateSelectionInstruction = "date_selection_instruction_arabic"
      case englishTimeSelectionInstruction = "time_selection_instruction_english"
      case arabicTimeSelectionInstruction = "time_selection_instruction_arabic"
      case englishNoteInstruction = "note_instruction_english"
      case arabicNoteInstruction = "note_instruction_arabic"
      
      case parentID = "parent"
      case englishTitleAsVariant = "title_as_variant_english"
      case arabicTitleAsVariant = "title_as_variant_arabic"
      case englishVariantSelectionInstruction = "variant_selection_instruction_english"
      case arabicVariantSelectionInstruction = "variant_selection_instruction_arabic"
      case rank = "rank"
   }
   
   // MARK: - Computed Properties
   var name: String { Environment.languageIsArabic ? self.arabicName : self.englishName }
   var weekdaySelectionInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicWeekdaySelectionInstruction : self.englishWeekdaySelectionInstruction
      return specificInstruction.isEmpty ?
         self.defaultWeekdaySelectionInstruction(for: self.frequency) : specificInstruction
   }
   
   var invalidWeekdaySelectionCountWarning: String {
      switch self.frequency {
      case 1: return "oneWeekdaySelectionWarning".localized
      case 2: return "twoWeekdaySelectionWarning".localized
      default:
         let formatter = NumberFormatter()
         formatter.numberStyle = .spellOut
         let number = formatter.string(from: self.frequency)
         return "\("multipleWeekdaySelectionWarning1".localized) \(number) \("multipleWeekdaySelectionWarning2".localized)"
      }
   }
   
   var dateSelectionInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicDateSelectionInstruction : self.englishDateSelectionInstruction
      let defaultInstruction: String = (
         self.serviceCount > 1 ? "multipleDateSelectionInstruction" : "dateSelectionInstruction"
      ).localized
      return specificInstruction.isEmpty ? defaultInstruction : specificInstruction
   }
   var timeSelectionInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicTimeSelectionInstruction : self.englishTimeSelectionInstruction
      return specificInstruction.isEmpty ? "timeSelectionInstruction".localized : specificInstruction
   }
   var noteInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicNoteInstruction : self.englishNoteInstruction
      return specificInstruction.isEmpty ? "noteInstruction".localized : specificInstruction
   }
   var parent: Service? {
      Self.service(withID: self.parentID)
   }
   var titleAsVariant: String {
      Environment.languageIsArabic ? self.arabicTitleAsVariant : self.englishTitleAsVariant
   }
   var variantSelectionInstruction: String {
      Environment.languageIsArabic ?
         self.arabicVariantSelectionInstruction : self.englishVariantSelectionInstruction
   }
   lazy var variants: [Service] = {
      Self.all.filter { $0.parentID == self.id }
   }()
   var serviceCount: Int { self.frequency * self.periodCount }
   
   // MARK: Other
   private func defaultWeekdaySelectionInstruction(for weekdayCount: Int) -> String {
      switch weekdayCount {
      case 1: return "oneWeekdaySelectionInstruction".localized
      case 2: return "twoWeekdaySelectionInstruction".localized
      default:
         let formatter = NumberFormatter()
         formatter.numberStyle = .spellOut
         let number = formatter.string(from: weekdayCount)
         return "\("multipleWeekdaySelectionInstruction1".localized) \(number) \("multipleWeekdaySelectionInstruction2".localized)"
      }
   }
}

// MARK: - Save and Load
extension Service {
   private class var archiveKey: String { "services" }
   
   class func service(withID id: Int?) -> Service? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   class var all: [Service] {
      get {
         let wrapper: Services? = try? CodableStorage().load(for: self.archiveKey)
         return wrapper?.services ?? []
      }
      set {
         do {
            try CodableStorage().save(Services(newValue), for: self.archiveKey)
         } catch {
            fatalError("[Service] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}

// MARK: - Equatable
extension Service: Equatable {
   static func ==(lhs: Service, rhs: Service) -> Bool { lhs.id == rhs.id }
}

// MARK: - Hashable
extension Service: Hashable {
   func hash(into hasher: inout Hasher) {
      hasher.combine(self.id)
   }
}

// MARK: - Comparable
extension Service: Comparable {
   static func <(lhs: Service, rhs: Service) -> Bool { lhs.rank < rhs.rank }
}
