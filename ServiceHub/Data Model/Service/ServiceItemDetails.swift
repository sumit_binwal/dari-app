//
//  ServiceItemDetails.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

struct ItemDetails: Codable {
   let item: Item
   let minimumItemCount: Int
   let maximumItemCount: Int
   let minimumItemMeasurement: Double?
   let maximumItemMeasurement: Double?
   let measurements: [Double]?
   
   enum CodingKeys: String, CodingKey {
      case item
      case minimumItemCount = "minimum_item_count"
      case maximumItemCount = "maximum_item_count"
      case minimumItemMeasurement = "minimum_item_measurement"
      case maximumItemMeasurement = "maximum_item_measurement"
      case measurements
   }
}
