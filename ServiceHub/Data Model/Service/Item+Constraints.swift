//
//  Item+Constraints.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

extension Item {
   struct Constraints: Codable {
      var itemID: Int
      var item: Item {
         guard let item = Item.item(withID: self.itemID) else {
            fatalError("[Item.Constraint] Failed to find item.")
         }
         return item
      }
      var instruction: String {
         Environment.languageIsArabic ? self.arabicInstruction : self.englishInstruction
      }
      let englishInstruction: String
      let arabicInstruction: String
      let quantities: [Int]
      
      enum CodingKeys: String, CodingKey {
         case itemID = "item"
         case englishInstruction = "instruction_english"
         case arabicInstruction = "instruction_arabic"
         case quantities
      }
      
      var defaultParameter: Parameter {
         .init(
            item: self.item.variants.first ?? self.item,
            serviceOption: self.item.serviceOptions.first,
            areaDimensions: self.item.rateType == .area ? .default : nil
         )
      }
   }
}
