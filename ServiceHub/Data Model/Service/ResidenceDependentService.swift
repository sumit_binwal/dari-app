//
//  ResidenceDependentService.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/5/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

class ResidenceDependantService: Service {
   // MARK: - Stored Properties
   let residenceTypes: [ResidenceType]
   let roomCounts: [Int]
   private let englishResidenceTypeInstruction: String
   private let arabicResidenceTypeInstruction: String
   private let englishRoomCountInstruction: String
   private let arabicRoomCountInstruction: String
   
   // MARK: - Computed Properties
   var roomCountInstruction: String {
      Environment.languageIsArabic ?
         self.arabicRoomCountInstruction : self.englishRoomCountInstruction
   }
   var residenceTypeInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicResidenceTypeInstruction : self.englishResidenceTypeInstruction
      return specificInstruction.isEmpty ?
         "residenceTypeSelectionInstruction".localized : specificInstruction
   }
   
   // MARK: - Codable
   private enum CodingKeys: String, CodingKey {
      case residenceTypes = "residence_types"
      case roomCounts = "room_counts"
      case englishResidenceTypeInstruction = "residence_type_instruction_english"
      case arabicResidenceTypeInstruction = "residence_type_instruction_arabic"
      case englishRoomCountInstruction = "room_count_instruction_english"
      case arabicRoomCountInstruction = "room_count_instruction_arabic"
   }
   
   required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.residenceTypes = try container.decode([ResidenceType].self, forKey: .residenceTypes)
      self.roomCounts = try container.decode([Int].self, forKey: .roomCounts)
      
      self.englishResidenceTypeInstruction = try container.decode(
         String.self,
         forKey: .englishResidenceTypeInstruction
      )
      self.arabicResidenceTypeInstruction = try container.decode(
         String.self,
         forKey: .arabicResidenceTypeInstruction
      )
      
      self.englishRoomCountInstruction = try container.decode(
         String.self,
         forKey: .englishRoomCountInstruction
      )
      self.arabicRoomCountInstruction = try container.decode(
         String.self,
         forKey: .arabicRoomCountInstruction
      )
      
      try super.init(from: decoder)
   }
   
   override func encode(to encoder: Encoder) throws {
      try super.encode(to: encoder)
      
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(self.residenceTypes, forKey: .residenceTypes)
      try container.encode(self.roomCounts, forKey: .roomCounts)
      try container.encode(
         self.englishResidenceTypeInstruction,
         forKey: .englishResidenceTypeInstruction
      )
      try container.encode(
         self.arabicResidenceTypeInstruction,
         forKey: .arabicResidenceTypeInstruction
      )
      try container.encode(self.englishRoomCountInstruction, forKey: .englishRoomCountInstruction)
      try container.encode(self.arabicRoomCountInstruction, forKey: .arabicRoomCountInstruction)
   }
}
