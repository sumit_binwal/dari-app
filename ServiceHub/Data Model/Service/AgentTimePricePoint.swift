//
//  AgentTimePricePoint.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct AgentTimePricePoint: PricePointValue {
   // MARK: - Properties
   let id: Int
   let dateModified: Date
   let valueAsString: String
   var rateType: RateType { .agentTime }
   var value: Double { Double(self.valueAsString) ?? 0 }
   
   // MARK: - Coding Keys
   enum CodingKeys: String, CodingKey {
      case id
      case dateModified = "date_modified"
      case valueAsString = "value"
   }
   
   // MARK: - Decoding
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.valueAsString = try container.decode(String.self, forKey: .valueAsString)
   }
   
   // MARK: - Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
      
      try container.encode(self.valueAsString, forKey: .valueAsString)
   }
   
   // MARK: - Other
   func orderItem(for parameters: [Order.Field: AnyHashable?]) -> Order.InvoiceItem? {
      guard let agentCount = parameters[.agentCount] as? Int,
         let agentType = parameters[.agentType] as? AgentType,
         let duration = parameters[.duration] as? Int else { return nil }
      
      return .init(
         parameters: [
            .agentCount: agentCount,
            .duration: duration,
            .agentType: agentType
         ],
         pricePointValue: self,
         price: Double(agentCount * duration) * value
      )
   }
   
   func parameterDescription(_ parameters: [Order.Field : AnyHashable?]) -> String? {
      guard let agentCount = parameters[.agentCount] as? Int,
         let duration = parameters[.duration] as? Int,
         let agentType = parameters[.agentType] as? AgentType else { return nil }
      
      var string = "\(agentType.agentCountDescription(agentCount))\(",".localized) "
      string += {
         let number = NumberFormatter.integer.string(from: duration)
         switch duration {
         case 1: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("hour".localized)"
         case 2: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("2hours".localized)"
         default: return "\(number) \("hours".localized)"
         }
      }()
      return string
   }
   
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String? {
      guard let agentType = parameters[.agentType] as? AgentType else { return nil }
      
      let formatter: NumberFormatter = self.value.isWholeNumber ? .shortCurrency : .currency
      return formatter.string(from: self.value) + "/".localized +
         "hour".localized.localizedCapitalized + "/".localized + agentType.singularName
   }
}
