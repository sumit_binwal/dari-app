//
//  Service+Availability.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/5/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Service {
   class Availability: Codable {
      static var cityWideExcludedDates: [Date] = []
      
      private static var calendar: Calendar = {
         var calendar = Calendar.current
         calendar.timeZone = Country.current?.timeZone ?? calendar.timeZone
         return calendar
      }()
      
      // MARK: - Stored Properties
      let isRollingWindow: Bool
      let minimumLeadTime: Int?
      let maximumLeadTime: Int?
      let startDate: Date?
      let endDate: Date?
      let isInTimeSlots: Bool
      let closingHour: Int?
      let hourLeadTime: Int
      let weekdays: [Int]
      let hours: [Int]
      let timeSlotIDs: [Int]
      let exclusions: [Exclusion]
      
      // MARK: - Coding Keys
      enum CodingKeys: String, CodingKey {
         case isRollingWindow = "is_rolling_window"
         case minimumLeadTime = "minimum_lead_time"
         case maximumLeadTime = "maximum_lead_time"
         case startDate = "start_date"
         case endDate = "end_date"
         case isInTimeSlots = "is_in_time_slots"
         case closingHour = "closing_hour"
         case hourLeadTime = "hour_lead_time"
         case weekdays
         case hours
         case timeSlotIDs = "time_slots"
         case exclusions
      }
      
      // MARK: - Decoding
      required init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         self.isRollingWindow = try container.decode(Bool.self, forKey: .isRollingWindow)
         self.minimumLeadTime = try? container.decode(Int.self, forKey: .minimumLeadTime)
         self.maximumLeadTime = try? container.decode(Int.self, forKey: .maximumLeadTime)
         
         if let startDateString = try? container.decode(String.self, forKey: .startDate) {
            if let date = DateFormatter.serverDate.date(from: startDateString) {
               self.startDate = date
            } else {
               throw DecodingError.dataCorruptedError(
                  forKey: .startDate,
                  in: container,
                  debugDescription: "Date string doesn't match format expected by formatter."
               )
            }
         } else {
            self.startDate = nil
         }
         
         if let endDateString = try? container.decode(String.self, forKey: .endDate) {
            if let date = DateFormatter.serverDate.date(from: endDateString) {
               self.endDate = date
            } else {
               throw DecodingError.dataCorruptedError(
                  forKey: .endDate,
                  in: container,
                  debugDescription: "Date string doesn't match format expected by formatter."
               )
            }
         } else {
            self.endDate = nil
         }
         
         self.isInTimeSlots = try container.decode(Bool.self, forKey: .isInTimeSlots)
         self.closingHour = try? container.decode(Int.self, forKey: .closingHour)
         self.hourLeadTime = try container.decode(Int.self, forKey: .hourLeadTime)
         self.weekdays = try container.decode([Int].self, forKey: .weekdays)
         self.hours = try container.decode([Int].self, forKey: .hours)
         self.timeSlotIDs = try container.decode([Int].self, forKey: .timeSlotIDs)
         self.exclusions = try  container.decode([Exclusion].self, forKey: .exclusions)
      }
      
      // MARK: - Encoding
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.isRollingWindow, forKey: .isRollingWindow)
         try container.encode(self.minimumLeadTime, forKey: .minimumLeadTime)
         try container.encode(self.maximumLeadTime, forKey: .maximumLeadTime)
         
         if let startDate = self.startDate {
            try container.encode(DateFormatter.serverDate.string(from: startDate), forKey: .startDate)
         }
         if let endDate = self.endDate {
            try container.encode(DateFormatter.serverDate.string(from: endDate), forKey: .endDate)
         }
         
         try container.encode(self.isInTimeSlots, forKey: .isInTimeSlots)
         try container.encode(self.closingHour, forKey: .closingHour)
         try container.encode(self.hourLeadTime, forKey: .hourLeadTime)
         try container.encode(self.weekdays, forKey: .weekdays)
         try container.encode(self.hours, forKey: .hours)
         try container.encode(self.timeSlotIDs, forKey: .timeSlotIDs)
         try container.encode(self.exclusions, forKey: .exclusions)
      }
      
      // MARK: - Computed Properties
      lazy var timeSlots: [TimeSlot] = {
         self.timeSlotIDs.compactMap { TimeSlot.timeSlot(withID: $0) }
      }()
      lazy var excludedDates: [Date] = {
         var dates = self.exclusions.compactMap { $0.isAllDay ? $0.date : nil }
         dates.append(contentsOf: Self.cityWideExcludedDates)
         return dates
      }()
      lazy var excludedHours: [Date: [Int]] = {
         let exclusions = self.exclusions.filter { $0.isHour }
         let exclusionSets: [Date: Set<Int>] = exclusions.reduce(into: [:]) {
            $0[$1.date, default: Set<Int>()].insert($1.hour!)
         }
         return exclusionSets.mapValues { Array($0) }
      }()
      lazy var excludedTimeSlots: [Date: [TimeSlot]] = {
         let exclusions = self.exclusions.filter { $0.isTimeSlot }
         let exclusionSets: [Date: Set<TimeSlot>] = exclusions.reduce(into: [:]) {
            $0[$1.date, default: Set<TimeSlot>()].insert($1.timeSlot!)
         }
         return exclusionSets.mapValues { Array($0) }
      }()
   }
}

extension Service.Availability {
   
   func dates(forSelectedDuration selectedDuration: Int? = nil) -> [Date] {
      var dates: [Date] = {
         if self.isRollingWindow {
            guard let minimumLeadTime = self.minimumLeadTime,
               let maximumLeadTime = self.maximumLeadTime else
            {
               assertionFailure("[Availability] Rolling window has missing lead times.")
               return []
            }
            return self.dates(
               fromMinimumLeadTime: minimumLeadTime,
               toMaximumLeadTime: maximumLeadTime
            )
         } else {
            guard let startDate = self.startDate, let endDate = self.endDate else {
               assertionFailure("[Availability] Missing start or end date.")
               return []
            }
            return self.dates(from: startDate, to: endDate, leadTime: self.minimumLeadTime ?? 1)
         }
      }()
      
      let calendar = Self.calendar
      
      dates = dates.filter {
         self.weekdays.contains(calendar.component(.weekday, from: $0))
      }
      
      dates = dates.filter {
         for date in excludedDates {
            let comparison = calendar.compare(date, to: $0, toGranularity: .day)
            if comparison == .orderedSame { return false }
         }
         return true
      }
      
      dates = dates.filter {
         if self.isInTimeSlots {
            return !self.timeSlots(forDate: $0, selectedDuration: selectedDuration).isEmpty
         } else {
            return !self.hours(forDate: $0).isEmpty
         }
      }
      
      return dates
   }
   
   func hours(forDate date: Date?) -> [Int] {
      guard let date = date else { return self.hours }
      
      let calendar = Self.calendar
      
      let excludedHours: [Int] = {
         for (exclusionDate, excludedHours) in self.excludedHours {
            if calendar.compare(date, to: exclusionDate, toGranularity: .day) == .orderedSame {
               return excludedHours
            }
         }
         return []
      }()
      
      var hours = self.hours.filter { !excludedHours.contains($0) }
      
      todayCheck: if calendar.isDateInToday(date) {
         let components = calendar.dateComponents([.hour], from: Date())
         guard let currentHour = components.hour else { break todayCheck }
         hours = hours.filter { $0 > currentHour + self.hourLeadTime}
      }
      
      return hours
   }
   
   func timeSlots(forDate date: Date?, selectedDuration: Int? = nil) -> [TimeSlot] {
      guard let date = date else { return self.timeSlots }
      
      let calendar = Self.calendar
      
      // Get and excluded time slots
      let excludedTimeSlots: [TimeSlot] = {
         for (exclusionDate, excludedTimeSlots) in self.excludedTimeSlots {
            if calendar.compare(date, to: exclusionDate, toGranularity: .day) == .orderedSame {
               return excludedTimeSlots
            }
         }
         return []
      }()
      
      // Remove excluded time slots
      var timeSlots = self.timeSlots.filter { !excludedTimeSlots.contains($0) }
      
      // If date is today, remove time slots that can no longer be booked
      todayCheck: if calendar.isDateInToday(date) {
         let components = calendar.dateComponents([.hour], from: Date())
         guard let currentHour = components.hour else { break todayCheck }
         timeSlots = timeSlots.filter { $0.startingHour > currentHour + self.hourLeadTime}
      }
      
      // If duration is too long and there's a closing hour, remove time slots that are too late
      if let selectedDuration = selectedDuration, let closingHour = closingHour {
         timeSlots = timeSlots.filter { $0.startingHour + selectedDuration <= closingHour }
      }
      
      return timeSlots
   }
   
   func dates(
      forSelectedWeekdays weekdays: [Int],
      andDuration selectedDuration: Int? = nil,
      returningAllIfEmpty: Bool = false
   ) -> [Date] {
      
      let dates = self.dates(forSelectedDuration: selectedDuration).filter {
         guard let weekday = Self.calendar.dateComponents([.weekday], from: $0).weekday else {
            return false
         }
         return weekdays.contains(weekday)
      }
      return (dates.isEmpty && returningAllIfEmpty) ? self.dates() : dates
   }
   
   private func dates(
      fromMinimumLeadTime minimumLeadTime: Int,
      toMaximumLeadTime maximumLeadTime: Int
   ) -> [Date] {
      let calendar = Self.calendar
      let earliestDate = calendar.startOfDay(
         for: Date().addingTimeInterval(Double(minimumLeadTime) * 24 * 3600)
      )
      let numberOfDays = maximumLeadTime - minimumLeadTime
      let lastDate = earliestDate.addingTimeInterval(Double(numberOfDays) * 24 * 3600)
      var dates = [earliestDate]
      calendar.enumerateDates(
         startingAfter: earliestDate,
         matching: DateComponents(hour: 0),
         matchingPolicy: .strict,
         using: { (matchedDate, _, stop) in
            guard let matchedDate = matchedDate, matchedDate <= lastDate else {
               stop = true
               return
            }
            dates.append(matchedDate)
      })
      
      return dates
   }
   
   private func dates(from startDate: Date, to endDate: Date, leadTime: Int) -> [Date] {
      let calendar = Self.calendar
      
      let actualStartDate: Date = {
         let leadTimeDate = Date().addingTimeInterval(Double(leadTime) * 24 * 3600)
         let comparison = calendar.compare(startDate, to: leadTimeDate, toGranularity: .day)
         return calendar.startOfDay(for: comparison == .orderedAscending ? leadTimeDate : startDate)
      }()
      
      var dates: [Date] = [actualStartDate]
      calendar.enumerateDates(
         startingAfter: actualStartDate,
         matching: DateComponents(hour: 0),
         matchingPolicy: .strict,
         using: { (matchedDate, _, stop) in
            guard let matchedDate = matchedDate, matchedDate <= endDate else {
               stop = true
               return
            }
            dates.append(matchedDate)
      })
      return dates
   }
}
