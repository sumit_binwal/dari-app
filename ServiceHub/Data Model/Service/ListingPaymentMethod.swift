//
//  ListingPaymentMethod.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

struct ListingPaymentMethod: Codable {
   let paymentMethod: PaymentMethod
   let feeString: String
   var fee: Double { Double(self.feeString) ?? 0 }
   
   private enum CodingKeys: String, CodingKey {
      case paymentMethod = "payment_method"
      case feeString = "fee"
   }
}
