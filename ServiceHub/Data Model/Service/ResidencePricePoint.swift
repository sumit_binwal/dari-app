//
//  ResidencePricePoint.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct ResidencePricePoint: PricePointValue {
   // MARK: - Properties
   let id: Int
   let dateModified: Date
   var rateType: RateType { .residence }
   let residenceType: ResidenceType
   let roomCount: Int
   let valueAsString: String
   var value: Double { Double(self.valueAsString) ?? 0 }
   
   // MARK: - Coding Keys
   enum CodingKeys: String, CodingKey {
      case id
      case dateModified = "date_modified"
      case residenceType = "residence_type"
      case roomCount = "room_count"
      case valueAsString = "value"
   }
   
   // MARK: - Decoding
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.residenceType = try container.decode(ResidenceType.self, forKey: .residenceType)
      self.roomCount = try container.decode(Int.self, forKey: .roomCount)
      self.valueAsString = try container.decode(String.self, forKey: .valueAsString)
   }
   
   // MARK: - Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
      
      try container.encode(self.residenceType, forKey: .residenceType)
      try container.encode(self.roomCount, forKey: .roomCount)
      try container.encode(self.valueAsString, forKey: .valueAsString)
   }
   
   // MARK: - Other
   func orderItem(for parameters: [Order.Field: AnyHashable?]) -> Order.InvoiceItem? {
      guard self.residenceType == parameters[.residenceType] as? ResidenceType &&
         self.roomCount == parameters[.roomCount] as? Int else { return nil }
      return .init(
         parameters: [.residenceType: residenceType, .roomCount: roomCount],
         pricePointValue: self,
         price: self.value
      )
   }
   
   func parameterDescription(_ parameters: [Order.Field : AnyHashable?]) -> String? {
      guard let residenceType = parameters[.residenceType] as? ResidenceType,
         let roomCount = parameters[.roomCount] as? Int else { return nil }
      
      let roomCountDescription: String = {
         let number = NumberFormatter.integer.string(from: roomCount)
         switch roomCount {
         case 1: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("room".localized)"
         case 2: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("2rooms".localized)"
         default: return "\(number) \("rooms".localized)"
         }
      }()
      return "\(residenceType.name)\(",".localized) \(roomCountDescription)"
   }
   
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String? { nil }
}
