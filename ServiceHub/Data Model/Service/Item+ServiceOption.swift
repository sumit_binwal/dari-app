//
//  Item+ServiceOption.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/23/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

extension Item {
   struct ServiceOption: Codable, Equatable, Hashable {
      let id: Int
      var name: String { Environment.languageIsArabic ? self.arabicName : self.englishName }
      let englishName: String
      let arabicName: String
      
      enum CodingKeys: String, CodingKey {
         case id
         case englishName = "name_english"
         case arabicName = "name_arabic"
      }
      
      static func ==(lhs: ServiceOption, rhs: ServiceOption) -> Bool { lhs.id == rhs.id }
   }
}
