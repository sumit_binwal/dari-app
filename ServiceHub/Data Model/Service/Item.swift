//
//  Item.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct Item: Codable {
   // MARK: - Stored Properties
   let id: Int
   let englishName: String
   let arabicName: String
   let arabicDualName: String
   let englishPluralName: String
   let arabicPluralName: String
   let rateType: RateType
   let parentID: Int?
   let englishVariationName: String
   let arabicVariationName: String
   let englishServiceOptionsName: String
   let arabicServiceOptionsName: String
   let serviceOptions: [ServiceOption]
   let rank: Int
   
   // MARK: - Computed Properties
   var name: String { Environment.languageIsArabic ? self.arabicName : self.englishName }
   var namePlural: String {
      Environment.languageIsArabic ? self.arabicPluralName : self.englishPluralName
   }
   var variationName: String {
      Environment.languageIsArabic ? self.arabicVariationName : self.englishVariationName
   }
   var serviceOptionsName: String {
      Environment.languageIsArabic ? self.arabicServiceOptionsName : self.englishServiceOptionsName
   }
   var verboseName: String {
      if let parent = self.parent { return "\(parent.name) - \(self.name)" }
      return self.name
   }
   var parent: Item? { Self.item(withID: self.parentID) }
   var variants: [Item] {
      Self.all.filter { $0.parentID == self.id }
   }
   var hasSpecifiableParameters: Bool {
      return self.rateType == .area ||
         !self.serviceOptions.isEmpty ||
         !self.variants.isEmpty
   }
   
   // MARK: - Codable
   enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case arabicDualName = "name_dual_arabic"
      case englishPluralName = "name_plural_english"
      case arabicPluralName = "name_plural_arabic"
      case rateType = "rate_type"
      case parentID = "parent"
      case englishVariationName = "variation_name_english"
      case arabicVariationName = "variation_name_arabic"
      case englishServiceOptionsName = "service_option_name_english"
      case arabicServiceOptionsName = "service_option_name_arabic"
      case serviceOptions = "service_options"
      case rank
   }
}

// MARK: - Equatable
extension Item: Equatable, Hashable, Comparable {
   static func ==(lhs: Item, rhs: Item) -> Bool { lhs.id == rhs.id }
   
   static func <(lhs: Item, rhs: Item) -> Bool {
      let lhParentOrNil = lhs.parent
      let rhParentOrNil = rhs.parent
      
      if let lhParent = lhParentOrNil {
         if let rhParent = rhParentOrNil {
            if lhParent == rhParent {
               return lhs.rank < rhs.rank
            } else {
               return lhParent < rhParent
            }
            
         } else {
            return lhParent < rhs
         }
      } else if let rhParent = rhParentOrNil {
         return lhs < rhParent
      } else {
         return lhs.rank < rhs.rank
      }
   }
}

// MARK: - Save and Load
extension Item {
   private static var archiveKey: String { "items" }
   
   static func item(withID id: Int?) -> Item? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   static var all: [Item] {
      get {
         return (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[Item] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
