//
//  Item+Parameter.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Item {
   struct Parameter: Hashable, Equatable {
      var item: Item
      var serviceOption: ServiceOption?
      var areaDimensions: AreaDimensions?
      
      var asDictionary: [Order.Field: AnyHashable] {
         var dictionary: [Order.Field: AnyHashable] = [.item: self.item]
         
         if let serviceOption = self.serviceOption {
            dictionary[.serviceOption] = serviceOption
         }
         
         if let areaDimensions = self.areaDimensions {
            dictionary[.area] = areaDimensions.area.value
         }
         
         return dictionary
      }
      
      static func ==(lhs: Parameter, rhs: Parameter) -> Bool {
         lhs.item == rhs.item &&
            lhs.serviceOption == rhs.serviceOption &&
            lhs.areaDimensions == rhs.areaDimensions
      }
   }
}

struct AreaDimensions: Equatable, Hashable {
   let unit: UnitLength = .centimeters
   let width: Double
   let length: Double
   
   var widthMeasurement: Measurement<UnitLength> { .init(value: self.width, unit: self.unit) }
   var lengthMeasurement: Measurement<UnitLength> { .init(value: self.length, unit: self.unit) }
   
   var area: Measurement<UnitArea> {
      var measurement = Measurement<UnitArea>(
         value: self.width * self.length,
         unit: .squareCentimeters
      ).converted(to: .squareMeters)
      
      measurement.value.round(.toNearestOrAwayFromZero, precision: 2)
      return measurement
   }
   
   static var `default`: AreaDimensions { .init(width: 100, length: 100) }
   
   static func ==(lhs: AreaDimensions, rhs: AreaDimensions) -> Bool {
      lhs.area == rhs.area
   }
}
