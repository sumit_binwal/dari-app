//
//  AreaPricePoint.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct AreaPricePoint: PricePointValue {
   // MARK: - Properties
   let id: Int
   let dateModified: Date
   let valueAsString: String
   var value: Double { Double(self.valueAsString) ?? 0 }
   var unit: UnitArea { .squareMeters }
   var rateType: RateType { .area }
   
   // MARK: - CodingKeys
   enum CodingKeys: String, CodingKey {
      case id
      case dateModified = "date_modified"
      case valueAsString = "value"
   }
   
   // MARK: - Decoding
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.valueAsString = try container.decode(String.self, forKey: .valueAsString)
   }
   
   // MARK: Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
      
      try container.encode(self.valueAsString, forKey: .valueAsString)
   }
   
   // MARK: - Other
   func orderItem(for parameters: [Order.Field : AnyHashable?]) -> Order.InvoiceItem? {
      guard let area = parameters[.area] as? Double else { return nil }
      
      let price = self.value * area
      
      return .init(
         parameters: [.area: area],
         pricePointValue: self,
         price: price)
   }
   
   func parameterDescription(_ parameters: [Order.Field : AnyHashable?]) -> String? {
      guard let area = parameters[.area] as? Double else { return nil }
      let measurement = Measurement(
         value: area,
         unit: UnitArea.squareMeters
      )
      return measurement.description
   }
   
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String? {
      let formatter: NumberFormatter = self.value.isWholeNumber ? .shortCurrency : .currency
      return formatter.string(from: self.value) + "/".localized + self.unit.symbol
   }
}
