//
//  TimeSlot.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

struct TimeSlot: Codable, Hashable {
   let id: Int
   var name: String { Environment.languageIsArabic ? self.arabicName : self.englishName }
   let englishName: String
   let arabicName: String
   let startingHour: Int
   let endingHour: Int
   
   enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case startingHour = "starting_hour"
      case endingHour = "ending_hour"
   }
}

extension TimeSlot: Equatable {
   static func ==(lhs: TimeSlot, rhs: TimeSlot) -> Bool { lhs.id == rhs.id }
}

// MARK: - Save and Load
extension TimeSlot {
   private static var archiveKey: String { "timeSlots" }
   
   static func timeSlot(withID id: Int?) -> TimeSlot? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   static var all: [TimeSlot] {
      get {
         (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[TimeSlot] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
