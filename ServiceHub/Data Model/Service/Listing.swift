//
//  Listing.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation
import UIKit

// Listing is a class instead of a struct to make it easier to track image download tasks.
class Listing: Codable {
   // MARK: - Stored Properties
   let id: Int
   let dateModified: Date
   let category: Category?
   let englishTitle: String
   let arabicTitle: String
   let englishSubtitle: String
   let arabicSubtitle: String
   let englishDescription: String
   let arabicDescription: String
   let englishTag: String
   let arabicTag: String
   let imageURLString: String?
   let imageWidth: Int?
   let imageHeight: Int?
   let imageDateModified: Date?
   let rank: Int
   let parentListingID: Int?
   
   let serviceID: Int?
   let serviceAvailability: Service.Availability?
   let serviceProviderIDs: [Int]
   let requiresServiceProviderSelection: Bool
   let pricePoints: [PricePoint]
   let paymentMethods: [ListingPaymentMethod]
   
   // MARK: - Coding Keys
   enum CodingKeys: String, CodingKey {
      case id
      case dateModified = "date_modified"
      case category
      case englishTitle = "title_english"
      case arabicTitle = "title_arabic"
      case englishSubtitle = "subtitle_english"
      case arabicSubtitle = "subtitle_arabic"
      case englishDescription = "description_english"
      case arabicDescription = "description_arabic"
      case englishTag = "tag_english"
      case arabicTag = "tag_arabic"
      case imageURLString = "image"
      case imageWidth = "image_width"
      case imageHeight = "image_height"
      case imageDateModified = "image_date_modified"
      case rank
      case parentListingID = "parent"
      case serviceID = "service"
      case pricePoints = "price_points"
      case serviceAvailability = "service_availability"
      case serviceProviderIDs = "service_providers"
      case requiresServiceProviderSelection = "requires_service_provider_selection"
      case paymentMethods = "payment_methods"
   }
   
   // MARK: - Decoding
   required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.category = try? container.decode(Category.self, forKey: .category)
      self.englishTitle = try container.decode(String.self, forKey: .englishTitle)
      self.arabicTitle = try container.decode(String.self, forKey: .arabicTitle)
      self.englishSubtitle = try container.decode(String.self, forKey: .englishSubtitle)
      self.arabicSubtitle = try container.decode(String.self, forKey: .arabicSubtitle)
      self.englishDescription = try container.decode(String.self, forKey: .englishDescription)
      self.arabicDescription = try container.decode(String.self, forKey: .arabicDescription)
      self.englishTag = try container.decode(String.self, forKey: .englishTag)
      self.arabicTag = try container.decode(String.self, forKey: .arabicTag)
      self.imageURLString = try? container.decode(String.self, forKey: .imageURLString)
      self.imageWidth = try? container.decode(Int.self, forKey: .imageWidth)
      self.imageHeight = try? container.decode(Int.self, forKey: .imageHeight)
      
      if let imageDateString = try? container.decode(String.self, forKey: .imageDateModified),
         let date = DateFormatter.serverTime.date(from: imageDateString) {
         self.imageDateModified = date
      } else {
         self.imageDateModified = nil
      }
      
      self.rank = try container.decode(Int.self, forKey: .rank)
      self.parentListingID = try? container.decode(Int.self, forKey: .parentListingID)
      self.serviceID = try? container.decode(Int.self, forKey: .serviceID)
      self.serviceAvailability = try? container.decode(
         Service.Availability.self,
         forKey: .serviceAvailability
      )
      self.serviceProviderIDs = try container.decode([Int].self, forKey: .serviceProviderIDs)
      self.requiresServiceProviderSelection = try container.decode(
         Bool.self,
         forKey: .requiresServiceProviderSelection
      )
      self.pricePoints = try container.decode([PricePoint].self, forKey: .pricePoints)
      self.paymentMethods = try container.decode(
         [ListingPaymentMethod].self,
         forKey: .paymentMethods
      )
   }
   
   // MARK: - Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
      
      try container.encode(self.category, forKey: .category)
      try container.encode(self.englishTitle, forKey: .englishTitle)
      try container.encode(self.arabicTitle, forKey: .arabicTitle)
      try container.encode(self.englishSubtitle, forKey: .englishSubtitle)
      try container.encode(self.arabicSubtitle, forKey: .arabicSubtitle)
      try container.encode(self.englishDescription, forKey: .englishDescription)
      try container.encode(self.arabicDescription, forKey: .arabicDescription)
      try container.encode(self.englishTag, forKey: .englishTag)
      try container.encode(self.arabicTag, forKey: .arabicTag)
      try container.encode(self.imageURLString, forKey: .imageURLString)
      try container.encode(self.imageWidth, forKey: .imageWidth)
      try container.encode(self.imageHeight, forKey: .imageHeight)
      
      if let dateModified = self.imageDateModified {
         let imageDateString = DateFormatter.serverTime.string(from: dateModified)
         try container.encode(imageDateString, forKey: .imageDateModified)
      }
      
      try container.encode(self.rank, forKey: .rank)
      try container.encode(self.parentListingID, forKey: .parentListingID)
      try container.encode(self.serviceID, forKey: .serviceID)
      try container.encode(self.serviceAvailability, forKey: .serviceAvailability)
      try container.encode(self.serviceProviderIDs, forKey: .serviceProviderIDs)
      try container.encode(
         self.requiresServiceProviderSelection,
         forKey: .requiresServiceProviderSelection
      )
      try container.encode(self.pricePoints, forKey: .pricePoints)
      try container.encode(self.paymentMethods, forKey: .paymentMethods)
   }
   
   // MARK: - Computed and Lazy Properties
   var service: Service? { Service.service(withID: self.serviceID) }
   var title: String { Environment.languageIsArabic ? self.arabicTitle : self.englishTitle }
   var subtitle: String {
      Environment.languageIsArabic ? self.arabicSubtitle : self.englishSubtitle
   }
   var description: String {
      Environment.languageIsArabic ? self.arabicDescription : self.englishDescription
   }
   var tag: String { Environment.languageIsArabic ? self.arabicTag : self.englishTag }
   var image: UIImage? {
      set {
         guard let localImageURL = self.localImageURL else { return }
         
         do {
            let folderURL = localImageURL.deletingLastPathComponent()
            if !FileManager.default.fileExists(atPath: folderURL.path) {
               try FileManager.default.createDirectory(
                  at: folderURL, withIntermediateDirectories: true, attributes: nil)
            }
            if FileManager.default.fileExists(atPath: localImageURL.path) {
               try FileManager.default.removeItem(at: localImageURL)
            }
            
            FileManager.default.createFile(
               atPath: localImageURL.path,
               contents: newValue?.pngData(),
               attributes: nil
            )
            
         } catch {
            assertionFailure("[Listing] \(error.localizedDescription)")
         }
      }
      
      get {
         guard let url = self.localImageURL, let data = try? Data(contentsOf: url) else {
            return nil
         }
         return UIImage(data: data)
      }
   }
   var imageSize: CGSize? {
      guard let width = self.imageWidth, let height = self.imageHeight else { return nil }
      return CGSize(width: width, height: height)
   }
   var shouldDownloadImage: Bool {
      // Check if there's a url, no image is saved, and no download task exists.
      if self.imageURL != nil && self.imageDownloadTask == nil && self.image == nil {
         return true
      }
      
      // If an image exists, compare server and local last modified dates.
      guard self.image != nil,
         let localURL = self.localImageURL,
         let dateModifiedOnServer = self.imageDateModified,
         let attributes = try? FileManager.default.attributesOfItem(atPath: localURL.path),
         let dateModifiedLocally = attributes[.modificationDate] as? Date else
      {
         return false
      }
      return dateModifiedLocally < dateModifiedOnServer
   }
   var imageURL: URL? {
      guard let string = self.imageURLString else { return nil }
      return Environment.fileURLsAreRelative ?
         ServerURL.getURL(fromRelativeURLString: string) : URL(string: string)
   }
   lazy var sublistings: [Listing] = {
      Self.all.filter { $0.parentListingID == self.id }.sorted()
   }()
   
   var imageDownloadTask: URLSessionDownloadTask?
}

// MARK: - Equatable and Comparable
extension Listing: Equatable, Comparable {
   static func == (lhs: Listing, rhs: Listing) -> Bool {
      return lhs.id == rhs.id
   }
   
   static func <(lhs: Listing, rhs: Listing) -> Bool {
      return lhs.rank < rhs.rank
   }
}

// MARK: - Save and Load
extension Listing {
   private static var archiveKey: String { "listings" }
   
   private static var imagesArchiveKey: String? {
      URL(string: Environment.imagesArchiveKey)?
         .appendingPathComponent(self.archiveKey).relativeString
   }
   private var imageArchiveKey: String? {
      guard let imagesArchiveKey = Self.imagesArchiveKey else { return nil }
      return URL(string: imagesArchiveKey)?.appendingPathComponent(String(self.id)).relativeString
   }
   private var localImageURL: URL? {
      guard let archiveKey = self.imageArchiveKey else { return nil }
      return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
         .first?
         .appendingPathComponent(archiveKey)
   }
   
   static func listing(withID id: Int?) -> Listing? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   static var all: [Listing] {
      get {
         return (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[Listing] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
