//
//  Services.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/8/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

/// Wrapper object that encodes an array of Service, including its subclasses.
struct Services: Codable {
   let services: [Service]
   
   enum RateTypeKey: String, CodingKey {
      case rateType = "rate_type"
   }
   
   func encode(to encoder: Encoder) throws {
      var container = encoder.singleValueContainer()
      try container.encode(self.services)
   }
   
   init(from decoder: Decoder) throws {
      var container = try decoder.unkeyedContainer()
      var services: [Service] = []
      
      var servicesContainer = container
      while (!container.isAtEnd) {
         let rateTypeContainer = try container.nestedContainer(keyedBy: RateTypeKey.self)
         let rateType = try rateTypeContainer.decode(RateType.self, forKey: .rateType)
         
         switch rateType {
         case .agentTime:
            services.append(try servicesContainer.decode(AgentDurationDependentService.self))
            
         case .residence:
            services.append(try servicesContainer.decode(ResidenceDependantService.self))
            
         default: services.append(try servicesContainer.decode(Service.self))
         }
      }
      
      self.services = services
   }
   
   init(_ services: [Service]) {
      self.services = services
   }
}
