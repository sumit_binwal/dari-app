//
//  RateType.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

enum RateType: String, Codable {
   case none
   case flat
   case agentTime = "agent_time"
   case residence
   case area
   case inherited
}
