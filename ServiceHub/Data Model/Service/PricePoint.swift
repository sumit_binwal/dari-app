//
//  PricePoint.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct PricePoint: Codable {
   static var taxRate: Double = 0
   
   // MARK: - Stored Properties
   let id: Int
   let dateModified: Date
   let serviceProviderID: Int?
   let serviceID: Int?
   let itemID: Int?
   let serviceOptionID: Int?
   let rateType: RateType
   let pricePointValue: PricePointValue?
   
   // MARK: - Codable
   private enum CodingKeys: String, CodingKey {
      case id
      case dateModified = "date_modified"
      case serviceProviderID = "service_provider"
      case serviceID = "service"
      case itemID = "item"
      case serviceOptionID = "service_option"
      case rateType = "rate_type"
      case pricePointValue = "price_point_value"
   }
   
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
      
      try container.encode(self.serviceProviderID, forKey: .serviceProviderID)
      try container.encode(self.serviceID, forKey: .serviceID)
      try container.encode(self.itemID, forKey: .itemID)
      try container.encode(self.serviceOptionID, forKey: .serviceOptionID)
      try container.encode(self.rateType, forKey: .rateType)
      
      switch self.pricePointValue {
      case let ppv as FlatPricePoint: try container.encode(ppv, forKey: .pricePointValue)
      case let ppv as AgentTimePricePoint: try container.encode(ppv, forKey: .pricePointValue)
      case let ppv as ResidencePricePoints: try container.encode(ppv, forKey: .pricePointValue)
      case let ppv as AreaPricePoint: try container.encode(ppv, forKey: .pricePointValue)
      default: break
      }
   }
   
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.serviceProviderID = try container.decode(Int.self, forKey: .serviceProviderID)
      self.serviceID = try? container.decode(Int.self, forKey: .serviceID)
      self.itemID = try? container.decode(Int.self, forKey: .itemID)
      self.serviceOptionID = try? container.decode(Int.self, forKey: .serviceOptionID)
      
      let rateType = try container.decode(RateType.self, forKey: .rateType)
      self.rateType = rateType
      
      switch rateType {
      case .flat:
         self.pricePointValue = try container.decode(FlatPricePoint.self, forKey: .pricePointValue)
         
      case .agentTime:
         self.pricePointValue = try container.decode(
            AgentTimePricePoint.self,
            forKey: .pricePointValue
         )
         
      case .residence:
         self.pricePointValue = try container.decode(
            ResidencePricePoints.self,
            forKey: .pricePointValue
         )
         
      case .area:
         self.pricePointValue = try container.decode(AreaPricePoint.self, forKey: .pricePointValue)
      
      default: self.pricePointValue = nil
         
      }
   }
}

extension PricePoint {
   func price(for parameters: [Order.Field: AnyHashable?]) -> Double? {
      self.orderItems(for: parameters).reduce(0) { $0 + $1.price }
   }
   
   func orderItems(
      for parameters: [Order.Field: AnyHashable?]
   ) -> [Order.InvoiceItem] {
      guard let serviceProvider = parameters[.serviceProvider] as? ServiceProvider,
         serviceProvider.id == self.serviceProviderID else { return [] }
      
      // If this price point is for the service
      if let serviceID = self.serviceID {
         guard let service = parameters[.service] as? Service, service.id == serviceID else {
            return []
         }
         
         if self.rateType == .none {
            return [.init(parameters: [.service: service], pricePointValue: nil, price: 0)]
         }
         
         if var item = self.pricePointValue?.orderItem(for: parameters) {
            item.parameters[.service] = service
            return [item]
         }
         
         return []
      }
      
      // If this price point is for an item
      if let itemID = self.itemID {
         guard let allItemParameters = parameters[.itemParameters] as? [Item.Parameter] else {
            return []
         }
         
         let itemParameters = allItemParameters.filter {
            $0.item.id == itemID && $0.serviceOption?.id == self.serviceOptionID
         }
         
         return itemParameters.compactMap {
            guard var orderItem = self.pricePointValue?.orderItem(for: $0.asDictionary) else {
               return nil
            }
            orderItem.parameters[.item] = $0.item
            orderItem.parameters[.serviceOption] = $0.serviceOption
            return orderItem
         }
      }
      
      return []
   }
   
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String? {
      guard let serviceProvider = parameters[.serviceProvider] as? ServiceProvider,
         serviceProvider.id == self.serviceProviderID else { return nil }
      
      // If this price point is for the service
      if let serviceID = self.serviceID {
         guard let service = parameters[.service] as? Service, service.id == serviceID else {
            return nil
         }
         
         if self.rateType == .none { return nil }
         
         return self.pricePointValue?.rateDescription(for: parameters)
      }
      
      // If this price point is for an item
      /*if let itemID = self.itemID {
         guard let allItemParameters = parameters[.itemParameters] as? [Item.Parameter] else {
            return nil
         }
         let itemParameters = allItemParameters.filter {
            $0.item.id == itemID && $0.serviceOption?.id == self.serviceOptionID
         }.map { $0.asDictionary }
         
         guard !itemParameters.isEmpty else { return nil }
         return itemParameters.reduce(0) { $0 + (self.pricePointValue?.price(for: $1) ?? 0) }
      }*/
      
      return nil
   }
}

// MARK: - Price Point Values
protocol PricePointValue: Codable {
   var id: Int { get }
   var dateModified: Date { get }
   var rateType: RateType { get }
   
   func orderItem(for parameters: [Order.Field: AnyHashable?]) -> Order.InvoiceItem?
   func parameterDescription(_ parameters: [Order.Field: AnyHashable?]) -> String?
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String?
}

extension PricePointValue {
   var metadata: PricePoint.Metadata {
      .init(id: self.id, dateModified: self.dateModified, rateType: self.rateType)
   }
}

struct ResidencePricePoints: PricePointValue {
   let points: [ResidencePricePoint]
   var id: Int { 0 }
   var dateModified: Date { Date() }
   var rateType: RateType { .residence }
   
   func price(for parameters: [Order.Field: AnyHashable?]) -> Double? {
      self.orderItem(for: parameters)?.price
   }
   
   func orderItem(for parameters: [Order.Field: AnyHashable?]) -> Order.InvoiceItem? {
      guard parameters[.residenceType] != nil && parameters[.roomCount] != nil else { return nil }
      
      return {
         for point in points {
            if let item = point.orderItem(for: parameters) {
               return item
            }
         }
         return nil
      }()
   }
   
   func parameterDescription(_ parameters: [Order.Field : AnyHashable?]) -> String? {
      nil
   }
   
   func rateDescription(for parameters: [Order.Field: AnyHashable?]) -> String? {
      guard let price = self.price(for: parameters) else { return nil }
      let formatter: NumberFormatter = price.isWholeNumber ? .shortCurrency : .currency
      return formatter.string(from: price)
   }
}
