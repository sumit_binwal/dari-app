//
//  AgentDurationDependentService.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/4/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

class AgentDurationDependentService: Service {
   // MARK: - Stored Properties
   let agentType: AgentType
   let agentCounts: [Int]
   let durations: [Int]
   
   private let englishAgentCountSelectionInstruction: String
   private let arabicAgentCountSelectionInstruction: String
   private let englishDurationSelectionInstruction: String
   private let arabicDurationSelectionInstruction: String
   
   // MARK: - Computed Properties
   var agentCountSelectionInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicAgentCountSelectionInstruction : self.englishAgentCountSelectionInstruction
      return specificInstruction.isEmpty ?
         self.defaultAgentCountSelectionInstruction : specificInstruction
   }
   private var defaultAgentCountSelectionInstruction: String {
      let agentString = Environment.languageIsArabic ?
         self.agentType.arabicSingularName : self.agentType.englishPluralName.lowercased()
      return "How many".localized + " \(agentString) " + "do you want?".localized
   }
   
   var durationSelectionInstruction: String {
      let specificInstruction = Environment.languageIsArabic ?
         self.arabicDurationSelectionInstruction : self.englishDurationSelectionInstruction
      return specificInstruction.isEmpty ? "serviceDurationInstruction".localized : specificInstruction
   }
   
   // MARK: - Codable
   private enum CodingKeys: String, CodingKey {
      case agentType = "agent_type"
      case agentCounts = "agent_counts"
      case durations
      
      case englishAgentCountSelectionInstruction = "agent_count_selection_instruction_english"
      case arabicAgentCountSelectionInstruction = "agent_count_selection_instruction_arabic"
      case englishDurationSelectionInstruction = "duration_selection_instruction_english"
      case arabicDurationSelectionInstruction = "duration_selection_instruction_arabic"
   }
   
   required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.agentType = try container.decode(AgentType.self, forKey: .agentType)
      self.agentCounts = try container.decode([Int].self, forKey: .agentCounts)
      self.durations = try container.decode([Int].self, forKey: .durations)
      
      self.englishAgentCountSelectionInstruction = try container.decode(
         String.self,
         forKey: .englishAgentCountSelectionInstruction
      )
      self.arabicAgentCountSelectionInstruction = try container.decode(
         String.self,
         forKey: .arabicAgentCountSelectionInstruction
      )
      self.englishDurationSelectionInstruction = try container.decode(
         String.self,
         forKey: .englishDurationSelectionInstruction
      )
      self.arabicDurationSelectionInstruction = try container.decode(
         String.self,
         forKey: .arabicDurationSelectionInstruction
      )
      
      try super.init(from: decoder)
   }
   
   override func encode(to encoder: Encoder) throws {
      try super.encode(to: encoder)
      
      var container  = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(self.agentType, forKey: .agentType)
      try container.encode(self.agentCounts, forKey: .agentCounts)
      try container.encode(self.durations, forKey: .durations)
      try container.encode(
         self.englishAgentCountSelectionInstruction,
         forKey: .englishAgentCountSelectionInstruction
      )
      try container.encode(
         self.arabicAgentCountSelectionInstruction,
         forKey: .arabicAgentCountSelectionInstruction
      )
      try container.encode(
         self.englishDurationSelectionInstruction,
         forKey: .englishDurationSelectionInstruction
      )
      try container.encode(
         self.arabicDurationSelectionInstruction,
         forKey: .arabicDurationSelectionInstruction
      )
   }
}
