//
//  Category.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

struct Category: Codable, Equatable, Comparable, Hashable {
   let id: Int
   let rank: Int
   var name: String { Environment.languageIsArabic ? self.arabicName : self.englishName }
   let englishName: String
   let arabicName: String
   
   enum CodingKeys: String, CodingKey {
      case id, rank
      case englishName = "name_english"
      case arabicName = "name_arabic"
   }
   
   static func ==(lhs: Category, rhs: Category) -> Bool {
      return lhs.id == rhs.id
   }
   
   static func <(lhs: Category, rhs: Category) -> Bool {
      return lhs.rank < rhs.rank
   }
   
   func hash(into hasher: inout Hasher) {
      hasher.combine(id)
   }
}
