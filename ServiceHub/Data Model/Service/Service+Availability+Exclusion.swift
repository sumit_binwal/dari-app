//
//  Service+Availability+Exclusion.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Service.Availability {
   class Exclusion: Codable {
      // MARK: - Stored Properties
      let date: Date
      let hour: Int?
      let timeSlotID: Int?
      
      // MARK: - Codable
      private enum CodingKeys: String, CodingKey {
         case date
         case hour
         case timeSlotID = "time_slot"
      }
      
      required init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         let dateString = try container.decode(String.self, forKey: .date)
         if let date = DateFormatter.serverDate.date(from: dateString) {
            self.date = date
         } else {
            throw DecodingError.dataCorruptedError(
               forKey: .date,
               in: container,
               debugDescription: "Date string doesn't match format expected by formatter.")
         }
         
         self.hour = try? container.decode(Int.self, forKey: .hour)
         self.timeSlotID = try? container.decode(Int.self, forKey: .timeSlotID)
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         let dateString = DateFormatter.serverDate.string(from: self.date)
         try container.encode(dateString, forKey: .date)
         
         try container.encode(self.hour, forKey: .hour)
         try container.encode(self.timeSlotID, forKey: .timeSlotID)
      }
      
      // MARK: - Computed Properties
      lazy var timeSlot: TimeSlot? = { TimeSlot.timeSlot(withID: self.timeSlotID) }()
      var isAllDay: Bool { self.hour == nil && self.timeSlot == nil }
      var isHour: Bool { self.hour != nil }
      var isTimeSlot: Bool { self.timeSlot != nil }
   }
}
