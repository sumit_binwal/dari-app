//
//  PricePoint+Metadata.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension PricePoint {
   struct Metadata: Codable {
      let id: Int
      let dateModified: Date
      let rateType: RateType
      
      private enum CodingKeys: String, CodingKey {
         case id = "price_point"
         case dateModified = "date_modified"
         case rateType = "rate_type"
      }
      
      init(id: Int, dateModified: Date, rateType: RateType) {
         self.id = id
         self.dateModified = dateModified
         self.rateType = rateType
      }
      
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         self.id = try container.decode(Int.self, forKey: .id)
         
         let dateString = try container.decode(String.self, forKey: .dateModified)
         if let date = DateFormatter.serverTime.date(from: dateString) {
            self.dateModified = date
         } else {
            throw DecodingError.dataCorruptedError(
               forKey: .dateModified,
               in: container,
               debugDescription: "Date string doesn't match format expected by formatter.")
         }
         
         self.rateType = try container.decode(RateType.self, forKey: .rateType)
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.id, forKey: .id)
         
         let dateString = DateFormatter.serverTime.string(from: self.dateModified)
         try container.encode(dateString, forKey: .dateModified)
         
         try container.encode(self.rateType, forKey: .rateType)
      }
   }
}


