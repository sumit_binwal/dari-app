//
//  Catalog.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/8/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct Catalog: Codable {
   private let taxRateString: String
   var taxRate: Double { Double(self.taxRateString) ?? 0 }
   let announcements: [Announcement]
   let listings: [Listing]
   let services: Services
   let items: [Item]
   let timeSlots: [TimeSlot]
   let serviceProviders: [ServiceProvider]
   fileprivate let cityWideExclusions: [CityWideAvailabilityOverride]
   fileprivate let cancellationRefundRates: [String: Double]
   let gracePeriodInMinutes: Int
   
   enum CodingKeys: String, CodingKey {
      case taxRateString = "tax_rate"
      case announcements
      case listings
      case services
      case items
      case timeSlots = "time_slots"
      case serviceProviders = "service_providers"
      case cityWideExclusions = "city_wide_availability_overrides"
      case gracePeriodInMinutes = "grace_period"
      case cancellationRefundRates = "cancellation_refund_rates"
   }
   
   var categorizedListings: [(category: Category, listings: [Listing])] {
      let topLevelListings: [Listing] = listings.filter { $0.parentListingID == nil }.sorted()
      let categories: [Category] = Array(
         Set(topLevelListings.compactMap { $0.category })
      ).sorted()
      
      return categories.map { category in
         (category: category, listings: topLevelListings.filter { $0.category == category })
      }
   }
   
   func save() {
      PricePoint.taxRate = self.taxRate
      Listing.all = self.listings
      Service.all = self.services.services
      Item.all = self.items
      TimeSlot.all = self.timeSlots
      ServiceProvider.all = self.serviceProviders
      Service.Availability.cityWideExcludedDates = self.cityWideExclusions.map { $0.date }
      Order.gracePeriodInMinutes = self.gracePeriodInMinutes
      Order.cancellationRefundRates = self.cancellationRefundRates
         .map { (Double($0) ?? -1, $1) }
         .filter { $0.0 >= 0 }
         .sorted { $0.0 > $1.0 }
   }
}

fileprivate struct CityWideAvailabilityOverride: Codable {
   let date: Date
   
   enum CodingKeys: String, CodingKey {
      case date
   }
   
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      
      let dateString = try container.decode(String.self, forKey: .date)
      if let date = DateFormatter.serverDate.date(from: dateString) {
         self.date = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .date,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
   }
   
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      let dateString = DateFormatter.serverDate.string(from: self.date)
      try container.encode(dateString, forKey: .date)
   }
}
