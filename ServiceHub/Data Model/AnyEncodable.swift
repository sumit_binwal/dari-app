//
//  AnyEncodable.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/07/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

/// Used to encode the values in a dictionary.
struct AnyEncodable: Encodable {
    let value: Encodable
    
    init(_ value: Encodable) {
        self.value = value
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode("\(value)")
    }
}
