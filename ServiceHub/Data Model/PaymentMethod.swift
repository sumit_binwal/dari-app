//
//  PaymentMethod.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

enum PaymentMethod: Int, Codable {
   case card = 0
   case cash = 1
   case bankTransfer = 2
   
   var name: String {
      switch self {
      case .card: return "card".localized
      case .cash: return "cash".localized
      case .bankTransfer: return "bankTransfer".localized
      }
   }
   
   var image: UIImage? {
      UIImage(named: {
         switch self {
         case .card: return "Card"
         case .cash: return "Cash"
         case .bankTransfer: return ""
         }
      }())
   }
}
