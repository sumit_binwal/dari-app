//
//  ServerError.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/20/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

enum ServerError: Int {
    // User Management
    case firstNameIsEmpty
    case lastNameIsEmpty
    case emailAlreadyExists
    case emailIsEmpty
    case passwordTooShort
    case passwordInvalid
    case mobileNumberAlreadyExists
    case mobileNumberIsEmpty
    
    var field: String {
        switch self {
        case .firstNameIsEmpty, .lastNameIsEmpty: return "name"
        case .emailIsEmpty, .emailAlreadyExists: return "email"
        case .passwordTooShort, .passwordInvalid: return "password"
        case .mobileNumberIsEmpty, .mobileNumberAlreadyExists: return "mobileNumber"
        }
    }
    
    var message: String {
        switch self {
        case .firstNameIsEmpty: return "First name is empty."
        case .lastNameIsEmpty: return "Last name is empty."
        case .emailAlreadyExists: return "Email already exists."
        case .emailIsEmpty: return "Email is empty."
        case .passwordTooShort: return "Password is too short."
        case .passwordInvalid: return "Password invalid."
        case .mobileNumberAlreadyExists: return "Mobile number already exists."
        case .mobileNumberIsEmpty: return "Mobile number is empty."
        }
    }
}
