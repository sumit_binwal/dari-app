//
//  Address.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/5/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation
import CoreLocation

struct Address: Codable {
    var latitude: Double
    var longitude: Double
    var placemarkName: String?
    var description: String?
    var title: String { return description ?? placemarkName ?? "\(latitude), \(longitude)" }
    var location: CLLocation { return CLLocation(latitude: latitude, longitude: longitude) }
    
    init(latitude: Double, longitude: Double, description: String? = nil, placemarkName: String? = nil) {
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.placemarkName = placemarkName
    }
}
