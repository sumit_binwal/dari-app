//
//  Area.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/3/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

protocol Area {
    var id: Int { get }
    var name: String { get }
}
