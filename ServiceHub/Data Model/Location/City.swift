//
//  City.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/3/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

struct City: Area, Codable, Equatable {
   // MARK: - Properties
   let id: Int
   var name: String { return Environment.languageIsArabic ? arabicName : englishName }
   let englishName: String
   let arabicName: String
   let countryID: Int
   var country: Country? { return Country.getCountry(withId: countryID) }
   
   enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case countryID = "country"
   }
   
   // MARK: - Save and Load
   static var archiveKey: String = "cities"
   static var current: City? { return District.current?.city }
   
   static func getCity(withID id: Int) -> City? {
      return all.first { $0.id == id }
   }
   
   static var all: [City] {
      get {
         return (try? CodableStorage().load(for: archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: archiveKey)
         } catch {
            fatalError("[City] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
