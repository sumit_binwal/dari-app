//
//  District.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/3/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation
import UIKit

class District: Area, Codable, Equatable, Comparable {
   // MARK: - Properties
   let id: Int
   var name: String { return Environment.languageIsArabic ? arabicName : englishName }
   let englishName: String
   let arabicName: String
   let cityID: Int
   lazy var city: City? = City.getCity(withID: cityID)
   lazy var country: Country? = self.city?.country
   
   enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case cityID = "city"
   }
   
   // MARK: - Equatable and Comparable
   static func ==(lhs: District, rhs: District) -> Bool {
      return lhs.id == rhs.id
   }
   
   static func <(lhs: District, rhs: District) -> Bool {
      return lhs.name < rhs.name
   }
   
   // MARK: - Save and Load
   static var archiveKey: String { return "districts" }
   static var current: District? {
       get {
         if let districtID = User.current?.profile.districtID {
            return self.getDistrict(withID: districtID)
         } else {
            return self.getDistrict(withID: UserDefaults.standard.integer(forKey: "currentDistrictID"))
         }
      }
       set { UserDefaults.standard.set(newValue?.id, forKey: "currentDistrictID") }
   }
   
   static func getDistrict(withID id: Int) -> District? {
      return all.first { $0.id == id }
   }
   
   static func getDistricts(inCity city: City) -> [District] {
      return all.filter { $0.cityID == city.id }
   }
   
   static var all: [District] {
      get {
         return (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[District] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}

// MARK: - Searchable
extension District: Searchable {
   var title: String? { self.name }
   
   var subtitle: String? { self.city?.name }
   
   var image: UIImage? { nil }
   
   func contains(searchText: String) -> Bool {
      self.name.lowercased().contains(searchText) ||
         self.city?.name.lowercased().contains(searchText) ?? false
   }
}
