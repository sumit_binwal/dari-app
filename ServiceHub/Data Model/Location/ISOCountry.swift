//
//  ISOCountry.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/24/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

struct ISOCountry: Codable, Equatable {
   static let archiveKey: String = "isoCountries"
   static var `default`: ISOCountry {
      let isoCountries = ISOCountry.load()
      
      guard let firstIsoCountry = isoCountries.first else {
         fatalError("[ISOCountry] Country list is empty.")
      }
      
      // Get chosen county (not iso country)
      guard let country = District.current?.country else {
         return firstIsoCountry
      }
      
      // Find equivalent iso country
      guard let isoCountry = isoCountries.first(where: { (isoCountry) -> Bool in
         country.englishName == isoCountry.englishName
      }) else {
         assertionFailure("[ISOCountry] Failed to find equivalent iso country.")
         return firstIsoCountry
      }
      
      return isoCountry
   }
   let id: Int
   var name: String { return Environment.languageIsArabic ? arabicName : englishName }
   let englishName: String
   let arabicName: String
   let isoCode: String
   let idd: String
   var flag: UIImage? { return UIImage(named: isoCode.lowercased()) }
   
   static func ==(lhs: ISOCountry, rhs: ISOCountry) -> Bool {
      return lhs.id == rhs.id
   }
   
   static func country(withID id: Int) -> ISOCountry? {
      return load().first { $0.id == id }
   }
   
   static func save(_ countries: [ISOCountry]) {
      do {
         try CodableStorage().save(countries, for: archiveKey)
      } catch {
         fatalError("[Country] \(error.localizedDescription)")
      }
   }
   
   static func load() -> [ISOCountry] {
      do {
         return try CodableStorage().load(for: archiveKey)
      } catch {
         fatalError("[Country] Failed to load countries: \(error.localizedDescription)")
      }
   }
   
   static func extractFromFile() {
      guard let path = Bundle.main.path(forResource: "Country Codes", ofType: "csv") else {
         fatalError("[ISOCountry] Failed to locate Country Codes.csv.")
      }
      
      let fileContent: String
      do {
         try fileContent = String(contentsOfFile: path, encoding: String.Encoding.utf8)
      } catch {
         fatalError("[ISOCountry] Failed to read InventoryData.csv")
      }
      
      var lines = fileContent.split(separator: "\r\n")
      lines.remove(at: 0)
      
      var countries: [ISOCountry] = []
      for (index, line) in lines.enumerated() {
         let tokens = line.split(separator: ",").map { String($0) }
         guard tokens.count == 4 else {
            fatalError("[AppDelegate] Failed to extract country from tokens: \(tokens).")
         }
         
         let country = ISOCountry(
            id: index,
            englishName: tokens[0],
            arabicName: tokens[3],
            isoCode: tokens[1],
            idd: tokens[2]
         )
         countries.append(country)
      }
      ISOCountry.save(countries)
   }
}

// MARK: - Update
extension ISOCountry {
   /// Archive key changed from "countries" to "isoCountries".
   static func updateFrom13to14() {
      if let countries: [ISOCountry] = try? CodableStorage().load(for: "countries") {
         self.save(countries)
      }
      
      UserDefaults.standard.removeObject(forKey: "defaultCountryID")
   }
}

// MARK: - Searchable
extension ISOCountry: Searchable {
   var title: String? { "\(self.name) (\(self.idd))" }
   
   var subtitle: String? { nil }
   
   var image: UIImage? { self.flag }
   
   func contains(searchText: String) -> Bool {
      self.name.lowercased().contains(searchText) || self.idd.contains(searchText)
   }
}
