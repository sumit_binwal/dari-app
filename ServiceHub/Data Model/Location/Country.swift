//
//  Country.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct Country: Area, Codable, Equatable {
   // MARK: - Properties
   let id: Int
   var name: String { return Environment.languageIsArabic ? arabicName : englishName }
   let englishName: String
   let arabicName: String
   let code: String
   var currencyCode: String {
      Locale(identifier: self.code).currencyCode ?? self.currencySymbol
   }
   var currencySymbol: String {
      Environment.languageIsArabic ? self.arabicCurrencySymbol : self.englishCurrencySymbol
   }
   let englishCurrencySymbol: String
   let arabicCurrencySymbol: String
   let timeZoneIdentifier: String
   var timeZone: TimeZone {
      TimeZone(identifier: self.timeZoneIdentifier) ?? Calendar.current.timeZone
   }
   
   static var current: Country? { District.current?.city?.country }
   
   enum CodingKeys: String, CodingKey {
      case id
      case englishName = "name_english"
      case arabicName = "name_arabic"
      case code
      case englishCurrencySymbol = "currency_symbol_english"
      case arabicCurrencySymbol = "currency_symbol_arabic"
      case timeZoneIdentifier = "time_zone_identifier"
   }
   
   // MARK: - Equatable
   static func ==(lhs: Country, rhs: Country) -> Bool {
      return lhs.id == rhs.id
   }
   
   // MARK: - Save and Load
   static var archiveKey: String { return "countries" }
   
   static func getCountry(withId id: Int) -> Country? {
      return all.first { $0.id == id }
   }
   
   static var all: [Country] {
      get {
         return (try? CodableStorage().load(for: archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: archiveKey)
         } catch {
            fatalError("[Country] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
