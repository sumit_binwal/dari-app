//
//  Announcement.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/8/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

struct Announcement: Codable {
   // MARK: - Stored Properties
   let id: Int
   let englishTitle: String
   let arabicTitle: String
   let englishBody: String
   let arabicBody: String
   let urlString: String?
   let rank: Int
   
   // MARK: - Codable
   private enum CodingKeys: String, CodingKey {
      case id
      case englishTitle = "title_english"
      case arabicTitle = "title_arabic"
      case englishBody = "body_english"
      case arabicBody = "body_arabic"
      case urlString = "url"
      case rank
   }
   
   // MARK: - Computed Properties
   var title: String { Environment.languageIsArabic ? self.arabicTitle : self.englishTitle }
   var body: String { Environment.languageIsArabic ? self.arabicBody : self.englishBody }
}
