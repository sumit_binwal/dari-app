//
//  TimeBasedServicePricing.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/8/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

struct TimeBasedServicePricing: Codable {
    var serviceID: Int
    var hourlyRate: Double
    
    enum CodingKeys: String, CodingKey {
        case serviceID = "ServiceManagerId"
        case hourlyRate = "Price"
    }
}
