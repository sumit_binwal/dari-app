//
//  ServiceProvider.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/11/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class ServiceProvider: Codable {
   // MARK: - Stored Properties
   let id: Int
   let englishCompanyName: String
   let arabicCompanyName: String
   let logoURLString: String?
   let logoWidth: Int?
   let logoHeight: Int?
   let logoDateModified: Date?
   let isInternal: Bool
   
   // MARK: - Computed Properties
   var companyName: String {
      Environment.languageIsArabic ? self.arabicCompanyName : self.englishCompanyName
   }
   var logo: UIImage? {
      get {
         guard let url = self.localImageURL, let data = try? Data(contentsOf: url) else {
            return nil
         }
         return UIImage(data: data)?.withRenderingMode(.alwaysTemplate)
      }
      set {
         guard let localImageURL = self.localImageURL else { return }
         
         do {
            let folderURL = localImageURL.deletingLastPathComponent()
            if !FileManager.default.fileExists(atPath: folderURL.path) {
               try FileManager.default.createDirectory(
                  at: folderURL, withIntermediateDirectories: true, attributes: nil)
            }
            if FileManager.default.fileExists(atPath: localImageURL.path) {
               try FileManager.default.removeItem(at: localImageURL)
            }
            
            FileManager.default.createFile(
               atPath: localImageURL.path,
               contents: newValue?.pngData(),
               attributes: nil
            )
            
         } catch {
            assertionFailure("[ServiceProvider] \(error.localizedDescription)")
         }
      }
   }
   var shouldDownloadImage: Bool {
      // Check if there's a url, no image is saved, and no download task exists.
      if self.imageURL != nil && self.imageDownloadTask == nil && self.logo == nil {
         return true
      }
      
      // If a logo exists, compare server and local last modified dates.
      guard self.logo != nil,
         let localURL = self.localImageURL,
         let dateModifiedOnServer = self.logoDateModified,
         let attributes = try? FileManager.default.attributesOfItem(atPath: localURL.path),
         let dateModifiedLocally = attributes[.modificationDate] as? Date else
      {
         return false
      }
      return dateModifiedLocally < dateModifiedOnServer
   }
   var imageURL: URL? {
      guard let string = self.logoURLString else { return nil }
      return Environment.fileURLsAreRelative ?
         ServerURL.getURL(fromRelativeURLString: string) : URL(string: string)
   }
   
   var imageDownloadTask: URLSessionDownloadTask?
   
   // MARK: - Codable
   enum CodingKeys: String, CodingKey {
      case id
      case englishCompanyName = "company_name_english"
      case arabicCompanyName = "company_name_arabic"
      case logoURLString = "logo"
      case logoWidth = "logo_width"
      case logoHeight = "logo_height"
      case logoDateModified = "logo_date_modified"
      case isInternal = "is_internal"
   }
   
   // MARK: - Decoding
   required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      self.englishCompanyName = try container.decode(String.self, forKey: .englishCompanyName)
      self.arabicCompanyName = try container.decode(String.self, forKey: .arabicCompanyName)
      self.logoURLString = try? container.decode(String.self, forKey: .logoURLString)
      self.logoWidth = try? container.decode(Int.self, forKey: .logoWidth)
      self.logoHeight = try? container.decode(Int.self, forKey: .logoHeight)
      
      if let logoDateString = try? container.decode(String.self, forKey: .logoDateModified) {
         self.logoDateModified = DateFormatter.serverTime.date(from: logoDateString)
      } else {
         self.logoDateModified = nil
      }
      
      self.isInternal = try container.decode(Bool.self, forKey: .isInternal)
   }
   
   // MARK: - Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      try container.encode(self.englishCompanyName, forKey: .englishCompanyName)
      try container.encode(self.arabicCompanyName, forKey: .arabicCompanyName)
      try container.encode(self.logoURLString, forKey: .logoURLString)
      try container.encode(self.logoWidth, forKey: .logoWidth)
      try container.encode(self.logoHeight, forKey: .logoHeight)
      
      if let dateModified = self.logoDateModified {
         let logoDateString = DateFormatter.serverTime.string(from: dateModified)
         try container.encode(logoDateString, forKey: .logoDateModified)
      }
      
      try container.encode(self.isInternal, forKey: .isInternal)
   }
}

// MARK: - Equatable and Hashable
extension ServiceProvider: Equatable, Hashable {
   static func ==(lhs: ServiceProvider, rhs: ServiceProvider) -> Bool { lhs.id == rhs.id }
   
   func hash(into hasher: inout Hasher) {
      hasher.combine(self.id)
   }
}

// MARK: - Save and Load
extension ServiceProvider {
   private static var archiveKey: String { "serviceProviders" }
   
   private static var imagesArchiveKey: String? {
      URL(string: Environment.imagesArchiveKey)?
         .appendingPathComponent(self.archiveKey).relativeString
   }
   private var imageArchiveKey: String? {
      guard let imagesArchiveKey = Self.imagesArchiveKey else { return nil }
      return URL(string: imagesArchiveKey)?.appendingPathComponent(String(self.id)).relativeString
   }
   private var localImageURL: URL? {
      guard let archiveKey = self.imageArchiveKey else { return nil }
      return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
         .first?
         .appendingPathComponent(archiveKey)
   }
   
   static func serviceProvider(withID id: Int?) -> ServiceProvider? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   static var all: [ServiceProvider] {
      get {
         return (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[ServiceProvider] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
