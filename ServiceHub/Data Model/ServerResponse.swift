//
//  ServerResponse.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/6/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

struct ServerResponse<T: Codable>: Codable {
    var hasError: Bool
    var errorMessage: String?
    var errorCode: String?
    var data: T?
    
    enum CodingKeys: String, CodingKey {
        case hasError = "HasError"
        case errorMessage = "ErrorMessage"
        case errorCode = "Code"
        case data = "Data"
    }
}
