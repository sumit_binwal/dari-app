//
//  Order+ItemParameters.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct ItemParameters: Codable {
      // MARK: - Stored Properties
      let item: Int
      let englishItemName: String
      let arabicItemName: String
      let serviceOption: Int?
      let englishServiceOptionName: String
      let arabicServiceOptionName: String
      let itemCount: Int
      private let priceString: String
      let rateType: RateType
      let flatParameters: Order.FlatParameters?
      let agentTimeParameters: Order.AgentTimeParameters?
      let residenceParameters: Order.ResidenceParameters?
      let areaParameters: Order.AreaParameters?
      
      // MARK: - Computed Properties
      var itemName: String {
         Environment.languageIsArabic ? self.arabicItemName : self.englishItemName
      }
      var serviceOptionName: String {
         Environment.languageIsArabic ? self.arabicServiceOptionName : self.englishServiceOptionName
      }
      
      // MARK: - Coding Keys
      private enum CodingKeys: String, CodingKey {
         case item
         case englishItemName = "item_name_english"
         case arabicItemName = "item_name_arabic"
         case serviceOption = "service_option"
         case englishServiceOptionName = "service_option_name_english"
         case arabicServiceOptionName = "service_option_name_arabic"
         case itemCount = "item_count"
         case priceString = "price"
         case rateType = "item_rate_type"
         case flatParameters = "flat_item_details"
         case agentTimeParameters = "agent_time_item_details"
         case residenceParameters = "residence_item_details"
         case areaParameters = "area_item_details"
      }
   }
}

extension Order.ItemParameters: InvoiceRepresentable {
   var header: String {
      "\(self.itemName)\(self.serviceOptionName.isEmpty ? "" : ", \(self.serviceOptionName)")"
   }
   
   var parameterDescription: String? {
      let quantity = self.itemCount
      let descriptionOrNil: String? = {
         switch self.rateType {
         case .flat: return self.flatParameters?.parameterDescription
         case .agentTime: return self.agentTimeParameters?.parameterDescription
         case .residence: return self.residenceParameters?.parameterDescription
         case .area: return self.areaParameters?.parameterDescription
         default: return nil
         }
      }()
      
      if descriptionOrNil == nil && quantity == 1 { return nil }
      
      let quantityString: String = quantity > 1 ?
         NumberFormatter.integer.string(from: quantity) : ""
      let connector: String = (quantity > 1 && descriptionOrNil != nil) ? " × " : ""
      
      return quantityString + connector + (descriptionOrNil ?? "")
   }
   
   func rateDescription(
      currencyFormatter: NumberFormatter,
      shortCurrencyFormatter: NumberFormatter
   ) -> String? {
      let quantity = self.itemCount
      switch self.rateType {
      case .flat:
         if quantity == 1 { return nil }
         
         return self.flatParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
         
      case .agentTime:
         return self.agentTimeParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
         
      case .residence:
         return self.residenceParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
         
      case .area:
         return self.areaParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
         
      default: return nil
      }
   }
   
   var price: Double { Double(self.priceString) ?? 0 }
}
