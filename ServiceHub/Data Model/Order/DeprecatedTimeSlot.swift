//
//  TimeSlot.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension Order {
   @available(*, deprecated)
    struct DeprecatedTimeSlot: Codable, Equatable, Comparable {
        let id: Int
        var title: String { return Environment.languageIsArabic ? title_arabic : title_english }
        let title_english: String
        let title_arabic: String
        
        enum CodingKeys: String, CodingKey {
            case id = "AvalibleTimeSlotId"
            case title_english = "AvalibleTimeSlotName"
            case title_arabic = "AvalibleTimeSlotArabicName"
        }
        
        /// Should be set by individual services to be used by time selector.
        static var available: [DeprecatedTimeSlot] = []
        
        static func ==(lhs: DeprecatedTimeSlot, rhs: DeprecatedTimeSlot) -> Bool {
            return lhs.id == rhs.id
        }
        
        static func <(lhs: DeprecatedTimeSlot, rhs: DeprecatedTimeSlot) -> Bool { return lhs.id < rhs.id }
        
        static var fallback: [DeprecatedTimeSlot] = [.init(id: 1, title_english: "Morning", title_arabic: "Morning"),
                                           .init(id: 2, title_english: "Afternoon", title_arabic: "Afternoon")]
    }
}
