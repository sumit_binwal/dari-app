//
//  Order.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

struct Order: Codable {
   static let manager = Manager.default
   /// A list of (hour, refund rate) tuples. For example, a tuple of (1, 0.5) means that if the order is cancelled more than an hour before the delivery time, then 50% of the order value is refunded. Note that this list must be sorted by descending hours.
   static var cancellationRefundRates: [(Double, Double)] = [(0, 1)]
   static var gracePeriodInMinutes: Int = 10
   
   // MARK: - Stored Properties
   let id: Int
   let date: Date
   let listingID: Int
   let serviceProvider: ServiceProvider
   let status: Status
   
   let deliveryDate: Date
   let weekdays: [Int]
   let timeSlot: TimeSlot?
   let hour: Int?
   let timeZone: TimeZone
   
   let notes: String
   
   let serviceFrequency: Int
   let servicePeriodCount: Int
   
   let serviceParameters: ServiceParameters
   let itemParameters: [ItemParameters]
   
   let paymentMethodFee: Double
   let subtotal: Double
   let promoCodeDiscount: Double
   let discount: Double
   let tax: Double
   let total: Double
   let englishCurrencySymbol: String
   let arabicCurrencySymbol: String
   
   let paymentMethod: PaymentMethod
   
   // MARK: - Computed Properties
   var listing: Listing? { Listing.listing(withID: self.listingID) }
   var serviceCount: Int { self.serviceFrequency * self.servicePeriodCount }
   var currencySymbol: String {
      Environment.languageIsArabic ? self.arabicCurrencySymbol : self.englishCurrencySymbol
   }
   var currencyFormatter: NumberFormatter {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      formatter.currencySymbol = self.currencySymbol
      return formatter
   }
   var shortCurrencyFormatter: NumberFormatter {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      formatter.currencySymbol = self.currencySymbol
      formatter.maximumFractionDigits = 0
      return formatter
   }
   var earliestDeliveryTime: Date {
      self.deliveryDate.addingTimeInterval(
         Double(self.hour ?? self.timeSlot?.startingHour ?? 0) * 3600
      )
   }
   #warning("Fix to make more relevant.")
   var isActive: Bool { self.status != .canceled && self.status != .completed }
   #warning("Fix to make more relevant.")
   var isCancellable: Bool { self.isActive }
   var refundRateIfCancelledNow: Double {
      // Check if being canceled within the grace period
      let difference = (Date() - self.date) / 60
      if difference < Double(Order.gracePeriodInMinutes) {
         return 1
      }
      
      return Self.refundRate(hoursBeforeDelivery: self.earliestDeliveryTime - Date())
   }
   var refundIfCancelledNow: Double  {
      self.refundRateIfCancelledNow * self.total
   }
   
   // MARK: - Coding Keys
   private enum CodingKeys: String, CodingKey {
      case id
      case date = "order_date"
      case listingID = "listing"
      case serviceProvider = "service_provider"
      case status
      
      case deliveryDate = "delivery_date"
      case weekdays
      case timeSlot = "time_slot"
      case hour
      case timeZone = "time_zone_identifier"
      
      case notes
      
      case serviceFrequency = "service_frequency"
      case servicePeriodCount = "service_period_count"
      
      case serviceParameters = "ordered_service"
      case itemParameters = "ordered_items"
      
      case paymentMethodFee = "payment_method_fee"
      case subtotal
      case promoCodeDiscount = "promo_code_discount"
      case discount
      case tax
      case total
      
      case englishCurrencySymbol = "currency_symbol_english"
      case arabicCurrencySymbol = "currency_symbol_arabic"
      
      case paymentMethod = "payment_method"
   }
   
   // MARK: - Decoding
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.id = try container.decode(Int.self, forKey: .id)
      
      let dateString = try container.decode(String.self, forKey: .date)
      if let date = DateFormatter.serverTime.date(from: dateString) {
         self.date = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .date,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
      
      self.listingID = try container.decode(Int.self, forKey: .listingID)
      self.serviceProvider = try container.decode(ServiceProvider.self, forKey: .serviceProvider)
      self.status = try container.decode(Status.self, forKey: .status)
      
      let timeZoneIdentifier = try container.decode(String.self, forKey: .timeZone)
      let timeZone: TimeZone = {
         if let zone = TimeZone(identifier: timeZoneIdentifier) {
            return zone
         } else {
            assertionFailure("[Order] Unrecognized time zone.")
            return Calendar.current.timeZone
         }
      }()
      self.timeZone = timeZone
      
      let deliveryDateString = try container.decode(String.self, forKey: .deliveryDate)
      
      let dateFormatter = Self.codableDeliveryDateFormatter(for: timeZone)
      
      if let date = dateFormatter.date(from: deliveryDateString) {
         self.deliveryDate = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .deliveryDate,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter."
         )
      }
      
      self.weekdays = try container.decode([Int].self, forKey: .weekdays)
      self.timeSlot = try? container.decode(TimeSlot.self, forKey: .timeSlot)
      self.hour = try? container.decode(Int.self, forKey: .hour)
      self.notes = try container.decode(String.self, forKey: .notes)
      
      self.serviceFrequency = try container.decode(Int.self, forKey: .serviceFrequency)
      self.servicePeriodCount = try container.decode(Int.self, forKey: .servicePeriodCount)
      
      self.serviceParameters = try container.decode(
         ServiceParameters.self,
         forKey: .serviceParameters
      )
      self.itemParameters = try container.decode([ItemParameters].self, forKey: .itemParameters)
      
      let paymentMethodFeeString = try container.decode(String.self, forKey: .paymentMethodFee)
      guard let paymentMethodFee = Double(paymentMethodFeeString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .paymentMethodFee,
            in: container,
            debugDescription: "Failed to decode paymentMethodFee from string"
         )
      }
      self.paymentMethodFee = paymentMethodFee
      
      let subtotalString = try container.decode(String.self, forKey: .subtotal)
      guard let subtotal = Double(subtotalString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .subtotal,
            in: container,
            debugDescription: "Failed to decode subtotal from string"
         )
      }
      self.subtotal = subtotal
      
      let promoDiscountString = try container.decode(String.self, forKey: .promoCodeDiscount)
      guard let promoCodeDiscount = Double(promoDiscountString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .promoCodeDiscount,
            in: container,
            debugDescription: "Failed to decode promo code discount from string"
         )
      }
      self.promoCodeDiscount = promoCodeDiscount
      
      let discountString = try container.decode(String.self, forKey: .discount)
      guard let discount = Double(discountString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .discount,
            in: container,
            debugDescription: "Failed to decode discount from string"
         )
      }
      self.discount = discount
      
      let taxString = try container.decode(String.self, forKey: .tax)
      guard let tax = Double(taxString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .tax,
            in: container,
            debugDescription: "Failed to decode tax from string"
         )
      }
      self.tax = tax
      
      let totalString = try container.decode(String.self, forKey: .total)
      guard let total = Double(totalString) else {
         throw DecodingError.dataCorruptedError(
            forKey: .total,
            in: container,
            debugDescription: "Failed to decode total from string"
         )
      }
      self.total = total
      
      self.englishCurrencySymbol = try container.decode(
         String.self,
         forKey: .englishCurrencySymbol
      )
      self.arabicCurrencySymbol = try container.decode(
         String.self,
         forKey: .arabicCurrencySymbol
      )
      
      self.paymentMethod = try container.decode(PaymentMethod.self, forKey: .paymentMethod)
   }
   
   // MARK: - Encoding
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      try container.encode(self.listingID, forKey: .listingID)
      try container.encode(DateFormatter.serverTime.string(from: self.date), forKey: .date)
      try container.encode(self.serviceProvider, forKey: .serviceProvider)
      try container.encode(self.status, forKey: .status)
      
      try container.encode(self.timeZone.identifier, forKey: .timeZone)
      
      let dateFormatter = Self.codableDeliveryDateFormatter(for: self.timeZone)
      try container.encode(dateFormatter.string(from: self.deliveryDate), forKey: .deliveryDate)
      try container.encode(self.weekdays, forKey: .weekdays)
      try container.encode(self.timeSlot, forKey: .timeSlot)
      try container.encode(self.hour, forKey: .hour)
      
      try container.encode(self.notes, forKey: .notes)
      
      try container.encode(self.serviceFrequency, forKey: .serviceFrequency)
      try container.encode(self.servicePeriodCount, forKey: .servicePeriodCount)
      
      try container.encode(self.serviceParameters, forKey: .serviceParameters)
      try container.encode(self.itemParameters, forKey: .itemParameters)
      
      try container.encode(String(self.paymentMethodFee), forKey: .paymentMethodFee)
      try container.encode(String(self.subtotal), forKey: .subtotal)
      try container.encode(String(self.promoCodeDiscount), forKey: .promoCodeDiscount)
      try container.encode(String(self.discount), forKey: .discount)
      try container.encode(String(self.tax), forKey: .tax)
      try container.encode(String(self.total), forKey: .total)
      
      try container.encode(self.englishCurrencySymbol, forKey: .englishCurrencySymbol)
      try container.encode(self.arabicCurrencySymbol, forKey: .arabicCurrencySymbol)
      
      try container.encode(self.paymentMethod, forKey: .paymentMethod)
   }
   
   // MARK: - Other
   static func deliveryDateDescription(
      forDate date: Date,
      weekdays: [Int],
      periodCount: Int
   ) -> [String] {
      var date = DateFormatter.default.string(from: date)
      guard !weekdays.isEmpty else { return [date] }
      
      let weekdays = "\("every".localized) \(DateFormatter.string(describing: weekdays))"
      date = "starting".localized + " " + date
      
      let period: String = {
         let number = NumberFormatter.integer.string(from: periodCount)
         var string = "for".localized + " "
         
         switch periodCount {
         case 1: string += "\(Environment.languageIsArabic ? "" : "\(number) ")\("week".localized)"
         case 2: string += "\(Environment.languageIsArabic ? "" : "\(number) ")\("2weeks".localized)"
         default: string += "\(number) \("weeks".localized)"
         }
         
         return string
      }()
      return [weekdays, date, period]
   }
   
   internal static func codableDeliveryDateFormatter(
      for timeZone: TimeZone
   ) -> ISO8601DateFormatter {
      let dateFormatter = ISO8601DateFormatter()
      dateFormatter.formatOptions = .withFullDate
      dateFormatter.timeZone = timeZone
      return dateFormatter
   }
   
   static func refundRate(hoursBeforeDelivery hours: Double) -> Double {
      self.cancellationRefundRates.first { hours > $0.0 }?.1 ?? 0
   }
   
   static func refundDescription(
      refundAmount: Double,
      total: Double,
      currencyFormatter: NumberFormatter
   ) -> String {
      if refundAmount == 0 {
         return "noRefund".localized
      } else if refundAmount < total {
         let refundAmountString = currencyFormatter.string(from: refundAmount)
         return "\("partialRefund".localized) \(refundAmountString) \("refund".localized)."
      } else {
         return "fullRefund".localized
      }
   }
}

// MARK: - Save and Load
extension Order {
   private static var archiveKey: String { "orders" }
   
   static func order(withID id: Int?) -> Order? {
      guard let id = id else { return nil }
      return self.all.first { $0.id == id }
   }
   
   static func deleteOrder(withID id: Int?) {
      guard let id = id else { return }
      self.all.removeAll { $0.id == id }
   }
   
   private static var all: [Order] {
      get {
         return (try? CodableStorage().load(for: self.archiveKey)) ?? []
      }
      set {
         do {
            try CodableStorage().save(newValue, for: self.archiveKey)
         } catch {
            fatalError("[Item] Failed to save: \(error.localizedDescription)")
         }
      }
   }
}
