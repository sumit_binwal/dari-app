//
//  Order+Parameters.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

// MARK: - Flat
extension Order {
   struct FlatParameters: Codable {
      let rate: Double
      
      var parameterDescription: String? { nil }
      
      private enum CodingKeys: String, CodingKey {
         case rate
      }
      
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         let rateString = try container.decode(String.self, forKey: .rate)
         guard let rate = Double(rateString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .rate,
               in: container,
               debugDescription: "Failed to decode rate from string"
            )
         }
         self.rate = rate
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         try container.encode(String(self.rate), forKey: .rate)
      }
      
      func rateDescription(
         quantity: Int,
         currencyFormatter: NumberFormatter,
         shortCurrencyFormatter: NumberFormatter
      ) -> String? {
         (self.rate.isWholeNumber ? shortCurrencyFormatter : currencyFormatter)
            .string(from: self.rate)
      }
   }
}

// MARK: - Agent Time
extension Order {
   struct AgentTimeParameters: Codable {
      let agentType: AgentType
      let agentCount: Int
      let duration: Int
      let rate: Double
      
      var parameterDescription: String? {
         var string = "\(agentType.agentCountDescription(agentCount))\(",".localized) "
         string += {
            let number = NumberFormatter.integer.string(from: duration)
            switch duration {
            case 1: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("hour".localized)"
            case 2: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("2hours".localized)"
            default: return "\(number) \("hours".localized)"
            }
         }()
         return string
      }
      
      private enum CodingKeys: String, CodingKey {
         case agentType = "agent_type"
         case agentCount = "agent_count"
         case duration
         case rate
      }
      
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         self.agentType = try container.decode(AgentType.self, forKey: .agentType)
         self.agentCount = try container.decode(Int.self, forKey: .agentCount)
         self.duration = try container.decode(Int.self, forKey: .duration)
         
         
         let rateString = try container.decode(String.self, forKey: .rate)
         guard let rate = Double(rateString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .rate,
               in: container,
               debugDescription: "Failed to decode rate from string."
            )
         }
         self.rate = rate
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.agentType, forKey: .agentType)
         try container.encode(self.agentCount, forKey: .agentCount)
         try container.encode(self.duration, forKey: .duration)
         try container.encode(String(self.rate), forKey: .rate)
      }
      
      func rateDescription(
         quantity: Int,
         currencyFormatter: NumberFormatter,
         shortCurrencyFormatter: NumberFormatter
      ) -> String? {
         let formatter = self.rate.isWholeNumber ? shortCurrencyFormatter : currencyFormatter
         return formatter.string(from: self.rate) + "/".localized +
            "hour".localized + "/".localized + self.agentType.singularName
      }
   }
}

// MARK: - Residence
extension Order {
   struct ResidenceParameters: Codable {
      let residenceType: ResidenceType
      let roomCount: Int
      let rate: Double
      
      var parameterDescription: String? {
         let roomCountDescription: String = {
            let number = NumberFormatter.integer.string(from: roomCount)
            switch roomCount {
            case 1: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("room".localized)"
            case 2: return "\(Environment.languageIsArabic ? "" : "\(number) ")\("2rooms".localized)"
            default: return "\(number) \("rooms".localized)"
            }
         }()
         return "\(residenceType.name)\(",".localized) \(roomCountDescription)"
      }
      
      private enum CodingKeys: String, CodingKey {
         case residenceType = "residence_type"
         case roomCount = "room_count"
         case rate
      }
      
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         self.residenceType = try container.decode(ResidenceType.self, forKey: .residenceType)
         self.roomCount = try container.decode(Int.self, forKey: .roomCount)
         
         let rateString = try container.decode(String.self, forKey: .rate)
         guard let rate = Double(rateString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .rate,
               in: container,
               debugDescription: "Failed to decode rate from string."
            )
         }
         self.rate = rate
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.residenceType, forKey: .residenceType)
         try container.encode(self.roomCount, forKey: .roomCount)
         try container.encode(String(self.rate), forKey: .rate)
      }
      
      func rateDescription(
         quantity: Int,
         currencyFormatter: NumberFormatter,
         shortCurrencyFormatter: NumberFormatter
      ) -> String? {
         nil
      }
   }
}

// MARK: - Area
extension Order {
   struct AreaParameters: Codable {
      /// In square centimeters
      let area: Double
      let rate: Double
      private let unit: UnitArea = .squareMeters
      
      var parameterDescription: String? {
         Measurement(value: self.area, unit: self.unit).description
      }
      
      private enum CodingKeys: String, CodingKey {
         case area, rate
      }
      
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         let areaString = try container.decode(String.self, forKey: .area)
         guard let area = Double(areaString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .area,
               in: container,
               debugDescription: "Failed to decode area from string."
            )
         }
         self.area = area
         
         let rateString = try container.decode(String.self, forKey: .rate)
         guard let rate = Double(rateString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .rate,
               in: container,
               debugDescription: "Failed to decode rate from string."
            )
         }
         self.rate = rate
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(String(self.area), forKey: .area)
         try container.encode(String(self.rate), forKey: .rate)
      }
      
      func rateDescription(
         quantity: Int,
         currencyFormatter: NumberFormatter,
         shortCurrencyFormatter: NumberFormatter
      ) -> String? {
         let formatter = self.rate.isWholeNumber ? shortCurrencyFormatter : currencyFormatter
         return formatter.string(from: self.rate) + "/".localized + self.unit.symbol
      }
   }
}
