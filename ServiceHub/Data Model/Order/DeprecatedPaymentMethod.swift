//
//  PaymentMethod.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

extension Order {
   @available(*, deprecated)
    struct DeprecatedPaymentMethod: Equatable {
        let id: Int
        var title: String { return Environment.languageIsArabic ? arabicName : englishName }
        let englishName: String
        let arabicName: String
        var image: UIImage? {
            let name: String = {
                switch id {
                case 6: return "Cash"
                case 5: return "Credit Card"
                default:
                    assertionFailure("[PaymentMethod] Unrecognized payment method \(title).")
                    return ""
                }
            }()
            return UIImage(named: name)
        }
        
        static let cash: DeprecatedPaymentMethod = .init(id: 6, englishName: "Cash", arabicName: "نقدا")
        static let creditCard: DeprecatedPaymentMethod = .init(id: 5,
                                                     englishName: "Credit Card",
                                                     arabicName: "بطاقة الائتمان")
        static let all: [DeprecatedPaymentMethod] = [.cash]//, .creditCard]
        static var `default`: DeprecatedPaymentMethod { return .cash }
        
        static func ==(lhs: DeprecatedPaymentMethod, rhs: DeprecatedPaymentMethod) -> Bool {
            return lhs.id == rhs.id
        }
    }
}
