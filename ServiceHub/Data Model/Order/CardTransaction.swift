//
//  CardTransaction.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

struct CardTransaction: Codable {
   let isApproved: Bool
   let status: String
   let responseCode: String
   let responseSummary: String
   
   private enum CodingKeys: String, CodingKey {
      case isApproved = "is_approved"
      case status
      case responseCode = "response_code"
      case responseSummary = "response_summary"
   }
}
