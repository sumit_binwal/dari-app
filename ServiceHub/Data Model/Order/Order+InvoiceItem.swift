//
//  Order+InvoiceItem.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/19/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct InvoiceItem {
      var parameters: [Field: AnyHashable?]
      let pricePointValue: PricePointValue?
      let price: Double
      var quantity: Int = 1
      var total: Double { self.price * Double(self.quantity) }
      
      var header: String {
         if let service = parameters[.service] as? Service {
            return service.name
         } else if let item = parameters[.item] as? Item {
            let serviceOption = parameters[.serviceOption] as? Item.ServiceOption
            return "\(item.verboseName)\(serviceOption == nil ? "" : ", \(serviceOption!.name)")"
         }
         assertionFailure("[InvoiceItem] Unknown header.")
         return ""
      }
      
      var parameterDescription: String? {
         let descriptionOrNil = self.pricePointValue?.parameterDescription(self.parameters)
         if descriptionOrNil == nil && self.quantity == 1 { return nil }
         
         let quantity: String = self.quantity > 1 ?
            NumberFormatter.integer.string(from: self.quantity) : ""
         let connector: String = (self.quantity > 1 && descriptionOrNil != nil) ? " × " : ""
         
         return quantity + connector + (descriptionOrNil ?? "")
      }
      
      var rateDescription: String? {
         if self.pricePointValue is FlatPricePoint && self.quantity == 1 { return nil }
         return self.pricePointValue?.rateDescription(for: self.parameters)
      }
   }
}

extension Order.InvoiceItem: Equatable, Hashable, Comparable {
   static func ==(lhs: Order.InvoiceItem, rhs: Order.InvoiceItem) -> Bool {
      lhs.parameters == rhs.parameters
   }
   
   func hash(into hasher: inout Hasher) {
      hasher.combine(self.parameters)
   }
   
   static func <(lhs: Order.InvoiceItem, rhs: Order.InvoiceItem) -> Bool {
      let lhServiceOrNil = lhs.parameters[.service] as? Service
      let rhServiceOrNil = rhs.parameters[.service] as? Service
      let lhItemOrNil = lhs.parameters[.item] as? Item
      let rhItemOrNil = rhs.parameters[.item] as? Item
      
      if let lhService = lhServiceOrNil {
         if let rhService = rhServiceOrNil {
            return lhService < rhService
         } else {
            return false
         }
      } else if rhServiceOrNil != nil {
         return true
      }
      
      guard let lhItem = lhItemOrNil, let rhItem = rhItemOrNil else { return false }
      return lhItem < rhItem
   }
}
