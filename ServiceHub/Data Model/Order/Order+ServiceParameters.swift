//
//  Order+ServiceParameters.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct ServiceParameters: Codable {
      // MARK: - Stored Properties
      let serviceID: Int
      let englishServiceName: String
      let arabicServiceName: String
      private let priceString: String
      let rateType: RateType
      let flatParameters: Order.FlatParameters?
      let agentTimeParameters: Order.AgentTimeParameters?
      let residenceParameters: Order.ResidenceParameters?
      
      // MARK: - Coding Keys
      private enum CodingKeys: String, CodingKey {
         case serviceID = "service"
         case englishServiceName = "service_name_english"
         case arabicServiceName = "service_name_arabic"
         case priceString = "price"
         case rateType = "service_rate_type"
         case flatParameters = "flat_service_details"
         case agentTimeParameters = "agent_time_service_details"
         case residenceParameters = "residence_service_details"
      }
      
      // MARK: - Computed Properties
      var serviceName: String {
         Environment.languageIsArabic ? self.arabicServiceName : self.englishServiceName
      }
   }
}

// MARK: - Invoice Representable
extension Order.ServiceParameters: InvoiceRepresentable {
   var header: String { self.serviceName }
   
   var parameterDescription: String? {
      //let quantity: Int = 1
      let descriptionOrNil: String? = {
         switch self.rateType {
         case .flat: return self.flatParameters?.parameterDescription
         case .agentTime: return self.agentTimeParameters?.parameterDescription
         case .residence: return self.residenceParameters?.parameterDescription
         default: return nil
         }
      }()
      
      //if descriptionOrNil == nil && quantity == 1 { return nil }
      
      //let quantityString: String = quantity > 1 ?
         //NumberFormatter.integer.string(from: quantity) : ""
      //let connector: String = (quantity > 1 && descriptionOrNil != nil) ? " × " : ""
      
      //return quantityString + connector + (descriptionOrNil ?? "")
      return descriptionOrNil
   }
   
   func rateDescription(
      currencyFormatter: NumberFormatter,
      shortCurrencyFormatter: NumberFormatter
   ) -> String? {
      let quantity: Int = 1
      
      if self.rateType == .flat && quantity == 1 { return nil }
      switch self.rateType {
      case .flat:
         return self.flatParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
      
      case .agentTime:
         return self.agentTimeParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
      
      case .residence:
         return self.residenceParameters?.rateDescription(
            quantity: quantity,
            currencyFormatter: currencyFormatter,
            shortCurrencyFormatter: shortCurrencyFormatter
         )
         
      default: return nil
      }
   }
   
   var price: Double { Double(self.priceString) ?? 0}
}
