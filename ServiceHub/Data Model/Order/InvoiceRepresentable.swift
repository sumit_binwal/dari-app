//
//  InvoiceRepresentable.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

protocol InvoiceRepresentable {
   var header: String { get }
   var parameterDescription: String? { get }
   func rateDescription(
      currencyFormatter: NumberFormatter,
      shortCurrencyFormatter: NumberFormatter
   ) -> String?
   var price: Double { get }
}
