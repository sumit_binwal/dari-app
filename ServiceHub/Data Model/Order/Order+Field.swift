//
//  Field.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension Order {
   enum Field: Int, Comparable {
      case listing
      case price
      case agentCount
      case duration
      case residenceType
      case agentType
      case roomCount
      case area
      
      case serviceProvider
      case service
      case serviceCount
      case item
      case itemParameters
      case serviceOption
      
      static func <(lhs: Field, rhs: Field) -> Bool {
         lhs.rawValue < rhs.rawValue
      }
   }
}
