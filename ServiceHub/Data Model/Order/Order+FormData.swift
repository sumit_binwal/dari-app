//
//  Order+FormData.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/9/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation
import UIKit

extension Order {
   class FormData {
      // MARK: - Constraint Properties
      // Service
      var listing: Listing
      lazy var serviceProviders: [ServiceProvider] = {
         self.listing.serviceProviderIDs.compactMap { ServiceProvider.serviceProvider(withID: $0) }
      }()
      var serviceProvider: ServiceProvider?
      let primaryService: Service
      lazy var itemConstraints: [Item.Constraints] = self.primaryService.itemConstraints
      
      // Date and Time
      let availability: Service.Availability
      lazy var dates: [Date] = self.availability.dates(forSelectedDuration: selectedDuration)
      lazy var requiresMultipleDates: Bool = {
         self.primaryService.serviceCount > 1 ||
            self.primaryService.variants.contains { $0.serviceCount > 1 }
      }()
      lazy var taxRate: Double = PricePoint.taxRate
      
      // Payment
      var promoCode: PromoCode?
      var paymentMethods: [PaymentMethod] {
         self.listing.paymentMethods.map { $0.paymentMethod }
      }
      var paymentMethodFees: [Double?] {
         self.listing.paymentMethods.map { $0.fee > 0 ? $0.fee : nil }
      }
      lazy var cards: [Card] = Card.all
      
      // MARK: - Selection Properties
      lazy var selectedAgentCount: Int? = {
         (self.primaryService as? AgentDurationDependentService)?.agentCounts.first
      }()
      lazy var selectedDuration: Int? = {
         (self.primaryService as? AgentDurationDependentService)?.durations.first
      }()
      
      lazy var selectedResidenceType: ResidenceType? = {
         (self.primaryService as? ResidenceDependantService)?.residenceTypes.first
      }()
      lazy var selectedRoomCount: Int? = {
         (self.primaryService as? ResidenceDependantService)?.roomCounts.first
      }()
      
      lazy var selectedService: Service = {
         self.primaryService.variants.first ?? self.primaryService
      }()
      
      lazy var parameterSets: [Int: [Item.Parameter]] = {
         var sets: [Int: [Item.Parameter]] = [:]
         
         self.itemConstraints.forEach {
            let set = [Item.Parameter].init(
               repeating: $0.defaultParameter,
               count: $0.quantities.first ?? 0
            )
            sets[$0.itemID] = set
         }
         
         return sets
      }()
      
      lazy var selectedWeekdays: [Int] = []
      lazy var selectedDate: Date? = self.dates.first
      lazy var selectedHour: Int? = {
         self.availability.hours(forDate: self.selectedDate).first
      }()
      lazy var selectedTimeSlot: TimeSlot? = {
         availability.timeSlots(forDate: selectedDate, selectedDuration: selectedDuration).first
      }()
      
      lazy var notes: String = ""
      
      lazy var paymentMethod: PaymentMethod? = self.paymentMethods.first
      var paymentMethodFee: Double? {
         guard let index = self.paymentMethods.firstIndex(of: self.paymentMethod) else {
            return  nil
         }
         return self.paymentMethodFees[index]
      }
      lazy var card: Card? = {
         guard self.paymentMethod == .card else { return nil }
         return Card.all.first
      }() {
         didSet {
            cvv = nil
         }
      }
      var cvv: String?
      var saveCard: Bool?
      
      // MARK: - Init
      
      init?(listing: Listing, serviceProvider: ServiceProvider? = nil) {
         self.listing = listing
         
         guard let service = listing.service else { return nil }
         self.primaryService = service
         
         let serviceProviderOrNil = serviceProvider ?? ServiceProvider.serviceProvider(
            withID: listing.serviceProviderIDs.first
         )
         
         if listing.requiresServiceProviderSelection && serviceProviderOrNil == nil {
            return nil
         }
         self.serviceProvider = serviceProviderOrNil
         
         guard let availability = listing.serviceAvailability else { return nil }
         self.availability = availability
      }
      
      func removeUnsavedCards() -> [Card] {
         let unsavedCards = cards.filter { !$0.isSaved }
         cards.removeAll { !$0.isSaved }
         card = cards.first
         return unsavedCards
      }
   }
}

// MARK: - Validation
extension Order.FormData {
   var weekdaySelectionCountIsValid: Bool {
      self.selectedWeekdays.count == self.selectedService.frequency
   }
   
   func verifyConstraintProperties() -> Bool {
      guard let listing = Listing.listing(withID: self.listing.id) else { return false }
      guard listing.serviceProviderIDs.contains(self.serviceProvider?.id) else { return false }
      self.listing = listing
      return true
   }
   
   /// Reloads the form with the modified listing. Returns the first critical field that has changed. If nil is returned, then listing modification had no effect on the price.
   func refresh() -> Order.Field? {
      let currentPrice = self.total
      let currentInvoiceItems = self.invoiceItems
      
      // Listing
      guard let listing = Listing.listing(withID: self.listing.id) else { return .listing }
      self.listing = listing
      
      // Service Provider
      guard listing.serviceProviderIDs.contains(self.serviceProvider?.id) else {
         return .serviceProvider
      }
      
      // Check if all parameters are still priced
      guard currentInvoiceItems.count == self.invoiceItems.count else { return .service }
      
      // Check if price has changed
      self.taxRate = PricePoint.taxRate
      if currentPrice != self.total { return .price }
      
      return nil
   }
}

// MARK: - Invoice
extension Order.FormData {
   var invoiceItems: [Order.InvoiceItem] {
      self.listing.pricePoints.flatMap { $0.orderItems(for: self.pricingParameters) }
   }
   
   var aggregatedInvoiceItems: [Order.InvoiceItem] {
      let counts: [Order.InvoiceItem: Int] = self.invoiceItems.reduce(into: [:]) {
         $0[$1, default: 0] += 1
      }
      return counts.map {
         var item = $0.key
         item.quantity = $0.value
         return item
      }.sorted()
   }
}

// MARK: - Pricing
extension Order.FormData {
   var pricingParameters: [Order.Field: AnyHashable?] {
      let agentDurationService = (self.selectedService as? AgentDurationDependentService) ??
         (self.selectedService.parent as? AgentDurationDependentService)
      return [
         .service: self.selectedService,
         .serviceCount: self.selectedService.serviceCount,
         .serviceProvider: self.serviceProvider,
         .agentCount: self.selectedAgentCount,
         .agentType: agentDurationService?.agentType,
         .duration: self.selectedDuration,
         .residenceType: self.selectedResidenceType,
         .roomCount: self.selectedRoomCount,
         .itemParameters: self.parameterSets.flatMap { $1 }
      ]
   }
   
   func rateDescription(forService service: Service) -> String? {
      guard let serviceProvider = self.serviceProvider else { return nil }
      let pricePoint = self.listing.pricePoints.first {
         $0.serviceProviderID == serviceProvider.id && $0.serviceID == service.id
      }
      
      let parameters: [Order.Field: AnyHashable?] = [
         .serviceProvider: serviceProvider,
         .service: service,
         .agentType: {
            let agentDurationService = (service as? AgentDurationDependentService) ??
               (service.parent as? AgentDurationDependentService)
            return agentDurationService?.agentType
         }()
      ]
      return pricePoint?.rateDescription(for: parameters)
   }
   
   var subtotal: Double? {
      guard let serviceProvider = self.serviceProvider else { return nil }
      let pricePoints = self.listing.pricePoints.filter {
         $0.serviceProviderID == serviceProvider.id
      }
      let parameters = self.pricingParameters
      let subtotal = pricePoints.reduce(0) {
         $0 + ($1.price(for: parameters) ?? 0)
      } * Double(self.selectedService.serviceCount)
      return subtotal + (self.paymentMethodFee ?? 0)
   }
   
   var promoCodeDisount: Double? {
      guard let subtotal = self.subtotal else { return nil }
      guard let discount = self.promoCode?.discount(on: subtotal) else { return nil }
      return min(discount, subtotal)
   }
   
   var totalBeforeTax: Double? {
      guard let subtotal = self.subtotal else { return nil }
      return subtotal - (self.promoCodeDisount ?? 0)
   }
   
   var tax: Double? {
      guard let totalBeforeTax = self.totalBeforeTax else { return nil }
      return totalBeforeTax * self.taxRate
   }
   
   var total: Double? {
      guard let totalBeforeTax = self.totalBeforeTax else { return nil }
      return totalBeforeTax * (1 + self.taxRate)
   }
   
   var totalString: String? {
      guard let total = self.total else { return nil }
      return NumberFormatter.currency.string(from: total)
   }
}

// MARK: - Formatting
extension Order.FormData {
   func attributedStringForTotal(currencyFont: UIFont, numberFont: UIFont) -> NSAttributedString? {
      guard let totalString = self.totalString else { return nil }
      
      let string = NSMutableAttributedString(string: totalString, attributes: [.font: currencyFont])
      string.setAttributesForNumericCharacters([.font: numberFont])
      return string
   }
}
