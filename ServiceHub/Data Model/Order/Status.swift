//
//  Status.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

extension Order {
   enum Status: String, Codable {
      case pending
      case canceled
      case confirmed
      case inProgress = "in_progress"
      case completed
   }
}
