//
//  PromoCode.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 9/30/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct PromoCode: Codable {
      let id: Int
      let code: String
      let discountIsPercentage: Bool
      private let discountString: String
      private let maximumDiscountAmountString: String?
      var discount: Double { Double(self.discountString) ?? 0 }
      var maximumDiscountAmount: Double? {
         guard let string = self.maximumDiscountAmountString else { return nil }
         return Double(string)
      }
      
      private enum CodingKeys: String, CodingKey {
         case id
         case code
         case discountIsPercentage = "discount_is_percentage"
         case discountString = "discount"
         case maximumDiscountAmountString = "max_discount_amount"
      }
      
      func discount(on amount: Double) -> Double {
         if self.discountIsPercentage {
            let discount = self.discount * amount
            return min(discount, self.maximumDiscountAmount ?? discount)
         } else {
            return min(self.discount, self.maximumDiscountAmount ?? self.discount)
         }
      }
   }
}
