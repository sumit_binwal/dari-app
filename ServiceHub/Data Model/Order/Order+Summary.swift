//
//  Order+Summary.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct Summary: Codable {
      // MARK: - Stored Properties
      let id: Int
      let date: Date
      private let englishServiceName: String
      private let arabicServiceName: String
      let serviceFrequency: Int
      let servicePeriodCount: Int
      let serviceProvider: ServiceProvider
      let status: Status
      
      let deliveryDate: Date
      let weekdays: [Int]
      let timeSlot: TimeSlot?
      let hour: Int?
      let timeZone: TimeZone
      
      let total: Double
      private let englishCurrencySymbol: String
      private let arabicCurrencySymbol: String
      
      let paymentMethod: PaymentMethod
      
      // MARK: - Computed Properties
      var serviceName: String {
         Environment.languageIsArabic ? self.arabicServiceName : self.englishServiceName
      }
      var deliveryDescription: [String] {
         Order.deliveryDateDescription(
            forDate: self.deliveryDate,
            weekdays: self.weekdays,
            periodCount: self.servicePeriodCount
         )
      }
      var earliestDeliveryTime: Date {
         self.deliveryDate.addingTimeInterval(
            Double(self.hour ?? self.timeSlot?.startingHour ?? 0) * 3600
         )
      }
      #warning("Fix to make more relevant.")
      var isActive: Bool { self.status != .canceled && self.status != .completed }
      #warning("Fix to make more relevant.")
      var isCancellable: Bool { self.isActive }
      var refundRateIfCancelledNow: Double {
         // Check if being canceled within the grace period
         let difference = (Date() - self.date) / 60
         if difference < Double(Order.gracePeriodInMinutes) {
            return 1
         }
         
         return Order.refundRate(hoursBeforeDelivery: self.earliestDeliveryTime - Date())
      }
      var refundIfCancelledNow: Double  {
         self.refundRateIfCancelledNow * self.total
      }
      var currencySymbol: String {
         Environment.languageIsArabic ? self.arabicCurrencySymbol : self.englishCurrencySymbol
      }
      var currencyFormatter: NumberFormatter {
         let formatter = NumberFormatter()
         formatter.numberStyle = .currency
         formatter.currencySymbol = self.currencySymbol
         return formatter
      }
      
      // MARK: - Coding Keys
      private enum CodingKeys: String, CodingKey {
         case id
         case date = "order_date"
         case englishServiceName = "service_name_english"
         case arabicServiceName = "service_name_arabic"
         case serviceFrequency = "service_frequency"
         case servicePeriodCount = "service_period_count"
         case serviceProvider = "service_provider"
         case status
         
         case deliveryDate = "delivery_date"
         case weekdays
         case timeSlot = "time_slot"
         case hour
         case timeZone = "time_zone_identifier"
         
         case total
         
         case englishCurrencySymbol = "currency_symbol_english"
         case arabicCurrencySymbol = "currency_symbol_arabic"
         
         case paymentMethod = "payment_method"
      }
      
      // MARK: - Init
      init(from order: Order) {
         self.id = order.id
         self.date = order.date
         self.englishServiceName = order.serviceParameters.englishServiceName
         self.arabicServiceName = order.serviceParameters.arabicServiceName
         self.serviceFrequency = order.serviceFrequency
         self.servicePeriodCount = order.servicePeriodCount
         self.serviceProvider = order.serviceProvider
         self.status = order.status
         self.deliveryDate = order.deliveryDate
         self.weekdays = order.weekdays
         self.timeSlot = order.timeSlot
         self.hour = order.hour
         self.timeZone = order.timeZone
         self.total = order.total
         self.englishCurrencySymbol = order.englishCurrencySymbol
         self.arabicCurrencySymbol = order.arabicCurrencySymbol
         self.paymentMethod = order.paymentMethod
      }
      
      // MARK: Decoding
      init(from decoder: Decoder) throws {
         let container = try decoder.container(keyedBy: CodingKeys.self)
         
         self.id = try container.decode(Int.self, forKey: .id)
         
         let dateString = try container.decode(String.self, forKey: .date)
         if let date = DateFormatter.serverTime.date(from: dateString) {
            self.date = date
         } else {
            throw DecodingError.dataCorruptedError(
               forKey: .date,
               in: container,
               debugDescription: "Date string doesn't match format expected by formatter.")
         }

         self.englishServiceName = try container.decode(String.self, forKey: .englishServiceName)
         self.arabicServiceName = try container.decode(String.self, forKey: .arabicServiceName)
         self.serviceFrequency = try container.decode(Int.self, forKey: .serviceFrequency)
         self.servicePeriodCount = try container.decode(Int.self, forKey: .servicePeriodCount)
         self.serviceProvider = try container.decode(ServiceProvider.self, forKey: .serviceProvider)
         self.status = try container.decode(Status.self, forKey: .status)

         let timeZoneIdentifier = try container.decode(String.self, forKey: .timeZone)
         let timeZone: TimeZone = {
            if let zone = TimeZone(identifier: timeZoneIdentifier) {
               return zone
            } else {
               assertionFailure("[Order] Unrecognized time zone.")
               return Calendar.current.timeZone
            }
         }()
         self.timeZone = timeZone
         
         let deliveryDateString = try container.decode(String.self, forKey: .deliveryDate)
         
         let dateFormatter = Order.codableDeliveryDateFormatter(for: timeZone)
         if let date = dateFormatter.date(from: deliveryDateString) {
            self.deliveryDate = date
         } else {
            throw DecodingError.dataCorruptedError(
               forKey: .deliveryDate,
               in: container,
               debugDescription: "Date string doesn't match format expected by formatter."
            )
         }

         self.weekdays = try container.decode([Int].self, forKey: .weekdays)
         self.timeSlot = try? container.decode(TimeSlot.self, forKey: .timeSlot)
         self.hour = try? container.decode(Int.self, forKey: .hour)
         
         let totalString = try container.decode(String.self, forKey: .total)
         guard let total = Double(totalString) else {
            throw DecodingError.dataCorruptedError(
               forKey: .total,
               in: container,
               debugDescription: "Failed to decode total from string"
            )
         }
         self.total = total
         
         self.englishCurrencySymbol = try container.decode(
            String.self,
            forKey: .englishCurrencySymbol
         )
         self.arabicCurrencySymbol = try container.decode(
            String.self,
            forKey: .arabicCurrencySymbol
         )
         
         self.paymentMethod = try container.decode(PaymentMethod.self, forKey: .paymentMethod)
      }
      
      // MARK: - Encoding
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.id, forKey: .id)
         try container.encode(self.englishServiceName, forKey: .englishServiceName)
         try container.encode(self.arabicServiceName, forKey: .arabicServiceName)
         try container.encode(self.serviceFrequency, forKey: .serviceFrequency)
         try container.encode(self.servicePeriodCount, forKey: .servicePeriodCount)
         try container.encode(DateFormatter.serverTime.string(from: self.date), forKey: .date)
         try container.encode(self.serviceProvider, forKey: .serviceProvider)
         try container.encode(self.status, forKey: .status)
         
         try container.encode(self.timeZone.identifier, forKey: .timeZone)
         
         let dateFormatter = Order.codableDeliveryDateFormatter(for: self.timeZone)
         try container.encode(dateFormatter.string(from: self.deliveryDate), forKey: .deliveryDate)
         try container.encode(self.weekdays, forKey: .weekdays)
         try container.encode(self.timeSlot, forKey: .timeSlot)
         try container.encode(self.hour, forKey: .hour)
         
         try container.encode(String(self.total), forKey: .total)
         
         try container.encode(self.englishCurrencySymbol, forKey: .englishCurrencySymbol)
         try container.encode(self.arabicCurrencySymbol, forKey: .arabicCurrencySymbol)
         
         try container.encode(self.paymentMethod, forKey: .paymentMethod)
      }
   }
}
