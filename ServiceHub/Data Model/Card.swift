//
//  Card.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class Card: Codable {
   
   static var manager: Manager { .default }
   static var all: [Card] { User.current?.cards ?? [] }
   
   // MARK: - Stored Properties
   
   let id: Int
   let expiryMonth: Int
   let expiryYear: Int
   let last4: String
   let bin: String
   let token: String?
   
   // MARK: Coding Keys
   
   enum CodingKeys: String, CodingKey {
      case id
      case expiryMonth = "expiry_month"
      case expiryYear = "expiry_year"
      case last4 = "last4"
      case bin
      case token
   }
   
   // MARK: - Lazy and Computed Properties
   
   lazy var schemeName: String? = { Card.manager.schemeName(for: self.bin) }()
   var image: UIImage? { Card.manager.schemeIcon(for: self.bin) }
   var templateImage: UIImage? { Card.manager.schemeTemplateIcon(for: self.bin) }
   lazy var expiryDate: Date = {
      let calendar = Calendar(identifier: .gregorian)
      guard let date = calendar.date(from: DateComponents(year: expiryYear, month: expiryMonth)) else {
         assertionFailure("[Card] Failed to generate date from expiryMonth and expiryYear.")
         return Date()
      }
      guard let expiryDate = calendar.date(byAdding: .month, value: 1, to: date) else {
         assertionFailure("[Card] Failed to generate expiryDate.")
         return Date()
      }
      return expiryDate
   }()
   var isExpired: Bool { self.expiryDate < Date() }
   var isSaved: Bool {
      id > 0
   }
   
   // MARK: - Init
   init(id: Int, expiryMonth: Int, expiryYear: Int, last4: String, bin: String, token: String?) {
      self.id = id
      self.expiryMonth = expiryMonth
      self.expiryYear = expiryYear
      self.last4 = last4
      self.bin = bin
      self.token = token
   }
}

// MARK: - Equatable

extension Card: Equatable {
   static func ==(lhs: Card, rhs: Card) -> Bool { lhs.id == rhs.id && lhs.token == rhs.token }
}

// MARK: - Mada

extension Card {
   var isMada: Bool {
      Self.madaBins.contains(bin)
   }
   
   fileprivate class var madaBins: Set<String> {
      Set([
         "588845", "440647", "440795", "446404", "457865", "968208",
         "588846", "493428", "539931", "558848", "557606", "968210",
         "636120", "417633", "468540", "468541", "468542", "468543",
         "968201", "446393", "588847", "400861", "409201", "458456",
         "484783", "968205", "462220", "455708", "588848", "455036",
         "968203", "486094", "486095", "486096", "504300", "440533",
         "489317", "489318", "489319", "445564", "968211", "401757",
         "410685", "432328", "428671", "428672", "428673", "968206",
         "446672", "543357", "434107", "431361", "604906", "521076",
         "588850", "968202", "535825", "529415", "543085", "524130",
         "554180", "549760", "588849", "968209", "524514", "529741",
         "537767", "535989", "536023", "513213", "585265", "588983",
         "588982", "589005", "508160", "531095", "530906", "532013",
         "588851", "605141", "968204", "422817", "422818", "422819",
         "428331", "483010", "483011", "483012", "589206", "968207",
         "419593", "439954", "407197", "407395", "520058", "530060",
         "531196", "412565", "506968"
      ])
   }
}
