//
//  AgentType.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/4/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

struct AgentType: Codable, Equatable, Hashable {
   // MARK: - Stored Properties
   let id: Int
   let englishSingularName: String
   let arabicSingularName: String
   let arabicDualName: String
   let englishPluralName: String
   let arabicPluralName: String
   
   // MARK: - Computed Properties
   var singularName: String {
      Environment.languageIsArabic ? self.arabicSingularName : self.englishSingularName
   }
   var dualName: String {
      Environment.languageIsArabic ? self.arabicDualName : self.englishPluralName
   }
   var pluralName: String {
      Environment.languageIsArabic ? self.arabicPluralName : self.englishPluralName
   }
   
   // MARK: - Coding Keys
   enum CodingKeys: String, CodingKey {
      case id
      case englishSingularName = "singular_name_english"
      case arabicSingularName = "singular_name_arabic"
      case arabicDualName = "dual_name_arabic"
      case englishPluralName = "plural_name_english"
      case arabicPluralName = "plural_name_arabic"
   }
   
   // MARK: - Other
   func agentCountDescription(_ count: Int) -> String {
      let number = NumberFormatter.integer.string(from: count)
      switch count {
      case 1: return "\(Environment.languageIsArabic ? "" : "\(number) ")\(self.singularName)"
      case 2: return "\(Environment.languageIsArabic ? "" : "\(number) ")\(self.dualName)"
      default: return "\(number) \(self.pluralName)"
      }
   }
   
   static func ==(lhs: AgentType, rhs: AgentType) -> Bool { lhs.id == rhs.id }
}
