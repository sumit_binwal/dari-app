//
//  ImageMetadata.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/5/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation
import UIKit

struct ImageMetadata: Codable {
   let dateModified: Date
   private let width: Int
   private let height: Int
   var size: CGSize { CGSize(CGFloat(self.width), CGFloat(self.height)) }
   
   enum CodingKeys: String, CodingKey {
      case dateModified = "date_modified"
      case width = "width"
      case height = "height"
   }
   
   init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      
      self.width = try container.decode(Int.self, forKey: .width)
      self.height = try container.decode(Int.self, forKey: .height)
      
      let dateString = try container.decode(String.self, forKey: .dateModified)
      
      let formatter = ISO8601DateFormatter()
      formatter.formatOptions = .withTimeZone
      
      if let date = formatter.date(from: dateString) {
         self.dateModified = date
      } else {
         throw DecodingError.dataCorruptedError(
            forKey: .dateModified,
            in: container,
            debugDescription: "Date string doesn't match format expected by formatter.")
      }
   }
   
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.width, forKey: .width)
      try container.encode(self.height, forKey: .height)
      
      let dateString = ISO8601DateFormatter().string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
   }
}
