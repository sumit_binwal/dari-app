//
//  ResidenceType.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/13/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

enum ResidenceType: Int, Codable, CaseIterable {
   case apartment
   case villa
   
   var name: String {
      switch self {
      case .apartment: return "apartment".localized
      case .villa: return "villa".localized
      }
   }
   
   var image: UIImage? {
      switch self {
      case .apartment: return UIImage(named: "Apartment")
      case .villa: return UIImage(named: "Villa")
      }
   }
}

@available(*, deprecated)
struct DeprecatedResidenceType: Codable, Equatable, Hashable {
    let id: Int
    var title: String { return Environment.languageIsArabic ? title_arabic : title_english }
    let title_english: String
    let title_arabic: String
    
    static func ==(lhs: DeprecatedResidenceType, rhs: DeprecatedResidenceType) -> Bool {
        return lhs.id == rhs.id
    }
    
    static let allCases: [DeprecatedResidenceType] = {
        /*for serviceProvider in ServiceProvider.loadServiceProviders() {
            guard let pricing = serviceProvider.residenceBasedInventoryList.first else { continue }
            return pricing.pricePoints.map { $0.residenceType }
        }*/
        return []
    }()
}
