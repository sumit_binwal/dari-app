//
//  ErrorTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/28/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class ErrorTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLable: UILabel!
    private var view: UIView? { return titleLabel.superview }
    let normalBackgroundColor: UIColor = .unhighlightedBackground
    let highlightedBackgroundColor: UIColor = .highlightedBackground
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        backgroundColor = .clear
        [titleLabel, subtitleLable].forEach { $0?.textColor = .darkBackgroundProminentText }
        titleLabel.font = .mediumFont(ofSize: titleLabel.font.pointSize)
        titleLabel.superview?.layer.cornerRadius = .cornerRadius
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setAppearance(highlighted: selected, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        setAppearance(highlighted: highlighted, animated: true)
    }
    
    private func setAppearance(highlighted: Bool, animated: Bool) {
        UIView.animate(withDuration: animated ? .highlightAnimationDuration : 0) {
            self.titleLabel.superview?.backgroundColor = highlighted ?
                self.highlightedBackgroundColor : self.normalBackgroundColor
            self.transform = highlighted ? .highlightTransformForLargeViews : .identity
        }
    }
    
    func jiggle() {
        let scaleValues = [1.0, 1.04, 1.0]
        let calculationMode = CAAnimationCalculationMode.cubic
        let xScaleAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
            animation.valueFunction = CAValueFunction(name: .scaleX)
            animation.values = scaleValues
            animation.calculationMode = calculationMode
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = 0.25
            return animation
        }()
        let yScaleAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
            animation.valueFunction = CAValueFunction(name: .scaleY)
            animation.values = scaleValues
            animation.isAdditive = true
            animation.calculationMode = calculationMode
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = 0.25
            return animation
        }()
        let wobbleAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
            animation.valueFunction = CAValueFunction(name: .rotateZ)
            let wobbleAngle: Float = .pi / 288
            animation.values = [0.0, wobbleAngle, 0, -wobbleAngle, 0]
            animation.isAdditive = true
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = 0.05
            animation.calculationMode = calculationMode
            return animation
        }()
        let highlightAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.backgroundColor))
            animation.values = [normalBackgroundColor.cgColor,
                                highlightedBackgroundColor.cgColor,
                                normalBackgroundColor.cgColor]
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = 0.25
            animation.calculationMode = calculationMode
            return animation
        }()
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = [xScaleAnimation, yScaleAnimation, wobbleAnimation, highlightAnimation]
        animationGroup.isRemovedOnCompletion = true
        animationGroup.duration = 0.5
        view?.layer.add(animationGroup, forKey: nil)
    }
}
