//
//  AuthView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AuthView: UIScrollView {
   var registerFormViewDelegate: UserFormViewDelegate? {
      get {
         self.registerFormView.delegate
      }
      set {
         self.registerFormView.delegate = newValue
      }
   }
   var showLogInView: Bool = true
   
   // MARK: - UI
   private lazy var contentView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.backgroundColor = .background
      
      view.addSubview(self.registerFormView)
      let marginsGuide = view.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.registerFormView.topAnchor.constraint(
            equalToSystemSpacingBelow: view.topAnchor, multiplier: 3),
         self.registerFormView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.registerFormView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor)
      ])
      
      if self.showLogInView {
         view.addSubview(self.logInView)
         NSLayoutConstraint.activate([
            self.logInView.topAnchor.constraint(
               equalToSystemSpacingBelow: self.registerFormView.bottomAnchor, multiplier: 2),
            self.logInView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
            self.logInView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
            self.logInView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
         ])
      } else {
         self.registerFormView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
            .isActive = true
      }
      
      return view
   }()
   lazy var registerFormView: RegisterFormView = .init()
   private lazy var logInView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .lighterBackground
      view.layer.cornerRadius = .cornerRadius
      view.layer.masksToBounds = true
      
      view.addSubviews([self.logInLabel, self.logInButton])
      NSLayoutConstraint.activate([
         self.logInLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
         self.logInLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         self.logInLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
         
         self.logInButton.topAnchor.constraint(
            equalToSystemSpacingBelow: self.logInLabel.bottomAnchor, multiplier: 1),
         self.logInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         self.logInButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var logInLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "alreadyAMember".localized
      label.textAlignment = .center
      label.textColor = .subtleText
      label.font = .regularFont(ofSize: 17)
      return label
   }()
   lazy var logInButton: BigButton = {
      let button = BigButton()
      button.setTitle("signIn".localized, for: .normal)
      return button
   }()
   
   // MARK: - Init
   
   convenience init(showLogInView: Bool = true) {
      self.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.backgroundColor = .background
      self.showsVerticalScrollIndicator = false
      self.showsHorizontalScrollIndicator = false
      self.keyboardDismissMode = .interactive
      
      self.showLogInView = showLogInView
      self.addSubview(self.contentView)
      NSLayoutConstraint.activate([
         self.contentView.topAnchor.constraint(equalTo: self.topAnchor),
         self.contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
      
      if showLogInView {
         self.logInButton.widthAnchor.constraint(
            equalTo: self.registerFormView.submitButton.widthAnchor
         ).isActive = true
      }
      
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillChange(notification:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil)
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillHideNotification),
         name: UIResponder.keyboardWillHideNotification,
         object: nil)
   }
   
   deinit { NotificationCenter.default.removeObserver(self) }
   
   
   // MARK: - Other
   
   override func didMoveToSuperview() {
      super.didMoveToSuperview()
      guard let superview = self.superview else { return }
      self.contentView.widthAnchor.constraint(equalTo: superview.widthAnchor).isActive = true
   }
   
   @objc
   func handleKeyboardWillChange(notification: Notification) {
      if let userInfo = notification.userInfo,
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue)?.cgRectValue.height {
         self.contentInset.bottom = keyboardHeight - self.safeAreaInsets.bottom
      }
   }
   
   @objc
   func handleKeyboardWillHideNotification() {
      self.contentInset.bottom = 0
   }
   
   func setAddress(_ address: String?, latitude: Double?, longitude: Double?) {
      self.registerFormView.setAddress(address, latitude: latitude, longitude: longitude)
   }
}
