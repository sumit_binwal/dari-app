//
//  DistrictTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/3/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class DistrictTableViewCell: UITableViewCell {
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: nil)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.textLabel?.textColor = .prominentText
      if let pointSize = self.textLabel?.font.pointSize {
         self.textLabel?.font = .regularFont(ofSize: pointSize)
      }
   }
}
