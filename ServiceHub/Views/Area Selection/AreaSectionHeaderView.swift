//
//  AreaSectionHeaderView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/2/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

#warning("Rewrite to remove xib.")
class AreaSectionHeaderView: UITableViewHeaderFooterView {
   @IBOutlet weak var imageView: UIImageView!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var arrowImageView: UIImageView!
   weak var delegate: AreaSectionHeaderViewDelegate?
   private var isExpanded: Bool = false
   var sectionNumber: Int = 0
   
   override func awakeFromNib() {
      super.awakeFromNib()
      arrowImageView.tintColor = .calypso
      titleLabel.textColor = .prominentText
      imageView.tintColor = .calypso
      self.contentView.backgroundColor = .background
      addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
   }
   
   func setIsExpanded(_ expanded: Bool, animated: Bool) {
      guard isExpanded != expanded else { return }
      isExpanded = expanded
      UIView.animate(withDuration: animated ? 0.2 : 0) {
         self.arrowImageView.transform = expanded ? CGAffineTransform(rotationAngle: .pi) : .identity
      }
   }
   
   @objc private func tapped() {
      let expand = !isExpanded
      setIsExpanded(expand, animated: true)
      delegate?.sectionHeaderExpanded(expand, section: sectionNumber)
   }
}

protocol AreaSectionHeaderViewDelegate: class {
   func sectionHeaderExpanded(_ expanded: Bool, section: Int)
}
