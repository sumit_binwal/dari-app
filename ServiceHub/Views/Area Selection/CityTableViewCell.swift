//
//  CityTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/2/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        cityLabel.textColor = selected ? .prominentText : .lightGray
        cityImageView.tintColor = selected ? .prominentText : .lightGray
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        cityLabel.textColor = highlighted ? .prominentText : .lightGray
        cityImageView.tintColor = highlighted ? .prominentText : .lightGray
    }
}
