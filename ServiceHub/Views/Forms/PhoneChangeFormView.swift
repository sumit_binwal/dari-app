//
//  PhoneChangeFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PhoneChangeFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] { [self.phoneNumberStackView] }
   override internal var requiredFields: [UITextField] { [self.phoneNumberTextField] }
   override internal var submitButtonTitle: String { "change".localized }
   override internal var phoneNumberInfo: String? { nil }
   override internal var termsViewIncluded: Bool { false }
   
   override internal var phoneNumberIsValid: Bool {
      super.phoneNumberIsValid && self.phoneNumber != self.currentUser?.phoneNumber
   }
   override internal var isValid: Bool { self.phoneNumberIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      var dictionary: [String: Codable] = [:]
      
      if self.phoneNumberIsValid {
         dictionary[User.CodingKeys.phoneNumber.rawValue] = self.phoneNumber
      }
      
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitWithKeyValuePairs: dictionary)
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages)
      -> (stacks: [UIStackView], messages: [[String]])
   {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let errors = errorMessages.all {
         stacksToUpdate.append(self.phoneNumberStackView)
         messages.append(errors)
      }
      
      return (stacksToUpdate, messages)
   }
}
