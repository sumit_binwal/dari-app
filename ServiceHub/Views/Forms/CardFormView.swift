//
//  CardFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardFormView: UIView {
   // MARK: - Properties
   weak var delegate: CardViewDelegate?
   var standardizedCardNumber: String { self.cardNumberInputView.standardizedCardNumber }
   var cardNumberIsValid: Bool { self.cardNumberInputView.cardNumberIsValid }
   var cardTypeIsAccepted: Bool {
      Card.manager.cardTypeIsAccepted(for: self.standardizedCardNumber)
   }
   var expiryDateIsValid: Bool { self.expiryDateTextField.expiryDateIsValid }
   var expiryMonth: String { self.expiryDateTextField.expiryMonth }
   var expiryYear: String { self.expiryDateTextField.expiryYear }
   var cvv: String { self.cvvTextField.cvv }
   var cvvIsValid: Bool {
      Card.manager.isValid(cvv: self.cvv, cardNumber: self.standardizedCardNumber)
   }
   var hasEmptyRequiredField: Bool {
      self.standardizedCardNumber.isEmpty ||
         (self.expiryDateTextField.text ?? "").isEmpty ||
         (self.cvvTextField.text ?? "").isEmpty
   }
   var name: String? {
      guard let text = self.nameTextField.text, !text.isEmpty else { return nil }
      return text
   }
   let purpose: Purpose
   var showSaveCardOption: Bool { purpose == .pay }
   var saveCard: Bool = true
   
   // MARK: - UI Properties
   private var genericInputInstructionStackView: UIStackView {
      let stackView = UIStackView()
      stackView.translatesAutoresizingMaskIntoConstraints = false
      stackView.axis = .vertical
      stackView.spacing = 5
      return stackView
   }
   lazy var cardNumberInputView: CardNumberFieldView = .init()
   lazy var expiryDateTextField: ExpiryDateTextField = .init()
   lazy var cvvTextField: CVVTextField = .init()
   lazy var nameTextField: UnderLineTextField = {
      let textField = UnderLineTextField()
      textField.placeholder = "Name (Optional)"
      textField.textContentType = .name
      textField.textAlignment = .left
      return textField
   }()
   
   private var genericInstructionLabel: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = label.font.withSize(14)
      label.numberOfLines = 3
      label.textColor = .red
      label.isHidden = true
      return label
   }
   lazy var cardNumberInstructionLabel: UILabel = {
      let label = self.genericInstructionLabel
      label.text = "cardNumberInvalid".localized
      return label
   }()
   lazy var expiryDateInstructionLabel: UILabel = {
      let label = self.genericInstructionLabel
      label.text = "expiryDateInvalid".localized
      return label
   }()
   lazy var cvvInstructionLabel: UILabel = {
      let label = self.genericInstructionLabel
      label.text = "cvvInvalid".localized
      return label
   }()
   lazy var saveCardButton: CheckBoxButton = {
      let button = CheckBoxButton()
      button.setTitle("saveCard".localized, for: .normal)
      button.isChecked = self.saveCard
      button.addTarget(self, action: #selector(self.saveCardButtonWasTapped), for: .touchUpInside)
      return button
   }()
   
   // MARK: - Initialization
   init(purpose: Purpose) {
      self.purpose = purpose
      super.init(frame: .zero)
      self.setUp()
      self.setDelegatesAndActions()
      self.setTextFieldToolbars()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      backgroundColor = .clear
      semanticContentAttribute = .forceLeftToRight
      
      // Stack View
      let stackView = UIStackView()
      stackView.translatesAutoresizingMaskIntoConstraints = false
      stackView.axis = .vertical
      stackView.spacing = 20
      addSubview(stackView)
      stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
      stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      
      // Card Number Input
      stackView.addArrangedSubview({
         let stackView = self.genericInputInstructionStackView
         stackView.addArrangedSubview(self.cardNumberInputView)
         stackView.addArrangedSubview(self.cardNumberInstructionLabel)
         return stackView
      }())
      
      // Expiry and CVV
      stackView.addArrangedSubview({
         let horizontalStackView = UIStackView()
         horizontalStackView.axis = .horizontal
         horizontalStackView.spacing = 6
         horizontalStackView.distribution = .fillEqually
         horizontalStackView.addArrangedSubview(self.expiryDateTextField)
         horizontalStackView.addArrangedSubview(self.cvvTextField)
         
         let stackView = self.genericInputInstructionStackView
         stackView.addArrangedSubview(horizontalStackView)
         stackView.addArrangedSubview(self.expiryDateInstructionLabel)
         stackView.addArrangedSubview(self.cvvInstructionLabel)
         return stackView
      }())
      
      // Name
      stackView.addArrangedSubview(self.nameTextField)
      
      // Save card view
      guard showSaveCardOption else { return }
      stackView.addArrangedSubview({
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.backgroundColor = backgroundColor
         
         view.addSubview(self.saveCardButton)
         self.saveCardButton.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
         self.saveCardButton.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
         self.saveCardButton.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         
         return view
      }())
   }
   
   private func setDelegatesAndActions() {
      self.cardNumberInputView.delegate = self
      self.expiryDateTextField.externalDelegate = self
      self.cvvTextField.addTarget(
         self,
         action: #selector(self.cvvTextFieldDidChange),
         for: .editingChanged
      )
      self.nameTextField.delegate = self
   }
   
   private func setTextFieldToolbars() {
      let textFields: [UITextField] = [
         self.cardNumberInputView.cardTextField,
         self.expiryDateTextField,
         self.cvvTextField,
         self.nameTextField
      ]
      
      for (index, textField) in textFields.enumerated() { textField.tag = index + 1 }
      textFields.forEach { $0.inputAccessoryView = $0.toolbar(in: self) }
      
      self.nameTextField.returnKeyType = .done
   }
   
   // MARK: - Actions
   @objc private func cvvTextFieldDidChange() {
      self.hideCVVInstructionLabelIfNecessary()
      self.delegate?.cardDetailsDidChange()
   }
   
   @objc private func saveCardButtonWasTapped() {
      self.saveCard = saveCardButton.isChecked
      self.delegate?.cardDetailsDidChange()
   }
   
   // MARK: - Validation
   public func validate() -> Bool {
      guard cardTypeIsAccepted else {
         self.cardNumberInstructionLabel.text = "cardTypeUnaccepted".localized
         self.setCardNumberInstructionLabelHidden(false)
         return false
      }
      guard cardNumberIsValid else {
         self.cardNumberInstructionLabel.text = "cardNumberInvalid".localized
         self.setCardNumberInstructionLabelHidden(false)
         return false
      }
      
      guard expiryDateIsValid else {
         self.setExpiryDateInstructionLabelHidden(false)
         return false
      }
      
      guard cvvIsValid else {
         self.setCVVInstructionLabelHidden(false)
         return false
      }
      
      return true
   }
   
   private func hideCardNumberInstructionLabelIfNecessary() {
      guard !self.cardNumberInstructionLabel.isHidden else { return }
      guard self.cardNumberInputView.cardNumberIsValid else { return }
      self.setCardNumberInstructionLabelHidden(true)
   }
   
   private func hideExpiryDateInstructionLabelIfNecessary() {
      guard !self.expiryDateInstructionLabel.isHidden else { return }
      guard self.expiryDateTextField.expiryDateIsValid else { return }
      self.setExpiryDateInstructionLabelHidden(true)
   }
   
   private func hideCVVInstructionLabelIfNecessary() {
      guard !self.cvvInstructionLabel.isHidden else { return }
      guard self.cvvTextField.cvvIsValid else { return }
      self.setCVVInstructionLabelHidden(true)
   }
   
   private func setCardNumberInstructionLabelHidden(_ isHidden: Bool) {
      guard isHidden != self.cardNumberInstructionLabel.isHidden else { return }
      self.setInstructionLabelHidden(self.cardNumberInstructionLabel, isHidden: isHidden)
   }
   
   private func setExpiryDateInstructionLabelHidden(_ isHidden: Bool) {
      guard isHidden != self.expiryDateInstructionLabel.isHidden else { return }
      self.setInstructionLabelHidden(self.expiryDateInstructionLabel, isHidden: isHidden)
   }
   
   private func setCVVInstructionLabelHidden(_ isHidden: Bool) {
      guard isHidden != self.cvvInstructionLabel.isHidden else { return }
      self.setInstructionLabelHidden(self.cvvInstructionLabel, isHidden: isHidden)
   }
   
   private func setInstructionLabelHidden(_ label: UILabel, isHidden: Bool) {
      UIView.animate(
         withDuration: .defaultUIAnimationDuration,
         delay: 0,
         options: .curveEaseInOut,
         animations: {
            label.isHidden = isHidden
            label.superview?.layoutIfNeeded()
      }, completion: nil)
   }
}

// MARK: - VXCardNumberInputViewDelegate
extension CardFormView: CardNumberFieldViewDelegate {
   func cardNumberDidChange(_ cardNumber: String) {
      self.cvvTextField.cardNumber = cardNumber
      self.hideCardNumberInstructionLabelIfNecessary()
      self.delegate?.cardDetailsDidChange()
   }
}

// MARK: - ExpiryDateTextFieldDelegate
extension CardFormView: ExpiryDateTextFieldDelegate {
   func expiryDateDidChange() {
      self.hideExpiryDateInstructionLabelIfNecessary()
      self.delegate?.cardDetailsDidChange()
   }
}

// MARK: - UITextFieldDelegate
extension CardFormView: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      return textField.resignFirstResponder()
   }
}

// MARK: - Form Purpose
extension CardFormView {
   enum Purpose {
      case add
      case pay
   }
}

// MARK: - Protocol
protocol CardViewDelegate: AnyObject {
   func cardDetailsDidChange()
}
