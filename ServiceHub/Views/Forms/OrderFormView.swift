//
//  OrderFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/19/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderFormView: UITableView {
   
   // MARK: - Properties
   
   weak var orderFormViewDelegate: OrderFormViewDelegate?
   /// Keeps track of the parent service when there are variants.
   let formData: Order.FormData
   var availability: Service.Availability { self.formData.availability }
   
   // MARK: Init
   
   init(data: Order.FormData) {
      self.formData = data
      super.init(frame: .zero, style: .plain)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 100
      self.separatorInset = .zero
      self.tableFooterView = UIView()
      self.keyboardDismissMode = .interactive
      
      self.delegate = self
      self.dataSource = self
      
      self.register(
         NumberPickerTableViewCell.self,
         forCellReuseIdentifier: "NumberPickerTableViewCell"
      )
      self.register(
         ResidencePickerTableViewCell.self,
         forCellReuseIdentifier: "ResidencePickerTableViewCell"
      )
      self.register(
         ServiceVariantPickerTableViewCell.self,
         forCellReuseIdentifier: "ServiceVariantPickerTableViewCell"
      )
      self.register(
         OrderItemParametersView.self,
         forCellReuseIdentifier: "OrderItemParametersTableViewCell"
      )
      self.register(
         DatePickerTableViewCell.self,
         forCellReuseIdentifier: "DatePickerTableViewCell"
      )
      self.register(
         HourPickerTableViewCell.self,
         forCellReuseIdentifier: "HourPickerTableViewCell"
      )
      self.register(
         TimeSlotPickerTableViewCell.self,
         forCellReuseIdentifier: "TimeSlotPickerTableViewCell"
      )
      self.register(TextTableViewCell.self, forCellReuseIdentifier: "TextTableViewCell")
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   
   private func reloadDateAndTimeIfNecessary() {
      reloadDateIfNecessary()
      reloadTimeIfNecessary()
   }
   
   private func reloadDateIfNecessary() {
      let datePickerCellOrNil = cellForRow(
         at: .init(row: rowForDatePicker, section: sectionForDateAndTime)
      ) as? DatePickerTableViewCell
      guard let datePickerCell = datePickerCellOrNil else { return }
      
      let dates = availability.dates(forSelectedDuration: formData.selectedDuration)
      formData.selectedDate = dates.contains(formData.selectedDate) ?
         formData.selectedDate : dates.first
      datePickerCell.reload(dates: dates, selectedDate: formData.selectedDate)
   }
   
   private func reloadTimeIfNecessary() {
      let date = formData.selectedDate
      
      if self.availability.isInTimeSlots {
         let timeSlots = availability.timeSlots(
            forDate: date,
            selectedDuration: formData.selectedDuration
         )
         self.formData.selectedTimeSlot = timeSlots.contains(self.formData.selectedTimeSlot) ?
            self.formData.selectedTimeSlot : timeSlots.first
         
         let timeSlotPickerCell = self.cellForRow(
            at: .init(row: self.rowForTimePicker, section: self.sectionForDateAndTime)
         ) as? TimeSlotPickerTableViewCell
         timeSlotPickerCell?.reload(
            timeSlots: timeSlots,
            selectedTimeSlot: self.formData.selectedTimeSlot
         )
         
      } else {
         let hours = self.availability.hours(forDate: date)
         self.formData.selectedHour =
            hours.contains(self.formData.selectedHour) ? self.formData.selectedHour : hours.first
         
         let hourPickerCell = self.cellForRow(
            at: .init(row: self.rowForTimePicker, section: self.sectionForDateAndTime)
         ) as? HourPickerTableViewCell
         hourPickerCell?.reload(hours: hours, selectedHour: self.formData.selectedHour)
      }
   }
}

// MARK: - Table View

extension OrderFormView: UITableViewDelegate, UITableViewDataSource {
   private var sectionForService: Int { 0 }
   private var sectionForServiceVariants: Int { 1 }
   private var sectionForItems: Int { 2 }
   private var sectionForDateAndTime: Int { 3 }
   private var sectionForOthers: Int { 4 }
   
   private var rowForDatePicker: Int { 0 }
   private var rowForTimePicker: Int { 1 }
   
   private var tagForAgentCount: Int { 1 }
   private var tagForDuration: Int { 2 }
   private var tagForRoomCount: Int { 3 }
   
   func numberOfSections(in tableView: UITableView) -> Int { 5 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case self.sectionForService:
         switch self.formData.primaryService {
         case _ as AgentDurationDependentService: return 2
         case _ as ResidenceDependantService: return 2
         default: return 0
         }
         
      case self.sectionForServiceVariants:
         return self.formData.primaryService.variants.isEmpty ? 0 : 1
      
      case self.sectionForItems: return self.formData.itemConstraints.count
      
      case self.sectionForDateAndTime: return self.formData.requiresMultipleDates ? 0 : 2
         
      case self.sectionForOthers: return self.formData.requiresMultipleDates ? 0 : 1
         
      default: return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      // MARK: - Service
      case self.sectionForService:
         switch self.formData.primaryService {
         case let service as AgentDurationDependentService:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "NumberPickerTableViewCell", for: indexPath)
               as? NumberPickerTableViewCell else
            {
               fatalError("[RecurringServiceOrderFormView] Failed to dequeue NumberPickerTableViewCell.")
            }
            
            let isAgentCount = indexPath.row == 0
            cell.setValues(
               numbers: isAgentCount ? service.agentCounts : service.durations,
               selectedNumber: isAgentCount ?
                  self.formData.selectedAgentCount : self.formData.selectedDuration,
               instruction: isAgentCount ?
                  service.agentCountSelectionInstruction : service.durationSelectionInstruction
            )
            cell.tag = isAgentCount ? self.tagForAgentCount : self.tagForDuration
            cell.delegate = self
            
            return cell
            
         case let service as ResidenceDependantService:
            if indexPath.row == 0 {
               guard let cell = tableView
                  .dequeueReusableCell(withIdentifier: "ResidencePickerTableViewCell", for: indexPath)
                  as? ResidencePickerTableViewCell else
               {
                  fatalError("[ServiceOrderFormView] Failed to dequeue ResidencePickerTableViewCell.")
               }
               cell.setValues(
                  instruction: service.residenceTypeInstruction,
                  selectedResidenceType: self.formData.selectedResidenceType
               )
               cell.delegate = self
               return cell
            }
            
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "NumberPickerTableViewCell", for: indexPath)
               as? NumberPickerTableViewCell else
            {
               fatalError("[RecurringServiceOrderFormView] Failed to dequeue NumberPickerTableViewCell.")
            }
            
            cell.setValues(
               numbers: service.roomCounts,
               selectedNumber: self.formData.selectedRoomCount,
               instruction: service.roomCountInstruction
            )
            cell.tag = self.tagForRoomCount
            cell.delegate = self
            
            return cell
            
         default: fatalError("[ServiceOrderFormView] Unrecognized index path.")
         }
      
      // MARK: - Service Variants
      case self.sectionForServiceVariants:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "ServiceVariantPickerTableViewCell", for: indexPath)
            as? ServiceVariantPickerTableViewCell else
         {
            fatalError("[ServiceOrderFormView] Failed to dequeue ServiceVariantPickerTableViewCell.")
         }
         cell.setValues(
            services: self.formData.primaryService.variants,
            selectedService: self.formData.selectedService,
            rateDescriptions: self.formData.primaryService.variants.map {
               self.formData.rateDescription(forService: $0)
            },
            instruction: self.formData.primaryService.variantSelectionInstruction
         )
         cell.delegate = self
         return cell
      
      // MARK: - Items
      case self.sectionForItems:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "OrderItemParametersTableViewCell", for: indexPath)
            as? OrderItemParametersView else
         {
            fatalError("[ServiceOrderFormView] Failed to dequeue OrderItemParametersTableViewCell.")
         }
         
         let constraints = self.formData.itemConstraints[indexPath.row]
         let parameters = self.formData.parameterSets[constraints.itemID] ?? []
         cell.configure(withItemConstraints: constraints, parameters: parameters )
         cell.delegate = self
         return cell
      
      // MARK: - Dates and Times
      case self.sectionForDateAndTime:
         switch indexPath.row {
         case rowForDatePicker:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "DatePickerTableViewCell", for: indexPath)
               as? DatePickerTableViewCell else {
               fatalError("[ServiceOrderFormView] Failed to dequeue DatePickerTableViewCell.")
            }
            
            cell.configure(
               withDates: self.formData.dates,
               selectedDate: self.formData.selectedDate,
               instruction: self.formData.primaryService.dateSelectionInstruction
            )
            cell.delegate = self
            return cell
            
         case rowForTimePicker:
            if self.availability.isInTimeSlots {
               guard let cell = tableView
                  .dequeueReusableCell(withIdentifier: "TimeSlotPickerTableViewCell", for: indexPath)
                  as? TimeSlotPickerTableViewCell else
               {
                  fatalError("[ServiceOrderFormView] Failed to dequeue TimeSlotPickerTableViewCell.")
               }
               
               let timeSlots = availability.timeSlots(
                  forDate: formData.selectedDate,
                  selectedDuration: formData.selectedDuration
               )
               
               cell.setValues(
                  timeSlots: timeSlots,
                  selectedTimeSlot: self.formData.selectedTimeSlot,
                  instruction: self.formData.primaryService.timeSelectionInstruction
               )
               cell.delegate = self
               return cell
               
            } else {
               guard let cell = tableView
                  .dequeueReusableCell(withIdentifier: "HourPickerTableViewCell", for: indexPath)
                  as? HourPickerTableViewCell else
               {
                  fatalError("[ServiceOrderFormView] Failed to dequeue HourPickerTableViewCell.")
               }
               
               cell.configure(
                  withHours: self.availability.hours(forDate: self.formData.selectedDate),
                  selectedHour: self.formData.selectedHour,
                  instruction: self.formData.primaryService.timeSelectionInstruction
               )
               cell.delegate = self
               return cell
            }
            
         default: fatalError("[ServiceOrderFormView] Unrecognized index path.")
         }
         
      // MARK: - Additional Notes
      case self.sectionForOthers:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "TextTableViewCell", for: indexPath)
            as? TextTableViewCell else
         {
            fatalError("[ServiceOrderFormView] Failed to dequeue TextTableViewCell.")
         }
         cell.configure(
            withText: self.formData.notes,
            instruction: self.formData.primaryService.noteInstruction
         )
         cell.delegate = self
         return cell
         
      default: fatalError()
      }
   }
}

// MARK: - ParameterSetterDelegate
extension OrderFormView: ParameterSetterDelegate {
   func parameterSetter(forItem item: Item, didChangeParameters parameterSet: [Item.Parameter]) {
      self.formData.parameterSets[item.id] = parameterSet
      self.orderFormViewDelegate?.orderFormViewDataDidChange()
   }
}

// MARK: - DatePickerTableViewCellDelegate
extension OrderFormView: DatePickerTableViewCellDelegate {
   func datePicker(didPickDate date: Date) {
      formData.selectedDate = date
      reloadTimeIfNecessary()
   }
}

// MARK: - HourPickerTableViewCellDelegate
extension OrderFormView: HourPickerTableViewCellDelegate {
   func pickerView(didSelectHour hour: Int) {
      self.formData.selectedHour = hour
   }
}

// MARK: TimeSlotPickerViewDelegate
extension OrderFormView: TimeSlotPickerViewDelegate {
   func timeSlotPicker(didSelectTimeSlot timeSlot: TimeSlot) {
      self.formData.selectedTimeSlot = timeSlot
   }
}

// MARK: NumberPickerTableViewCellDelegate
extension OrderFormView: NumberPickerTableViewCellDelegate {
   func numberPicker(withTag tag: Int, didSelectNumber number: Int) {
      switch tag {
      case tagForAgentCount:
         formData.selectedAgentCount = number
      
      case tagForDuration:
         formData.selectedDuration = number
         reloadDateAndTimeIfNecessary()
         
      case tagForRoomCount:
         formData.selectedRoomCount = number
         
      default: assertionFailure("[ServiceOrderFormView] Unrecognized number picker tag.")
      }
      self.orderFormViewDelegate?.orderFormViewDataDidChange()
   }
}

// MARK: ResidencePickerTableViewCellDelegate
extension OrderFormView: ResidencePickerTableViewCellDelegate {
   func residencePicker(didPickResidenceType residenceType: ResidenceType) {
      self.formData.selectedResidenceType = residenceType
      self.orderFormViewDelegate?.orderFormViewDataDidChange()
   }
}

// MARK: TextTableViewCellDelegate
extension OrderFormView: TextTableViewCellDelegate {
   func textTableViewCell(didChangeText text: String) {
      self.formData.notes = text
   }
}

// MARK: - ServiceVariantPickerTableViewCell
extension OrderFormView: ServiceVariantPickerTableViewCellDelegate {
   func serviceVariantPicker(didSelectService service: Service) {
      self.formData.selectedService = service
      self.orderFormViewDelegate?.orderFormViewDataDidChange()
   }
}

// MARK: - Protocol
protocol OrderFormViewDelegate: AnyObject {
   func orderFormViewDataDidChange()
}
