//
//  EmailChangeFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class EmailChangeFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] { [self.emailStackView] }
   override internal var requiredFields: [UITextField] { [self.emailTextField] }
   override internal var submitButtonTitle: String { "change".localized }
   override internal var termsViewIncluded: Bool { false }
   
   override internal var emailIsValid: Bool {
      super.emailIsValid && self.email != self.currentUser?.email
   }
   override internal var isValid: Bool { self.emailIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      var dictionary: [String: Codable] = [:]
      
      if self.emailIsValid {
         dictionary[User.CodingKeys.email.rawValue] = self.email
      }
      
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitWithKeyValuePairs: dictionary)
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages)
      -> (stacks: [UIStackView], messages: [[String]])
   {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let errors = errorMessages.all {
         stacksToUpdate.append(self.emailStackView)
         messages.append(errors)
      }
      
      return (stacksToUpdate, messages)
   }
}
