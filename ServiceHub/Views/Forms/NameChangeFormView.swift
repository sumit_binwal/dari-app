//
//  NameChangeFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class NameChangeFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] { [self.nameStackView] }
   override internal var requiredFields: [UITextField] {
      [self.firstNameTextField, self.lastNameTextField]
   }
   override internal var submitButtonTitle: String { "change".localized }
   override internal var termsViewIncluded: Bool { false }
   
   override internal var firstNameIsValid: Bool {
      super.firstNameIsValid && self.firstName != self.currentUser?.firstName
   }
   override internal var lastNameIsValid: Bool {
      super.lastNameIsValid && self.lastName != self.currentUser?.lastName
   }
   override internal var isValid: Bool { self.firstNameIsValid || self.lastNameIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      var dictionary: [String: Codable] = [:]
      
      if self.firstNameIsValid {
         dictionary[User.CodingKeys.firstName.rawValue] = self.firstName
      }
      if self.lastNameIsValid {
         dictionary[User.CodingKeys.lastName.rawValue] = self.lastName
      }
      
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitWithKeyValuePairs: dictionary)
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages)
      -> (stacks: [UIStackView], messages: [[String]])
   {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let errors = errorMessages.all {
         stacksToUpdate.append(self.nameStackView)
         messages.append(errors)
      }
      
      return (stacksToUpdate, messages)
   }
}
