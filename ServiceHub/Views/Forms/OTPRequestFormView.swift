//
//  OTPRequestFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OTPRequestFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] { [self.emailStackView] }
   override internal var requiredFields: [UITextField] { [self.emailTextField] }
   
   override internal var termsViewIncluded: Bool { false }
   override internal var submitButtonTitle: String { "requestOTP".localized }
   
   override internal var isValid: Bool { self.emailIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      self.removeErrorMessages()
      self.delegate?.userForm(didRequestOTPWithEmail: self.email ?? "")
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages
   ) -> (stacks: [UIStackView], messages: [[String]]) {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let errors = errorMessages.all {
         stacksToUpdate.append(self.emailStackView)
         messages.append(errors)
      }
      
      return (stacksToUpdate, messages)
   }
}
