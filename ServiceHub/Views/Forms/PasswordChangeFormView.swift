//
//  PasswordChangeFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PasswordChangeFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] { [self.passwordStackView] }
   override internal var requiredFields: [UITextField] { [self.passwordTextField] }
   override internal var submitButtonTitle: String { "change".localized }
   override internal var termsViewIncluded: Bool { false }
   
   override internal var isValid: Bool { self.passwordIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      // Password should already be validated
      let password = self.password ?? ""
      
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitPasswordReset: password, confirmPassword: password)
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages)
      -> (stacks: [UIStackView], messages: [[String]])
   {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let errors = errorMessages.all {
         stacksToUpdate.append(self.passwordStackView)
         messages.append(errors)
      }
      
      return (stacksToUpdate, messages)
   }
}
