//
//  DateFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DateFormView: UITableView {
   // MARK: - Properties
   weak var dateFormViewDelegate: DateFormViewDelegate?
   let formData: Order.FormData
   var availability: Service.Availability { self.formData.availability }
   var weekdaySelectionWarningStatus: WeekdayPickerTableViewCell.WarningStatus = .none
   
   // MARK: - Init
   init(data: Order.FormData) {
      // Check if selected weekday count exceeds service weekly frequency
      let difference = data.selectedWeekdays.count - data.selectedService.frequency
      if difference > 0 {
         data.selectedWeekdays.removeLast(difference)
      }
      
      let dates = data.availability.dates(
         forSelectedWeekdays: data.selectedWeekdays,
         returningAllIfEmpty: true
      )
      data.selectedDate = dates.contains(data.selectedDate) ? data.selectedDate : dates.first
      
      self.formData = data
      super.init(frame: .zero, style: .plain)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 100
      self.separatorInset = .zero
      self.tableFooterView = UIView()
      self.keyboardDismissMode = .interactive
      
      self.delegate = self
      self.dataSource = self
      
      self.register(
         DatePickerTableViewCell.self,
         forCellReuseIdentifier: "DatePickerTableViewCell"
      )
      self.register(
         HourPickerTableViewCell.self,
         forCellReuseIdentifier: "HourPickerTableViewCell"
      )
      self.register(
         TimeSlotPickerTableViewCell.self,
         forCellReuseIdentifier: "TimeSlotPickerTableViewCell"
      )
      self.register(
         WeekdayPickerTableViewCell.self,
         forCellReuseIdentifier: "WeekdayPickerTableViewCell"
      )
      self.register(TextTableViewCell.self, forCellReuseIdentifier: "TextTableViewCell")
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func reloadTimeCellsIfNecessary() {
      guard let date = self.formData.selectedDate else { return }
      
      if self.availability.isInTimeSlots {
         let timeSlots = self.availability.timeSlots(
            forDate: date,
            selectedDuration: formData.selectedDuration
         )
         self.formData.selectedTimeSlot = timeSlots.contains(self.formData.selectedTimeSlot) ?
            self.formData.selectedTimeSlot : timeSlots.first
         
         let timeSlotPickerCell = self.cellForRow(
            at: .init(row: self.rowForTimePicker, section: self.sectionForDateAndTime)
         ) as? TimeSlotPickerTableViewCell
         timeSlotPickerCell?.reload(
            timeSlots: timeSlots,
            selectedTimeSlot: self.formData.selectedTimeSlot
         )
         
      } else {
         let hours = self.availability.hours(forDate: date)
         self.formData.selectedHour =
            hours.contains(self.formData.selectedHour) ? self.formData.selectedHour : hours.first
         
         let hourPickerCell = self.cellForRow(
            at: .init(row: self.rowForTimePicker, section: self.sectionForDateAndTime)
         ) as? HourPickerTableViewCell
         hourPickerCell?.reload(hours: hours, selectedHour: self.formData.selectedHour)
      }
   }
   
   func showWeekdaySelectionInvalidWarning(_ show: Bool = true) {
      DispatchQueue.main.async {
         let indexPath = IndexPath(
            row: self.rowForWeekdayPicker,
            section: self.sectionForDateAndTime
         )
         self.scrollToRow(at: indexPath, at: .top, animated: true)
         
         (self.cellForRow(at: indexPath) as? WeekdayPickerTableViewCell)?.showInvalidCountWarning(
            show,
            text: self.formData.selectedService.invalidWeekdaySelectionCountWarning
         )
      }
   }
}

// MARK: - Table View
extension DateFormView: UITableViewDelegate, UITableViewDataSource {
   private var sectionForDateAndTime: Int { 0 }
   private var sectionForOthers: Int { 1 }
   
   private var rowForWeekdayPicker: Int { 0 }
   private var rowForDatePicker: Int { 1 }
   private var rowForTimePicker: Int { 2 }
   
   func numberOfSections(in tableView: UITableView) -> Int { 2 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case self.sectionForDateAndTime: return 3
      case self.sectionForOthers: return 1
      default: return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      // MARK: - Date and Time
      case self.sectionForDateAndTime:
         switch indexPath.row {
         case self.rowForWeekdayPicker:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "WeekdayPickerTableViewCell", for: indexPath)
               as? WeekdayPickerTableViewCell else {
               fatalError("[ServiceOrderFormView] Failed to dequeue WeekdayPickerTableViewCell.")
            }
            
            let warningText = self.weekdaySelectionWarningStatus == .invalidSelectionCount ?
               self.formData.selectedService.invalidWeekdaySelectionCountWarning : nil
            
            cell.setValues(
               weekdays: self.formData.availability.weekdays,
               selectedWeekdays: self.formData.selectedWeekdays,
               maximumSelectionCount: self.formData.selectedService.frequency,
               instruction: self.formData.selectedService.weekdaySelectionInstruction,
               warningStatus: self.weekdaySelectionWarningStatus,
               warningText: warningText
            )
            cell.delegate = self
            return cell
            
         case self.rowForDatePicker:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "DatePickerTableViewCell", for: indexPath)
               as? DatePickerTableViewCell else {
               fatalError("[ServiceOrderFormView] Failed to dequeue DatePickerTableViewCell.")
            }
            
            cell.configure(
               withDates: self.availability.dates(
                  forSelectedWeekdays: self.formData.selectedWeekdays,
                  returningAllIfEmpty: true
               ),
               selectedDate: self.formData.selectedDate,
               instruction: self.formData.selectedService.dateSelectionInstruction
            )
            cell.delegate = self
            return cell
            
         case self.rowForTimePicker:
            if self.availability.isInTimeSlots {
               guard let cell = tableView
                  .dequeueReusableCell(withIdentifier: "TimeSlotPickerTableViewCell", for: indexPath)
                  as? TimeSlotPickerTableViewCell else
               {
                  fatalError("[ServiceOrderFormView] Failed to dequeue TimeSlotPickerTableViewCell.")
               }
               
               let timeSlots = availability.timeSlots(
                  forDate: formData.selectedDate,
                  selectedDuration: formData.selectedDuration
               )
               
               cell.setValues(
                  timeSlots: timeSlots,
                  selectedTimeSlot: self.formData.selectedTimeSlot,
                  instruction: self.formData.primaryService.timeSelectionInstruction
               )
               cell.delegate = self
               return cell
               
            } else {
               guard let cell = tableView
                  .dequeueReusableCell(withIdentifier: "HourPickerTableViewCell", for: indexPath)
                  as? HourPickerTableViewCell else
               {
                  fatalError("[ServiceOrderFormView] Failed to dequeue HourPickerTableViewCell.")
               }
               
               cell.configure(
                  withHours: self.availability.hours(forDate: self.formData.selectedDate),
                  selectedHour: self.formData.selectedHour,
                  instruction: self.formData.primaryService.timeSelectionInstruction
               )
               cell.delegate = self
               return cell
            }
            
         default: fatalError("[DateFormView] Unrecognized indexPath.")
         }
         
      // MARK: - Additional Notes
      case self.sectionForOthers:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "TextTableViewCell", for: indexPath)
            as? TextTableViewCell else
         {
            fatalError("[ServiceOrderFormView] Failed to dequeue TextTableViewCell.")
         }
         cell.configure(
            withText: self.formData.notes,
            instruction: self.formData.primaryService.noteInstruction
         )
         cell.delegate = self
         return cell
         
      default: fatalError("[DateFormView] Unrecognized indexPath.")
      }
   }
}

// MARK: - WeekdayPickerTableViewCellDelegate
extension DateFormView: WeekdayPickerTableViewCellDelegate {
   func pickerView(didSelectWeekdays weekdays: [Int]) {
      self.formData.selectedWeekdays = weekdays
      
      let dates = self.availability.dates(forSelectedWeekdays: weekdays, returningAllIfEmpty: true)
      self.formData.selectedDate = dates.contains(self.formData.selectedDate) ?
         self.formData.selectedDate : dates.first
      
      let datePickerCell = self.cellForRow(
         at: .init(row: self.rowForDatePicker, section: self.sectionForDateAndTime)
      ) as? DatePickerTableViewCell
      datePickerCell?.reload(dates: dates, selectedDate: self.formData.selectedDate)
      
      self.reloadTimeCellsIfNecessary()
      
      if self.weekdaySelectionWarningStatus == .invalidSelectionCount &&
         self.formData.weekdaySelectionCountIsValid
      {
         self.showWeekdaySelectionInvalidWarning(false)
      }
   }
   
   func pickerView(warningStatusDidChange warningStatus: WeekdayPickerTableViewCell.WarningStatus) {
      self.weekdaySelectionWarningStatus = warningStatus
   }
}

// MARK: - DatePickerTableViewCellDelegate
extension DateFormView: DatePickerTableViewCellDelegate {
   func datePicker(didPickDate date: Date) {
      self.formData.selectedDate = date
      self.reloadTimeCellsIfNecessary()
   }
}

// MARK: - HourPickerTableViewCellDelegate
extension DateFormView: HourPickerTableViewCellDelegate {
   func pickerView(didSelectHour hour: Int) {
      self.formData.selectedHour = hour
   }
}

// MARK: TimeSlotPickerViewDelegate
extension DateFormView: TimeSlotPickerViewDelegate {
   func timeSlotPicker(didSelectTimeSlot timeSlot: TimeSlot) {
      self.formData.selectedTimeSlot = timeSlot
   }
}

extension DateFormView: TextTableViewCellDelegate {
   func textTableViewCell(didChangeText text: String) {
      self.formData.notes = text
   }
}

// MARK: - Protocol
protocol DateFormViewDelegate: AnyObject {
   
}
