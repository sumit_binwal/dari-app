//
//  RegisterFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class RegisterFormView: UIView {
   // MARK: - UI
   private lazy var topStackView: UIStackView = {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .equalSpacing
      view.spacing = UIScreen.main.bounds.height < 600 ? 10 : 20
      view.addArrangedSubviews(self.fieldStackViews)
      return view
   }()
   
   // MARK: Text Fields
   internal var genericTextField: UnderLineTextField {
      let textField = UnderLineTextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
      textField.returnKeyType = .next
      textField.delegate = self
      return textField
   }
   internal lazy var firstNameTextField: UnderLineTextField = {
      let textField = self.genericTextField
      textField.tag = 1
      textField.textContentType = .givenName
      textField.autocapitalizationType = .words
      textField.placeholder = currentUser?.firstName ?? "firstName".localized
      return textField
   }()
   internal lazy var lastNameTextField: UnderLineTextField = {
      let textField = self.genericTextField
      textField.tag = 2
      textField.textContentType = .familyName
      textField.autocapitalizationType = .words
      textField.placeholder = currentUser?.lastName ?? "lastName".localized
      return textField
   }()
   internal lazy var emailTextField: UnderLineTextField = {
      let textField = self.genericTextField
      textField.textAlignment = .left
      textField.tag = 3
      textField.textContentType = .emailAddress
      textField.keyboardType = .emailAddress
      textField.autocapitalizationType = .none
      textField.placeholder = currentUser?.email ?? "email".localized
      return textField
   }()
   internal lazy var otpTextField: UnderLineTextField = {
      let textField = self.genericTextField
      textField.tag = 4
      textField.textContentType = .oneTimeCode
      textField.keyboardType = .numberPad
      textField.autocapitalizationType = .none
      textField.placeholder = "otp".localized
      return textField
   }()
   internal lazy var passwordTextField: UnderLineTextField = {
      let textField = self.genericTextField
      textField.tag = 5
      textField.textContentType = self.passwordInfo == nil ? .newPassword : .password
      textField.autocapitalizationType = .none
      textField.isSecureTextEntry = true
      textField.placeholder = (currentUser == nil ? "password" : "newPassword").localized
      return textField
   }()
   internal lazy var phoneNumberTextField: PhoneTextField = {
      let textField = PhoneTextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
      textField.returnKeyType = .next
      textField.delegate = self
      textField.textAlignment = .left
      textField.setCountry(ISOCountry.default)
      textField.tag = 6
      textField.textContentType = .telephoneNumber
      textField.keyboardType = .phonePad
      
      if let phoneNumber = self.currentUser?.phoneNumber {
         textField.setPhoneNumberPlaceholder(phoneNumber)
      } else {
         textField.placeholder = "mobileNumber".localized
      }
      
      return textField
   }()
   internal lazy var addressTextField: UnderLineTextField = {
      let textField = UnderLineTextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.delegate = self
      textField.tag = 7
      textField.rightView = {
         let imageView = UIImageView(image: UIImage(named: "Small Trailing Chevron"))
         imageView.tintColor = .darkCalypso
         return imageView
      }()
      textField.rightViewMode = .always
      textField.placeholder = "address".localized
      return textField
   }()
   
   // MARK: Field Stack Views
   private var genericFieldStackView: UIStackView {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fill
      view.spacing = 5
      return view
   }
   private var genericErrorStackView: UIStackView {
      let view = self.genericFieldStackView
      view.spacing = 0
      return view
   }
   internal lazy var nameStackView: UIStackView = {
      let horizontalStackView = self.genericFieldStackView
      horizontalStackView.axis = .horizontal
      horizontalStackView.distribution = .fillEqually
      horizontalStackView.addArrangedSubviews([self.firstNameTextField, self.lastNameTextField])
      
      let view = self.genericFieldStackView
      view.addArrangedSubview(horizontalStackView)
      return view
   }()
   internal lazy var emailStackView: UIStackView = {
      let view = self.genericFieldStackView
      view.addArrangedSubview(self.emailTextField)
      return view
   }()
   internal lazy var otpStackView: UIStackView = {
      let view = self.genericFieldStackView
      view.addArrangedSubview(self.otpTextField)
      return view
   }()
   internal lazy var passwordStackView: UIStackView = {
      let view = self.genericFieldStackView
      view.addArrangedSubview(self.passwordTextField)
      if self.passwordInfo != nil {
         view.addArrangedSubview(self.passwordInfoLabel)
      } else {
         view.addArrangedSubview(self.forgotPasswordView)
      }
      
      return view
   }()
   internal lazy var phoneNumberStackView: UIStackView = {
      let view = self.genericFieldStackView
      view.addArrangedSubview(self.phoneNumberTextField)
      if self.phoneNumberInfo != nil {
         view.addArrangedSubview(self.phoneNumberInfoLabel)
      }
      if self.showForgotPasswordButton {
         view.addArrangedSubview(self.forgotPasswordView)
      }
      return view
   }()
   internal lazy var addressStackView: UIStackView = {
      let view = self.genericFieldStackView
      view.addArrangedSubview(self.addressTextField)
      return view
   }()
   
   // MARK: Labels
   private var genericInfoLabel: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = label.font.withSize(14)
      label.textAlignment = .center
      label.numberOfLines = 3
      label.adjustsFontSizeToFitWidth = true
      label.minimumScaleFactor = 0.8
      label.textColor = .subtleText
      label.tag = 50
      return label
   }
   private var genericErrorLabel: UILabel {
      let label = self.genericInfoLabel
      label.textColor = .red
      label.textAlignment = .natural
      label.font = .regularFont(ofSize: 16)
      label.tag = 0
      return label
   }
   private lazy var passwordInfoLabel: UILabel = {
      let label = self.genericInfoLabel
      label.text = self.passwordInfo
      return label
   }()
   private lazy var phoneNumberInfoLabel: UILabel = {
      let label = self.genericInfoLabel
      label.text = self.phoneNumberInfo
      return label
   }()
   
   // MARK: Other
   lazy var submitButton: ProminentButton = {
      let button = ProminentButton()
      button.setTitle(self.submitButtonTitle, for: .normal)
      button.addTarget(self, action: #selector(self.submitButtonWasTapped), for: .touchUpInside)
      button.isEnabled = false
      return button
   }()
   private lazy var termsView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .clear
      view.addSubviews([self.termsLabel, self.termsButton])
      
      NSLayoutConstraint.activate([
         self.termsLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.termsLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.termsLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         
         self.termsButton.topAnchor.constraint(equalTo: self.termsLabel.bottomAnchor),
         self.termsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         self.termsButton.widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor),
         self.termsButton.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var termsLabel: UILabel = {
      let label = self.genericInfoLabel
      label.font = label.font.withSize(12)
      label.text = "registrationTnC1".localized
      return label
   }()
   private lazy var termsButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.titleLabel?.font = .boldFont(ofSize: 12)
      let title: String = "registrationTnC2".localized
      button.setTitle(title, for: .normal)
      button.addTarget(self, action: #selector(self.termsButtonWasTapped), for: .touchUpInside)
      
      let height = NSAttributedString(
         string: title,
         attributes: [.font: UIFont.boldFont(ofSize: 12)]
      ).size().height
      button.heightAnchor.constraint(equalToConstant: height).isActive = true
      
      return button
   }()
   lazy var forgotPasswordView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      
      let button = self.forgotPasswordButton
      view.addSubview(button)
      
      NSLayoutConstraint.activate([
         button.topAnchor.constraint(equalTo: view.topAnchor),
         button.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         button.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   lazy var forgotPasswordButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.titleLabel?.font = .regularFont(ofSize: 13)
      button.setTitle("forgotPassword".localized, for: .normal)
      button.addTarget(
         self,
         action: #selector(self.forgotPasswordButtonWasTapped),
         for: .touchUpInside
      )
      return button
   }()
   
   // MARK: Overridable
   internal var fieldStackViews: [UIStackView] {
      return [
         self.nameStackView,
         self.emailStackView,
         self.passwordStackView,
         self.phoneNumberStackView,
         self.addressStackView
      ]
   }
   internal var requiredFields: [UITextField] {
      return [
         self.firstNameTextField,
         self.lastNameTextField,
         self.emailTextField,
         self.passwordTextField,
         self.phoneNumberTextField,
         self.addressTextField
      ]
   }
   internal var termsViewIncluded: Bool { return true }
   internal var submitButtonTitle: String { return "signUp".localized }
   internal var passwordInfo: String? { return "passwordMinimumCharacters".localized }
   internal var showForgotPasswordButton: Bool { false }
   internal var phoneNumberInfo: String? { return "mobileNumberUsage".localized }
   internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages)
      -> (stacks: [UIStackView], messages: [[String]])
   {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let nameErrors = errorMessages.name {
         stacksToUpdate.append(self.nameStackView)
         messages.append(nameErrors)
      }
      if let emailErrors = errorMessages.email {
         stacksToUpdate.append(self.emailStackView)
         messages.append(emailErrors)
      }
      if let passwordErrors = errorMessages.password {
         stacksToUpdate.append(self.passwordStackView)
         messages.append(passwordErrors)
      }
      if let phoneNumberErrors = errorMessages.phoneNumber {
         stacksToUpdate.append(self.phoneNumberStackView)
         messages.append(phoneNumberErrors)
      }
      if let otherErrors = errorMessages.addressAndOthers {
         stacksToUpdate.append(self.addressStackView)
         messages.append(otherErrors)
      }
      return (stacks: stacksToUpdate, messages: messages)
   }
   
   weak var delegate: UserFormViewDelegate?
   
   // MARK: - Initializers and Setup
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.addViews()
      self.setFieldToolbars()
      self.setAppearance()
   }
   
   internal func addViews() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.addSubviews([self.topStackView, self.submitButton])
      
      let padding: CGFloat = 10
      let stackView = self.topStackView
      NSLayoutConstraint.activate([
         // Top stack view
         stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: padding),
         stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding),
         stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -padding),
         
         // Register button
         self.submitButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20),
         self.submitButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
         self.submitButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7),
      ])
      
      if self.termsViewIncluded {
         self.addSubview(self.termsView)
         NSLayoutConstraint.activate([
            self.termsView.topAnchor.constraint(
               equalTo: self.submitButton.bottomAnchor,
               constant: 15),
            self.termsView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            self.termsView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            self.termsView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15)
         ])
      } else {
         self.submitButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15)
            .isActive = true
      }
   }
   
   private func setFieldToolbars() {
      guard self.requiredFields.count > 1 else { return }
      self.requiredFields.forEach {
         $0.inputAccessoryView = self.toolbar(for: $0)
      }
   }
   
   private func setAppearance() {
      self.backgroundColor = .lighterBackground
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
   }
   
   private func textField(following textField: UITextField) -> UITextField? {
      guard let index = self.requiredFields.firstIndex(of: textField) else { return nil }
      return index < self.requiredFields.count - 1 ? self.requiredFields[index + 1] : nil
   }
   
   private func textField(preceding textField: UITextField) -> UITextField? {
      guard let index = self.requiredFields.firstIndex(of: textField) else { return nil }
      return index > 0 ? self.requiredFields[index - 1] : nil
   }
   
   private func toolbar(for textField: UITextField) -> UIToolbar {
      let toolbar: UIToolbar = {
         let toolbar = UIToolbar(frame: .init(0, 0, UIScreen.main.bounds.width, 35))
         toolbar.tintColor = .tint
         toolbar.translatesAutoresizingMaskIntoConstraints = false
         return toolbar
      }()
      
      let previousTextField = self.textField(preceding: textField)
      let nextTextField = self.textField(following: textField)
      
      let previousButton = UIBarButtonItem(
         image: UIImage(named: "Upward Chevron"),
         style: .plain,
         target: previousTextField,
         action: #selector(UITextField.becomeFirstResponder))
      previousButton.isEnabled = previousTextField != nil
      
      let nextButton = UIBarButtonItem(
         image: UIImage(named: "Downward Chevron"),
         style: .plain,
         target: nextTextField,
         action: #selector(UITextField.becomeFirstResponder))
      nextButton.isEnabled = nextTextField != nil
      
      let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
      
      toolbar.setItems([space, previousButton, nextButton], animated: false)
      
      return toolbar
   }
   
   // MARK: - Actions
   @objc
   private func textFieldDidChange() {
      self.refreshRegisterButton()
   }
   
   /// Call only after an unauthenticated error.
   func resubmit() {
      self.submitButtonWasTapped()
   }
   
   @objc
   internal func submitButtonWasTapped() {
      guard let user = self.user else {
         assertionFailure("[RegisterFormView] Register button enabled with invalid data.")
         return
      }
      self.firstResponder?.resignFirstResponder()
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitWithUser: user, password: self.password!)
   }
   
   @objc
   private func termsButtonWasTapped() {
      guard let url = Environment.termsAndConditionsURL else { return }
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
   }
   
   @objc
   internal func forgotPasswordButtonWasTapped() {}
   
   // MARK: - Form Data
   var country = ISOCountry.default
   internal var firstName: String? {
      get { return self.firstNameTextField.text?.trimmed }
      set { self.firstNameTextField.text = newValue }
   }
   internal var lastName: String? {
      get { return self.lastNameTextField.text?.trimmed }
      set { self.lastNameTextField.text = newValue }
   }
   internal var email: String? {
      get { return self.emailTextField.text?.trimmed }
      set { self.emailTextField.text = newValue }
   }
   internal var otp: String? {
      get { self.otpTextField.text }
      set { self.otpTextField.text = newValue }
   }
   internal var password: String? {
      get { return self.passwordTextField.text }
      set { self.passwordTextField.text = newValue }
   }
   internal var phoneNumber: String? {
      get { return self.phoneNumberTextField.rawPhoneNumber }
      set { self.phoneNumberTextField.text = newValue }
   }
   var address: String? {
      get { return self.addressTextField.text?.trimmed }
      set { self.addressTextField.text = newValue }
   }
   var latitude: Double?
   var longitude: Double?
   
   // MARK: - Validation
   internal func refreshRegisterButton() {
      self.submitButton.isEnabled = self.isValid
   }
   
   internal var firstNameIsValid: Bool { return !(self.firstName?.isEmpty ?? true) }
   internal var lastNameIsValid: Bool { return !(self.lastName?.isEmpty ?? true) }
   internal var emailIsValid: Bool { return !(self.email?.isEmpty ?? true) }
   internal var otpIsValid: Bool { !(self.otp?.isEmpty ?? true) }
   internal var passwordIsValid: Bool { return !(self.password?.isEmpty ?? true) }
   internal var phoneNumberIsValid: Bool {
      return !(self.phoneNumberTextField.text?.isEmpty ?? true) &&
         !(self.phoneNumber?.isEmpty ?? true)
   }
   internal var addressIsValid: Bool {
      return !(self.address?.isEmpty ?? true) && latitude != nil && longitude != nil
   }
   internal var isValid: Bool {
      return self.firstNameIsValid
         && self.lastNameIsValid
         && self.emailIsValid
         && self.passwordIsValid
         && self.phoneNumberIsValid
         && self.addressIsValid
   }
   internal lazy var currentUser = User.current
   private var user: User? {
      guard self.isValid else { return nil }
      guard let district = District.current else {
         assertionFailure("[RegisterFormView] No current district.")
         return nil
      }
      
      return User(
         id: 0,
         email: self.email!,
         firstName: self.firstName!,
         lastName: self.lastName!,
         phoneNumber: self.phoneNumber!,
         externalID: nil,
         profile: User.Profile(
            districtID: district.id,
            address: self.address!,
            latitude: self.latitude!,
            longitude: self.longitude!)
      )
   }
   
   // MARK: - Feedback
   func displayErrorMessages(_ errorMessages: User.ErrorMessages) {
      let (stacksToUpdate, messages) = self.getStacksToUpdateAndMessages(
         fromErrorMessages: errorMessages)
      
      for (index, stackView) in stacksToUpdate.enumerated() {
         let infoLabel = stackView.viewWithTag(50)
         DispatchQueue.main.async {
            let errorStackView = self.generateStackView(forErrorMessages: messages[index])
            errorStackView.isHidden = true
            
            stackView.addArrangedSubview(errorStackView)
            
            UIView.animate(
               withDuration: 0.3,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  infoLabel?.isHidden = true
                  errorStackView.isHidden = false
            })
         }
      }
   }
   
   func removeErrorMessages() {
      self.fieldStackViews.forEach {
         let errorStack = $0.viewWithTag(100)
         let infoLabel = $0.viewWithTag(50)
         
         DispatchQueue.main.async {
            UIView.animate(
               withDuration: 0.3,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  errorStack?.isHidden = true
                  infoLabel?.isHidden = false
                  
            }) { (completed) in
               if completed {
                  errorStack?.removeFromSuperview()
               }
            }
         }
      }
   }
   
   private func generateStackView(forErrorMessages messages: [String]) -> UIStackView {
      let stackView = self.genericFieldStackView
      stackView.spacing = 0
      stackView.tag = 100
      
      stackView.addArrangedSubviews(messages.map {
         let label = self.genericErrorLabel
         label.text = $0
         return label
      })
      
      return stackView
   }
   
   // MARK: - Other
   
   func setAddress(_ address: String?, latitude: Double?, longitude: Double?) {
      self.address = address
      self.latitude = latitude
      self.longitude = longitude
      self.refreshRegisterButton()
   }
}

// MARK: - UITextFieldDelegate
extension RegisterFormView: UITextFieldDelegate {
   func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      guard textField === self.addressTextField else { return true }
      self.delegate?.userFormDidRequestAddress(
         currentAddress: self.address,
         latitude: self.latitude,
         longitude: self.longitude
      )
      return false
   }
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      if let nextTextField = self.textField(following: textField) {
         nextTextField.becomeFirstResponder()
      } else {
         self.submitButtonWasTapped()
      }
      return false
   }
   
   func textField(
      _ textField: UITextField,
      shouldChangeCharactersIn range: NSRange,
      replacementString string: String)
      -> Bool
   {
      guard let phoneTextField = textField as? PhoneTextField else { return true }
      defer { self.textFieldDidChange() }
      return phoneTextField.textField(
         textField,
         shouldChangeCharactersIn: range,
         replacementString: string
      )
   }
}

// MARK: - PhoneTextFieldDelegate
extension RegisterFormView: PhoneTextFieldDelegate {
   func countryCodeWasTapped() {
      self.delegate?.userFormDidRequestPhoneNumberCountry(currentCountry: self.country)
   }
}

// MARK: - Country Selection Delegate
extension RegisterFormView: CountrySelectorDelegate {
   func didSelectCountry(_ country: ISOCountry) {
      self.country = country
      self.phoneNumberTextField.setCountry(country)
      self.refreshRegisterButton()
   }
}

// MARK: - Map View Delegate

extension RegisterFormView: AddressViewControllerDelegate {
   func addressViewController(didSetAddress address: String, latitude: Double, longitude: Double) {
      self.address = address
      self.latitude = latitude
      self.longitude = longitude
      self.refreshRegisterButton()
   }
}

// MARK: - Protocol
protocol UserFormViewDelegate: AnyObject {
   func userFormDidRequestAddress(currentAddress: String?, latitude: Double?, longitude: Double?)
   func userFormDidRequestPhoneNumberCountry(currentCountry country: ISOCountry?)
   func userFormDidTapForgotPassword(email: String?)
   func userForm(didSubmitWithUser user: User, password: String)
   func userForm(didSubmitWithEmail email: String, password: String)
   func userForm(didSubmitWithKeyValuePairs pairs: [String: Codable])
   func userForm(didSubmitPasswordReset password: String, confirmPassword: String)
   func userForm(didRequestOTPWithEmail email: String)
   func userForm(didSubmitOTP otp: String, email: String, password: String, confirmPassword: String)
}
