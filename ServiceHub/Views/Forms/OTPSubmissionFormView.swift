//
//  OTPSubmissionFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/9/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OTPSubmissionFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] {
      [self.emailStackView, self.otpStackView, self.passwordStackView]
   }
   override internal var requiredFields: [UITextField] {
      [self.emailTextField, self.otpTextField, self.passwordTextField]
   }
   
   override internal var termsViewIncluded: Bool { false }
   override internal var submitButtonTitle: String { "submit".localized }
   
   override internal var isValid: Bool {
      self.emailIsValid && self.otpIsValid && self.passwordIsValid
   }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      
      self.removeErrorMessages()
      self.delegate?.userForm(
         didSubmitOTP: self.otp ?? "",
         email: self.email ?? "",
         password: self.password ?? "",
         confirmPassword: self.password ?? ""
      )
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages
   ) -> (stacks: [UIStackView], messages: [[String]]) {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let emailErrors = errorMessages.email {
         stacksToUpdate.append(self.emailStackView)
         messages.append(emailErrors)
      }
      
      if let otpErrors = errorMessages.otp {
         stacksToUpdate.append(self.otpStackView)
         messages.append(otpErrors)
      }
      
      var otherErrors: [String] = errorMessages.newPassword ?? []
      otherErrors.append(contentsOf: errorMessages.nonFieldErrors ?? [])
      if !otherErrors.isEmpty {
         stacksToUpdate.append(self.passwordStackView)
         messages.append(otherErrors)
      }
      
      return (stacksToUpdate, messages)
   }
}
