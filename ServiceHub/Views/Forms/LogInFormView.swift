//
//  LogInFormView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class LogInFormView: RegisterFormView {
   override internal var fieldStackViews: [UIStackView] {
      [self.emailStackView, self.passwordStackView]
   }
   override internal var requiredFields: [UITextField] {
      [self.emailTextField, self.passwordTextField]
   }
   override internal var submitButtonTitle: String { "signIn".localized }
   override internal var termsViewIncluded: Bool { false }
   override internal var passwordInfo: String? { nil }
   override internal var showForgotPasswordButton: Bool { true }
   
   override internal var isValid: Bool { self.emailIsValid && self.passwordIsValid }
   
   @objc
   override internal func submitButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      self.removeErrorMessages()
      self.delegate?.userForm(didSubmitWithEmail: self.email!, password: self.password!)
   }
   
   @objc
   override internal func forgotPasswordButtonWasTapped() {
      self.firstResponder?.resignFirstResponder()
      self.removeErrorMessages()
      self.delegate?.userFormDidTapForgotPassword(email: self.email)
   }
   
   override internal func getStacksToUpdateAndMessages(
      fromErrorMessages errorMessages: User.ErrorMessages
   ) -> (stacks: [UIStackView], messages: [[String]]) {
      var stacksToUpdate: [UIStackView] = []
      var messages: [[String]] = []
      
      if let emailErrors = errorMessages.email {
         stacksToUpdate.append(self.emailStackView)
         messages.append(emailErrors)
      }
      
      let otherErrorsOrNil: [String]? = {
         let messages: [String] = [
            errorMessages.password,
            errorMessages.firstName,
            errorMessages.lastName,
            errorMessages.phoneNumber,
            errorMessages.profileErrorMessages?.all,
            errorMessages.nonFieldErrors]
            .compactMap { $0 }
            .flatMap { $0 }
         return messages.isEmpty ? nil : messages
      }()
      
      if let otherErrors = otherErrorsOrNil {
         stacksToUpdate.append(self.passwordStackView)
         messages.append(otherErrors)
      }
      
      return (stacksToUpdate, messages)
   }
}
