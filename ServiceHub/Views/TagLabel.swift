//
//  TagLabel.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class TagLabel: UILabel {
   override var intrinsicContentSize: CGSize {
      let size = super.intrinsicContentSize
      return CGSize(size.width + 6, size.height + 4)
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setUp()
   }
   
   private func setUp() {
      self.textAlignment = .center
      self.translatesAutoresizingMaskIntoConstraints = false
      self.font = .boldFont(ofSize: 12)
      self.textColor = .white
      self.layer.cornerRadius = 4
      self.layer.masksToBounds = true
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.backgroundColor = self.tintColor
   }
}
