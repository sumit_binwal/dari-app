//
//  OrderItemParametersView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/20/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderItemParametersView: UITableViewCell {
   // MARK: - Properties
   weak var delegate: ParameterSetterDelegate?
   var itemConstraints: Item.Constraints!
   var itemParameters: [Item.Parameter] = []
   var item: Item { self.itemConstraints.item }
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   lazy var numberPickerView: NumberPickerView = {
      let view = NumberPickerView(numbers: [])
      view.delegate = self
      return view
   }()
   /// Prevents a bug that's caused by having an empty stack view.
   private lazy var fillerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
      return view
   }()
   
   lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.fillerView])
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 5
      
      return view
   }()
   private lazy var borderLayer: CALayer = {
      let layer = CALayer()
      layer.backgroundColor = UIColor.subtleBorder.cgColor
      return layer
   }()
   
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.numberPickerView, self.stackView])
      let margins = self.contentView.layoutMarginsGuide
      
      let stackBottomConstraint = self.stackView.bottomAnchor.constraint(
         equalTo: self.contentView.bottomAnchor)
      stackBottomConstraint.priority = .init(249)
      
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.numberPickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.numberPickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.numberPickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         
         self.stackView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.numberPickerView.bottomAnchor, multiplier: 1),
         self.stackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         stackBottomConstraint
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func configure(withItemConstraints constraints: Item.Constraints, parameters: [Item.Parameter]) {
      self.itemConstraints = constraints
      self.itemParameters = parameters
      
      self.instructionLabel.text = constraints.instruction
      self.numberPickerView.numbers = constraints.quantities
      self.numberPickerView.select(number: parameters.count)
      
      self.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
      self.stackView.addArrangedSubview(self.fillerView)
      
      if constraints.item.hasSpecifiableParameters {
         self.stackView.addArrangedSubviews(self.itemParameters.enumerated().map {
            let picker = ItemOptionsPicker(index: $0, item: self.item, itemOrderParameters: $1)
            picker.delegate = self
            return picker
         })
      }
   }
}

// MARK: - NumberPickerDelegate
extension OrderItemParametersView: NumberPickerDelegate {
   func numberPicker(didSelectNumber number: Int) {
      let currentCount = self.itemParameters.count
      guard number != currentCount else { return }
      
      defer {
         self.delegate?.parameterSetter(
            forItem: self.item,
            didChangeParameters: self.itemParameters
         )
      }
      
      if number < currentCount {
         self.itemParameters = self.itemParameters.dropLast(currentCount - number)
         guard self.item.hasSpecifiableParameters else { return }
         let views = Array(self.stackView.arrangedSubviews[number + 1..<currentCount + 1])
         
         DispatchQueue.main.async {
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  views.forEach { $0.isHidden = true }
            }) { _ in
               views.forEach { $0.removeFromSuperview() }
            }
            (self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
         }
         
      } else {
         self.itemParameters.append(contentsOf: [Item.Parameter](
            repeating: self.itemParameters.last ?? self.itemConstraints.defaultParameter,
            count: number - currentCount
         ))
         guard self.item.hasSpecifiableParameters else { return }
         
         DispatchQueue.main.async {
            let views: [ItemOptionsPicker] = (currentCount..<number).map {
               let view = ItemOptionsPicker(
                  index: $0,
                  item: self.item,
                  itemOrderParameters: self.itemParameters[$0]
               )
               view.delegate = self
               view.isHidden = true
               return view
            }
            
            self.stackView.addArrangedSubviews(views)
            
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  views.forEach { $0.isHidden = false }
            })
            (self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
         }
      }
   }
}

// MARK: - ItemOptionsPickerDelegate
extension OrderItemParametersView: ItemOptionsPickerDelegate {
   func itemOptionsPicker(
      atIndex index: Int,
      didChangeItemParameters parameters: Item.Parameter
   ) {
      self.itemParameters[index] = parameters
      self.delegate?.parameterSetter(forItem: self.item, didChangeParameters: self.itemParameters)
   }
}

// MARK: - Protocol
protocol ParameterSetterDelegate: AnyObject {
   func parameterSetter(forItem item: Item, didChangeParameters parameterSet: [Item.Parameter])
}
