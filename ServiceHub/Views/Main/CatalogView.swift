//
//  CatalogView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CatalogView: UITableView {
   // MARK: - Properties
   
   weak var catalogDelegate: CatalogViewDelegate?
   var error: Error?
   var announcements: [Announcement] = []
   var categorizedListings: [(category: Category, listings: [Listing])] = []
   /// Caches collection view horizontal offsets.
   private var xOffsets: [Int: CGFloat] = [:]
   private var originalGreetingViewY: CGFloat?
   var isRefreshing: Bool { self.refreshControl?.isRefreshing ?? false }
   
   private var greetingView: GreetingView? {
      headerView(forSection: 2) as? GreetingView
   }
   
   // MARK: - Initialization and Setup
   override init(frame: CGRect, style: UITableView.Style) {
      super.init(frame: frame, style: style)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.separatorStyle = .none
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 250
      self.separatorInset = .zero
      self.tableFooterView = UIView()
      self.refreshControl = {
         let control = UIRefreshControl()
         control.translatesAutoresizingMaskIntoConstraints = false
         control.tintColor = .calypso
         control.addTarget(
            self,
            action: #selector(self.refreshControlWasPulled),
            for: .valueChanged
         )
         return control
      }()
      
      self.delegate = self
      self.dataSource = self
      
      self.register(CatalogTableViewCell.self, forCellReuseIdentifier: "CatalogTableViewCell")
      self.register(AnnouncementTableViewCell.self, forCellReuseIdentifier: "AnnouncementTableViewCell")
   }
   
   // MARK: - Actions
   @objc
   private func refreshControlWasPulled() {
      self.catalogDelegate?.catalogViewRefreshControlWasPulled()
   }
   
   // MARK: - Other
   public func startRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      self.isUserInteractionEnabled = false
      DispatchQueue.main.async {
         self.refreshControl?.startRefreshing(animateScrollViewContentOffset: animate)
      }
   }
   
   public func endRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      self.isUserInteractionEnabled = true
      DispatchQueue.main.async {
         self.refreshControl?.stopRefreshing(animateScrollViewContentOffset: animate)
      }
   }
   
   public func displayError(_ error: Error, animated: Bool = true) {
      DispatchQueue.main.async {
         if error.localizedDescription == self.error?.localizedDescription {
            guard animated else { return }
            
            (self.cellForRow(at: .zero) as? AnnouncementTableViewCell)?.jiggle()
            return
         }
         
         self.originalGreetingViewY = nil
         
         if animated {
            self.beginUpdates()
            
            let oldSections = IndexSet(integersIn: 0..<self.numberOfSections)
            self.announcements.removeAll()
            self.categorizedListings.removeAll()
            self.deleteSections(oldSections)
            
            self.error = error
            self.insertSections([0, 1, 2])
            
            self.endUpdates()
            
         } else {
            self.error = error
            self.announcements.removeAll()
            self.categorizedListings.removeAll()
            self.reloadData()
         }
         
      }
   }
   
   public func setCatalog(
      _ catalog: Catalog,
      animated: Bool = true,
      completion: (() -> Void)? = nil
   ) {
      self.xOffsets.removeAll()
      
      DispatchQueue.main.async {
         self.originalGreetingViewY = nil
         
         if animated {
            self.performBatchUpdates {
               let oldSections = IndexSet(integersIn: 0..<self.numberOfSections)
               self.error = nil
               self.announcements.removeAll()
               self.categorizedListings.removeAll()
               self.deleteSections(oldSections)
               
               self.announcements = catalog.announcements
               self.categorizedListings = catalog.categorizedListings
               self.insertSections([0, 1, 2])
            } completion: { _ in
               completion?()
            }
            
         } else {
            self.error = nil
            self.announcements = catalog.announcements
            self.categorizedListings = catalog.categorizedListings
            self.reloadData()
            completion?()
         }
      }
   }
   
   public func reloadGreetingView() {
      greetingView?.reloadVisualEffect()
   }
}

// MARK: Delegate and Datasource
extension CatalogView: UITableViewDelegate, UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int { 3 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case 0: return self.error == nil ? 0 : 1
      case 1: return self.announcements.count
      case 2: return self.categorizedListings.count
      default: return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
         guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "AnnouncementTableViewCell",
            for: indexPath)
            as? AnnouncementTableViewCell else
         {
            fatalError("[CatalogView] Failed to dequeue AnnouncementTableViewCell.")
         }
         guard let error = self.error else {
            assertionFailure("[CatalogView] Error is nil.")
            return cell
         }
         
         cell.configure(for: error)
         return cell
         
      case 1:
         guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "AnnouncementTableViewCell",
            for: indexPath)
            as? AnnouncementTableViewCell else
         {
            fatalError("[CatalogView] Failed to dequeue AnnouncementTableViewCell.")
         }
         
         cell.configure(for: announcements[indexPath.row])
         return cell
      
      case 2:
         guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "CatalogTableViewCell",
            for: indexPath)
            as? CatalogTableViewCell else
         {
            fatalError("[CatalogView] Failed to dequeue CatalogTableViewCell.")
         }
         
         if self.xOffsets[cell.collectionView.tag] != nil {
            self.xOffsets[cell.collectionView.tag] = cell.collectionView.contentOffset.x
         }
         
         cell.collectionView.tag = indexPath.row
         
         let (category, listings) = self.categorizedListings[indexPath.row]
         cell.setCategory(category.name)
         cell.listings = listings
         cell.delegate = self
         cell.collectionView.reloadData()
         
         cell.collectionView.contentOffset.x = self.xOffsets[indexPath.row] ?? 0
         self.xOffsets[indexPath.row] = cell.collectionView.contentOffset.x
         
         return cell
         
      default:
         fatalError("[CatalogView] Invalid index path.")
      }
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      guard section == 2 && !self.categorizedListings.isEmpty else { return nil }
      return GreetingView(userName: User.current?.firstName ?? "")
   }
   
   func tableView(
      _ tableView: UITableView,
      willDisplayHeaderView view: UIView,
      forSection section: Int
   ) {
      // There's a background view with a dark gray color
      (view as? UITableViewHeaderFooterView)?.backgroundView?.alpha = 0
   }
   
   
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      guard section == 2 && !self.categorizedListings.isEmpty else { return 0 }
      return Self.automaticDimension
   }
   
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
      guard let greetingView = self.greetingView else { return }
      if self.originalGreetingViewY == nil {
         self.originalGreetingViewY = greetingView.frame.origin.y
      }
      guard let originalGreetingViewY = self.originalGreetingViewY else {
         assertionFailure()
         return
      }
      DispatchQueue.main.async {
         let fractionComplete: CGFloat = {
            let actualGreetingViewY = greetingView.frame.origin.y
            let greetingViewOffset = actualGreetingViewY - originalGreetingViewY
            let greetingViewHeight = greetingView.frame.height
            
            return min(max(0, greetingViewOffset / greetingViewHeight), 1)
         }()
         greetingView.setAnimatorProgress(fractionComplete)
      }
   }
}

// MARK: - CatalogTableViewCellDelegate
extension CatalogView: CatalogTableViewCellDelegate {
   func catalogTableViewCell(didSelectListing listing: Listing) {
      self.catalogDelegate?.catalogView(didSelectListing: listing)
   }
}

// MARK: - Protocol
protocol CatalogViewDelegate: AnyObject {
   func catalogView(didSelectListing listing: Listing)
   func catalogViewRefreshControlWasPulled()
}
