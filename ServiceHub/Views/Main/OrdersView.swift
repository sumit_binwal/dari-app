//
//  OrdersView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrdersView: UITableView {
   // MARK: - Properties
   weak var ordersViewDelegate: OrdersViewDelegate?
   var orders: [Order.Summary] = []
   var currentOrders: [Order.Summary] = []
   var pastOrders: [Order.Summary] = []
   var error: Error?
   var isRefreshing: Bool { self.refreshControl?.isRefreshing ?? false }
   var ordersBeingFetched: Set<Int> = []
   
   // MARK: - Init
   convenience init() {
      self.init(frame: .zero, style: .grouped)
   }
   
   override init(frame: CGRect, style: UITableView.Style) {
      super.init(frame: frame, style: style)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 80
      self.tableFooterView = UIView()
      self.refreshControl = {
         let control = UIRefreshControl()
         control.translatesAutoresizingMaskIntoConstraints = false
         control.tintColor = .calypso
         control.addTarget(
            self,
            action: #selector(self.refreshControlWasPulled),
            for: .valueChanged
         )
         return control
      }()
      
      self.delegate = self
      self.dataSource = self
      
      self.tableHeaderView = nil
      
      self.register(OrderTableViewCell.self, forCellReuseIdentifier: "OrderTableViewCell")
      self.register(AnnouncementTableViewCell.self, forCellReuseIdentifier: "AnnouncementTableViewCell")
   }
   
   // MARK: - Action
   @objc
   private func refreshControlWasPulled() {
      self.ordersViewDelegate?.ordersViewRefreshControlWasPulled()
   }
   
   // MARK: -  Other
   public func startRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      self.isUserInteractionEnabled = false
      DispatchQueue.main.async {
         self.refreshControl?.startRefreshing(animateScrollViewContentOffset: animate)
      }
   }
   
   public func endRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      self.isUserInteractionEnabled = true
      DispatchQueue.main.async {
         self.refreshControl?.stopRefreshing(animateScrollViewContentOffset: animate)
      }
   }
   
   public func displayError(_ error: Error, animated: Bool = true) {
      DispatchQueue.main.async {
         if error.localizedDescription == self.error?.localizedDescription {
            guard animated else { return }
            
            (self.cellForRow(at: .zero) as? AnnouncementTableViewCell)?.jiggle()
            return
         }
         self.separatorColor = .clear
         if animated {
            self.performBatchUpdates({
               let oldSections = IndexSet(integersIn: 0..<self.numberOfSections)
               self.orders.removeAll()
               self.currentOrders.removeAll()
               self.pastOrders.removeAll()
               self.deleteSections(oldSections)
               
               self.error = error
               self.insertSections([0, 1, 2])
            }, completion: nil)
            
         } else {
            self.error = error
            self.orders.removeAll()
            self.currentOrders.removeAll()
            self.pastOrders.removeAll()
            self.reloadData()
         }
      }
   }
   
   public func setOrders(_ orders: [Order.Summary], animated: Bool = true) {
      DispatchQueue.main.async {
         if animated {
            self.performBatchUpdates({
               let oldSections = IndexSet(integersIn: 0..<self.numberOfSections)
               self.error = nil
               self.orders.removeAll()
               self.currentOrders.removeAll()
               self.pastOrders.removeAll()
               self.deleteSections(oldSections)
               
               self.orders = orders
               self.currentOrders = orders.filter { $0.isActive }
               self.pastOrders = orders.filter { !$0.isActive }
               self.insertSections([0, 1, 2])
               
            }, completion: { _ in self.separatorColor = .separator })
         } else {
            self.separatorColor = .separator
            self.error = nil
            self.orders = orders
            self.currentOrders = orders.filter { $0.isActive }
            self.pastOrders = orders.filter { !$0.isActive }
            self.reloadData()
         }
      }
   }
   
   func cancelOrder(_ order: Order.Summary) {
      self.ordersViewDelegate?.ordersView(didCancelOrder: order)
   }
   
   func delete(orderWithID id: Int) {
      let index = self.orders.firstIndex { $0.id == id }
      let currentIndex = self.currentOrders.firstIndex { $0.id == id }
      let pastIndex = self.pastOrders.firstIndex { $0.id == id}
      
      if let index = index {
         self.orders.remove(at: index)
      }
      
      if let currentIndex = currentIndex {
         self.currentOrders.remove(at: currentIndex)
         self.deleteRows(at: [.init(row: currentIndex, section: 1)])
         
         if self.currentOrders.isEmpty {
            self.reloadSections([1], with: .none)
         }
         
      } else if let pastIndex = pastIndex {
         
         self.pastOrders.remove(at: pastIndex)
         self.deleteRows(at: [.init(row: pastIndex, section: 2)])
         
         if self.pastOrders.isEmpty {
            self.reloadSections([2], with: .none)
         }
      }
   }
   
   func setOrderAsFetched(id: Int) {
      self.ordersBeingFetched.remove(id)
      let indexPathOrNil: IndexPath? = {
         if let index = self.currentOrders.firstIndex(where: { $0.id == id }) {
            return .init(row: index, section: 1)
         } else if let index = self.pastOrders.firstIndex(where: { $0.id == id }) {
            return .init(row: index, section: 2)
         }
         return nil
      }()
      
      guard let indexPath = indexPathOrNil else { return }
      
      let cell = self.cellForRow(at: indexPath) as? OrderTableViewCell
      cell?.displayActivityIndicator(false, animated: true)
   }
   
   func clearOrdersBeingFetched() {
      self.ordersBeingFetched.forEach { self.setOrderAsFetched(id: $0) }
   }
}

// MARK: - Table View
extension OrdersView: UITableViewDataSource, UITableViewDelegate {
   func numberOfSections(in tableView: UITableView) -> Int { 3 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case 0: return self.error == nil ? 0 : 1
      case 1: return self.currentOrders.count
      case 2: return self.pastOrders.count
      default: return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
         let cell: AnnouncementTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "AnnouncementTableViewCell",
            for: indexPath
         ) as? AnnouncementTableViewCell ?? .init(
               style: .default,
               reuseIdentifier: "AnnouncementTableViewCell"
         )
         guard let error = self.error else {
            assertionFailure("[OrdersView] Error is nil.")
            return cell
         }
         cell.configure(for: error)
         return cell
         
      case 1:
         let cell: OrderTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "OrderTableViewCell", for: indexPath
         ) as? OrderTableViewCell ?? .init(style: .default, reuseIdentifier: "OrderTableViewCell")
         
         cell.setOrder(self.currentOrders[indexPath.row])
         cell.delegate = self
         return cell
         
      case 2:
         let cell: OrderTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "OrderTableViewCell", for: indexPath
            ) as? OrderTableViewCell ?? .init(style: .default, reuseIdentifier: "OrderTableViewCell")
         
         cell.setOrder(self.pastOrders[indexPath.row])
         cell.delegate = self
         return cell
         
      default: fatalError("[OrdersView] Invalid index path.")
      }
   }
   
   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      switch section {
      case 0: return nil
      case 1: return self.currentOrders.isEmpty ? nil : "currentOrders".localized
      case 2: return self.pastOrders.isEmpty ? nil : "pastOrders".localized
      default: return nil
      }
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      switch section {
      case 1, 2: return 38
      default: return .leastNormalMagnitude
      }
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      switch section {
      case 1, 2: return UITableViewHeaderFooterView()
      default: return nil
      }
   }
   
   func tableView(
      _ tableView: UITableView,
      trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath
   ) -> UISwipeActionsConfiguration? {
      guard [1, 2].contains(indexPath.section) else { return nil }
      let order = (indexPath.section == 1 ? self.currentOrders : self.pastOrders)[indexPath.row]
      guard order.isCancellable else { return nil }
      
      let action = UIContextualAction(
         style: .destructive,
         title: "cancel".localized) { (action, view, isComplete) in
            self.cancelOrder(order)
            isComplete(true)
      }
      let config = UISwipeActionsConfiguration(actions: [action])
      config.performsFirstActionWithFullSwipe = false
      return config
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      guard [1, 2].contains(indexPath.section) else { return }
      
      tableView.deselectRow(at: indexPath, animated: true)
      
      guard self.ordersBeingFetched.isEmpty else  { return }
      
      let order: Order.Summary = indexPath.section == 1 ?
         self.currentOrders[indexPath.row] : self.pastOrders[indexPath.row]
      self.ordersBeingFetched.insert(order.id)
      
      let cell = tableView.cellForRow(at: indexPath) as? OrderTableViewCell
      cell?.displayActivityIndicator(true, animated: true)
      self.ordersViewDelegate?.ordersView(didSelectOrder: order)
   }
}

// MARK: OrderTableViewCellDelegate
extension OrdersView: OrderTableViewCellDelegate {
   func orderTableViewCell(didCancel order: Order.Summary) {
      self.cancelOrder(order)
   }
}

// MARK: - Protocol
protocol OrdersViewDelegate: AnyObject {
   func ordersView(didSelectOrder order: Order.Summary)
   func ordersView(didCancelOrder order: Order.Summary)
   func ordersViewRefreshControlWasPulled()
}
