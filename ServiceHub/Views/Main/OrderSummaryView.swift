//
//  OrderSummaryView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderSummaryView: UITableView {
   // MARK: - Properties
   weak var orderSummaryDelegate: OrderSummaryViewDelegate?
   let formData: Order.FormData
   
   lazy var invoiceItems = self.formData.aggregatedInvoiceItems
   
   lazy var promoCodeState: PromoCodeTableViewCell.State = {
      if let promoCode = self.formData.promoCode, let discount = self.formData.promoCodeDisount {
         return .validatedCode(code: promoCode, discount: discount)
      } else {
         return .unvalidatedCode
      }
   }()
   var unvalidatedPromoCode: String?
   
   private lazy var summaryFields: [SummaryField] = {
      var fields: [SummaryField] = [.subtotal, .promoCode, .tax, .total]
      
      if let index = self.formData.paymentMethods.firstIndex(of: self.formData.paymentMethod),
         self.formData.paymentMethodFees[index] != nil
      {
         fields.insert(.paymentMethodFee, at: 0)
      }
      
      return fields
   }()
   
   // MARK: - Init
   init(formData: Order.FormData) {
      self.formData = formData
      super.init(frame: .zero, style: .grouped)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init?(coder:) not implemented.")
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 50
      self.keyboardDismissMode = .interactive
      self.tableFooterView = UIView()
      
      self.delegate = self
      self.dataSource = self
      
      self.register(InvoiceItemTableViewCell.self, forCellReuseIdentifier: "InvoiceItemTableViewCell")
      self.register(
         InvoiceSummaryItemTableViewCell.self,
         forCellReuseIdentifier: "InvoiceSummaryItemTableViewCell"
      )
      self.register(
         InvoicePrimarySummaryItemTableViewCell.self,
         forCellReuseIdentifier: "InvoicePrimarySummaryItemTableViewCell"
      )
      self.register(PromoCodeTableViewCell.self, forCellReuseIdentifier: "PromoCodeTableViewCell")
      self.register(
         PaymentMethodTableViewCell.self,
         forCellReuseIdentifier: "PaymentMethodTableViewCell"
      )
      self.register(FooterView.self, forHeaderFooterViewReuseIdentifier: "cancelationPolicyFooter")
   }
   
   // MARK: - Other
   func addCard(_ card: Card) {
      let cell = self.cellForRow(at: .init(row: 0, section: self.sectionForPayment))
         as? PaymentMethodTableViewCell
      cell?.addCard(card)
   }
   
   func refreshCardSelection() {
      let cell = self.cellForRow(at: .init(row: 0, section: self.sectionForPayment))
         as? PaymentMethodTableViewCell
      cell?.select(self.formData.card)
   }
   
   func reload() {
      self.summaryFields = {
         var fields: [SummaryField] = [.subtotal, .promoCode, .tax, .total]
         
         if let index = self.formData.paymentMethods.firstIndex(of: self.formData.paymentMethod),
            self.formData.paymentMethodFees[index] != nil
         {
            fields.insert(.paymentMethodFee, at: 0)
         }
         
         return fields
      }()
      self.reloadData()
   }
   
   func removeCards(_ cards: [Card]) {
      let cell = cellForRow(at: .init(row: 0, section: sectionForPayment))
         as? PaymentMethodTableViewCell
      cell?.removeCards(cards)
   }
}

// MARK: - Table View
extension OrderSummaryView: UITableViewDataSource, UITableViewDelegate {
   private func indexPath(forSummaryField field: SummaryField) -> IndexPath? {
      guard let row = self.summaryFields.firstIndex(of: field) else { return nil }
      return .init(row: row, section: self.sectionForSummary)
   }
   
   private func cell(forSummaryField field: SummaryField) -> UITableViewCell? {
      guard let indexPath = self.indexPath(forSummaryField: field) else { return nil }
      return self.cellForRow(at: indexPath)
   }
   
   private var sectionForSummary: Int { 1 }
   private var sectionForPayment: Int { 2 }
   
   func numberOfSections(in tableView: UITableView) -> Int {
      3
   }
   
   func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
      switch section {
      case 0:
         guard self.formData.selectedService.serviceCount > 1 else { return nil }
         return "rescheduleWarning".localized
      default: return nil
      }
   }
   
   func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      guard section == self.sectionForPayment else { return nil }
      
      let view = tableView.dequeueReusableHeaderFooterView(
         withIdentifier: "cancelationPolicyFooter"
      ) as? FooterView ?? FooterView(reuseIdentifier: "cancelationPolicyFooter")
      
      let cancelationPolicyAction: () -> Void = { [weak self] in
         self?.orderSummaryDelegate?.orderSummaryDidTapCancelationPolicy()
      }
      let termsAndConditionsAction: () -> Void = { [weak self] in
         self?.orderSummaryDelegate?.orderSummaryDidTapTermsAndConditions()
      }
      view.setValues(
         "cancelationPolicyDisclaimer".localized,
         tappableTexts: ["cancelationPolicy".localized, "TnC".localized],
         tapActions: [cancelationPolicyAction, termsAndConditionsAction]
      )
      
      return view
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case 0: return self.invoiceItems.count
      case self.sectionForSummary: return self.summaryFields.count
      default: return 1
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "InvoiceItemTableViewCell", for: indexPath)
            as? InvoiceItemTableViewCell else
         {
            fatalError("[OrderSummaryView] Failed to dequeue InvoiceItemTableViewCell.")
         }
         let invoiceItem = self.invoiceItems[indexPath.row]
         let serviceCount = Double(self.formData.selectedService.serviceCount)
         let serviceCountText: String? = {
            guard serviceCount > 1 else { return nil }
            let number = NumberFormatter.integer.string(from: serviceCount)
            switch serviceCount {
            case 2:
               return "× \(Environment.languageIsArabic ? "" : "\(number) ")\("2visits".localized)"
            default: return "× \(number) \("visits".localized)"
            }
         }()
         
         cell.setValues(
            header: invoiceItem.header,
            parameterDescription: invoiceItem.parameterDescription,
            rateDescription: invoiceItem.rateDescription,
            visitCountText: serviceCountText,
            price: NumberFormatter.currency.string(from: invoiceItem.total * serviceCount)
         )
         return cell
         
      case self.sectionForSummary:
         switch self.summaryFields[indexPath.row] {
         case .paymentMethodFee:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "InvoiceSummaryItemTableViewCell", for: indexPath)
               as? InvoiceSummaryItemTableViewCell else
            {
               fatalError("[OrderSummaryView] Failed to dequeue InvoiceSummaryItemTableViewCell.")
            }
            cell.setValues(
               item: "paymentMethodFee".localized,
               price: NumberFormatter.currency.string(from: self.formData.paymentMethodFee ?? 0)
            )
            return cell
            
         case .subtotal:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "InvoiceSummaryItemTableViewCell", for: indexPath)
               as? InvoiceSummaryItemTableViewCell else
            {
               fatalError("[OrderSummaryView] Failed to dequeue InvoiceSummaryItemTableViewCell.")
            }
            cell.setValues(
               item: "subtotal".localized,
               price: NumberFormatter.currency.string(from: self.formData.subtotal ?? 0)
            )
            return cell
            
         case .promoCode:
            guard let cell = tableView.dequeueReusableCell(
               withIdentifier: "PromoCodeTableViewCell", for: indexPath)
               as? PromoCodeTableViewCell else
            {
               fatalError("[OrderSummaryView] Failed to dequeue PromoCodeTableViewCell.")
            }
            cell.setValues(code: self.unvalidatedPromoCode, state: self.promoCodeState)
            cell.delegate = self
            return cell
            
         case .tax:
            guard let cell = tableView
               .dequeueReusableCell(withIdentifier: "InvoiceItemTableViewCell", for: indexPath)
               as? InvoiceItemTableViewCell else
            {
               fatalError("[OrderSummaryView] Failed to dequeue InvoiceItemTableViewCell.")
            }
            
            cell.setValues(
               header: "vat".localized,
               parameterDescription: nil,
               rateDescription: NumberFormatter.percent.string(from: PricePoint.taxRate),
               visitCountText: nil,
               price: NumberFormatter.currency.string(from: self.formData.tax ?? 0)
            )
            return cell
            
         case .total:
            guard let cell = tableView.dequeueReusableCell(
               withIdentifier: "InvoicePrimarySummaryItemTableViewCell", for: indexPath)
               as? InvoicePrimarySummaryItemTableViewCell else
            {
               fatalError("[OrderSummaryView] Failed to dequeue InvoicePrimarySummaryItemTableViewCell.")
            }
            cell.setValues(
               item: "total".localized,
               price: self.formData.totalString ?? ""
            )
            return cell
         }
         
      case self.sectionForPayment:
         guard let cell = tableView
            .dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell", for: indexPath)
            as? PaymentMethodTableViewCell else
         {
            fatalError("[OrderSummaryView] Failed to dequeue PaymentMethodTableViewCell.")
         }
         cell.setValues(
            paymentMethods: self.formData.paymentMethods,
            fees: self.formData.paymentMethodFees,
            selectedPaymentMethod: self.formData.paymentMethod,
            cards: self.formData.cards,
            selectedCard: self.formData.card
         )
         cell.delegate = self
         return cell
         
      default:
         fatalError("[OrderSummaryView] Unrecognized index path.")
      }
   }
}

// MARK: - PromoCodeTableViewCellDelegate
extension OrderSummaryView: PromoCodeTableViewCellDelegate {
   func promoCodeView(didSubmitCode code: String) {
      guard let serviceProvider = self.formData.serviceProvider else {
         assertionFailure("[OrderSummaryView] No service provider selected.")
         return
      }
      guard let date = self.formData.selectedDate else {
         assertionFailure("[OrderSummaryView] No service date selected.")
         return
      }
      
      Order.manager.delegate = self
      Order.manager.validate(
         promoCode: code,
         listing: self.formData.listing,
         serviceProvider: serviceProvider,
         serviceDate: date,
         timeSlot: self.formData.selectedTimeSlot
      )
   }
   
   func promoCodeView(stateDidChange state: PromoCodeTableViewCell.State, code: String?) {
      self.promoCodeState = state
      switch state {
      case .unvalidatedCode:
         self.unvalidatedPromoCode = code
         self.formData.promoCode = nil
         
      case .validatedCode(code: let promoCode, discount: _):
         self.unvalidatedPromoCode = nil
         self.formData.promoCode = promoCode
         
      default: self.formData.promoCode = nil
      }
      
      DispatchQueue.main.async {
         self.performBatchUpdates({
            
         }, completion: { _ in
            self.reloadRows(
               at: [SummaryField.tax, .total].compactMap { self.indexPath(forSummaryField: $0) },
               with: .none
            )
         })
         
      }
   }
}

// MARK: - OrderManagerDelegate
extension OrderSummaryView: OrderManagerDelegate {
   func orderManager(
      didValidatePromoCode promoCode: Order.PromoCode?,
      completedWithError error: Error?,
      errorMessage: String?
   ) {
      DispatchQueue.main.async {
         let cell = self.cell(forSummaryField: .promoCode) as? PromoCodeTableViewCell
         
         self.formData.promoCode = promoCode
         
         if let error = error {
            self.promoCodeState = .error(message: errorMessage ?? error.localizedDescription)
            cell?.setValues(
               code: self.unvalidatedPromoCode,
               state: self.promoCodeState,
               animated: true
            )
            return
         }
         
         guard let promoCode = promoCode else {
            assertionFailure("[OrderSummaryView] Uncaught error.")
            self.promoCodeState = .unvalidatedCode
            cell?.setValues(
               code: self.unvalidatedPromoCode,
               state: self.promoCodeState,
               animated: true
            )
            return
         }
         
         self.unvalidatedPromoCode = promoCode.code
         self.promoCodeState = .validatedCode(
            code: promoCode,
            discount: self.formData.promoCodeDisount ?? 0
         )
         cell?.setValues(code: nil, state: self.promoCodeState, animated: true)
      }
   }
   
   func orderManager(
      didPlaceOrder order: Order?,
      completedWithError error: Error?,
      cardTransaction: CardTransaction?
   ) {}
   func orderManager(didGetOrders orders: [Order], completedWithError error: Error?) {}
   func orderManager(didRedirectTo url: URL) {}
}

// MARK: - PaymentMethodTableViewCellDelegate
extension OrderSummaryView: PaymentMethodPickerViewDelegate {
   func paymentMethodPickerView(didSelectPaymentMethod paymentMethod: PaymentMethod, card: Card?) {
      self.formData.paymentMethod = paymentMethod
      self.formData.card = card
      
      var fields: [SummaryField] = [.subtotal, .promoCode, .tax, .total]
      
      // FIXME: Promo code cell with an error message has a minor glitch.
      DispatchQueue.main.async {
         self.performBatchUpdates({
            if self.formData.paymentMethodFee != nil &&
               self.indexPath(forSummaryField: .paymentMethodFee) == nil
            {
               self.summaryFields.insert(.paymentMethodFee, at: 0)
               self.insertRows(at: [.init(row: 0, section: self.sectionForSummary)], with: .fade)
               
            } else if let indexPath = self.indexPath(forSummaryField: .paymentMethodFee) {
               self.summaryFields.remove(at: indexPath.row)
               self.deleteRows(at: [indexPath], with: .fade)
            } else {
               fields.append(.paymentMethodFee)
            }
         }, completion: { _ in
            self.reloadRows(
               at: fields.compactMap { self.indexPath(forSummaryField: $0) },
               with: .none
            )
         })
      }
   }
   
   func paymentMethodPickerViewDidTapAddCard() {
      self.orderSummaryDelegate?.orderSummaryViewDidTapAddCard()
   }
   
   func paymentMethodPickerViewDidToggleCardPickerVisibility() {
      self.performBatchUpdates(nil, completion: nil)
   }
}

// MARK: - Summary Fields
fileprivate enum SummaryField {
   case paymentMethodFee
   case subtotal
   case promoCode
   case tax
   case total
}

// MARK: - Protocol
protocol OrderSummaryViewDelegate: AnyObject {
   func orderSummaryViewDidTapAddCard()
   func orderSummaryDidTapCancelationPolicy()
   func orderSummaryDidTapTermsAndConditions()
}

// MARK: - FooterView
fileprivate class FooterView: UITableViewHeaderFooterView {
   lazy var label: TappableLabel = {
      let label = TappableLabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .subtleText
      label.numberOfLines = 0
      return label
   }()
   
   override init(reuseIdentifier: String?) {
      super.init(reuseIdentifier: reuseIdentifier)
      self.contentView.addFillingSubviewConstrainedByMargins(self.label)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   func setValues(_ text: String, tappableTexts: [String], tapActions: [(() -> Void)?]) {
      self.label.set(
         text: text,
         tappableTexts: tappableTexts,
         tapActions: tapActions,
         font: .regularFont(ofSize: 13),
         tappableFont: .mediumFont(ofSize: 13)
      )
   }
}
