//
//  ServiceProvidersView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ServiceProvidersView: UITableView {
   // MARK: - Properties
   weak var serviceProviderViewDelegate: ServiceProvidersViewDelegate?
   let formData: Order.FormData
   var serviceProviders: [ServiceProvider] { self.formData.serviceProviders }
   
   // MARK: - Init
   init(data: Order.FormData) {
      self.formData = data
      super.init(frame: .zero, style: .grouped)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 80
      //self.separatorInset = .zero
      self.tableFooterView = UIView()
      self.keyboardDismissMode = .interactive
      
      self.delegate = self
      self.dataSource = self
      self.prefetchDataSource = self
      
      self.register(
         ServiceProviderTableViewCell.self,
         forCellReuseIdentifier: "ServiceProviderTableViewCell"
      )
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}

// MARK: - Delegate and Datasource
extension ServiceProvidersView: UITableViewDataSource, UITableViewDelegate {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.serviceProviders.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell: ServiceProviderTableViewCell = tableView.dequeueReusableCell(
         withIdentifier: "ServiceProviderTableViewCell",
         for: indexPath
      ) as? ServiceProviderTableViewCell ?? .init(
         style: .default,
         reuseIdentifier: "ServiceProviderTableViewCell"
      )
      
      let serviceProvider = self.serviceProviders[indexPath.row]
      cell.setValues(serviceProvider: serviceProvider)
      if serviceProvider.shouldDownloadImage {
         tableView.prefetchDataSource?.tableView(tableView, prefetchRowsAt: [indexPath])
      }
      
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let serviceProvider = self.serviceProviders[indexPath.row]
      self.formData.serviceProvider = serviceProvider
      self.serviceProviderViewDelegate?.serviceProvidersView(
         didSelectServiceProvider: serviceProvider
      )
   }
}

extension ServiceProvidersView: UITableViewDataSourcePrefetching {
   func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
      for indexPath in indexPaths {
         let serviceProvider = self.serviceProviders[indexPath.row]
         guard serviceProvider.shouldDownloadImage else { continue }
         guard let url = serviceProvider.imageURL else { continue }
         
         serviceProvider.imageDownloadTask = Environment.sessionManager.download(url: url) { url in
            guard let url = url, let data = try? Data(contentsOf: url) else { return }
            
            let image = UIImage(data: data)?.withRenderingMode(.alwaysTemplate)
            serviceProvider.logo = image
            
            DispatchQueue.main.async {
               let cell = tableView.cellForRow(at: indexPath) as? ServiceProviderTableViewCell
               cell?.set(logo: image)
            }
         }
      }
   }
}

// MARK: - Protocol
protocol ServiceProvidersViewDelegate: AnyObject {
   func serviceProvidersView(didSelectServiceProvider serviceProvider: ServiceProvider)
}
