//
//  UserView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class UserView: UITableView {
   
   // MARK: - PROPERTIES
   
   weak var userViewDelegate: UserViewDelegate?
   private let fields: [[Field]] = [
      [.name,
       .phone,
       .email,
       .changePassword],
      
      [.address,
       .manageCards],
      
      [.language],
      
      [.logOut]
   ]
   var user: User {
      didSet {
         self.reloadKeepingSelectionAndContentOffsetState()
      }
   }
   
   // MARK: - Initialization and Setup
   init(user: User) {
      self.user = user
      super.init(frame: .zero, style: .grouped)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.showsVerticalScrollIndicator = false
      self.separatorInset = .zero
      
      self.delegate = self
      self.dataSource = self
      
      self.register(UserTableViewCell.self, forCellReuseIdentifier: "UserTableViewCell")
   }
}

// MARK: - Table View
extension UserView: UITableViewDelegate, UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int { self.fields.count }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.fields[section].count
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { 44 }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(
         withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else
      {
         fatalError("[UserView] Failed to dequeue UserTableViewCell.")
      }
      
      let field = self.fields[indexPath.section][indexPath.row]
      cell.iconImageView.image = field.image
      cell.label.text = field.title(for: self.user)
      cell.accessoryType = field.hasDisclosureIndicator ? .disclosureIndicator : .none
      
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let field = self.fields[indexPath.section][indexPath.row]
      switch field {
      case .logOut: self.userViewDelegate?.userViewDidRequestLogOut(indexPath: indexPath)
      case .language: self.userViewDelegate?.userViewDidRequestLanguageChange(indexPath: indexPath)
      case .manageCards: self.userViewDelegate?.userViewDidRequestCardManagement()
      default: self.userViewDelegate?.userView(didRequestChangeOf: field)
      }
   }
}

// MARK: - Fields
extension UserView {
   enum Field: String {
      case name = "Name"
      case phone = "Phone"
      case email = "Email"
      case changePassword = "Password"
      case address = "Address"
      case language = "Language"
      case manageCards = "Cards"
      case logOut = "Sign Out"
      
      var hasDisclosureIndicator: Bool { ![Field.language, .logOut].contains(self) }
      
      func title(for user: User) -> String {
         switch self {
         case .name: return user.name
         case .phone: return user.formattedPhoneNumber
         case .email: return user.email
         case .changePassword: return "changePassword".localized
         case .address: return user.address
         case .manageCards: return "manageCards".localized
         case .language: return "changeLanguage".localized
         case .logOut: return "signOut".localized
         }
      }
      
      var formView: RegisterFormView? {
         return {
            switch self {
            case .name: return NameChangeFormView()
            case .phone: return PhoneChangeFormView()
            case .email: return EmailChangeFormView()
            case .changePassword: return PasswordChangeFormView()
            default: return nil
            }
         }()
      }
      
      var image: UIImage? { return UIImage(named: rawValue) }
   }
}

// MARK: - Delegate Protocol

protocol UserViewDelegate: AnyObject {
   func userView(didRequestChangeOf field: UserView.Field)
   func userView(didRequestChangeUsingFormView formView: RegisterFormView)
   func userViewDidRequestCardManagement()
   func userViewDidRequestLanguageChange(indexPath: IndexPath)
   func userViewDidRequestLogOut(indexPath: IndexPath)
}
