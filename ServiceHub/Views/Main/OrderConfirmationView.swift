//
//  OrderConfirmationView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/4/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderConfirmationView: UIView {
   weak var delegate: OrderConfirmationViewDelegate?
   let order: Order
   
   // MARK: - UI
   private lazy var confirmationLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .boldFont(ofSize: 18)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.text = "orderPlaced".localized
      return label
   }()
   private lazy var logoImageView: UIImageView = {
      let view = UIImageView(image: UIImage(named: "Large Logo"))
      view.contentMode = .center
      return view
   }()
   private lazy var thankYouLabel1: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .boldFont(ofSize: 18)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.text = "thanks".localized
      return label
   }()
   private lazy var thankYouLabel2: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.text = "\("thanks2".localized) \(User.current?.firstName ?? "")"
      return label
   }()
   lazy var thanksStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.thankYouLabel1, self.thankYouLabel2])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 0
      return view
   }()
   private lazy var serviceProviderLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.numberOfLines = 5
      label.text = {
         var text = "\("relax".localized) "
         let companyName = self.order.serviceProvider.companyName
         text += self.order.serviceProvider.isInternal ?
            "internalGetInTouch".localized : "\(companyName) \("getInTouch".localized)"
         return text
      }()
      return label
   }()
   private lazy var orderPageLabel: TappableLabel = {
      let label = TappableLabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.numberOfLines = 5
      label.textColor = .prominentText
      label.textAlignment = .center
      
      label.set(
         text: "orderTracking".localized,
         tappableTexts: ["myOrders".localized],
         tapActions: [{ [weak self] in self?.delegate?.orderConfirmationViewDidTapMyOrders() }],
         font: .regularFont(ofSize: 17),
         tappableFont: .boldFont(ofSize: 17)
      )
      
      return label
   }()
   private lazy var cancelLabel: TappableLabel = {
      let label = TappableLabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.numberOfLines = 0
      label.textColor = .prominentText
      label.textAlignment = .center
      
      let gracePeriodText: String = {
         let gracePeriod = Order.gracePeriodInMinutes
         let periodMeasurement = Measurement(value: Double(gracePeriod), unit: UnitDuration.minutes)
         let formatter = MeasurementFormatter()
         formatter.unitStyle = .long
         formatter.unitOptions = .providedUnit
         formatter.numberFormatter = .integer
         return formatter.string(from: periodMeasurement)
      }()
      
      label.set(
         text: "\("secondThoughts".localized) \(gracePeriodText).",
         tappableTexts: ["cancel".localized],
         tapActions: [
            {
               [weak self] in
               guard let order = self?.order else { return }
               self?.delegate?.orderConfirmationView(didCancelOrder: order)
            }
         ],
         font: .regularFont(ofSize: 16),
         tappableFont: .mediumFont(ofSize: 16)
      )
      return label
   }()
   private lazy var orderIDLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.text = "\("Order ID:".localized) \(self.order.id)"
      return label
   }()
   private lazy var homeButton: ProminentButton = {
      let button = ProminentButton()
      button.addTarget(
         self,
         action: #selector(self.homePageButtonWasTapped(_:)),
         for: .touchUpInside
      )
      button.setTitle("homePage".localized, for: .normal)
      return button
   }()
   lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.confirmationLabel,
         self.logoImageView,
         self.thanksStackView,
         self.serviceProviderLabel,
         self.orderPageLabel,
         self.orderIDLabel,
         self.cancelLabel,
         self.homeButton
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .equalSpacing
      view.setCustomSpacing(0, after: self.thankYouLabel1)
      return view
   }()
   
   // MARK: - Init
   init(order: Order) {
      self.order = order
      super.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubviewConstrainedByMargins(self.stackView)
   }
   
   required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
   
   // MARK: - Life Cycle
   override func tintColorDidChange() {
      guard let attributedText = self.orderPageLabel.attributedText else { return }
      let range = (attributedText.string as NSString).range(of: "myOrders".localized)
      
      let mutableText = NSMutableAttributedString(attributedString: attributedText)
      mutableText.setAttributes(
         [
            .font: UIFont.boldFont(ofSize: 17),
            .foregroundColor: UIColor.tint
         ],
         range: range
      )
      self.orderPageLabel.attributedText = mutableText
   }
   
   // MARK: - Actions
   @objc
   func homePageButtonWasTapped(_ sender: Any) {
      self.delegate?.orderConfirmationViewDidUnwindToCatalog()
   }
}

protocol OrderConfirmationViewDelegate: AnyObject {
   func orderConfirmationViewDidUnwindToCatalog()
   func orderConfirmationViewDidTapMyOrders()
   func orderConfirmationView(didCancelOrder order: Order)
}
