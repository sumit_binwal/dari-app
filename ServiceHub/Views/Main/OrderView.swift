//
//  OrderView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/16/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderView: UITableView {
   // MARK: - Properties
   weak var orderViewDelegate: OrderViewDelegate?
   let order: Order
   private lazy var invoiceItems: [InvoiceRepresentable] = {
      var items: [InvoiceRepresentable] = [self.order.serviceParameters]
      items.append(contentsOf: self.order.itemParameters)
      return items
   }()
   private lazy var summaryFields: [SummaryField] = {
      var fields: [SummaryField] = []
      
      if self.order.paymentMethodFee > 0 { fields.append(.paymentMethodFee) }
      fields.append(.subtotal)
      
      if self.order.promoCodeDiscount > 0 { fields.append(.promoCodeDiscount) }
      if self.order.discount > 0 { fields.append(.discount) }
      
      fields.append(contentsOf: [.tax, .total])
      
      return fields
   }()
   private lazy var actions: [Action] = {
      var actions: [Action] = []
      //if self.order.isCancellable { actions.append(.reschedule) }
      if self.order.isCancellable { actions.append(.cancel) }
      return actions
   }()
   
   // MARK: - Init
   init(_ order: Order) {
      self.order = order
      super.init(frame: .zero, style: .grouped)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.showsVerticalScrollIndicator = false
      self.rowHeight = Self.automaticDimension
      self.estimatedRowHeight = 70
      self.tableFooterView = UIView()
      
      self.delegate = self
      self.dataSource = self
      
      self.register(InvoiceItemTableViewCell.self, forCellReuseIdentifier: "InvoiceItemTableViewCell")
      self.register(
         InvoiceSummaryItemTableViewCell.self,
         forCellReuseIdentifier: "InvoiceSummaryItemTableViewCell"
      )
      self.register(
         InvoicePrimarySummaryItemTableViewCell.self,
         forCellReuseIdentifier: "InvoicePrimarySummaryItemTableViewCell"
      )
   }
}

// MARK: - Table View
extension OrderView: UITableViewDataSource, UITableViewDelegate {
   func numberOfSections(in tableView: UITableView) -> Int { 3 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case 0: return self.invoiceItems.count
      case 1: return self.summaryFields.count
      case 2: return self.actions.count
      default: return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
         let cell: InvoiceItemTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "InvoiceItemTableViewCell",
            for: indexPath
         ) as? InvoiceItemTableViewCell ?? .init(
            style: .default,
            reuseIdentifier: "InvoiceItemTableViewCell"
         )
         
         let serviceCount = Double(self.order.serviceCount)
         let serviceCountText: String? = {
            guard serviceCount > 1 else { return nil }
            let number = NumberFormatter.integer.string(from: serviceCount)
            switch serviceCount {
            case 2:
               return "× \(Environment.languageIsArabic ? "" : "\(number) ")\("2visits".localized)"
            default: return "× \(number) \("visits".localized)"
            }
         }()
         
         let invoiceItem = self.invoiceItems[indexPath.row]
         let priceString = invoiceItem.price == 0 ?
            "" : NumberFormatter.currency.string(from: invoiceItem.price * serviceCount)
         
         cell.setValues(
            header: invoiceItem.header,
            parameterDescription: invoiceItem.parameterDescription,
            rateDescription: invoiceItem.rateDescription(
               currencyFormatter: self.order.currencyFormatter,
               shortCurrencyFormatter: self.order.shortCurrencyFormatter
            ),
            visitCountText: serviceCountText,
            price: priceString
         )
         
         return cell
         
      case 1:
         let field = self.summaryFields[indexPath.row]
         let identifier: String = field == .total ?
            "InvoicePrimarySummaryItemTableViewCell" : "InvoiceSummaryItemTableViewCell"
         
         let cell: InvoiceSummaryItemTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: identifier,
            for: indexPath
         ) as? InvoiceSummaryItemTableViewCell ?? .init(
            style: .default,
            reuseIdentifier: identifier
         )
         
         cell.setValues(item: field.name, price: field.value(in: self.order))
         
         return cell
         
      case 2:
         let action = self.actions[indexPath.row]
         let identifier = "actionCell"
         let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
         
         cell.textLabel?.textColor = action.textColor
         cell.textLabel?.text = action.name
         return cell
      
      default: fatalError("[OrderView] Unrecognized index path.")
      }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      guard indexPath.section == 2 else { return }
      
      DispatchQueue.main.async { tableView.deselectRow(at: indexPath, animated: true) }
      
      switch self.actions[indexPath.row] {
      case .reschedule: break
      case .cancel: self.orderViewDelegate?.orderView(didCancelOrder: self.order)
      }
   }
}

// MARK: - Summary Fields
fileprivate enum SummaryField: String {
   case paymentMethodFee
   case subtotal
   case promoCodeDiscount
   case discount
   case tax = "vat"
   case total
   
   var name: String { self.rawValue.localized }
   
   func value(in order: Order) -> String {
      let value: Double = {
         switch self {
         case .paymentMethodFee: return order.paymentMethodFee
         case .subtotal: return order.subtotal
         case .promoCodeDiscount: return -order.promoCodeDiscount
         case .discount: return -order.discount
         case .tax: return order.tax
         case .total: return order.total
         }
      }()
      return order.currencyFormatter.string(from: value)
   }
}

// MARK: - Actions
fileprivate enum Action: String {
   case reschedule
   case cancel
   
   var name: String { self.rawValue.localized }
   
   var textColor: UIColor {
      switch self {
      case .reschedule: return .prominentText
      case .cancel: return .red
      }
   }
}

// MARK: - Protocol
protocol OrderViewDelegate: AnyObject {
   func orderView(didCancelOrder order: Order)
}
