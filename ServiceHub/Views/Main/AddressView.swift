//
//  MapView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/15/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit
import MapKit

class AddressView: UIView {
   weak var delegate: AddressViewDelegate?
   
   // MARK: - Properties
   var beginningAddress: String?
   var beginningLatitude: Double?
   var beginningLongitude: Double?
   
   var currentAddress: String { self.addressTextField.text ?? "" }
   var currentLatitude: Double {
      self.mapView.centerCoordinate.latitude.rounded(.toNearestOrAwayFromZero, precision: 6)
   }
   var currentLongitude: Double {
      self.mapView.centerCoordinate.longitude.rounded(.toNearestOrAwayFromZero, precision: 6)
   }
   
   private let regionRadius: CLLocationDistance = 500
   private var setButton: UIBarButtonItem!
   private let locationManager = CLLocationManager()
   private let geocoder = CLGeocoder()
   private var placemark: CLPlacemark?
   
   // MARK: - UI
   private lazy var mapView: MKMapView = {
      let view = MKMapView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.showsUserLocation = true
      view.showsCompass = false
      view.delegate = self
      return view
   }()
   lazy var addressTextFieldView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .lighterBackground
      view.layer.cornerRadius = .cornerRadius
      view.layer.masksToBounds = true
      
      view.layoutMargins = .init(top: 15, left: 8, bottom: 15, right: 8)
      view.addFillingSubviewConstrainedByMargins(self.addressTextField)
      
      return view
   }()
   lazy var addressTextField: UnderLineTextField = {
      let textField = UnderLineTextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.placeholder = "address".localized
      textField.text = self.beginningAddress
      textField.delegate = self
      textField.addTarget(
         self,
         action: #selector(self.addressTextFieldDidChange),
         for: .editingChanged
      )
      return textField
   }()
   private lazy var dotView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .highlightedBackground
      view.heightAnchor.constraint(equalToConstant: 6).isActive = true
      view.widthAnchor.constraint(equalToConstant: 6).isActive = true
      view.layer.masksToBounds = false
      view.layer.cornerRadius = 3
      view.layer.shadowRadius = 2
      view.layer.shadowOpacity = 0.7
      view.layer.shadowOffset = .zero
      view.transform = CGAffineTransform(scaleX: 1.0/6, y: 1.0/6)
      //view.alpha = 0
      return view
   }()
   private lazy var  userLocationButtonView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.backgroundColor = .lighterBackground
      view.layer.cornerRadius = .cornerRadius
      view.layer.maskedCorners = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ?
            [.layerMinXMinYCorner, .layerMinXMaxYCorner] :
            [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
      }()
      view.layer.masksToBounds = false
      view.layer.shadowOpacity = 1
      view.layer.shadowRadius = .pixel
      view.layer.shadowColor = UIColor.lightGray.cgColor
      view.layer.shadowOffset = .zero
      
      view.addFillingSubviewConstrainedByMargins(self.userLocationButton)
      
      return view
   }()
   private lazy var userLocationButton: UIButton = {
      let button = UIButton()
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setImage(UIImage(named: "Location"), for: .normal)
      button.addTarget(
         self,
         action: #selector(self.userLocationButtonWasTapped),
         for: .touchUpInside
      )
      return button
   }()
   
   // MARK: Init and Setup
   init(address: String?, latitude: Double?, longitude: Double?) {
      self.beginningAddress = address
      self.beginningLatitude = latitude
      self.beginningLongitude = longitude
      super.init(frame: .zero)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.locationManager.delegate = self
      
      self.addFillingSubview(self.mapView)
      self.addSubviews([
         self.addressTextFieldView,
         self.dotView,
         self.userLocationButtonView
      ])
      NSLayoutConstraint.activate([
         self.addressTextFieldView.topAnchor.constraint(equalToSystemSpacingBelow: self.safeAreaLayoutGuide.topAnchor, multiplier: 2),
         self.addressTextFieldView.leadingAnchor.constraint(equalToSystemSpacingAfter: self.safeAreaLayoutGuide.leadingAnchor, multiplier: 1),
         self.addressTextFieldView.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
         
         self.dotView.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
         self.dotView.centerYAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerYAnchor),
         
         self.userLocationButtonView.bottomAnchor.constraint(
            equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -30),
         self.userLocationButtonView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
      ])
      
      
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Actions
   @objc
   private func addressTextFieldDidChange() {
      self.delegate?.addressView(
         didChangeAddress: self.currentAddress,
         latitude: self.currentLatitude,
         longitude: self.currentLongitude
      )
   }
   
   @objc
   private func userLocationButtonWasTapped() {
      guard let location = self.locationManager.location else { return }
      self.centerMapOnLocation(location, animated: true)
   }
   
   // MARK: - Other
   private func showDotView(_ show: Bool) {
      UIView.animate(withDuration: 0.2) {
         self.dotView.transform = show ? .identity : CGAffineTransform(scaleX: 1.0/6, y: 1.0/6)
         self.dotView.alpha = show ? 1 : 0
      }
   }
   
   func centerMapOnLocation(_ location: CLLocation, animated: Bool = true) {
      let coordinateRegion = MKCoordinateRegion(
         center: location.coordinate,
         latitudinalMeters: self.regionRadius,
         longitudinalMeters: self.regionRadius)
      self.mapView.setRegion(coordinateRegion, animated: animated)
   }
}

// MARK: - UITextFieldDelegate
extension AddressView: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
   }
}

// MARK: - CLLocationManagerDelegate
extension AddressView: CLLocationManagerDelegate {
   func locationManager(
      _ manager: CLLocationManager,
      didChangeAuthorization status: CLAuthorizationStatus
   ) {
      switch status {
      case .authorizedWhenInUse, .authorizedAlways:
         if let latitude = beginningLatitude, let longitude = beginningLongitude {
            self.centerMapOnLocation(
               CLLocation(latitude: latitude, longitude: longitude),
               animated: false
            )
            
         } else if let userLocation = manager.location {
            self.centerMapOnLocation(userLocation, animated: false)
         }
         
         self.userLocationButton.superview?.isHidden = false
         
      case .denied, .restricted:
         if let latitude = beginningLatitude,
            let longitude = beginningLongitude
         {
            self.centerMapOnLocation(
               CLLocation(latitude: latitude, longitude: longitude),
               animated: false
            )
         }
         
         self.userLocationButton.superview?.isHidden = true
         
      case .notDetermined: manager.requestWhenInUseAuthorization()
         
      @unknown default:
         assertionFailure("[MapVC] Unknown status \(status).")
         if let latitude = beginningLatitude, let longitude = beginningLongitude {
            self.centerMapOnLocation(
               CLLocation(latitude: latitude, longitude: longitude),
               animated: false
            )
         }
         
         self.userLocationButton.superview?.isHidden = true
      }
      
      self.userLocationButton.superview?.isHidden =
         ![CLAuthorizationStatus.authorizedWhenInUse, .authorizedAlways].contains(status)
   }
}

// MARK: - MKMapViewDelegate
extension AddressView: MKMapViewDelegate {
   func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
      mapView.removeAnnotations(mapView.annotations)
      self.geocoder.cancelGeocode()
      self.placemark = nil
      self.showDotView(true)
      DispatchQueue.main.async {
         self.addressTextField.resignFirstResponder()
      }
   }
   
   func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
      defer { self.showDotView(false) }
      
      let annotation = MKPointAnnotation()
      annotation.coordinate = mapView.centerCoordinate
      mapView.addAnnotation(annotation)
      
      let location = CLLocation(latitude: mapView.centerCoordinate.latitude,
                                longitude: mapView.centerCoordinate.longitude)
      self.geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
         if let error = error {
            debugPrint(error.localizedDescription)
         }
         if let placemark = placemarks?.first {
            annotation.title = placemark.name
            self.placemark = placemark
            for containedAnnotation in mapView.annotations {
               if containedAnnotation.coordinate.latitude == annotation.coordinate.latitude &&
                  containedAnnotation.coordinate.longitude == annotation.coordinate.longitude {
                  mapView.selectAnnotation(annotation, animated: true)
                  return
               }
            }
         }
      }
   }
   
   func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      guard annotation is MKPointAnnotation else { return nil }
      
      let identifier = "Annotation"
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
      
      if annotationView == nil {
         annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
         annotationView!.canShowCallout = true
      } else {
         annotationView!.annotation = annotation
      }
      
      return annotationView
   }
}

// MARK: - Delegate Protocol
protocol AddressViewDelegate: AnyObject {
   func addressView(didChangeAddress address: String, latitude: Double, longitude: Double)
}
