//
//  UserTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/1/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
   // MARK: - UI
   lazy var iconImageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.tintColor = .calypso
      view.heightAnchor.constraint(equalToConstant: 25).isActive = true
      view.widthAnchor.constraint(equalToConstant: 25).isActive = true
      return view
   }()
   lazy var label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      return label
   }()
   
   // MARK: - Init and Setup
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubviews([self.iconImageView, self.label])
      let marginsGuide = self.contentView.layoutMarginsGuide
      let constraint = self.iconImageView.heightAnchor.constraint(
         lessThanOrEqualTo: marginsGuide.heightAnchor, multiplier: 1)
      constraint.priority = .defaultLow
      NSLayoutConstraint.activate([
         self.iconImageView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.iconImageView.centerYAnchor.constraint(equalTo: marginsGuide.centerYAnchor),
         constraint,
         
         self.label.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.iconImageView.trailingAnchor, multiplier: 1),
         self.label.topAnchor.constraint(equalTo: marginsGuide.topAnchor),
         self.label.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor),
         self.label.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor)
      ])
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.iconImageView.tintColor = self.tintAdjustmentMode == .dimmed ? .dimmedTint : .calypso
   }
}
