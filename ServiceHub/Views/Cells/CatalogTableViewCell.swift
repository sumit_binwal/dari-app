//
//  CatalogTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CatalogTableViewCell: UITableViewCell {
   
   // MARK: - Properties
   
   var listings: [Listing] = []
   weak var delegate: CatalogTableViewCellDelegate?
   
   // MARK: - UI
   
   lazy var categoryLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .mediumFont(ofSize: 19)
      return label
   }()
   lazy var collectionView: UICollectionView = {
      let collectionView = UICollectionView(
         frame: .zero, collectionViewLayout: CollectionViewFlowLayout()
      )
      collectionView.translatesAutoresizingMaskIntoConstraints = false
      collectionView.preservesSuperviewLayoutMargins = true
      collectionView.delegate = self
      collectionView.dataSource = self
      collectionView.prefetchDataSource = self
      collectionView.heightAnchor.constraint(equalToConstant: 150).isActive = true
      collectionView.showsHorizontalScrollIndicator = false
      collectionView.register(
         ListingCollectionViewCell.self,
         forCellWithReuseIdentifier: "ListingCollectionViewCell"
      )
      return collectionView
   }()
   private lazy var borderLayer: CAShapeLayer = {
      let layer = CAShapeLayer()
      layer.backgroundColor = UIColor.separator.withAlphaComponent(0.7).cgColor
      return layer
   }()
   
   // MARK: - Initializers and Setup
   
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.selectionStyle = .none
      self.contentView.addSubviews([self.categoryLabel, self.collectionView])
      NSLayoutConstraint.activate([
         self.categoryLabel.topAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.topAnchor),
         self.categoryLabel.leadingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.leadingAnchor),
         self.categoryLabel.trailingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.trailingAnchor),
         
         self.collectionView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.categoryLabel.bottomAnchor, multiplier: 1),
         self.collectionView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.collectionView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.collectionView.bottomAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.bottomAnchor)
      ])
      self.layer.addSublayer(self.borderLayer)
   }
   
   // MARK: - Life Cycle
   
   override func layoutSubviews() {
      super.layoutSubviews()
      self.borderLayer.frame = CGRect(
         0,
         self.contentView.bounds.height - .pixel,
         self.contentView.bounds.width,
         .pixel
      )
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.borderLayer.borderColor = UIColor.subtleBorder.cgColor
   }
   
   // MARK: - Other
   
   func setCategory(_ text: String) {
      guard !Environment.languageIsArabic else {
         categoryLabel.text = text
         return
      }
      
      categoryLabel.attributedText = NSAttributedString(string: text, attributes: [.kern : -0.2])
   }
}

// MARK: - Collection View

extension CatalogTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
   func numberOfSections(in collectionView: UICollectionView) -> Int { 1 }
   
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int)
      -> Int
   {
      self.listings.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath)
      -> UICollectionViewCell
   {
      guard let cell = collectionView.dequeueReusableCell(
         withReuseIdentifier: "ListingCollectionViewCell", for: indexPath)
         as? ListingCollectionViewCell else
      {
         fatalError("[CatalogTableViewCell] Failed to dequeue ListingCollectionViewCell.")
      }
      
      let listing = self.listings[indexPath.item]
      cell.configure(for: listing)
      
      if listing.shouldDownloadImage {
         self.collectionView(collectionView, prefetchItemsAt: [indexPath])
      }
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.delegate?.catalogTableViewCell(didSelectListing: self.listings[indexPath.item])
   }
}

extension CatalogTableViewCell: UICollectionViewDataSourcePrefetching {
   func collectionView(
      _ collectionView: UICollectionView,
      prefetchItemsAt indexPaths: [IndexPath]
   ) {
      for indexPath in indexPaths {
         let listing = self.listings[indexPath.item]
         guard listing.shouldDownloadImage else { continue }
         guard let url = listing.imageURL else { continue }
         
         listing.imageDownloadTask = Environment.sessionManager.download(url: url) { url in
            guard let url = url, let data = try? Data(contentsOf: url) else { return }
            
            listing.image = UIImage(data: data)
            
            DispatchQueue.main.async {
               collectionView.reloadItems(at: [indexPath])
               listing.imageDownloadTask = nil
            }
         }
      }
   }
}

extension CatalogTableViewCell: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath)
      -> CGSize
   {
      let size: CGSize = {
         let listing = self.listings[indexPath.item]
         if let image = listing.image {
            return image.size
         } else if let size = listing.imageSize {
            return size
         } else {
            return CGSize(150, 150)
         }
      }()
      
      let aspectRatio: CGFloat = size.width / size.height
      return CGSize(aspectRatio * 150, 150)
   }
}

// MARK: - Protocol

protocol CatalogTableViewCellDelegate: AnyObject {
   func catalogTableViewCell(didSelectListing listing: Listing)
}
