//
//  DatePickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DatePickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: DatePickerTableViewCellDelegate?
   var dates: [Date] = []
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   private lazy var datePickerView: DatePickerView = {
      let view = DatePickerView(dates: self.dates)
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.datePickerView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.datePickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.datePickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.datePickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.datePickerView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func configure(withDates dates: [Date], selectedDate: Date?, instruction: String) {
      self.instructionLabel.text = instruction
      self.dates = dates
      self.datePickerView.dates = dates
      self.datePickerView.select(selectedDate)
   }
   
   func reload(dates: [Date], selectedDate: Date?) {
      self.dates = dates
      self.datePickerView.reload(dates: dates, selectedDate: selectedDate)
   }
}

// MARK: - DatePickerViewDelegate
extension DatePickerTableViewCell: DatePickerViewDelegate {
   func datePicker(didPickDate date: Date) {
      self.delegate?.datePicker(didPickDate: date)
   }
}

// MARK: - Protocol
protocol DatePickerTableViewCellDelegate: AnyObject {
   func datePicker(didPickDate date: Date)
}
