//
//  OrderTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/28/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
   private var order: Order.Summary?
   weak var delegate: OrderTableViewCellDelegate?
   
   // MARK: - UI
   private lazy var serviceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .mediumFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .natural
      label.numberOfLines = 3
      label.text = "Service"
      label.setContentHuggingPriority(.init(249), for: .horizontal)
      return label
   }()
   private lazy var totalLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .text
      label.numberOfLines = 1
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      label.setContentCompressionResistancePriority(.required, for: .horizontal)
      return label
   }()
   private lazy var headerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      
      view.addSubviews([self.serviceLabel, self.totalLabel])
      NSLayoutConstraint.activate([
         self.serviceLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.serviceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.serviceLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         
         self.totalLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.totalLabel.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.serviceLabel.trailingAnchor, multiplier: 1),
         self.totalLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         self.totalLabel.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor)
      ])
      return view
   }()
   private var deliveryLabel: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .text
      label.numberOfLines = 3
      label.tag = 20
      return label
   }
   private lazy var serviceProviderLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 14)
      label.textColor = .subtleText
      return label
   }()
   private lazy var orderIdLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 14)
      label.textColor = .subtleText
      label.setContentHuggingPriority(.init(249), for: .vertical)
      return label
   }()
   private lazy var cancelButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setAttributedTitle(
         .init(
            string: "cancel".localized, attributes: [.font: UIFont.regularFont(ofSize: 13)]),
         for: .normal
      )
      button.setContentCompressionResistancePriority(.init(248), for: .vertical)
      button.addTarget(self, action: #selector(self.didTapCancelButton), for: .touchUpInside)
      return button
   }()
   private lazy var footerView: UIView = {
      let view = UIStackView(arrangedSubviews: [self.orderIdLabel, self.cancelButton])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.distribution = .equalSpacing
      view.alignment = .fill
      return view
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.headerView,
         self.serviceProviderLabel,
         self.footerView
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.distribution = .fill
      view.alignment = .fill
      return view
   }()
   private lazy var mainView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.addSubviews([self.headerView, self.stackView])
      
      let bottomConstraint = self.stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      
      NSLayoutConstraint.activate([
         self.headerView.topAnchor.constraint(equalTo: view.topAnchor),
         self.headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         
         self.stackView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         bottomConstraint
      ])
      
      return view
   }()
   private lazy var activityIndicator: UIActivityIndicatorView = {
      let view = UIActivityIndicatorView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.color = .calypso
      view.setContentHuggingPriority(.init(200), for: .vertical)
      view.setContentCompressionResistancePriority(.init(256), for: .horizontal)
      view.hidesWhenStopped = true
      view.transform = .zeroScale
      return view
   }()
   private lazy var indicatorStackView: UIStackView = {
      let fillerView: UIView = {
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.widthAnchor.constraint(equalToConstant: .pixel).isActive = true
         return view
      }()
      
      let view = UIStackView(arrangedSubviews: [self.activityIndicator])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.distribution = .fillProportionally
      view.alignment = .center
      let constraint = view.widthAnchor.constraint(equalToConstant: 0)
      constraint.priority = .init(255)
      constraint.isActive = true
      
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.backgroundColor = .lighterBackground
      self.accessoryType = .disclosureIndicator
      self.editingAccessoryType = .none
      
      let view = self.indicatorStackView
      self.contentView.addSubviews([view, self.mainView])
      let margin = self.contentView.layoutMarginsGuide
      
      let bottomConstraint = self.mainView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      
      NSLayoutConstraint.activate([
         self.mainView.topAnchor.constraint(equalTo: margin.topAnchor),
         self.mainView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         bottomConstraint,
         
         view.topAnchor.constraint(equalTo: margin.topAnchor),
         view.leadingAnchor.constraint(equalTo: self.mainView.trailingAnchor),
         view.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         view.bottomAnchor.constraint(equalTo: self.mainView.bottomAnchor)
      ])
   }
   
   // MARK: - Actions
   @objc
   private func didTapCancelButton() {
      guard let order = self.order else { return }
      self.delegate?.orderTableViewCell(didCancel: order)
   }
   
   // MARK: Other
   func setOrder(_ order: Order.Summary) {
      self.order = order
      self.serviceLabel.text = order.serviceName
      self.totalLabel.text = order.currencyFormatter.string(from: order.total)
      
      self.setDeliveryDateLabels(order.deliveryDescription)
      
      self.serviceProviderLabel.text = order.serviceProvider.companyName
      self.serviceProviderLabel.isHidden = order.serviceProvider.isInternal
      self.orderIdLabel.text = "\("Order ID:".localized) \(order.id)"
      self.cancelButton.isHidden = !order.isCancellable
   }
   
   private func setDeliveryDateLabels(_ texts: [String]) {
      self.stackView.arrangedSubviews
         .filter { $0.tag == 20 }
         .forEach { $0.removeFromSuperview() }
      
      let labels: [UILabel] = texts.map {
         let label = self.deliveryLabel
         label.text = $0
         return label
      }
      
      self.stackView.insertArrangedSubviews(labels, at: 0)
   }
   
   func displayActivityIndicator(_ display: Bool, animated: Bool) {
      let isHidden = !display
      guard self.activityIndicator.isHidden != isHidden else { return }
      
      DispatchQueue.main.async{
         UIView.animate(
            withDuration: animated ? 0.25 : 0,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
               if isHidden { self.activityIndicator.stopAnimating() }
               else { self.activityIndicator.startAnimating() }
               self.isEditing = !isHidden
               self.activityIndicator.transform = isHidden ? .zeroScale : .identity
         }, completion: nil)
      }
   }
}

protocol OrderTableViewCellDelegate: AnyObject {
   func orderTableViewCell(didCancel order: Order.Summary)
}
