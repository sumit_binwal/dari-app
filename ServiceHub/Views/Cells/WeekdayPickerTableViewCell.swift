//
//  WeekdayPickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class WeekdayPickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: WeekdayPickerTableViewCellDelegate?
   var weekdays: [Int] = []
   
   var warningStatus: WarningStatus = .none {
      didSet { self.delegate?.pickerView(warningStatusDidChange: self.warningStatus) }
   }
   enum WarningStatus {
      case none
      case attemptToSelectMoreThanLimit
      case invalidSelectionCount
   }
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   private lazy var pickerView: MultipleSelectionPickerView = {
      let view = MultipleSelectionPickerView()
      view.delegate = self
      return view
   }()
   private lazy var selectionCountLimitWarningLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .red
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      label.isHidden = true
      label.text = "maximumWeekdaySelectionCountExceededWarning".localized
      return label
   }()
   private lazy var selectionCountInvalidWarningLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .red
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      label.isHidden = true
      label.text = "invalidWeekdaySelectionCount".localized
      return label
   }()
   
   private lazy var stackView: UIStackView = {
      let fillerView: UIView = {
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.backgroundColor = .clear
         view.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
         return view
      }()
      
      let view = UIStackView(arrangedSubviews: [
         fillerView,
         self.selectionCountLimitWarningLabel,
         self.selectionCountInvalidWarningLabel
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 5
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.pickerView, self.stackView])
      let margins = self.contentView.layoutMarginsGuide
      
      let bottomConstraint = self.stackView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      bottomConstraint.priority = .init(249)
      
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.pickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.pickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.pickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         
         self.stackView.topAnchor.constraint(equalTo: self.pickerView.bottomAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         bottomConstraint
      ])
   }
   
   // MARK: - Other
   func setValues(
      weekdays: [Int],
      selectedWeekdays: [Int],
      maximumSelectionCount: Int,
      instruction: String,
      warningStatus: WarningStatus,
      warningText: String?
   ) {
      self.weekdays = weekdays
      self.instructionLabel.text = instruction
      self.pickerView.setValues(
         items: weekdays.map { Calendar.current.weekdaySymbols[$0 - 1] },
         indicesForSelectedItems: selectedWeekdays.compactMap { weekdays.firstIndex(of: $0) },
         maximumSelectionCount: maximumSelectionCount
      )
      
      switch warningStatus {
      case .none: self.showMaximumSelectionExceededWarning(false, animated: false)
      case .attemptToSelectMoreThanLimit:
         self.showMaximumSelectionExceededWarning(false, animated: false)
      case .invalidSelectionCount:
         self.showInvalidCountWarning(true, text: warningText, animated: false)
      }
   }
   
   func showMaximumSelectionExceededWarning(_ show: Bool, animated: Bool = true) {
      let newStatus: WarningStatus = show ? .attemptToSelectMoreThanLimit : .none
      guard self.warningStatus != newStatus else { return }
      
      self.warningStatus = newStatus
      DispatchQueue.main.async {
         UIView.animate(
            withDuration: animated ? 0.25 : 0,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
               self.selectionCountLimitWarningLabel.isHidden = !show
               self.selectionCountInvalidWarningLabel.isHidden = true
         })
         (self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
      }
   }
   
   func showInvalidCountWarning(_ show: Bool, text: String? = nil, animated: Bool = true) {
      let newStatus: WarningStatus = show ? .invalidSelectionCount : .none
      guard self.warningStatus != newStatus else { return }
      
      self.warningStatus = newStatus
      DispatchQueue.main.async {
         if let text = text { self.selectionCountInvalidWarningLabel.text = text }
         UIView.animate(
            withDuration: animated ? 0.25 : 0,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
               self.selectionCountLimitWarningLabel.isHidden = true
               self.selectionCountInvalidWarningLabel.isHidden = !show
         })
         (self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
      }
   }
}

// MARK: MultipleSelectionPickerViewDelegate
extension WeekdayPickerTableViewCell: MultipleSelectionPickerViewDelegate {
   func pickerView(
      _ pickerView: MultipleSelectionPickerView,
      didSelectItemsAtIndices indices: [Int]
   ) {
      self.delegate?.pickerView(didSelectWeekdays: indices.map { self.weekdays[$0] })
   }
   
   func pickerViewHadMultipleSelectionAttemptsAboveLimit() {
      self.showMaximumSelectionExceededWarning(true)
   }
   
   func pickerViewSelectionCountDidFallUnderLimit() {
      self.showMaximumSelectionExceededWarning(false)
   }
}

// MARK: - Protocol
protocol WeekdayPickerTableViewCellDelegate: AnyObject {
   func pickerView(didSelectWeekdays weekdays: [Int])
   func pickerView(warningStatusDidChange warningStatus: WeekdayPickerTableViewCell.WarningStatus)
}
