//
//  NumberPickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class NumberPickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: NumberPickerTableViewCellDelegate?
   var numbers: [Int] = []
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   lazy var numberPickerView: NumberPickerView = {
      let view = NumberPickerView(numbers: self.numbers)
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: .default, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.numberPickerView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.numberPickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.numberPickerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.numberPickerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.numberPickerView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: Other
   func setValues(numbers: [Int], selectedNumber: Int?, instruction: String) {
      self.instructionLabel.text = instruction
      self.numbers = numbers
      self.numberPickerView.numbers = numbers
      self.numberPickerView.select(number: selectedNumber)
   }
}

extension NumberPickerTableViewCell: NumberPickerDelegate {
   func numberPicker(didSelectNumber number: Int) {
      self.delegate?.numberPicker(withTag: self.tag, didSelectNumber: number)
   }
}

protocol NumberPickerTableViewCellDelegate: AnyObject {
   func numberPicker(withTag tag: Int, didSelectNumber number: Int)
}
