//
//  ServiceVariantPickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/3/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ServiceVariantPickerTableViewCell: UITableViewCell {
   weak var delegate: ServiceVariantPickerTableViewCellDelegate?
   var services: [Service] = []
   var indexForSelectedVariant: Int?
   var rateDescriptions: [String?] = []
   
   // MARK: - UI
   private lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      label.setContentHuggingPriority(.init(249), for: .vertical)
      return label
   }()
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      layout.minimumInteritemSpacing = 10
      layout.sectionInset = .init(top: 0, left: 8, bottom: 0, right: 8)
      
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.showsVerticalScrollIndicator = false
      
      view.showsHorizontalScrollIndicator = false
      
      view.register(
         ServiceCollectionViewCell.self,
         forCellWithReuseIdentifier: "ServiceCollectionViewCell"
      )
      
      return view
   }()
   private lazy var collectionViewHeightConstraint: NSLayoutConstraint = {
      let constraint = self.collectionView.heightAnchor.constraint(equalToConstant: 70)
      constraint.priority = .defaultLow
      return constraint
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.contentView.addSubviews([self.instructionLabel, self.collectionView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.collectionView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.collectionView.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         self.collectionViewHeightConstraint
      ])
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func setValues(
      services: [Service],
      selectedService: Service?,
      rateDescriptions: [String?],
      instruction: String
   ) {
      self.services = services
      self.select(service: selectedService)
      self.rateDescriptions = rateDescriptions
      self.instructionLabel.text = instruction
      let maxCellHeight: CGFloat = {
         var maxHeight: CGFloat = 70
         guard services.count == rateDescriptions.count else { return maxHeight }
         
         for (index, service) in services.enumerated() {
            let height = ServiceCollectionViewCell.size(
               forTitle: service.titleAsVariant,
               subtitle: rateDescriptions[index] ?? ""
            ).height
            maxHeight = max(maxHeight, height)
         }
         
         return maxHeight
      }()
      self.collectionViewHeightConstraint.constant = maxCellHeight
      self.collectionView.collectionViewLayout.invalidateLayout()
      self.contentView.setNeedsLayout()
   }
   
   func select(service: Service?) {
      let row = self.services.firstIndex(of: service)
      self.indexForSelectedVariant = row
      let indexPath: IndexPath? = {
         guard let row = row else { return nil }
         return IndexPath(row: row, section: 0)
      }()
      self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
   }
}

// MARK: - Collection View
extension ServiceVariantPickerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.services.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath)
         as? ServiceCollectionViewCell else
      {
         fatalError("[ServiceVariantPickerTableViewCell] Failed to dequeue ServiceCollectionViewCell.")
      }
      cell.setValues(
         title: self.services[indexPath.item].titleAsVariant,
         rateDescription: self.rateDescriptions[indexPath.item]
      )
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.delegate?.serviceVariantPicker(didSelectService: self.services[indexPath.row])
   }
}

extension ServiceVariantPickerTableViewCell: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      let rateDescription: String = {
         guard self.services.count == self.rateDescriptions.count else { return "" }
         return self.rateDescriptions[indexPath.item] ?? ""
      }()
      let size = ServiceCollectionViewCell.size(
         forTitle: self.services[indexPath.item].titleAsVariant,
         subtitle: rateDescription
      )
      return .init(size.width, collectionView.bounds.height)
   }
}

// MARK: - Protocol
protocol ServiceVariantPickerTableViewCellDelegate: AnyObject {
   func serviceVariantPicker(didSelectService service: Service)
}
