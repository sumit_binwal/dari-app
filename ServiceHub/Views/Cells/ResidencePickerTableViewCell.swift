//
//  ResidencePickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ResidencePickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: ResidencePickerTableViewCellDelegate?
   private lazy var residenceTypes = ResidenceType.allCases
   var selectedResidenceType: ResidenceType?
   
   // MARK: - UI
   private lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   private lazy var pickerView: PickerView = {
      let view = PickerView(
         title: nil,
         items: self.residenceTypes.map { $0.name },
         icons: self.residenceTypes.map { $0.image }
      )
      view.verticalInset = 0
      view.heightAnchor.constraint(equalToConstant: 50).isActive = true
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      //self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.pickerView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.pickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.pickerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.pickerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.pickerView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func setValues(instruction: String, selectedResidenceType: ResidenceType?) {
      self.instructionLabel.text = instruction
      self.selectedResidenceType = selectedResidenceType
      self.pickerView.select(itemAtIndex: self.residenceTypes.firstIndex(of: selectedResidenceType))
   }
}

// MARK: - PickerViewDelegate
extension ResidencePickerTableViewCell: PickerViewDelegate {
   func pickerView(_ pickerView: PickerView, didSelectItemAtIndex index: Int) {
      self.delegate?.residencePicker(didPickResidenceType: self.residenceTypes[index])
   }
}

// MARK: - Protocol
protocol ResidencePickerTableViewCellDelegate: AnyObject {
   func residencePicker(didPickResidenceType residenceType: ResidenceType)
}
