//
//  ServiceCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/3/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   class var titleFont: UIFont { .mediumFont(ofSize: 17) }
   class var titleMaxLines: Int { 2 }
   class var priceFont: UIFont { .mediumFont(ofSize: 15) }
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = Self.titleFont
      label.textAlignment = .natural
      label.textColor = .darkBackgroundProminentText
      label.numberOfLines = Self.titleMaxLines
      label.text = "One-time Visit"
      return label
   }()
   private lazy var rateLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      label.textColor = .darkBackgroundText
      label.attributedText = {
         let font = UIFont.regularFont(ofSize: 13)
         
         let attributedText = NSMutableAttributedString(string: "SAR ", attributes: [.font: font])
         attributedText.append(.init(
            string: "50",
            attributes: [
               .font: Self.priceFont,
               .foregroundColor: UIColor.darkBackgroundProminentText ?? .white
            ]
         ))
         attributedText.append(.init(string: "/Hour", attributes: [.font: font]))
         return attributedText
      }()
      
      label.setContentCompressionResistancePriority(.init(751), for: .horizontal)
      
      return label
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addSubviews([self.titleLabel, self.rateLabel])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.titleLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.titleLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.titleLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.rateLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.rateLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         self.rateLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
      
      self.setSelected(self.isSelected)
   }
   
   // MARK: - Life Cycle
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   // MARK: - Other
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   func setValues(title: String, rateDescription: String?) {
      self.titleLabel.text = title
      
      
      self.rateLabel.attributedText = {
         guard let string = rateDescription else { return .init() }
         let text = NSMutableAttributedString(
            string: string,
            attributes: [.font: UIFont.regularFont(ofSize: 13)]
         )
         text.setAttributesForNumericCharacters([
            .font: UIFont.mediumFont(ofSize: 15),
            .foregroundColor: UIColor.darkBackgroundProminentText ?? .white
         ])
         return text
      }()
   }
   
   class func size(forTitle title: String, subtitle: String) -> CGSize {
      let titleText = NSAttributedString(string: title, attributes: [.font: self.titleFont])
      let detailText = NSAttributedString(string: subtitle, attributes: [.font: self.priceFont])
      
      let horizontalPadding: CGFloat = 8
      let verticalPadding: CGFloat = 8
      let spacing: CGFloat = 8
      
      let titleSize = titleText.size()
      let subtitleSize = detailText.size()
      
      let titleWidth: CGFloat = {
         let minTitleWidth: CGFloat = 150
         let maxTitleWidth: CGFloat = 190
         let titleWidth = max(titleSize.width, subtitleSize.width)
         
         if titleWidth < minTitleWidth { return minTitleWidth }
         else if titleWidth > maxTitleWidth { return maxTitleWidth }
         else { return titleWidth }
      }()
      
      let titleHeight: CGFloat = {
         if titleSize.width <= titleWidth { return titleSize.height }
         
         let multiplier = ceil(titleSize.width / titleWidth)
         return min(multiplier, CGFloat(self.titleMaxLines)) * titleSize.height
      }()
      
      let detailSize = detailText.size()
      
      return .init(
         titleWidth + 2 * horizontalPadding,
         titleHeight + spacing + detailSize.height + 2 * verticalPadding
      )
   }
}
