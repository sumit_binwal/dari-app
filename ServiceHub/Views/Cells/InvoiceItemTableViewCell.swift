//
//  InvoiceItemTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class InvoiceItemTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var stackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: [
         self.parameterDescriptionLabel,
         self.rateDescriptionLabel,
         self.visitCountLabel
      ])
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.axis = .vertical
      stack.alignment = .fill
      stack.distribution = .fillProportionally
      stack.spacing = 0
      
      let constraint = stack.heightAnchor.constraint(equalToConstant: 0)
      constraint.priority = .init(245)
      constraint.isActive = true
      
      return stack
   }()
   private lazy var headerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      
      view.addSubviews([self.headerLabel, self.priceLabel])
      NSLayoutConstraint.activate([
         self.headerLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.headerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.headerLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         
         self.priceLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.priceLabel.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.headerLabel.trailingAnchor, multiplier: 1),
         self.priceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         self.priceLabel.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor)
      ])
      return view
   }()
   private lazy var headerLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 16)
      label.textColor = .prominentText
      label.textAlignment = .natural
      label.numberOfLines = 2
      label.text = "Service"
      label.setContentHuggingPriority(.init(249), for: .horizontal)
      return label
   }()
   private lazy var priceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 16)
      label.textColor = .prominentText
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      // Having a number of lines that's unequal to headerLabel's number of lines breaks constraints
      label.numberOfLines = 2
      label.text = "SAR 100"
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      return label
   }()
   private lazy var parameterDescriptionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .subtleText
      label.textAlignment = .natural
      label.numberOfLines = 3
      label.text = "Item 1: parameters"
      return label
   }()
   private lazy var rateDescriptionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .subtleText
      label.textAlignment = .natural
      label.numberOfLines = 3
      label.text = "Item 1: parameters"
      return label
   }()
   private lazy var visitCountLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .subtleText
      label.textAlignment = .natural
      label.numberOfLines = 3
      label.text = "Item 1: parameters"
      return label
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubviews([self.headerView, self.stackView])
      let margin = self.contentView.layoutMarginsGuide
      
      NSLayoutConstraint.activate([
         self.headerView.topAnchor.constraint(equalTo: margin.topAnchor),
         self.headerView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         self.headerView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         
         self.stackView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func setValues(
      header: String,
      parameterDescription: String?,
      rateDescription: String?,
      visitCountText: String?,
      price: String
   ) {
      self.headerLabel.text = header
      self.parameterDescriptionLabel.text = parameterDescription
      self.parameterDescriptionLabel.isHidden = parameterDescription == nil
      
      let at: String = parameterDescription == nil ? "" : "@"
      self.rateDescriptionLabel.text = "\(at)\(at.isEmpty ? "" : " ")\(rateDescription ?? "")"
      self.rateDescriptionLabel.isHidden = rateDescription == nil
      
      self.visitCountLabel.text = visitCountText
      self.visitCountLabel.isHidden = visitCountText == nil
      
      self.priceLabel.text = price
   }
}
