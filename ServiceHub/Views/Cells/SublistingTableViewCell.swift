//
//  SublistingTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SublistingTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var stackView: UIStackView = {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 0
      view.addArrangedSubviews([self.titleLabel, self.subtitleLabel])
      return view
   }()
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      return label
   }()
   private lazy var subtitleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .subtleText
      label.font = .regularFont(ofSize: 14)
      return label
   }()
   
   // MARK: - Init and Setup
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.contentView.addSubview(self.stackView)
      let marginsGuide = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.stackView.topAnchor.constraint(equalTo: marginsGuide.topAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func configure(for listing: Listing) {
      self.titleLabel.text = listing.title
      self.subtitleLabel.text = listing.subtitle
      
      self.subtitleLabel.isHidden = listing.subtitle.isEmpty
   }
}
