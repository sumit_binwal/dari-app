//
//  ImageCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   var isSelectable: Bool = false
   override var isSelected: Bool {
      didSet {
         guard self.isSelectable else { return }
         self.setSelected(self.isSelected)
      }
   }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   lazy var imageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.tintColor = .darkBackgroundProminentText
      view.contentMode = .center
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.imageView)
      
      self.setSelected(self.isSelected)
   }
   
   // MARK: - Other
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   func setValues(image: UIImage) {
      self.imageView.image = image
   }
}
