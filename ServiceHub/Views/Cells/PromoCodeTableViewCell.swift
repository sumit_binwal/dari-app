//
//  PromoCodeTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/28/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PromoCodeTableViewCell: UITableViewCell {
   weak var delegate: PromoCodeTableViewCellDelegate?
   var state: State = .unvalidatedCode {
      didSet {
         self.delegate?.promoCodeView(
            stateDidChange: self.state,
            code: self.promoCodeTextField.text
         )
      }
   }
   
   // MARK: - UI
   private lazy var promoCodeTextField: UITextField = {
      let field = UITextField()
      field.translatesAutoresizingMaskIntoConstraints = false
      field.textContentType = .none
      field.autocorrectionType = .no
      field.autocapitalizationType = .none
      field.font = .regularFont(ofSize: 17)
      field.textColor = .prominentText
      field.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
      field.delegate = self
      field.attributedPlaceholder = .init(
         string: "promoCode".localized,
         attributes: [.foregroundColor: UIColor.gray]
      )
      return field
   }()
   private lazy var applyButton: UIButton = {
      let button = ProminentButton()
      button.layer.cornerRadius = 5
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setTitle("apply".localized, for: .normal)
      button.addTarget(self, action: #selector(self.didTapApplyButton), for: .touchUpInside)
      button.setContentHuggingPriority(.required, for: .horizontal)
      button.heightAnchor.constraint(equalToConstant: 30).isActive = true
      button.isEnabled = false
      return button
   }()
   private lazy var discountLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .red
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      label.text = "SAR 400"
      label.setContentHuggingPriority(.required, for: .horizontal)
      label.isHidden = true
      return label
   }()
   private lazy var activityIndicator: UIActivityIndicatorView = {
      let indicator = UIActivityIndicatorView()
      indicator.translatesAutoresizingMaskIntoConstraints = false
      indicator.tintColor = .calypso
      indicator.hidesWhenStopped = false
      indicator.isHidden = true
      return indicator
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.promoCodeTextField,
         self.applyButton,
         self.discountLabel,
         self.activityIndicator
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 5
      return view
   }()
   /// Ideally, this view should not exist; the error stack view should hold all other views. However, this leads to an autolayout constraint issue that I'm unsure how to resolve yet.
   private lazy var fillerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
      return view
   }()
   private lazy var errorMessageLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .red
      label.numberOfLines = 3
      label.text = "error message"
      label.font = .regularFont(ofSize: 16)
      label.setContentCompressionResistancePriority(.init(248), for: .vertical)
      label.isHidden = true
      return label
   }()
   private lazy var errorStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.fillerView, self.errorMessageLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 0
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubviews([self.stackView, self.errorStackView])
      let margins = self.contentView.layoutMarginsGuide
      
      let bottomConstraint = self.errorStackView.bottomAnchor.constraint(
         equalTo: margins.bottomAnchor
      )
      bottomConstraint.priority = .init(249)
      
      NSLayoutConstraint.activate([
         self.stackView.topAnchor.constraint(equalTo: margins.topAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.errorStackView.topAnchor.constraint(equalTo: self.stackView.bottomAnchor),
         self.errorStackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.errorStackView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         bottomConstraint
      ])
   }
   
   // MARK: - Actions
   @objc
   private func didTapApplyButton() {
      self.submitPromoCode()
   }
   
   @objc
   private func textFieldDidChange() {
      self.applyButton.isEnabled = !self.promoCodeTextField.isEmpty
      
      self.displayErrorMessage(nil)
      if self.applyButton.isHidden {
         self.toggleDiscountHidden(true)
      }
      
      self.state = .unvalidatedCode
   }
   
   // MARK: - Other
   private func submitPromoCode() {
      guard let code = self.promoCodeTextField.text else { return }
      
      DispatchQueue.main.async {
         self.firstResponder?.resignFirstResponder()
         self.showActivityIndicator()
         self.delegate?.promoCodeView(didSubmitCode: code)
      }
   }
   
   func setValues(code: String?, state: State, animated: Bool = false) {
      self.promoCodeTextField.text = code
      self.applyButton.isEnabled = !(code ?? "").isEmpty
      self.state = state
      
      switch state {
      case .unvalidatedCode:
         self.toggleDiscountHidden(true, animated: animated)
         self.displayErrorMessage(nil, animated: animated)
         
      case .validatedCode(code: let promoCode, discount: let discount):
         self.promoCodeTextField.text = promoCode.code
         self.discountLabel.text = NumberFormatter.currency.string(from: discount)
         self.toggleDiscountHidden(false, animated: animated)
         
      case .error(message: let message):
         self.toggleDiscountHidden(true, animated: animated)
         self.displayErrorMessage(message, animated: animated)
         
      case .validating: self.showActivityIndicator(animated: animated)
      }
   }
   
   private func toggleDiscountHidden(_ isHidden: Bool, animated: Bool = true) {
      let changes: () -> Void = {
         self.activityIndicator.isHidden = true
         self.applyButton.isHidden = !isHidden
         self.discountLabel.isHidden = isHidden
      }
      
      DispatchQueue.main.async {
         self.activityIndicator.stopAnimating()
         
         if animated {
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  changes()
            }, completion: { _ in
               // FIXME: For some reason, the animation doesn't work without this.
               self.discountLabel.isHidden = isHidden
            })
         } else {
            changes()
         }
      }
   }
   
   private func showActivityIndicator(animated: Bool = true) {
      let changes: () -> Void = {
         self.activityIndicator.isHidden = false
         self.applyButton.isHidden = true
         self.discountLabel.isHidden = true
      }
      
      DispatchQueue.main.async {
         self.activityIndicator.startAnimating()
         
         if animated {
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  changes()
            }, completion: nil)
         } else {
            changes()
         }
      }
   }
   
   func displayErrorMessage(_ message: String?, animated: Bool = true) {
      guard self.errorMessageLabel.text != message else { return }
      
      DispatchQueue.main.async {
         if animated {
            if message != nil { self.errorMessageLabel.text = message }
            
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  self.errorMessageLabel.isHidden = message == nil
            }, completion: { _ in
               if message == nil { self.errorMessageLabel.text = message }
            })
            
         } else {
            self.errorMessageLabel.text = message
            self.errorMessageLabel.isHidden = message == nil
         }
         
         // No longer necessary because changing state triggers this fumction in the table view itself.
         //(self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
         
      }
   }
}

// MARK: - UITextFieldDelegate
extension PromoCodeTableViewCell: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      self.submitPromoCode()
      return true
   }
}

// MARK: - Status
extension PromoCodeTableViewCell {
   enum State {
      case unvalidatedCode
      case validatedCode(code: Order.PromoCode, discount: Double)
      case error(message: String)
      case validating
   }
}

// MARK: - Protocol
protocol PromoCodeTableViewCellDelegate: AnyObject {
   func promoCodeView(didSubmitCode code: String)
   func promoCodeView(stateDidChange state: PromoCodeTableViewCell.State, code: String?)
}
