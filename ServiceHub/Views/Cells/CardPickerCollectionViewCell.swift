//
//  CardPickerCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardPickerCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   private lazy var dotsLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .darkBackgroundText
      label.font = .lightFont(ofSize: 10)
      label.textAlignment = .left
      label.text = "••••"
      label.setContentHuggingPriority(.init(251), for: .horizontal)
      label.setContentHuggingPriority(.init(249), for: .vertical)
      return label
   }()
   private lazy var numberLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textAlignment = .left
      label.textColor = .darkBackgroundProminentText
      label.font = .regularFont(ofSize: 17)
      return label
   }()
   private lazy var schemeImageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.tintColor = .darkBackgroundProminentText
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addSubviews([self.dotsLabel, self.numberLabel, self.schemeImageView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.dotsLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.dotsLabel.leftAnchor.constraint(equalTo: margins.leftAnchor),
         self.dotsLabel.bottomAnchor.constraint(equalTo: self.numberLabel.bottomAnchor),
         
         self.numberLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.numberLabel.leftAnchor.constraint(equalTo: self.dotsLabel.rightAnchor),
         self.numberLabel.rightAnchor.constraint(equalTo: margins.rightAnchor),
         
         self.schemeImageView.rightAnchor.constraint(equalTo: margins.rightAnchor),
         self.schemeImageView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
      
      self.setSelected(self.isSelected)
   }
   
   // MARK: - Other
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   func setValues(last4: String, schemeImage: UIImage?) {
      self.numberLabel.text = last4
      self.schemeImageView.image = schemeImage
   }
}
