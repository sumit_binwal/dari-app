//
//  TimeSlotCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class TimeSlotCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   class var font: UIFont { .mediumFont(ofSize: 17) }
   class var amPmFont: UIFont { .regularFont(ofSize: 11) }
   class var spacing: CGFloat { 0 }
   
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   lazy var nameLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.textColor = .darkBackgroundProminentText
      label.text = "Name"
      return label
   }()
   lazy var rangeLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.textColor = .darkBackgroundProminentText
      label.text = "Range"
      return label
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.nameLabel, self.rangeLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillEqually
      view.spacing = Self.spacing
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addSubview(self.stackView)
      NSLayoutConstraint.activate([
         self.stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
         self.stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
      ])
      
      self.setSelected(self.isSelected)
   }
   
   // MARK: - Other
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   func setValue(timeSlot: TimeSlot) {
      self.nameLabel.text = timeSlot.name
      self.rangeLabel.attributedText = Self.attributedRangeText(forTimeSlot: timeSlot)
   }
   
   private class func attributedRangeText(forTimeSlot timeSlot: TimeSlot) -> NSAttributedString {
      let text = NSMutableAttributedString(attributedString: DateFormatter.attributeText(
         forHour: timeSlot.startingHour,
         hourFont: Self.font,
         apPmFont: Self.amPmFont
      ))
      text.append(NSAttributedString(string: " - ", attributes: [
         .font: UIFont.lightFont(ofSize: Self.font.pointSize)
      ]))
      text.append(DateFormatter.attributeText(
         forHour: timeSlot.endingHour,
         hourFont: Self.font,
         apPmFont: Self.amPmFont
      ))
      return text
   }
   
   class func width(forTimeSlot timeSlot: TimeSlot) -> CGFloat {
      let nameText = NSAttributedString(string: timeSlot.name, attributes: [.font: self.font])
      let rangeText = self.attributedRangeText(forTimeSlot: timeSlot)
      return max(nameText.size().width, rangeText.size().width) + 30
   }
}
