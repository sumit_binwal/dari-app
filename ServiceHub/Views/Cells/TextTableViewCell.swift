//
//  TextTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {
   weak var delegate: TextTableViewCellDelegate?
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   lazy var textView: TextView = {
      let view = TextView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.textColor = .text
      view.backgroundColor = .lighterBackground
      view.textContainerInset = .init(top: 8, left: 5, bottom: 8, right: 5)
      view.isScrollEnabled = false
      view.setContentHuggingPriority(.init(245), for: .vertical)
      view.font = .regularFont(ofSize: 15)
      view.delegate = self
      
      view.layer.cornerRadius = .cornerRadius
      view.layer.masksToBounds = true
      view.layer.borderColor = UIColor.subtleBorder.withAlphaComponent(0.4).cgColor
      view.layer.borderWidth = .pixel
      
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.textView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.textView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.textView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.textView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         self.textView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: - Life Cycle
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.textView.layer.borderColor = UIColor.subtleBorder.withAlphaComponent(0.4).cgColor
   }
   
   // MARK: - Other
   func configure(withText text: String?, instruction: String) {
      self.instructionLabel.text = instruction
      self.textView.text = text
   }
}

// MARK: - UITextViewDelegate
extension TextTableViewCell: UITextViewDelegate {
   func textViewDidChange(_ textView: UITextView) {
      self.delegate?.textTableViewCell(didChangeText: textView.text)
      
      guard abs(textView.intrinsicContentSize.height - textView.bounds.height) > 2 else { return }
      DispatchQueue.main.async {
         (self.superview as? UITableView)?.performBatchUpdates(nil, completion: nil)
      }
   }
}

// MARK: - Protocol
protocol TextTableViewCellDelegate: AnyObject {
   func textTableViewCell(didChangeText text: String)
}
