//
//  AnnouncementTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AnnouncementTableViewCell: UITableViewCell {
   static var normalBackgroundColor: UIColor { .unhighlightedBackground }
   static var highlightedBackgroundColor: UIColor { .highlightedBackground }
   
   // MARK: - UI
   lazy var enclosingView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.layer.cornerRadius = .cornerRadius
      
      view.addSubview(stackView)
      NSLayoutConstraint.activate([
         self.stackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
      ])
      
      return view
   }()
   lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.titleLabel, self.detailLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      return view
   }()
   lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .darkBackgroundProminentText
      label.font = .mediumFont(ofSize: 17)
      label.numberOfLines = 2
      label.textAlignment = .center
      return label
   }()
   lazy var detailLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .darkBackgroundProminentText
      label.font = .regularFont(ofSize: 15)
      label.numberOfLines = 0
      label.textAlignment = .center
      return label
   }()
   
   // MARK: Initialization and Setup
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.selectionStyle = .none
      self.contentView.backgroundColor = .clear
      
      self.contentView.addSubview(self.enclosingView)
      NSLayoutConstraint.activate([
         self.enclosingView.topAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.topAnchor),
         self.enclosingView.leadingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.leadingAnchor),
         self.enclosingView.trailingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.trailingAnchor),
         self.enclosingView.bottomAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.bottomAnchor)
      ])
      
   }
   
   // MARK: - Life Cycle
   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      self.setAppearance(highlighted: selected, animated: animated)
   }
   
   override func setHighlighted(_ highlighted: Bool, animated: Bool) {
      super.setHighlighted(highlighted, animated: animated)
      self.setAppearance(highlighted: highlighted, animated: true)
   }
   
   // MARK: - Other
   private func setAppearance(highlighted: Bool, animated: Bool) {
      UIView.animate(withDuration: animated ? .highlightAnimationDuration : 0) {
         self.enclosingView.backgroundColor = highlighted ?
            Self.highlightedBackgroundColor : Self.normalBackgroundColor
         self.transform = highlighted ? .highlightTransformForLargeViews : .identity
      }
   }
   
   func configure(for error: Error) {
      self.titleLabel.text = ""
      self.titleLabel.isHidden = true
      
      self.detailLabel.text = error.localizedDescription
   }
   
   func configure(for announcement: Announcement) {
      self.titleLabel.text = announcement.title
      self.titleLabel.isHidden = announcement.title.isEmpty
      
      self.detailLabel.text = announcement.body
   }
   
   func jiggle() {
      let scaleValues = [1.0, 1.04, 1.0]
      let calculationMode = CAAnimationCalculationMode.cubic
      let xScaleAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
         animation.valueFunction = CAValueFunction(name: .scaleX)
         animation.values = scaleValues
         animation.calculationMode = calculationMode
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.25
         return animation
      }()
      let yScaleAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
         animation.valueFunction = CAValueFunction(name: .scaleY)
         animation.values = scaleValues
         animation.isAdditive = true
         animation.calculationMode = calculationMode
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.25
         return animation
      }()
      let wobbleAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
         animation.valueFunction = CAValueFunction(name: .rotateZ)
         let wobbleAngle: Float = .pi / 288
         animation.values = [0.0, wobbleAngle, 0, -wobbleAngle, 0]
         animation.isAdditive = true
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.05
         animation.calculationMode = calculationMode
         return animation
      }()
      let highlightAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.backgroundColor))
         animation.values = [Self.normalBackgroundColor.cgColor,
                             Self.highlightedBackgroundColor.cgColor,
                             Self.normalBackgroundColor.cgColor]
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.25
         animation.calculationMode = calculationMode
         return animation
      }()
      
      let animationGroup = CAAnimationGroup()
      animationGroup.animations = [xScaleAnimation, yScaleAnimation, wobbleAnimation, highlightAnimation]
      animationGroup.isRemovedOnCompletion = true
      animationGroup.duration = 0.5
      self.enclosingView.layer.add(animationGroup, forKey: nil)
   }
}
