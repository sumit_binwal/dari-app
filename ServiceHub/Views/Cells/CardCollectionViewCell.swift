//
//  CardCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
   // MARK: - UI
   private lazy var schemeLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .subtleText
      label.textAlignment = .left
      label.setContentHuggingPriority(UILayoutPriority(200), for: .horizontal)
      label.font = .mediumFont(ofSize: 17)
      return label
   }()
   private lazy var expiryLabel: UILabel = {
      let label = WarningLabel()
      label.text = "expired".localized.localizedUppercase
      return label
   }()
   
   private lazy var last4Label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      label.setContentHuggingPriority(UILayoutPriority(100), for: .vertical)
      return label
   }()
   private var dotsLabel: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = self.last4Label.textColor
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.text = "••••"
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      label.setContentHuggingPriority(UILayoutPriority(100), for: .vertical)
      return label
   }
   private lazy var numberStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.dotsLabel,
         self.dotsLabel,
         self.dotsLabel,
         self.last4Label,
         self.fillerView
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fill
      view.spacing = 8
      return view
   }()
   
   private lazy var schemeImageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      view.setContentHuggingPriority(.defaultHigh, for: .vertical)
      view.contentMode = .bottomRight
      view.tintColor = .prominentText
      return view
   }()
   private var fillerView: UIView {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .clear
      view.setContentHuggingPriority(UILayoutPriority(rawValue: 1), for: .horizontal)
      return view
   }
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addSubviews([
         self.schemeLabel,
         self.numberStackView,
         self.expiryLabel,
         self.schemeImageView
      ])
      let marginsGuide = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.schemeLabel.topAnchor.constraint(equalTo: marginsGuide.topAnchor),
         self.schemeLabel.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.schemeLabel.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         
         self.numberStackView.centerYAnchor.constraint(equalTo: marginsGuide.centerYAnchor),
         self.numberStackView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.numberStackView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         
         self.expiryLabel.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.expiryLabel.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor),
         
         self.schemeImageView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         self.schemeImageView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func configure(with card: Card) {
      self.schemeLabel.text = card.schemeName
      self.last4Label.text = card.last4
      self.schemeImageView.image = card.image
      self.expiryLabel.isHidden = !card.isExpired
   }
}
