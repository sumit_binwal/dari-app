//
//  PickerCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/20/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PickerCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   class var horizontalSpacing: CGFloat { 10 }
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   lazy var imageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.tintColor = .darkBackgroundProminentText
      view.contentMode = .center
      return view
   }()
   lazy var label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.textColor = .darkBackgroundProminentText
      return label
   }()
   lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.imageView, self.label])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = Self.horizontalSpacing
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      
      // Autoresizing masks are removed in order to resolve an issue that occurs when the collection view is shown/hidden in a stack view.
      self.translatesAutoresizingMaskIntoConstraints = false
      self.contentView.translatesAutoresizingMaskIntoConstraints = false
      self.addFillingSubview(self.contentView)
      
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.setUpViews()
      self.setSelected(self.isSelected)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   func setUpViews() {
      self.contentView.addSubview(self.stackView)
      NSLayoutConstraint.activate([
         self.stackView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
         self.stackView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
      ])
   }
   
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   // MARK: - Life Cycle
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   // MARK: - Other
   func setValues(
      text: String? = nil,
      attributedText: NSAttributedString? = nil,
      image: UIImage? = nil
   ) {
      if let attributedText = attributedText {
         self.label.attributedText = attributedText
      } else {
         self.label.text = text
      }
      
      self.imageView.image = image
      self.imageView.isHidden = image == nil
   }
}
