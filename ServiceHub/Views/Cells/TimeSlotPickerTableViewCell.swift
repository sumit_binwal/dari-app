//
//  TimeSlotPickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class TimeSlotPickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: TimeSlotPickerViewDelegate?
   var timeSlots: [TimeSlot] = []
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   private lazy var timeSlotPickerView: TimeSlotPickerView = {
      let view = TimeSlotPickerView()
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.timeSlotPickerView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.timeSlotPickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.timeSlotPickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.timeSlotPickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.timeSlotPickerView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func setValues(timeSlots: [TimeSlot], selectedTimeSlot: TimeSlot?, instruction: String) {
      self.instructionLabel.text = instruction
      self.timeSlotPickerView.timeSlots = timeSlots
      self.timeSlotPickerView.select(selectedTimeSlot)
   }
   
   func reload(timeSlots: [TimeSlot], selectedTimeSlot: TimeSlot?) {
      self.timeSlots = timeSlots
      self.timeSlotPickerView.reload(timeSlots: timeSlots, selectedTimeSlot: selectedTimeSlot)
   }
}

// MARK: - DatePickerViewDelegate
extension TimeSlotPickerTableViewCell: TimeSlotPickerViewDelegate {
   func timeSlotPicker(didSelectTimeSlot timeSlot: TimeSlot) {
      self.delegate?.timeSlotPicker(didSelectTimeSlot: timeSlot)
   }
}

// MARK: - Protocol
protocol TimeSlotPickerTableViewCellDelegate: AnyObject {
   func timeSlotPicker(didSelectTimeSlot timeSlot: TimeSlot)
}
