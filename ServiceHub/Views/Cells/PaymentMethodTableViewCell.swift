//
//  PaymentMethodTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: PaymentMethodPickerViewDelegate? {
      get { self.paymentMethodPickerView.delegate }
      set { self.paymentMethodPickerView.delegate = newValue }
   }
   
   // MARK: UI
   private lazy var paymentMethodPickerView: PaymentMethodPickerView = {
      let view = PaymentMethodPickerView()
      view.preservesSuperviewLayoutMargins = true
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubview(self.paymentMethodPickerView)
      
      let margins = self.contentView.layoutMarginsGuide
      let bottomConstraint = self.paymentMethodPickerView.bottomAnchor.constraint(
         equalTo: margins.bottomAnchor)
      bottomConstraint.priority = .init(249)
      
      NSLayoutConstraint.activate([
         self.paymentMethodPickerView.topAnchor.constraint(equalTo: margins.topAnchor),
         self.paymentMethodPickerView.leadingAnchor.constraint(
            equalTo: self.contentView.leadingAnchor),
         self.paymentMethodPickerView.trailingAnchor.constraint(
            equalTo: self.contentView.trailingAnchor),
         bottomConstraint
      ])
   }
   
   // MARK: - Other
   func showCardPicker(_ show: Bool, animated: Bool, completion: (() -> Void)? = nil) {
      self.paymentMethodPickerView.showCardPicker(show, animated: animated, completion: completion)
   }
   
   func setValues(
      paymentMethods: [PaymentMethod],
      fees: [Double?],
      selectedPaymentMethod: PaymentMethod?,
      cards: [Card],
      selectedCard: Card?
   ) {
      self.paymentMethodPickerView.setValues(
         paymentMethods: paymentMethods,
         fees: fees,
         selectedPaymentMethod: selectedPaymentMethod,
         cards: cards,
         selectedCard: selectedCard
      )
   }
   
   func addCard(_ card: Card) { self.paymentMethodPickerView.addCard(card) }
   
   func select(_ card: Card?) { self.paymentMethodPickerView.select(card) }
   
   func removeCards(_ cards: [Card]) {
      paymentMethodPickerView.removeCards(cards)
   }
}
