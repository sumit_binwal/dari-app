//
//  HourPickerTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/26/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class HourPickerTableViewCell: UITableViewCell {
   // MARK: - Properties
   weak var delegate: HourPickerTableViewCellDelegate?
   var hours: [Int] = []
   
   // MARK: - UI
   lazy var instructionLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 3
      label.textAlignment = .natural
      return label
   }()
   private lazy var hourPickerView: HourPickerView = {
      let view = HourPickerView(hours: self.hours)
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      
      self.contentView.addSubviews([self.instructionLabel, self.hourPickerView])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.instructionLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.instructionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.instructionLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.hourPickerView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.instructionLabel.bottomAnchor, multiplier: 1),
         self.hourPickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.hourPickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.hourPickerView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func configure(withHours hours: [Int], selectedHour: Int?, instruction: String) {
      self.instructionLabel.text = instruction
      self.hours = hours
      self.hourPickerView.hours = hours
      self.hourPickerView.select(hour: selectedHour)
   }
   
   func reload(hours: [Int], selectedHour: Int?) {
      self.hours = hours
      self.hourPickerView.reload(hours: hours, selectedHour: selectedHour)
   }
}

// MARK: - HourPickerViewDelegate
extension HourPickerTableViewCell: HourPickerViewDelegate {
   func pickerView(didSelectHour hour: Int) {
      self.delegate?.pickerView(didSelectHour: hour)
   }
}

// MARK: - Protocol
protocol HourPickerTableViewCellDelegate: AnyObject {
   func pickerView(didSelectHour hour: Int)
}
