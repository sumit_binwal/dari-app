//
//  ListTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/3/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class ListTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var stackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: [self.label])
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.axis = .vertical
      stack.alignment = .fill
      stack.distribution = .fillEqually
      stack.spacing = 0//Self.detailSpacing
      return stack
   }()
   private var label: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .subtleText
      label.text = "• "
      label.numberOfLines = 3
      return label
   }
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.selectionStyle = .none
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.stackView)
   }
   
   // MARK: - Other
   func setValues(details: [String]) {
      self.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
      
      self.stackView.addArrangedSubviews(details.map {
         let label = self.label
         label.text = "• \($0)"
         return label
      })
   }
}
