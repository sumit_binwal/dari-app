//
//  NumberCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class NumberCollectionViewCell: UICollectionViewCell {
   lazy var label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.textColor = .darkBackgroundProminentText
      return label
   }()
   
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         UIView.animate(
            withDuration: .highlightAnimationDuration,
            delay: 0,
            options: .highlightAnimationCurve,
            animations: {
               self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
         }, completion: nil)
      }
   }
   
   override init(frame: CGRect) {
      super.init(frame: .zero)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.label)
      self.setSelected(self.isSelected)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setSelected(_ selected: Bool) {
      UIView.animate(withDuration: 0) {
         self.layer.borderWidth = selected ? 4 : 0
      }
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
}

@available(*, deprecated)
class DeprecatedNumberCollectionViewCell: UICollectionViewCell {
    var number: Int = 0 { didSet{ numberLabel.text = NumberFormatter.integer.string(from: number) } }
    @IBOutlet weak var numberLabel: UILabel!
    
    override var isSelected: Bool { didSet {setUpColors(selected: isSelected) } }
    override var isHighlighted: Bool { didSet {setUpColors(selected: isHighlighted) } }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = .cornerRadius
        layer.masksToBounds = true
        setUpColors(selected: isSelected)
    }

    private func setUpColors(selected: Bool) {
        numberLabel.backgroundColor = selected ? .highlightedBackground : .unhighlightedBackground
        numberLabel.textColor = selected ? .tint : .darkBackgroundProminentText
        let pointSize = numberLabel.font.pointSize
        numberLabel.font = selected ?
            UIFont.mediumFont(ofSize: pointSize) : UIFont.regularFont(ofSize: pointSize)
    }
}
