//
//  InvoiceSummaryItemTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/28/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class InvoiceSummaryItemTableViewCell: UITableViewCell {
   // MARK: - UI
   lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .prominentText
      label.textAlignment = .natural
      label.text = "Summary Item"
      return label
   }()
   lazy var priceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .prominentText
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      label.text = "SAR 400"
      label.setContentHuggingPriority(.init(251), for: .horizontal)
      return label
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   fileprivate func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubviews([self.titleLabel, self.priceLabel])
      let margins = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.titleLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.titleLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.titleLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         
         self.priceLabel.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.titleLabel.trailingAnchor, multiplier: 1),
         self.priceLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.priceLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         self.priceLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   // MARK: - Other
   func setValues(item: String, price: String) {
      self.titleLabel.text = item
      self.priceLabel.text = price
   }
}

class InvoicePrimarySummaryItemTableViewCell: InvoiceSummaryItemTableViewCell {
   override fileprivate func setUp() {
      super.setUp()
      self.titleLabel.font = .mediumFont(ofSize: 18)
      self.titleLabel.textColor = .prominentText
      self.priceLabel.font = .boldFont(ofSize: 18)
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.priceLabel.textColor = self.tintColor
   }
}
