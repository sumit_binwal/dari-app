//
//  DetailedPickerCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DetailedPickerCollectionViewCell: PickerCollectionViewCell {
   // MARK: - UI
   class var verticalSpacing: CGFloat { 0 }
   lazy var detailLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textAlignment = .center
      label.textColor = .darkBackgroundText
      return label
   }()
   lazy var outerStackView: UIView = {
      let view = UIStackView(arrangedSubviews: [self.stackView, self.detailLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 0
      return view
   }()
   
   // MARK: Setup
   override func setUpViews() {
      self.stackView.spacing = 0
      self.contentView.addSubview(self.outerStackView)
      NSLayoutConstraint.activate([
         self.outerStackView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
         self.outerStackView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
      ])
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      self.stackView.spacing = Self.horizontalSpacing
   }
   
   // MARK: - Other
   func setValues(
      text: String? = nil,
      attributedText: NSAttributedString? = nil,
      image: UIImage? = nil,
      detailText: String? = nil
   ) {
      super.setValues(text: text, attributedText: attributedText, image: image)
      self.detailLabel.text = detailText
      self.detailLabel.isHidden = detailText == nil
   }
}
