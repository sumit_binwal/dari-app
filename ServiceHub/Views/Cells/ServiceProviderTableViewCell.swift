//
//  ServiceProviderTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ServiceProviderTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var logoImageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.contentMode = .scaleAspectFit
      
      NSLayoutConstraint.activate([
         view.widthAnchor.constraint(equalToConstant: 20),
         view.heightAnchor.constraint(equalToConstant: 20)
      ])
      return view
   }()
   private lazy var nameLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      return label
   }()
   private lazy var stackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: [self.logoImageView, self.nameLabel])
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.axis = .horizontal
      stack.distribution = .fill
      stack.alignment = .center
      stack.spacing = 5
      return stack
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.contentView.addFillingSubviewConstrainedByMargins(self.stackView)
      self.accessoryType = .disclosureIndicator
      self.backgroundColor = .lighterBackground
   }
   
   // MARK: - Other
   func setValues(serviceProvider: ServiceProvider) {
      self.logoImageView.image = serviceProvider.logo
      self.logoImageView.isHidden = serviceProvider.logo == nil
      self.nameLabel.text = serviceProvider.companyName
   }
   
   func set(logo: UIImage?) {
      self.logoImageView.image = logo
      
      guard self.logoImageView.isHidden else { return }
      
      UIView.animate(
         withDuration: 0.25,
         delay: 0,
         options: .curveEaseInOut,
         animations: {
            self.logoImageView.isHidden = logo == nil
      },
         completion: nil
      )
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.logoImageView.tintColor = self.tintAdjustmentMode == .dimmed ? .dimmedTint : .calypso
   }
}
