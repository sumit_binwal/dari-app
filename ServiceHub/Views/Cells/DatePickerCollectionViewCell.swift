//
//  DatePickerCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DatePickerCollectionViewCell: UICollectionViewCell {
   // MARK: - Properties
   override var isSelected: Bool { didSet { self.setSelected(self.isSelected) } }
   override var isHighlighted: Bool {
      didSet {
         DispatchQueue.main.async{
            UIView.animate(
               withDuration: .highlightAnimationDuration,
               delay: 0,
               options: .highlightAnimationCurve,
               animations: {
                  self.transform = self.isHighlighted ? .highlightTransformForSmallViews : .identity
            }, completion: nil)
         }
      }
   }
   
   // MARK: - UI
   lazy var monthLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 11)
      label.textAlignment = .center
      label.textColor = .darkBackgroundText
      label.minimumScaleFactor = 0.8
      label.adjustsFontSizeToFitWidth = true
      return label
   }()
   lazy var dayLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .mediumFont(ofSize: 17)
      label.textAlignment = .center
      label.textColor = .darkBackgroundProminentText
      return label
   }()
   lazy var weekdayLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 11)
      label.textAlignment = .center
      label.textColor = .darkBackgroundText
      label.minimumScaleFactor = 0.8
      label.adjustsFontSizeToFitWidth = true
      return label
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.weekdayLabel, self.dayLabel, self.monthLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillEqually
      view.spacing = 0
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.backgroundColor = .unhighlightedBackground
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.stackView)
      self.setSelected(self.isSelected)
   }
   
   // MARK: - Other
   private func setSelected(_ selected: Bool) {
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0) {
            self.layer.borderWidth = selected ? 4 : 0
         }
      }
   }
   
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.layer.borderColor = self.tintColor.cgColor
   }
   
   func configure(withDate date: Date) {
      self.monthLabel.text = date.shortMonthSymbol.uppercased()
      self.dayLabel.text = date.day
      self.weekdayLabel.text = date.weekdaySymbol.uppercased()
   }
}
