//
//  OrderCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class OrderCollectionViewCell: UICollectionViewCell {
   private lazy var serviceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      return label
   }()
}
