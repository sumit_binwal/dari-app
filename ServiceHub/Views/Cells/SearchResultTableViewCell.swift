//
//  SearchResultTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/15/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.iv, self.labelStackView])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .center
      view.distribution = .fillProportionally
      view.spacing = 8
      return view
   }()
   private lazy var iv: UIImageView = {
      let view = UIImageView()
      view.contentMode = .scaleAspectFit
      view.widthAnchor.constraint(equalToConstant: 33).isActive = true
      return view
   }()
   private lazy var labelStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.titleLabel, self.subtitleLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      return view
   }()
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      return label
   }()
   private lazy var subtitleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .subtleText
      label.font = .regularFont(ofSize: 14)
      return label
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.stackView)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func configure(forItem item: Searchable) {
      self.titleLabel.text = item.title
      
      self.iv.image = item.image
      self.iv.isHidden = item.image == nil
      
      self.subtitleLabel.text = item.subtitle
      self.subtitleLabel.isHidden = item.subtitle == nil
   }
}
