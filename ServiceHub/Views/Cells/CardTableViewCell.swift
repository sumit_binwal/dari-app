//
//  CardTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {
   // MARK: - UI
   private lazy var schemeLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .subtleText
      label.textAlignment = .left
      label.setContentHuggingPriority(UILayoutPriority(200), for: .horizontal)
      label.font = .mediumFont(ofSize: 17)
      return label
   }()
   private lazy var expiryLabel: UILabel = {
      let label = WarningLabel()
      label.text = "expired".localized.localizedUppercase
      return label
   }()
   private lazy var topStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.schemeLabel, self.expiryLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .center
      view.distribution = .fill
      view.spacing = 8
      return view
   }()
   
   private lazy var last4Label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      return label
   }()
   private var dotsLabel: UILabel {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = self.last4Label.textColor
      label.font = .regularFont(ofSize: 17)
      label.textAlignment = .center
      label.text = "••••"
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      return label
   }
   private lazy var numberStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.dotsLabel,
         self.dotsLabel,
         self.dotsLabel,
         self.last4Label,
         self.fillerView
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fill
      view.spacing = 8
      return view
   }()
   
   private lazy var schemeImageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      view.tintColor = .prominentText
      return view
   }()
   private var fillerView: UIView {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .clear
      view.setContentHuggingPriority(UILayoutPriority(rawValue: 1), for: .horizontal)
      return view
   }
   private lazy var bottomStackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.fillerView, self.schemeImageView])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fill
      return view
   }()
   
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [
         self.topStackView,
         self.numberStackView,
         self.bottomStackView
      ])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .equalCentering
      view.spacing = 8
      return view
   }()
   
   // MARK: - Init
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addFillingSubviewConstrainedByMargins(self.stackView)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func configure(with card: Card) {
      self.schemeLabel.text = card.schemeName
      self.last4Label.text = card.last4
      self.schemeImageView.image = card.templateImage
      self.expiryLabel.isHidden = !card.isExpired
   }
}
