//
//  ListingCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ListingCollectionViewCell: UICollectionViewCell {
   // MARK: - UI
   private lazy var imageView: UIImageView = {
      let view = UIImageView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .offWhite
      view.layer.addSublayer(self.gradientLayer)
      return view
   }()
   private lazy var gradientView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .clear
      view.layer.insertSublayer(self.gradientLayer, at: 0)
      return view
   }()
   private lazy var gradientLayer: CAGradientLayer = {
      let layer = CAGradientLayer()
      layer.colors = [UIColor.clear.cgColor, UIColor.darkCalypso.withAlphaComponent(0.9).cgColor]
      layer.locations = [0.0, 1]
      return layer
   }()
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      label.font = .regularFont(ofSize: 17)
      label.numberOfLines = 2
      return label
   }()
   private lazy var subtitleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      label.font = .regularFont(ofSize: 14)
      label.numberOfLines = 3
      label.minimumScaleFactor = 0.7
      label.adjustsFontSizeToFitWidth = true
      return label
   }()
   private lazy var startingPriceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .white
      label.font = .regularFont(ofSize: 14)
      return label
   }()
   private lazy var tagLabel: TagLabel = .init()
   
   // MARK: - Initialization and Setup
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.contentView.backgroundColor = .background
      self.layer.borderColor = UIColor.subtleBorder.cgColor
      self.layer.borderWidth = .pixel
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      
      self.contentView.addSubviews([self.imageView, self.gradientView, self.tagLabel])
      NSLayoutConstraint.activate([
         self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
         self.imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
         
         self.gradientView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
         self.gradientView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.gradientView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.gradientView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
         
         self.tagLabel.topAnchor.constraint(equalTo: self.contentView.layoutMarginsGuide.topAnchor),
         self.tagLabel.leadingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.leadingAnchor),
         self.tagLabel.trailingAnchor.constraint(
            lessThanOrEqualTo: self.contentView.layoutMarginsGuide.trailingAnchor)
      ])
      
      let stackView: UIStackView = {
         let view = UIStackView(arrangedSubviews: [
            self.titleLabel,
            self.subtitleLabel,
            self.startingPriceLabel
         ])
         view.translatesAutoresizingMaskIntoConstraints = false
         view.axis = .vertical
         view.alignment = .fill
         view.distribution = .fill
         view.spacing = 2
         return view
      }()
      
      self.contentView.addSubview(stackView)
      NSLayoutConstraint.activate([
         stackView.topAnchor.constraint(
            greaterThanOrEqualToSystemSpacingBelow: self.tagLabel.bottomAnchor,
            multiplier: 1
         ),
         stackView.leadingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.leadingAnchor),
         stackView.trailingAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.trailingAnchor),
         stackView.bottomAnchor.constraint(
            equalTo: self.contentView.layoutMarginsGuide.bottomAnchor)
      ])
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      self.gradientLayer.frame = self.bounds
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.layer.borderColor = UIColor.subtleBorder.cgColor
   }
   
   override var isHighlighted: Bool {
      didSet {
         UIView.animate(withDuration: .highlightAnimationDuration) {
            self.transform = self.isHighlighted ? .highlightTransformForLargeViews : .identity
         }
      }
   }
   
   // MARK: - Other
   func configure(for listing: Listing) {
      self.titleLabel.text = listing.title
      
      self.subtitleLabel.text = listing.subtitle
      self.subtitleLabel.isHidden = listing.subtitle.isEmpty
      
      self.startingPriceLabel.isHidden = true
      
      self.tagLabel.text = listing.tag
      self.tagLabel.isHidden = listing.tag.isEmpty
      
      self.imageView.image = listing.image
   }
}
