//
//  StarRating.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/10/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class StarRating: UIControl {
   
   // MARK: - Properties
   
   private(set) var rating: CGFloat
   var maxRating: CGFloat
   
   /// Caches the location of the first touch when tracking begins.
   private var firstTouchedLocation: CGPoint?
   private var animationDuration: TimeInterval { 0.1 }
   
   // MARK: UI
   
   private lazy var stackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: stars)
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.axis = .horizontal
      stack.spacing = 1
      stack.isUserInteractionEnabled = false
      return stack
   }()
   private lazy var stars: [RatingStar] = {
      (0 ..< Int(maxRating)).map {
         let star = RatingStar(rating: 0)
         star.tag = $0
         return star
      }
   }()
   
   var backgroundStarColor: UIColor? {
      didSet {
         stars.forEach { $0.starBackgroundColor = backgroundStarColor }
      }
   }
   
   // MARK: - Init
   
   init(_ rating: CGFloat = 0, outOf maxRating: CGFloat) {
      self.rating = 0
      self.maxRating = maxRating
      super.init(frame: .zero)
      setUp()
      setRating(rating, animated: false)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      addFillingSubview(stackView)
      backgroundStarColor = .calypso
   }
   
   func setRating(_ newValue: CGFloat, animated: Bool) {
      let currentValue = rating
      rating = newValue
      let currentStarIndex = Int(currentValue)
      let newStarIndex = Int(newValue)
      
      let step = newStarIndex >= currentStarIndex ? 1 : -1
      
      UIView.animateKeyframes(
         withDuration: animationDuration,
         delay: 0,
         options: .calculationModeLinear
      ) {
         var relativeStartTime: Double = 0
         
         for index in stride(from: currentStarIndex, through: newStarIndex, by: step) {
            // Make sure the index isn't out of bounds
            let adjustedIndex = min(self.stars.count - 1, index)
            let star = self.stars[adjustedIndex]
            
            
            let currentStarRating = star.rating
            let newStarRating: CGFloat = {
               if index == newStarIndex {
                  // If the index is out of bounds, the star rating should be 1
                  let starRating = index == adjustedIndex ?
                     newValue.truncatingRemainder(dividingBy: 1) : 1
                  
                  return starRating
                  
               } else {
                  
                  return step > 0 ? 1 : 0
               }
            }()
            let relativeDuration = Double(
               abs((newStarRating - currentStarRating) / (newValue - currentValue))
            )
            
            UIView.addKeyframe(withRelativeStartTime: relativeStartTime, relativeDuration: relativeDuration) {
               star.rating = newStarRating
            }
            relativeStartTime += relativeDuration
         }
      }
   }
   
   private func star(at location: CGPoint) -> RatingStar {
      let tappedStarIndex = Int(location.x / bounds.width * maxRating)
      return stars[tappedStarIndex]
   }
   
   override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
      let location = touch.location(in: self)
      firstTouchedLocation = location
      
      let star = self.star(at: location)
      setStarHighlighted(star, highlighted: true)
      return true
   }
   
   override func cancelTracking(with event: UIEvent?) {
      guard let location = firstTouchedLocation else { return }
      firstTouchedLocation = nil
      
      let star = self.star(at: location)
      setStarHighlighted(star, highlighted: false)
   }
   
   override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
      guard let location = firstTouchedLocation else { return }
      firstTouchedLocation = nil
      
      let star = self.star(at: location)
      setStarHighlighted(star, highlighted: false)
      
      let rating = ceil(location.x / self.bounds.width * self.maxRating)
      setRating(rating, animated: true)
   }
   
   private func setStarHighlighted(_ star: RatingStar, highlighted: Bool, animated: Bool = true) {
      UIView.animate(
         withDuration: animated ? animationDuration : 0,
         delay: 0,
         options: .curveEaseOut
      ) {
         star.transform = highlighted ? CGAffineTransform(scaleX: 1.25, y: 1.25) : .identity
      }
   }
}
