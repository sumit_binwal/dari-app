//
//  DatePickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DatePickerView: UIView {
   // MARK: - Properties
   weak var delegate: DatePickerViewDelegate?
   var dates: [Date] { didSet { self.collectionView.collectionViewLayout.invalidateLayout() } }
   private var indexForSelectedDate: Int?
   
   // MARK: - UI
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.preservesSuperviewLayoutMargins = true
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         DatePickerCollectionViewCell.self,
         forCellWithReuseIdentifier: "DatePickerCollectionViewCell"
      )
      
      layout.itemSize = .init(65, 65)
      view.heightAnchor.constraint(equalToConstant: 65).isActive = true
      
      return view
   }()
   
   // MARK: - Init
   init(dates: [Date]) {
      self.dates = dates
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      self.dates = []
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubview(self.collectionView)
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func select(_ date: Date?, animated: Bool = false, scrollToSelection: Bool = false) {
      let scrollPosition: UICollectionView.ScrollPosition = scrollToSelection ?
         .centeredHorizontally : .top
      
      DispatchQueue.main.async {
         if let row = self.dates.firstIndex(of: date) {
            self.indexForSelectedDate = row
            self.collectionView.selectItem(
               at: .init(item: row, section: 0),
               animated: animated,
               scrollPosition: scrollPosition
            )
         } else {
            self.indexForSelectedDate = nil
            self.collectionView.selectItem(
               at: nil,
               animated: animated,
               scrollPosition: scrollPosition
            )
         }
      }
   }
   
   func reload(dates: [Date], selectedDate: Date?) {
      guard dates != self.dates else { return }
      
      // FIXME: Improve animation.
      DispatchQueue.main.async {
         self.indexForSelectedDate = nil
         self.collectionView.performBatchUpdates({
            self.dates = dates
            self.collectionView.deleteSections([0])
            self.collectionView.insertSections([0])
         }) { _ in
            self.select(selectedDate, animated: true, scrollToSelection: true)
         }
      }
   }
}

// MARK: - Collection View
extension DatePickerView: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.dates.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "DatePickerCollectionViewCell", for: indexPath)
         as? DatePickerCollectionViewCell else {
         fatalError("[DatePickerView] Failed to dequeue DatePickerCollectionViewCell.")
      }
      
      cell.configure(withDate: self.dates[indexPath.item])
      cell.isSelected = self.indexForSelectedDate == indexPath.item
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.delegate?.datePicker(didPickDate: self.dates[indexPath.item])
   }
}

// MARK: - Protocol
protocol DatePickerViewDelegate: AnyObject {
   func datePicker(didPickDate date: Date)
}
