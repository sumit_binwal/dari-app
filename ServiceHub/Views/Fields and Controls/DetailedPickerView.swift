//
//  DetailedPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DetailedPickerView: PickerView {
   // MARK: - Properties
   private var details: [String?]
   override var cellHeight: CGFloat {
      if let cellHeight = self._cellHeight { return cellHeight }
      
      var height = super.cellHeight
      guard !details.compactMap({ $0 }).isEmpty else { return height }
      
      let attributedText = NSAttributedString(
         string: "AAA",
         attributes: [.font: UIFont.regularFont(ofSize: 15)]
      )
      
      height += DetailedPickerCollectionViewCell.verticalSpacing + attributedText.size().height
      self._cellHeight = height
      return height
   }
   override class var cellType: AnyClass { DetailedPickerCollectionViewCell.self }
   
   // MARK: - Init
   init(
      title: String?,
      items: [String],
      details: [String?] = [],
      icons: [UIImage?] = [],
      indexForSelectedItem index: Int? = nil,
      shouldIndentTitle: Bool = false,
      titleRelativeWidth: CGFloat = 0.3
   ) {
      self.details = {
         let nilDetails = [String?].init(repeating: nil, count: items.count)
         
         if details.isEmpty { return nilDetails }
         
         guard items.count == details.count else {
            assertionFailure("[DetailedPickerView] Mismatch between item and detail lists.")
            return nilDetails
         }
         
         return details
      }()
      super.init(
         title: title,
         items: items,
         icons: icons,
         indexForSelectedItem: index,
         shouldIndentTitle: shouldIndentTitle,
         titleRelativeWidth: titleRelativeWidth
      )
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Other
   func set(items: [String], icons: [UIImage?] = [], details: [String?] = []) {
      self.details = {
         let nilDetails = [String?].init(repeating: nil, count: items.count)
         
         if details.isEmpty { return nilDetails }
         
         guard items.count == details.count else {
            assertionFailure("[DetailedPickerView] Mismatch between item and detail lists.")
            return nilDetails
         }
         
         return details
      }()
      super.set(items: items, icons: icons)
   }
   
   // MARK: - Collection View
   override func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "PickerCollectionViewCell", for: indexPath)
         as? DetailedPickerCollectionViewCell else
      {
         fatalError("[PickerView] Failed to dequeue PickerCollectionViewCell.")
      }
      
      let item = indexPath.item
      cell.setValues(
         text: self.items[item],
         image: self.icons[item],
         detailText: self.details[item]
      )
      
      return cell
   }
   
   override func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      var size = super.collectionView(
         collectionView,
         layout: collectionViewLayout,
         sizeForItemAt: indexPath
      )
      
      guard let detail = self.details[indexPath.item] else { return size }
      
      let attributedText = NSAttributedString(
         string: detail,
         attributes: [.font: UIFont.regularFont(ofSize: 15)]
      )
      size.width = max(attributedText.size().width + self.horizontalCellPadding * 2, size.width)
      
      return size
   }
}
