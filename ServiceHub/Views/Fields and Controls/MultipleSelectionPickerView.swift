//
//  MultipleSelectionPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class MultipleSelectionPickerView: UIView {
   // MARK: - Properties
   weak var delegate: MultipleSelectionPickerViewDelegate?
   private var items: [String] = []
   private var indicesForSelectedItems: [Int] = []
   private var maximumSelectionCount: Int?
   
   let maximumInvalidSelectionAttempts: Int = 2
   var invalidSelectionCountAttempts: Int = 0
   
   override var intrinsicContentSize: CGSize { .init(super.intrinsicContentSize.width, 50) }
   
   // MARK: - UI
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = true
      view.preservesSuperviewLayoutMargins = true
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         PickerCollectionViewCell.self,
         forCellWithReuseIdentifier: "PickerCollectionViewCell"
      )
      
      return view
   }()
   
   // MARK: - Init
   init() {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubview(self.collectionView)
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func setValues(items: [String], indicesForSelectedItems: [Int], maximumSelectionCount: Int?) {
      guard items != self.items else { return }
      self.maximumSelectionCount = maximumSelectionCount
      self.items = items
      self.indicesForSelectedItems = indicesForSelectedItems
      indicesForSelectedItems.forEach {
         self.collectionView.selectItem(
            at: IndexPath(item: $0, section: 0),
            animated: false,
            scrollPosition: .top
         )
      }
   }
}

// MARK: - Collection View
extension MultipleSelectionPickerView: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.items.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "PickerCollectionViewCell", for: indexPath)
         as? PickerCollectionViewCell else
      {
         fatalError("[HourPickerView] Failed to dequeue PickerCollectionViewCell.")
      }
      
      cell.setValues(text: self.items[indexPath.row])
      cell.isSelected = self.indicesForSelectedItems.contains(indexPath.row)
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      guard let maximumSelectionCount = self.maximumSelectionCount else { return }
      
      guard self.indicesForSelectedItems.count < maximumSelectionCount else {
         collectionView.deselectItem(at: indexPath, animated: true)
         self.invalidSelectionCountAttempts += 1
         if self.invalidSelectionCountAttempts >= self.maximumInvalidSelectionAttempts {
            self.delegate?.pickerViewHadMultipleSelectionAttemptsAboveLimit()
         }
         return
      }
      self.indicesForSelectedItems.append(indexPath.item)
      self.delegate?.pickerView(self, didSelectItemsAtIndices: self.indicesForSelectedItems)
   }
   
   func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
      if let index = self.indicesForSelectedItems.firstIndex(of: indexPath.item) {
         self.indicesForSelectedItems.remove(at: index)
         self.delegate?.pickerView(self, didSelectItemsAtIndices: self.indicesForSelectedItems)
         
         if self.invalidSelectionCountAttempts >= self.maximumInvalidSelectionAttempts {
            self.delegate?.pickerViewSelectionCountDidFallUnderLimit()
         }
      }
   }
}

extension MultipleSelectionPickerView: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      let attributedText = NSAttributedString(
         string: self.items[indexPath.item],
         attributes: [.font: UIFont.regularFont(ofSize: 17)]
      )
      
      return .init(attributedText.size().width + 20, collectionView.bounds.height)
   }
}

// MARK: - Protocol
protocol MultipleSelectionPickerViewDelegate: AnyObject {
   func pickerView(
      _ pickerView: MultipleSelectionPickerView,
      didSelectItemsAtIndices indices: [Int]
   )
   func pickerViewHadMultipleSelectionAttemptsAboveLimit()
   func pickerViewSelectionCountDidFallUnderLimit()
}
