//
//  HourPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/26/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class HourPickerView: UIView {
   // MARK: - Properties
   weak var delegate: HourPickerViewDelegate?
   var hours: [Int] { didSet { self.collectionView.collectionViewLayout.invalidateLayout() } }
   private var indexForSelectedHour: Int?
   
   // MARK: - UI
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.preservesSuperviewLayoutMargins = true
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         PickerCollectionViewCell.self,
         forCellWithReuseIdentifier: "PickerCollectionViewCell"
      )
      
      layout.itemSize = .init(70, 50)
      view.heightAnchor.constraint(equalToConstant: 50).isActive = true
      
      return view
   }()
   
   // MARK: - Init
   init(hours: [Int]) {
      self.hours = hours
      super.init(frame: .zero)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubview(self.collectionView)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func select(hour: Int?, animated: Bool = false, scrollToSelection: Bool = false) {
      DispatchQueue.main.async {
         let scrollPosition: UICollectionView.ScrollPosition = scrollToSelection ?
            .centeredHorizontally : .top
         
         if let row = self.hours.firstIndex(of: hour) {
            self.indexForSelectedHour = row
            self.collectionView.selectItem(
               at: .init(item: row, section: 0),
               animated: animated,
               scrollPosition: scrollPosition
            )
         } else {
            self.indexForSelectedHour = nil
            self.collectionView.selectItem(
               at: nil,
               animated: animated,
               scrollPosition: scrollPosition
            )
         }
      }
   }
   
   func reload(hours: [Int], selectedHour: Int?) {
      guard hours != self.hours else { return }
      
      // FIXME: Improve animation.
      DispatchQueue.main.async {
         self.indexForSelectedHour = nil
         self.collectionView.performBatchUpdates({
            self.hours = hours
            self.collectionView.deleteSections([0])
            self.collectionView.insertSections([0])
         }) { _ in
            self.select(hour: selectedHour, animated: true, scrollToSelection: true)
         }
      }
   }
}

// MARK: - Collection View
extension HourPickerView: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.hours.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "PickerCollectionViewCell", for: indexPath)
         as? PickerCollectionViewCell else
      {
         fatalError("[HourPickerView] Failed to dequeue PickerCollectionViewCell.")
      }
      
      let hour = self.hours[indexPath.row]
      cell.setValues(attributedText: DateFormatter.attributeText(
         forHour: hour,
         hourFont: .mediumFont(ofSize: 17),
         apPmFont: .regularFont(ofSize: 11)
      ))
      cell.isSelected = self.indexForSelectedHour == indexPath.item
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.indexForSelectedHour = indexPath.item
      self.delegate?.pickerView(didSelectHour: self.hours[indexPath.item])
   }
}

// MARK: - Protocol
protocol HourPickerViewDelegate: AnyObject {
   func pickerView(didSelectHour hour: Int)
}
