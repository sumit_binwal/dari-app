//
//  CheckBoxButton.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CheckBoxButton: UIButton {
   // MARK: - Properties
   private static var checkmarkImage: UIImage? { return UIImage(named: "Checkmark") }
   var isChecked: Bool = false {
      didSet { boxImageView.image = isChecked ? CheckBoxButton.checkmarkImage : nil }
   }
   
   // MARK: UI
   lazy var boxImageView: UIImageView = {
      let imageView = UIImageView()
      imageView.translatesAutoresizingMaskIntoConstraints = false
      imageView.backgroundColor = .unhighlightedBackground
      imageView.contentMode = .center
      imageView.layer.cornerRadius = 3
      imageView.layer.masksToBounds = true
      
      imageView.heightAnchor.constraint(equalToConstant: 18).isActive = true
      imageView.widthAnchor.constraint(equalToConstant: 18).isActive = true
      
      return imageView
   }()
   
   // MARK: - Initialization
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      setUp()
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
      
      addSubview(boxImageView)
      boxImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
      boxImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
      
      titleLabel?.leadingAnchor.constraint(
         equalTo: boxImageView.trailingAnchor, constant: 5).isActive = true
      setTitleColor(.prominentText, for: .normal)
   }
   
   // MARK: - Actions
   @objc private func buttonTapped() { isChecked.toggle() }
}
