//
//  AreaInputView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AreaInputView: UIView {
   // MARK: - Properties
   weak var delegate: AreaInputViewDelegate?
   var width: Measurement<UnitLength>
   var length: Measurement<UnitLength>
   
   // MARK: - UI
   private lazy var widthView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      
      view.addSubviews([self.widthLabel, self.widthInputView])
      NSLayoutConstraint.activate([
         self.widthLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.widthLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         self.widthLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3),
         self.widthLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         
         self.widthInputView.topAnchor.constraint(equalTo: view.topAnchor),
         self.widthInputView.leadingAnchor.constraint(equalTo: self.widthLabel.trailingAnchor),
         self.widthInputView.trailingAnchor.constraint(
            equalTo: view.layoutMarginsGuide.trailingAnchor),
         self.widthInputView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var widthLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 16)
      label.numberOfLines = 3
      label.minimumScaleFactor = 0.7
      label.adjustsFontSizeToFitWidth = true
      label.textAlignment = .natural
      label.text = "width".localized
      return label
   }()
   private lazy var widthInputView: MeasurementInputView = {
      let view = MeasurementInputView(
         value: self.width.value,
         unit: self.width.unit,
         minimumStepperValue: 50,
         maximumStepperValue: 2000,
         stepValue: 10
      )
      view.textField.textAlignment = .center
      view.tag = 1
      view.delegate = self
      return view
   }()
   
   private lazy var lengthView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      
      view.addSubviews([self.lengthLabel, self.lengthInputView])
      NSLayoutConstraint.activate([
         self.lengthLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.lengthLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         self.lengthLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3),
         self.lengthLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         
         self.lengthInputView.topAnchor.constraint(equalTo: view.topAnchor),
         self.lengthInputView.leadingAnchor.constraint(equalTo: self.lengthLabel.trailingAnchor),
         self.lengthInputView.trailingAnchor.constraint(
            equalTo: view.layoutMarginsGuide.trailingAnchor),
         self.lengthInputView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var lengthLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 16)
      label.numberOfLines = 3
      label.minimumScaleFactor = 0.7
      label.adjustsFontSizeToFitWidth = true
      label.textAlignment = .natural
      label.text = "length".localized
      return label
   }()
   private lazy var lengthInputView: MeasurementInputView = {
      let view = MeasurementInputView(
         value: self.length.value,
         unit: self.length.unit,
         minimumStepperValue: 50,
         maximumStepperValue: 2000,
         stepValue: 10
      )
      view.textField.textAlignment = .center
      view.tag = 2
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   init(width: Measurement<UnitLength>, length: Measurement<UnitLength>) {
      self.width = width
      self.length = length
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      
      self.addSubviews([self.widthView, self.lengthView])
      let margins = self.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.widthView.topAnchor.constraint(equalTo: margins.topAnchor),
         self.widthView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.widthView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         
         self.lengthView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.widthView.bottomAnchor, multiplier: 0.5),
         self.lengthView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.lengthView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.lengthView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
}

// MARK: - MeasurementInputViewDelegate
extension AreaInputView: MeasurementInputViewDelegate {
   func measurementInputView(
      _ view: MeasurementInputView,
      didChangeMeasurement measurement: Measurement<Unit>
   ) {
      
      guard let unit = measurement.unit as? UnitLength else {
         fatalError("[AreaInputView] Unrecognized unit \(measurement.unit.symbol)")
      }
      let measurement = Measurement(value: measurement.value, unit: unit)
      
      switch view.tag {
      case 1: self.width = measurement
      case 2: self.length = measurement
      default: assertionFailure("[AreaInputView] Unrecognized MeasurementInputView.")
      }
      
      self.delegate?.areaInputView(didChangeWidth: self.width, length: self.length)
   }
}

// MARK: - Protocol
protocol AreaInputViewDelegate: AnyObject {
   func areaInputView(
      didChangeWidth width: Measurement<UnitLength>,
      length: Measurement<UnitLength>
   )
}
