//
//  CardTextField.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardTextField: UnderLineTextField {
   // MARK: - Properties
   private var previousTextCount: Int = 0
   
   // MARK: - Initializers
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setUp()
   }
   
   private func setUp() {
      placeholder = "Card Number"
      semanticContentAttribute = .forceLeftToRight
      textAlignment = .left
      textContentType = .creditCardNumber
      keyboardType = .numberPad
      delegate = self
      addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
   }
   
   // MARK: - Actions
   @objc private func textFieldDidChange(textField: UITextField) {
      var targetCursorPosition = 0
      if let startPosition = textField.selectedTextRange?.start {
         targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
      }
      
      let cardNumber = Card.manager.standardize(cardNumber: textField.text!)
      guard let cardNumberFormatted = Card.manager.format(cardNumber: cardNumber) else { return }
      
      textField.text = cardNumberFormatted
      
      if var targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
         if targetCursorPosition != 0 {
            let lastChar = (cardNumberFormatted as NSString)
               .substring(with: NSRange(location: targetCursorPosition - 1, length: 1))
            if lastChar == " " && previousTextCount < cardNumberFormatted.count {
               targetPosition = textField
                  .position(from: textField.beginningOfDocument, offset: targetCursorPosition + 1)!
            }
         }
         textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
      }
      previousTextCount = cardNumberFormatted.count
   }
}

// MARK: - UITextFieldDelegate
extension CardTextField: UITextFieldDelegate {
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                  replacementString string: String) -> Bool {
      let cardNumber = Card.manager.standardize(cardNumber: "\(textField.text!)\(string)")
      guard let maximumValidLength = Card.manager.maximumValidLength(for: cardNumber) else { return true }
      
      return cardNumber.count <= maximumValidLength
   }
}
