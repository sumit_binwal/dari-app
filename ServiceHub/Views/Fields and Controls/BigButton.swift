//
//  BigButton.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/26/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class BigButton: UIButton {
   
   private static var highlightTransform: CGAffineTransform { .init(scaleX: 0.97, y: 0.97) }
   
   override var isHighlighted: Bool {
      didSet {
         UIView.animate(withDuration: .highlightAnimationDuration) {
            self.transform = self.isHighlighted ? Self.highlightTransform : .identity
         }
      }
   }
   
   override var intrinsicContentSize: CGSize {
      .init(super.intrinsicContentSize.width + 10, super.intrinsicContentSize.height + 16)
   }
   
   override var isEnabled: Bool {
      didSet {
         refreshBorderColor()
      }
   }
   
   // MARK: - Initialization and Setup
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.setTitleColor(.prominentText, for: .normal)
      self.setTitleColor(.dimmedTint, for: .disabled)
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.layer.borderWidth = 2
      self.titleLabel?.font = .boldFont(ofSize: 17)
   }
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      self.setUp()
   }
   
   private func refreshBorderColor() {
      layer.borderColor = isEnabled ? tintColor.cgColor : UIColor.dimmedTint.cgColor
   }
   
   override func tintColorDidChange() {
      refreshBorderColor()
   }
}
