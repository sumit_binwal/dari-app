//
//  ExpiryDateTextField.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ExpiryDateTextField: UnderLineTextField {
   // MARK: - Properties
   weak var externalDelegate: ExpiryDateTextFieldDelegate?
   var expiryDateIsValid: Bool {
      return Card.manager.isValid(expiryMonth: expiryMonth, expiryYear: expiryYear)
   }
   var expiryMonth: String = ""
   var expiryYear: String = ""
   
   // MARK: - Initialization
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setUp()
   }
   
   private func setUp() {
      placeholder = "Expiry Date"
      semanticContentAttribute = .forceLeftToRight
      textAlignment = .left
      let expiryDatePicker = ExpiryDatePickerView()
      expiryDatePicker.externalDelegate = self
      inputView = expiryDatePicker
      delegate = self
   }
   
   // MARK: - Other
   override func caretRect(for position: UITextPosition) -> CGRect { return .zero }
}

// MARK: - ExpiryDatePickerViewDelegate
extension ExpiryDateTextField: ExpiryDatePickerViewDelegate {
   func expiryDateDidChange(month: Int, year: Int) {
      expiryMonth = "\(month > 9 ? "" : "0")\(month)"
      expiryYear = String(year)
      text = "\(expiryMonth) / \((year % 1000) % 100)"
      externalDelegate?.expiryDateDidChange()
   }
}

// MARK: - UITextFieldDelegate
extension ExpiryDateTextField: UITextFieldDelegate {
   func textField(
      _ textField: UITextField,
      shouldChangeCharactersIn range: NSRange,
      replacementString string: String) -> Bool {
      return false
   }
}

protocol ExpiryDateTextFieldDelegate: AnyObject {
   func expiryDateDidChange()
}
