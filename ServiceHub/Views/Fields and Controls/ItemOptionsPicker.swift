//
//  ItemOptionsPicker.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/20/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ItemOptionsPicker: UIView {
   weak var delegate: ItemOptionsPickerDelegate?
   
   // MARK: - Properties
   var index: Int
   var item: Item
   var indexForSelectedVariant: Int?
   var indexForSelectedOption: Int?
   var areaDimensions: AreaDimensions?
   var parameters: Item.Parameter {
      .init(
         item: {
            if let index = self.indexForSelectedVariant {
               return self.item.variants[index]
            } else {
               return self.item
            }
      }(),
         serviceOption: {
            if let index = self.indexForSelectedOption {
               return self.item.serviceOptions[index]
            } else {
               return nil
            }
      }(),
         areaDimensions: self.areaDimensions
      )
   }
   
   // MARK: - UI
   private lazy var label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 15)
      label.numberOfLines = 3
      label.textAlignment = .natural
      label.text = "\(self.item.name) \(NumberFormatter.integer.string(from: self.index + 1))"
      return label
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 5
      
      if !item.variants.isEmpty { view.addArrangedSubview(self.variantPickerView) }
      if !item.serviceOptions.isEmpty { view.addArrangedSubview(self.optionPickerView) }
      if item.rateType == .area { view.addArrangedSubview(self.areaInputView) }
      return view
   }()
   private lazy var variantPickerView: PickerView = {
      let view = PickerView(
         title: self.item.variationName,
         items: self.item.variants.map { $0.name },
         indexForSelectedItem: self.indexForSelectedVariant,
         shouldIndentTitle: true
      )
      view.tag = 1
      view.delegate = self
      return view
   }()
   private lazy var optionPickerView: PickerView = {
      let view = PickerView(
         title: self.item.serviceOptionsName,
         items: self.item.serviceOptions.map { $0.name },
         indexForSelectedItem: self.indexForSelectedOption,
         shouldIndentTitle: true
      )
      view.tag = 2
      view.delegate = self
      return view
   }()
   private lazy var areaInputView: AreaInputView = {
      let areaDimensions = self.areaDimensions ?? .default
      let view = AreaInputView(
         width: areaDimensions.widthMeasurement,
         length: areaDimensions.lengthMeasurement
      )
      view.delegate = self
      return view
   }()
   private lazy var borderLayer: CALayer = {
      let layer = CALayer()
      layer.backgroundColor = UIColor.subtleBorder.cgColor
      return layer
   }()
   
   // MARK: - Init
   init(index: Int, item: Item, itemOrderParameters parameters: Item.Parameter? = nil) {
      self.index = index
      self.item = item
      self.indexForSelectedVariant = item.variants.firstIndex(of: parameters?.item)
      self.indexForSelectedOption = item.serviceOptions.firstIndex(of: parameters?.serviceOption)
      self.areaDimensions = parameters?.areaDimensions
      super.init(frame: .zero)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      
      self.addSubviews([self.label, self.stackView])
      let margins = self.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.label.topAnchor.constraint(equalTo: margins.topAnchor),
         self.label.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.label.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.stackView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.label.bottomAnchor, multiplier: 1),
         self.stackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      self.borderLayer.frame = CGRect(
         self.layoutMargins.left,
         self.bounds.height - .pixel,
         self.bounds.width,
         .pixel
      )
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.borderLayer.borderColor = UIColor.subtleBorder.cgColor
   }
}

// MARK: - PickerViewDelegate
extension ItemOptionsPicker: PickerViewDelegate {
   func pickerView(_ pickerView: PickerView, didSelectItemAtIndex index: Int) {
      switch pickerView.tag {
      case 1: self.indexForSelectedVariant = index
      case 2: self.indexForSelectedOption = index
      default: assertionFailure("[ItemOptionsPicker] Unrecognized picker view tag.")
      }
      
      self.delegate?.itemOptionsPicker(
         atIndex: self.index,
         didChangeItemParameters: self.parameters
      )
   }
}

// MARK: - AreaInputViewDelegate
extension ItemOptionsPicker: AreaInputViewDelegate {
   func areaInputView(
      didChangeWidth width: Measurement<UnitLength>,
      length: Measurement<UnitLength>)
   {
      self.areaDimensions = .init(width: width.value, length: length.value)
      self.delegate?.itemOptionsPicker(
         atIndex: self.index,
         didChangeItemParameters: self.parameters
      )
   }
}

// MARK: - Protocol
protocol ItemOptionsPickerDelegate: AnyObject {
   func itemOptionsPicker(
      atIndex index: Int,
      didChangeItemParameters parameters: Item.Parameter
   )
}
