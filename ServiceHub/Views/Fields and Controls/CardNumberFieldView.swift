//
//  CardNumberFieldView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardNumberFieldView: UIView {
   // MARK: - Properties
   weak var delegate: CardNumberFieldViewDelegate?
   var standardizedCardNumber: String {
      return Card.manager.standardize(cardNumber: cardTextField.text ?? "")
   }
   var cardNumberIsValid: Bool { return Card.manager.isValid(cardNumber: standardizedCardNumber) }
   
   // MARK: UI
   private lazy var acceptedCardsLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 14)
      label.textColor = .prominentText
      label.text = "acceptedCards".localized
      return label
   }()
   private lazy var schemeIconsView: SchemeIconsView = .init()
   lazy var cardTextField: CardTextField = {
      let textField = CardTextField()
      textField.translatesAutoresizingMaskIntoConstraints = false
      return textField
   }()
   
   // MARK: - Initialization and Configuration
   init() {
      super.init(frame: .zero)
      setUp()
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      setUp()
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      backgroundColor = .lighterBackground
      semanticContentAttribute = .forceLeftToRight
      
      // Label
      addSubview(acceptedCardsLabel)
      acceptedCardsLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
      acceptedCardsLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      acceptedCardsLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      
      // Card Icons
      addSubview(schemeIconsView)
      schemeIconsView.topAnchor.constraint(equalTo: acceptedCardsLabel.bottomAnchor,
                                           constant: 2).isActive = true
      schemeIconsView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      schemeIconsView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
       
//       let image = UIImageView.init(frame: CGRect.init(schemeIconsView.frame.size.width, 0, 20, 20))
//       image.backgroundColor = .black
//       addSubview(image)
//       image.topAnchor.constraint(equalTo: acceptedCardsLabel.bottomAnchor,
//                                            constant: 2).isActive = true
//       image.leadingAnchor.constraint(equalTo: schemeIconsView.leadingAnchor, constant: 48).isActive = true
//
////       image.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//       image.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//      
      
      // Text Field
      addSubview(cardTextField)
      cardTextField.topAnchor.constraint(equalTo: schemeIconsView.bottomAnchor).isActive = true
      cardTextField.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      cardTextField.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      cardTextField.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      cardTextField.addTarget(self, action: #selector(cardTextFieldDidChange), for: .editingChanged)
   }
   
   // MARK: - Actions
   @objc private func cardTextFieldDidChange() {
      schemeIconsView.setHighlightedScheme(forCardNumber: standardizedCardNumber)
      delegate?.cardNumberDidChange(standardizedCardNumber)
   }
}

// MARK: - Protocol
protocol CardNumberFieldViewDelegate: AnyObject {
   /// Card number is standardized.
   func cardNumberDidChange(_ cardNumber: String)
}
