//
//  TappableLabel.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/18/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class TappableLabel: UILabel {
   // MARK: - Properties
   private var tappableTexts: [String] = []
   private var actions: [(() -> Void)?] = []
   
   //private var fontSize: CGFloat = 0
   private var regularFont: UIFont = .regularFont(ofSize: 0)
   private var tappableFont: UIFont = .regularFont(ofSize: 0)
   private lazy var gestureRecognizer = UITapGestureRecognizer(
      target: self,
      action: #selector(self.didTapLabel(gestureRecognizer:))
   )
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: frame)
      
      //self.fontSize = self.font.pointSize
      self.regularFont = self.font
      self.tappableFont = self.font
      self.isUserInteractionEnabled = true
      self.addGestureRecognizer(self.gestureRecognizer)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func tintColorDidChange() {
      guard let attributedText = self.attributedText else { return }
      guard let tintColor = self.tintColor else { return }
      
      let ranges: [NSRange] = self.tappableTexts.map {
         (attributedText.string as NSString).range(of: $0)
      }
      
      let mutableText = NSMutableAttributedString(attributedString: attributedText)
      
      ranges.forEach { mutableText.setAttributes(
         [
            .font: self.tappableFont,
            .foregroundColor: tintColor
         ],
         range: $0)
      }
      
      self.attributedText = mutableText
   }
   
   // MARK: - Actions
   @objc
   private func didTapLabel(gestureRecognizer: UITapGestureRecognizer) {
      guard let text = self.text else { return }
      
      let ranges: [NSRange] = self.tappableTexts.map { (text as NSString).range(of: $0) }
      
      for (index, range) in ranges.enumerated() {
         if gestureRecognizer.didTapAttributedText(inLabel: self, inRange: range) {
            self.actions[index]?()
            return
         }
      }
   }
   
   // MARK: - Other
   public func set(
      text: String,
      tappableTexts: [String],
      tapActions: [(() -> Void)?],
      font: UIFont? = nil,
      tappableFont: UIFont? = nil
   ) {
      guard tappableTexts.count == tapActions.count else {
         assertionFailure("[TappableLabel] Tappable texts and actions must have the same count.")
         return
      }
      self.tappableTexts = tappableTexts
      self.actions = tapActions
      self.regularFont = font ?? self.regularFont
      self.tappableFont = tappableFont ?? self.tappableFont
      
      let paragraph = NSMutableParagraphStyle()
      paragraph.alignment = self.textAlignment
      
      let attributedText = NSMutableAttributedString(string: text, attributes: [
         .font: self.regularFont,
         .foregroundColor: self.textColor ?? UIColor.text,
         .paragraphStyle: paragraph
      ])
      
      let ranges: [NSRange] = self.tappableTexts.map {
         (attributedText.string as NSString).range(of: $0)
      }
      
      ranges.forEach { attributedText.setAttributes(
         [
            .font: self.tappableFont,
            .foregroundColor: self.tintColor ?? UIColor.tint
         ],
         range: $0)
      }
      
      self.attributedText = attributedText
   }
}
