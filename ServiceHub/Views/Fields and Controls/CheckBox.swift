//
//  CheckBox.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/8/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    
    private static var checkmarkImage: UIImage? { return UIImage(named: "Checkmark") }
    
    var isChecked: Bool = false {
        didSet { setImage(isChecked ? CheckBox.checkmarkImage : nil, for: .normal) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        layer.cornerRadius = 3
        layer.masksToBounds = true
        backgroundColor = .unhighlightedBackground
        tintColor = .tint
        addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc private func buttonTapped() { isChecked.toggle() }
}
