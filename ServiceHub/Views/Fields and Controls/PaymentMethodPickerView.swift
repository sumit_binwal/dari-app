//
//  PaymentMethodPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

import UIKit

class PaymentMethodPickerView: UIView {
   // MARK: - Properties
   weak var delegate: PaymentMethodPickerViewDelegate?
   var paymentMethods: [PaymentMethod] = []
   var selectedPaymentMethod: PaymentMethod?
   lazy var cards: [Card] = []
   var selectedCard: Card?
   
   // MARK: - UI
   private lazy var paymentMethodPickerView: DetailedPickerView = {
      let view = DetailedPickerView(
         title: "payBy".localized,
         items: self.paymentMethods.map { $0.name },
         details: [],
         icons: self.paymentMethods.map { $0.image },
         titleRelativeWidth: 0.2
      )
      view.titleViewBackgroundColor = .lighterBackground
      view.verticalCellPadding = 10
      view.horizontalCellPadding = 12
      view.delegate = self
      return view
   }()
   
   private lazy var fillerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
      return view
   }()
   private lazy var cardPickerView: CardPickerView = {
      let view = CardPickerView(cards: self.cards)
      view.delegate = self
      return view
   }()
   private lazy var stackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: [self.fillerView, self.cardPickerView])
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.preservesSuperviewLayoutMargins = true
      stack.axis = .vertical
      stack.alignment = .fill
      stack.distribution = .fillProportionally
      stack.spacing = 8
      return stack
   }()
   
   // MARK: - Init
   init() {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .lighterBackground
      
      self.addSubviews([self.paymentMethodPickerView, self.stackView])
      
      let margins = self.layoutMarginsGuide
      
      NSLayoutConstraint.activate([
         self.paymentMethodPickerView.topAnchor.constraint(equalTo: margins.topAnchor),
         self.paymentMethodPickerView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.paymentMethodPickerView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.stackView.topAnchor.constraint(equalTo: self.paymentMethodPickerView.bottomAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   convenience init(
      paymentMethods: [PaymentMethod],
      fees: [Double?],
      selectedPaymentMethod: PaymentMethod?,
      cards: [Card],
      selectedCard: Card?
   ) {
      self.init()
      self.setValues(
         paymentMethods: paymentMethods,
         fees: fees,
         selectedPaymentMethod: selectedPaymentMethod,
         cards: cards,
         selectedCard: selectedCard
      )
   }
   
   // MARK: - Other
   func showCardPicker(_ show: Bool, animated: Bool, completion: (() -> Void)? = nil) {
      guard animated else {
         self.cardPickerView.isHidden = !show
         completion?()
         return
      }
      
      DispatchQueue.main.async {
         guard self.cardPickerView.isHidden == show else { return }
      
         UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
               self.cardPickerView.isHidden = !show
         }, completion: { _ in completion?() })
         self.delegate?.paymentMethodPickerViewDidToggleCardPickerVisibility()
      }
   }
   
   func setValues(
      paymentMethods: [PaymentMethod],
      fees: [Double?],
      selectedPaymentMethod: PaymentMethod?,
      cards: [Card],
      selectedCard: Card?
   ) {
      self.paymentMethods = paymentMethods
      self.selectedPaymentMethod = selectedPaymentMethod
      
      self.paymentMethodPickerView.set(
         items: paymentMethods.map { $0.name },
         icons: self.paymentMethods.map { $0.image },
         details: fees.map {
            guard let fee = $0 else { return nil }
            return "+" + NumberFormatter.shortCurrency.string(from: fee)
         }
      )
      self.paymentMethodPickerView.select(
         itemAtIndex: self.paymentMethods.firstIndex(of: selectedPaymentMethod)
      )
      
      self.showCardPicker(selectedPaymentMethod == .card, animated: false)
      
      self.cards = cards
      self.selectedCard = selectedCard
      
      self.cardPickerView.cards = cards
      self.cardPickerView.select(card: selectedCard)
   }
   
   func addCard(_ card: Card) {
      self.cards.append(card)
      self.cardPickerView.addCard(card)
   }
   
   func select(_ card: Card?) {
      self.cardPickerView.select(card: card)
   }
   
   func removeCards(_ cards: [Card]) {
      self.cards = self.cards.filter { !cards.contains($0) }
      cardPickerView.removeCards(cards)
   }
}

// MARK: - CardPickerViewDelegate
extension PaymentMethodPickerView: CardPickerViewDelegate {
   func cardPickerViewDidTapAddCard() {
      self.delegate?.paymentMethodPickerViewDidTapAddCard()
   }
   
   func cardPickerView(didSelectCard card: Card) {
      guard let paymentMethod = self.selectedPaymentMethod, paymentMethod == .card else {
         assertionFailure("[PaymentMethodTableViewCell] Invalid payment method.")
         return
      }
      self.delegate?.paymentMethodPickerView(didSelectPaymentMethod: .card, card: card)
   }
}

// MARK: - PickerViewDelegate
extension PaymentMethodPickerView: PickerViewDelegate {
   func pickerView(_ pickerView: PickerView, didSelectItemAtIndex index: Int) {
      let paymentMethod = self.paymentMethods[index]
      self.selectedPaymentMethod = paymentMethod
      
      self.selectedCard = self.selectedPaymentMethod == .card ? self.cards.first : nil
      self.showCardPicker(self.selectedPaymentMethod == .card, animated: true) {
         self.cardPickerView.select(card: self.selectedCard)
      }
      
      self.delegate?.paymentMethodPickerView(
         didSelectPaymentMethod: paymentMethod,
         card: self.selectedCard
      )
   }
}

// MARK: - Protocol
protocol PaymentMethodPickerViewDelegate: AnyObject {
   func paymentMethodPickerView(didSelectPaymentMethod paymentMethod: PaymentMethod, card: Card?)
   func paymentMethodPickerViewDidTapAddCard()
   func paymentMethodPickerViewDidToggleCardPickerVisibility()
}
