//
//  PhoneTextField.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/23/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import PhoneNumberKit

class PhoneTextField: UnderLineTextField {
    // MARK: - Properties
    private lazy var gestureRecognizer = UITapGestureRecognizer(
        target: self, action: #selector(accessoryViewWasTapped(gestureRecognizer:)))
    private lazy var flagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 33).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .tint
        return imageView
    }()
    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Small Downward Chevron")
        imageView.tintColor = .calypso
        return imageView
    }()
    private lazy var countryCodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .subtleText
        label.font = font
        label.text = "+966"
        return label
    }()
    private lazy var countryLabel: UILabel = {
        let label = UILabel()
        label.textColor = .subtleText
        label.font = font
        label.text = "Country"
        label.isHidden = true
        return label
    }()
    
    override var tintColor: UIColor! { didSet { flagImageView.tintColor = tintColor } }
    
    // MARK: Phone
    private lazy var phoneNumberKit = PhoneNumberKit()
    var partialFormatter: PartialFormatter? {
        guard let country = country else { return nil }
        
        return PartialFormatter(
            phoneNumberKit: phoneNumberKit,
            defaultRegion: country.isoCode,
            withPrefix: false,
            maxDigits: nil)
    }
    var rawPhoneNumber: String? {
        guard let text = text, let codeText = countryCodeLabel.text else { return nil }
        let number = "\(codeText)\(text)".filter { $0.isNumber }
        guard let phoneNumber = try? phoneNumberKit.parse(
            number, withRegion: Locale.current.regionCode ?? "SA", ignoreType: false) else { return "+\(number)" }
        return phoneNumberKit.format(phoneNumber, toType: .e164)
    }
    var country: ISOCountry?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        semanticContentAttribute = .forceLeftToRight
        textAlignment = .left
        setUpAccessoryView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        semanticContentAttribute = .forceLeftToRight
        textAlignment = .left
        setUpAccessoryView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        semanticContentAttribute = .forceLeftToRight
        textAlignment = .left
        setUpAccessoryView()
    }
    
    // MARK: - Initial Configuration
    private func setUpAccessoryView() {
        let stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.spacing = 5
            stackView.alignment = .center
            stackView.distribution = .fill
            stackView.semanticContentAttribute = .forceLeftToRight
            return stackView
        }()
        let enclosingView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = backgroundColor
            view.semanticContentAttribute = .forceLeftToRight
            return view
        }()
        
        [
            flagImageView,
            countryLabel,
            chevronImageView,
            countryCodeLabel
        ].forEach { stackView.addArrangedSubview($0) }
        
        enclosingView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: enclosingView.topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: enclosingView.leadingAnchor, constant: 3).isActive = true
        stackView.trailingAnchor.constraint(equalTo: enclosingView.trailingAnchor, constant: 3).isActive = true
        stackView.bottomAnchor.constraint(equalTo: enclosingView.bottomAnchor).isActive = true
        
        enclosingView.addGestureRecognizer(gestureRecognizer)
        leftView = enclosingView
        leftViewMode = .always
    }
    
    // MARK: - Actions
    @objc private func accessoryViewWasTapped(gestureRecognizer: UITapGestureRecognizer) {
        guard self.delegate != nil else { return }
        guard let delegate = self.delegate as? PhoneTextFieldDelegate else {
            assertionFailure("[PhoneTextField] Delegate is not an instance of PhoneTextFieldDelegate")
            return
        }
        delegate.countryCodeWasTapped()
    }
    
    // MARK: - Others
    func setCountry(_ country: ISOCountry) {
        self.country = country
        countryCodeLabel.text = {
            if let countryCode = phoneNumberKit.countryCode(for: country.isoCode),
                let partialFormatter = partialFormatter {
                return "+\(partialFormatter.formatPartial("\(countryCode)"))"
            } else {
                print("[PhoneTextField] Failed to get IDD from ISO.")
                return country.idd
            }
        }()
        
        guard let flagImage = country.flag else {
            assertionFailure("[Country] Failed to get image for country \(country.name)")
            flagImageView.isHidden = true
            return
        }
        
        flagImageView.image = flagImage
        flagImageView.isHidden = false
        countryLabel.isHidden = true
        countryCodeLabel.isHidden = false
        
        if let text = text, let partialFormatter = partialFormatter {
            self.text = partialFormatter.formatPartial(text)
        }
    }
    
    func setPhoneNumberPlaceholder(_ number: String) {
        func failure() {
            placeholder = number
            flagImageView.isHidden = true
            countryLabel.isHidden = false
            countryCodeLabel.isHidden = true
        }
        guard let phoneNumber = try? phoneNumberKit.parse(
            number,
            withRegion: Locale.current.regionCode ?? "SA",
            ignoreType: true) else {
                print("[ChangePhoneVC] Failed to parse phone number.")
                failure()
                return
        }
        guard let isoCode = phoneNumberKit.mainCountry(forCode: phoneNumber.countryCode) else {
            print("[ChangePhoneVC] Failed to get ISO code from phone number.")
            failure()
            return
        }
        
        guard let country = ISOCountry.load().first(where: { $0.isoCode == isoCode }) else {
            print("[ChangePhoneVC] Failed to find country with isoCode \(isoCode).")
            failure()
            return
        }
        self.country = country
        guard let partialFormatter = partialFormatter else {
            failure()
            return
        }
        
        
        placeholder = partialFormatter.formatPartial(String(phoneNumber.nationalNumber))
        countryCodeLabel.text = "+\(phoneNumber.countryCode)"
        countryCodeLabel.isHidden = false
        countryLabel.isHidden = true
        
        setCountry(country)
    }
}

protocol PhoneTextFieldDelegate: class {
    func countryCodeWasTapped()
}

extension PhoneTextField: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        guard let partialFormatter = partialFormatter else {
            assertionFailure("[PhoneTextField] partial formatter is nil.")
            return true
        }
        guard let text = text else { return true }
        let originalText = text as NSString
        var adjustedRange = range
        
        // Detects if what's being deleted is a space and adjusts the range to delete the character preceding it.
      if string.isEmpty && originalText.substring(with: range) == " " && adjustedRange.location > 0 {
            adjustedRange.location -= 1
        }
        let proposedText = originalText.replacingCharacters(in: adjustedRange, with: string)
        
        // Used to calculate the offset when changing cursor position
        let isDeletion: Bool = proposedText.count < text.count
        let cachedSelectedRange = textField.selectedTextRange
        
        let newText = partialFormatter.formatPartial(proposedText)
        self.text = newText
        
        let differential = newText.count - proposedText.count
        if let selectedRange = cachedSelectedRange,
            let newPosition = textField.position(from: selectedRange.end,
                                                 offset: differential + (isDeletion ? -1 : 1)) {
            
            selectedTextRange = textRange(from: newPosition, to: newPosition)
        } else { // The case when deleting the first character
            selectedTextRange = textRange(from: beginningOfDocument, to: beginningOfDocument)
        }
        return false
    }
}
