//
//  NumberPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/20/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class NumberPickerView: UIView {
   // MARK: - Properties
   weak var delegate: NumberPickerDelegate?
   var numbers: [Int] {
      didSet { self.collectionView.collectionViewLayout.invalidateLayout() }
   }
   private var indexForSelectedItem: Int?
   
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.preservesSuperviewLayoutMargins = true
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         PickerCollectionViewCell.self,
         forCellWithReuseIdentifier: "PickerCollectionViewCell"
      )
      
      layout.itemSize = .init(50, 50)
      view.heightAnchor.constraint(equalToConstant: 50).isActive = true
      
      return view
   }()
   
   // MARK: - Init
   convenience init(minimum: Int, maximum: Int, increment: Int) {
      self.init(numbers: Array(stride(from: minimum, through: maximum, by: increment)))
   }
   
   init(numbers: [Int]) {
      self.numbers = numbers
      super.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      
      self.addFillingSubview(self.collectionView)
      self.select(number: numbers.first)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: Other
   func select(number: Int?) {
      if let number = number, let index = self.numbers.firstIndex(of: number) {
         self.indexForSelectedItem = index
         self.collectionView.selectItem(
            at: .init(item: index, section: 0),
            animated: false,
            scrollPosition: .top
         )
      } else {
         self.indexForSelectedItem = nil
         self.collectionView.selectItem(at: nil, animated: false, scrollPosition: .top)
      }
   }
}

// MARK: - Collection View
extension NumberPickerView: UICollectionViewDelegate, UICollectionViewDataSource {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.numbers.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "PickerCollectionViewCell", for: indexPath)
         as? PickerCollectionViewCell else
      {
         fatalError("[NumberPickerView] Failed to dequeue PickerCollectionViewCell.")
      }
      
      cell.setValues(text: NumberFormatter.integer.string(from: self.numbers[indexPath.item]))
      cell.isSelected = self.indexForSelectedItem == indexPath.item
      
      return cell
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      didSelectItemAt indexPath: IndexPath
   ) {
      self.indexForSelectedItem = indexPath.item
      self.delegate?.numberPicker(didSelectNumber: self.numbers[indexPath.item])
   }
}

// MARK: - Protocol
protocol NumberPickerDelegate: AnyObject {
   func numberPicker(didSelectNumber number: Int)
}
