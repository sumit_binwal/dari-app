//
//  ExpiryDatePickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ExpiryDatePickerView: UIPickerView {
   // MARK: - Properties
   weak var externalDelegate: ExpiryDatePickerViewDelegate?
   let calendar = Calendar(identifier: .gregorian)
   let timeZone = TimeZone(secondsFromGMT: 0)
   
   let minimumDate = Date()
   var maximumDate = Date(timeIntervalSinceNow: 31556926 * 20)
   var selectedMonth: Int { return selectedRow(inComponent: 0) + 1 }
   var selectedYear: Int {
      guard let year = Int(years[selectedRow(inComponent: 1)]) else {
         assertionFailure("[ExpiryDatePickerView] Failed to get year.")
         return calendar.component(.year, from: minimumDate)
      }
      return year
   }
   var selectedDate: Date {
      guard let date = calendar.date(from: DateComponents(year: selectedYear, month: selectedMonth)) else {
         assertionFailure("[ExpiryDatePickerView] Failed to get selected date.")
         return minimumDate
      }
      return date
   }
   
   private lazy var months: [String] = (1...12).map { String($0) }
   private lazy var years: [String] = {
      let ints =
         calendar.component(.year, from: minimumDate)...calendar.component(.year, from: maximumDate)
      return ints.map { String($0) }
   }()
   
   // MARK: - Initialization
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      setUp()
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      semanticContentAttribute = .forceLeftToRight
      dataSource = self
      delegate = self
      setDate(minimumDate, animated: false)
   }
   
   // MARK: - Methods
   public func setDate(_ date: Date, animated: Bool) {
      let month = calendar.component(.month, from: date)
      let year = calendar.component(.year, from: date)
      
      let monthIndex = month - 1
      let yearIndex: Int = {
         guard let index = years.firstIndex(of: String(year)) else {
            assertionFailure("[ExpiryDatePickerView] Failed to get index of year.")
            return 0
         }
         return index
      }()
      
      selectRow(monthIndex, inComponent: 0, animated: animated)
      selectRow(yearIndex, inComponent:1, animated: animated)
   }
}

// MARK: - Picker Delegate
extension ExpiryDatePickerView: UIPickerViewDataSource, UIPickerViewDelegate {
   func numberOfComponents(in pickerView: UIPickerView) -> Int { return 2 }
   
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return component == 0 ? months.count : years.count
   }
   
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return component == 0 ? months[row] : years[row]
   }
   
   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      let selectedDate = self.selectedDate
      
      if selectedDate < minimumDate {
         setDate(minimumDate, animated: true)
      } else if selectedDate > maximumDate {
         setDate(maximumDate, animated: true)
      }
      
      externalDelegate?.expiryDateDidChange(month: selectedMonth, year: selectedYear)
   }
}

// MARK: - Protocol
protocol ExpiryDatePickerViewDelegate: AnyObject {
   func expiryDateDidChange(month: Int, year: Int)
}
