//
//  CVVTextField.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CVVTextField: UnderLineTextField {
   // MARK: - Properties
   var cvvIsValid: Bool {
      guard let text = text, let cardNumber = cardNumber else { return false }
      return Card.manager.isValid(cvv: text, cardNumber: cardNumber)
   }
   var cvv: String { return text ?? "" }
   private let maxCVVLength: Int = 4
   var cardNumber: String?
   
   // MARK: - Initialization
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setUp()
   }
   
   private func setUp() {
      placeholder = "CVV"
      semanticContentAttribute = .forceLeftToRight
      textAlignment = .left
      textContentType = nil
      textContentType = .creditCardNumber
      keyboardType = .numberPad
      delegate = self
   }
}

// MARK: - UITextFieldDelegate
extension CVVTextField: UITextFieldDelegate {
   func textField(
      _ textField: UITextField,
      shouldChangeCharactersIn range: NSRange,
      replacementString string: String) -> Bool {
      guard let text = textField.text else { return true }
      
      let cvvLength = text.count + string.count
      guard cvvLength <= Card.manager.maximumCVVLength else { return false }
      guard let cardNumber = cardNumber else { return true }
      return cvvLength <= Card.manager.maximumValidCVVLength(for: cardNumber)
   }
}
