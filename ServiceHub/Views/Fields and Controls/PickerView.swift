//
//  PickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/21/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class PickerView: UIView {
   // MARK: - Properties
   weak var delegate: PickerViewDelegate?
   private let title: String?
   var items: [String]
   var icons: [UIImage?]
   private var indexForSelectedItem: Int?
   private let titleIsHidden: Bool
   private let shouldIndentTitle: Bool
   internal var _cellHeight: CGFloat?
   var cellHeight: CGFloat {
      if let cellHeight = self._cellHeight { return cellHeight }
      
      let maxImageHeight: CGFloat = self.icons.compactMap { $0?.size.height }.max() ?? 0
      let attributedText = NSAttributedString(
         string: "AAA",
         attributes: [.font: UIFont.regularFont(ofSize: 17)]
      )
      let textHeight = attributedText.size().height
      let cellHeight = max(maxImageHeight, textHeight) + self.verticalCellPadding * 2
      
      self._cellHeight = cellHeight
      return cellHeight
   }
   var verticalInset: CGFloat = 3
   var verticalCellPadding: CGFloat = 7
   var horizontalCellPadding: CGFloat = 10
   var titleViewBackgroundColor: UIColor? = .background {
      didSet { self.titleView.backgroundColor = self.titleViewBackgroundColor }
   }
   
   override var intrinsicContentSize: CGSize {
      .init(super.intrinsicContentSize.width, self.cellHeight + self.verticalInset * 2)
   }
   
   // MARK: - UI
   lazy var titleView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = self.titleViewBackgroundColor
      view.layer.shadowColor = UIColor.black.cgColor
      let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
      view.layer.shadowOffset = .init(direction == .leftToRight ? -1 : 1, 0)
      view.layer.shadowRadius = 0
      view.layer.shadowOpacity = 1
      view.layer.masksToBounds = false
      
      view.preservesSuperviewLayoutMargins = true
      view.addSubview(self.titleLabel)
      NSLayoutConstraint.activate([
         self.titleLabel.topAnchor.constraint(equalTo: view.topAnchor),
         self.titleLabel.leadingAnchor.constraint(
            equalTo: self.shouldIndentTitle ?
               view.layoutMarginsGuide.leadingAnchor : view.leadingAnchor),
         self.titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         self.titleLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.font = .regularFont(ofSize: 16)
      label.numberOfLines = 3
      label.minimumScaleFactor = 0.7
      label.adjustsFontSizeToFitWidth = true
      label.textAlignment = .natural
      label.text = self.title
      return label
   }()
   /// Used to override the type of cell that the collection view uses. When overriding, the cellForRowAt method must be overridden as well.
   class var cellType: AnyClass { PickerCollectionViewCell.self }
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      layout.minimumInteritemSpacing = 10
      layout.sectionInset = .init(top: 0, left: 8, bottom: 0, right: 8)
      
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.showsVerticalScrollIndicator = false
      view.backgroundColor = nil
      
      view.showsHorizontalScrollIndicator = false
      
      view.register(Self.cellType, forCellWithReuseIdentifier: "PickerCollectionViewCell")
      
      return view
   }()
   
   // MARK: - Init
   init(
      title: String?,
      items: [String],
      icons: [UIImage?] = [],
      indexForSelectedItem index: Int? = nil,
      shouldIndentTitle: Bool = false,
      titleRelativeWidth: CGFloat = 0.3
   ) {
      self.title = title
      self.titleIsHidden = title == nil
      self.items = items
      self.icons = {
         let nilIcons = [UIImage?].init(repeating: nil, count: items.count)
         
         if icons.isEmpty { return nilIcons }
         
         guard items.count == icons.count else {
            assertionFailure("[PickerView] Mismatch between item and icon lists.")
            return nilIcons
         }
         
         return icons
      }()
      self.shouldIndentTitle = shouldIndentTitle
      super.init(frame: .zero)
      
      if !(self.icons.compactMap { $0 }).isEmpty {
         self.verticalCellPadding = 12
         self.horizontalCellPadding = 16
      }
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.layer.masksToBounds = true
      
      self.addSubview(self.collectionView)
      
      NSLayoutConstraint.activate([
         self.collectionView.topAnchor.constraint(equalTo: self.topAnchor),
         self.collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
      
      if !titleIsHidden {
         self.addSubview(self.titleView)
         NSLayoutConstraint.activate([
            self.titleView.topAnchor.constraint(equalTo: self.topAnchor),
            self.titleView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.titleView.widthAnchor.constraint(
               equalTo: self.widthAnchor, multiplier: titleRelativeWidth),
            self.titleView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.collectionView.leadingAnchor.constraint(equalTo: self.titleView.trailingAnchor)
         ])
      } else {
         self.collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
      }
      
      if !self.items.isEmpty { self.select(itemAtIndex: index ?? 0) }
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if self.titleIsHidden,
         let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func select(itemAtIndex index: Int?) {
      self.indexForSelectedItem = index
      self.collectionView.selectItem(
         at: {
            if let index = index { return .init(item: index, section: 0) }
            else { return nil}
      }(),
         animated: false,
         scrollPosition: .top
      )
   }
   
   func set(items: [String], icons: [UIImage?] = []) {
      self.items = items
      self.icons = {
         let nilIcons = [UIImage?].init(repeating: nil, count: items.count)
         
         if icons.isEmpty { return nilIcons }
         
         guard items.count == icons.count else {
            assertionFailure("[PickerView] Mismatch between item and icon lists.")
            return nilIcons
         }
         
         return icons
      }()
      self.invalidate()
   }
   
   private func invalidate() {
      self._cellHeight = nil
      self.invalidateIntrinsicContentSize()
      self.collectionView.collectionViewLayout.invalidateLayout()
      self.collectionView.reloadData()
   }
}

// MARK: - Collection View
extension PickerView: UICollectionViewDelegate, UICollectionViewDataSource {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.items.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "PickerCollectionViewCell", for: indexPath)
         as? PickerCollectionViewCell else
      {
         fatalError("[PickerView] Failed to dequeue PickerCollectionViewCell.")
      }
      
      cell.setValues(text: self.items[indexPath.item], image: self.icons[indexPath.item])
      
      return cell
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      didSelectItemAt indexPath: IndexPath
   ) {
      self.indexForSelectedItem = indexPath.item
      self.delegate?.pickerView(self, didSelectItemAtIndex: indexPath.item)
   }
   
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
      guard self.title != nil else { return }
      
      self.titleView.layer.shadowRadius = {
         let horizontalOffset = scrollView.contentOffset.x
         let inset: CGFloat = 8
         let fraction = min(max(0, horizontalOffset / inset), 1)
         return fraction * 4
      }()
   }
}

extension PickerView: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      let attributedText = NSAttributedString(
         string: self.items[indexPath.item],
         attributes: [.font: UIFont.regularFont(ofSize: 17)]
      )
      let imageWidth: CGFloat = self.icons[indexPath.item]?.size.width ?? 0
      let spacing: CGFloat = imageWidth > 0 ? PickerCollectionViewCell.horizontalSpacing : 0
      
      return CGSize(
         attributedText.size().width + imageWidth + spacing + self.horizontalCellPadding * 2,
         self.titleIsHidden ? collectionView.bounds.height : self.cellHeight
      )
   }
}

// MARK: - Protocols
protocol PickerViewDelegate: AnyObject {
   func pickerView(_ pickerView: PickerView, didSelectItemAtIndex index: Int)
}
