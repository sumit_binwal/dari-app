//
//  UnderlineTextField.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class UnderLineTextField: UITextField {
   // MARK: - Properties
   private let underlineLayer = CALayer()
   let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
   var underlineColor: UIColor = .subtleText {
      didSet { if !isFirstResponder { underlineLayer.backgroundColor = underlineColor.cgColor } }
   }
   var selectedUnderlineColor: UIColor = .prominentText {
      didSet { if isFirstResponder { underlineLayer.backgroundColor = selectedUnderlineColor.cgColor } }
   }
   var lineHeight: CGFloat = 1 {
      didSet { underlineLayer.frame = CGRect(0, bounds.height - lineHeight, bounds.width, bounds.height) }
   }
   var underlineLengthIsDynamic = false
   var isPadded: Bool = true
   private var underlineUnselectedWidth: CGFloat {
      guard underlineLengthIsDynamic else { return bounds.width }
      guard isPadded else { return bounds.width }
      return bounds.width - (isFirstResponder ? 0 : padding.left + padding.right)
   }
   private var underlineTransform: CATransform3D {
      let identity = CATransform3DIdentity
      guard underlineLengthIsDynamic else { return identity }
      guard isPadded else { return identity }
      let unselectedWidth = underlineUnselectedWidth
      return isFirstResponder ?
         CATransform3DMakeScale(bounds.width / unselectedWidth, 1, 1) : identity
   }
   override var intrinsicContentSize: CGSize {
      let width: CGFloat = super.intrinsicContentSize.width
      let height: CGFloat = {
         guard let font = font else {
            assertionFailure("[UnderlineTextField] Failed to retrieve font.")
            return 44
         }
         let text = NSAttributedString(string: "LABEL", attributes: [.font: font])
         return text.size().height + 24
      }()
      return CGSize(width, height)
   }
   
   // MARK: - Initializers and Initial Configuration
   override init(frame: CGRect) {
      super.init(frame: frame)
      setUp()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setUp()
   }
   
   override func awakeFromNib() {
      super.awakeFromNib()
      setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      borderStyle = .none
      layer.addSublayer(underlineLayer)
      underlineLayer.backgroundColor = underlineColor.cgColor
      backgroundColor = .lighterBackground
      textColor = .prominentText
      attributedPlaceholder = NSAttributedString(
         string: placeholder ?? "", attributes: [.foregroundColor: UIColor.gray])
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      underlineLayer.frame.size = CGSize(underlineUnselectedWidth, lineHeight)
      underlineLayer.frame.center = CGPoint(bounds.center.x, bounds.height - lineHeight)
      underlineLayer.backgroundColor = isFirstResponder ? selectedUnderlineColor.cgColor : underlineColor.cgColor
      underlineLayer.transform = underlineTransform
   }
   
   // MARK: - Other
   override open func textRect(forBounds bounds: CGRect) -> CGRect {
      guard isPadded else { return super.textRect(forBounds: bounds) }
      
      var paddedRect = bounds.inset(by: padding)
      adjustPaddingRectForAccessoryViews(&paddedRect)
      return paddedRect
   }
   
   override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
      guard isPadded else { return super.placeholderRect(forBounds: bounds) }
      
      var paddedRect = bounds.inset(by: padding)
      adjustPaddingRectForAccessoryViews(&paddedRect)
      return paddedRect
   }
   
   override open func editingRect(forBounds bounds: CGRect) -> CGRect {
      guard isPadded else { return super.editingRect(forBounds: bounds) }
      
      var paddedRect = bounds.inset(by: padding)
      adjustPaddingRectForAccessoryViews(&paddedRect)
      return paddedRect
   }
   
   private func adjustPaddingRectForAccessoryViews(_ paddedRect: inout CGRect) {
      if let rightView = rightView,
         (rightViewMode == .always || rightViewMode == .unlessEditing) {
         paddedRect.size.width -= rightView.frame.width
      }
      if let leftView = leftView,
         (leftViewMode == .always || leftViewMode == .unlessEditing) {
         paddedRect.size.width -= leftView.frame.width
         paddedRect.origin.x += leftView.frame.width
      }
   }
   
   func flash(andBecomeFirstResponder becomeFirstResponder: Bool) {
      let calculationMode = CAAnimationCalculationMode.cubicPaced
      let xScaleAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
         animation.valueFunction = CAValueFunction(name: .scaleX)
         animation.values = [1.0, 1.03, 1.0]
         animation.calculationMode = calculationMode
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.4
         return animation
      }()
      let yScaleAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
         animation.valueFunction = CAValueFunction(name: .scaleY)
         animation.values = [1.0, 2, 1.0]
         animation.isAdditive = true
         animation.calculationMode = calculationMode
         animation.repeatCount = .greatestFiniteMagnitude
         animation.duration = 0.4
         return animation
      }()
      
      let normalColor: CGColor = isFirstResponder ? selectedUnderlineColor.cgColor : underlineColor.cgColor
      let highlightAnimation: CAKeyframeAnimation = {
         let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.backgroundColor))
         let endingColor: CGColor = becomeFirstResponder ? selectedUnderlineColor.cgColor : normalColor
         animation.values = [normalColor, UIColor.red.cgColor, endingColor]
         animation.calculationMode = calculationMode
         return animation
      }()
      let animationGroup = CAAnimationGroup()
      animationGroup.animations = [xScaleAnimation, yScaleAnimation, highlightAnimation]
      animationGroup.isRemovedOnCompletion = true
      animationGroup.duration = 0.8
      underlineLayer.add(animationGroup, forKey: nil)
      
      if becomeFirstResponder {
         DispatchQueue.main.asyncAfter(deadline: .now() + animationGroup.duration) {
            self.becomeFirstResponder()
         }
      }
   }
}
