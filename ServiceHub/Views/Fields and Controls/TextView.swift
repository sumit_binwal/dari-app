//
//  TextView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/27/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class TextView: UITextView {
   override var intrinsicContentSize: CGSize {
      guard let font = self.font else { return super.intrinsicContentSize }
      let verticalInset = self.textContainerInset.top + self.textContainerInset.bottom
      let horizontalInset = self.textContainerInset.left + self.textContainerInset.right
      
      let actualHeight = self.sizeThatFits(
         .init(self.bounds.width - horizontalInset, .greatestFiniteMagnitude)
      ).height
      let minimumHeight = NSAttributedString(string: "A\rA", attributes: [.font: font])
         .size().height
      
      return .init(
         super.intrinsicContentSize.width,
         max(minimumHeight, actualHeight) + verticalInset
      )
   }
}
