//
//  ProminentButton.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/19/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class ProminentButton: UIButton {
   
   private static var highlightTransform: CGAffineTransform { .init(scaleX: 0.97, y: 0.97) }
   
   override var isHighlighted: Bool {
      didSet {
         UIView.animate(withDuration: .highlightAnimationDuration) {
            self.transform = self.isHighlighted ? Self.highlightTransform : .identity
         }
      }
   }
   
   override var intrinsicContentSize: CGSize {
      .init(super.intrinsicContentSize.width + 10, super.intrinsicContentSize.height + 16)
   }
   
   override var isEnabled: Bool {
      didSet {
         refreshBackground()
      }
   }
   
   // MARK: - Initialization and Setup
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.setTitleColor(.white, for: .normal)
      self.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .disabled)
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.titleLabel?.font = .boldFont(ofSize: 17)
   }
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      self.setUp()
   }
   
   override func tintColorDidChange() {
      refreshBackground()
   }
   
   private func refreshBackground() {
      backgroundColor = isEnabled ? tintColor : .dimmedTint
   }
}
