//
//  MeasurementInputView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/30/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class MeasurementInputView: UIView {
   weak var delegate: MeasurementInputViewDelegate?
   private lazy var formatter: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.alwaysShowsDecimalSeparator = false
      formatter.allowsFloats = false
      return formatter
   }()
   private var unit: Unit
   var measurement: Measurement<Unit> { .init(value: self.stepper.value, unit: self.unit) }
   
   // MARK: - UI
   lazy var textField: UITextField = {
      let field = UITextField()
      field.translatesAutoresizingMaskIntoConstraints = false
      field.textContentType = .none
      field.keyboardType = .numberPad
      field.autocorrectionType = .no
      field.font = .regularFont(ofSize: 17)
      field.textColor = .prominentText
      field.setContentHuggingPriority(.defaultLow, for: .horizontal)
      field.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
      return field
   }()
   private lazy var unitLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 16)
      label.textColor = .text
      label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
      return label
   }()
   private lazy var stepper: UIStepper = {
      let stepper = UIStepper()
      stepper.translatesAutoresizingMaskIntoConstraints = false
      stepper.isContinuous = true
      stepper.addTarget(self, action: #selector(self.stepperDidChange), for: .valueChanged)
      return stepper
   }()
   
   // MARK: - Init
   init(
      value: Double,
      unit: Unit,
      minimumStepperValue: Int,
      maximumStepperValue: Int,
      stepValue: Int
   ) {
      self.unit = unit
      super.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .lighterBackground
      
      self.layer.cornerRadius = .cornerRadius
      self.layer.masksToBounds = true
      self.layer.borderColor = UIColor.subtleBorder.withAlphaComponent(0.4).cgColor
      self.layer.borderWidth = .pixel
      
      self.setTextFieldValue(value)
      self.stepper.value = value
      
      self.unitLabel.text = unit.symbol
      
      self.stepper.minimumValue = Double(minimumStepperValue)
      self.stepper.maximumValue = Double(maximumStepperValue)
      self.stepper.stepValue = Double(stepValue)
      
      self.addSubviews([self.textField, self.unitLabel, self.stepper])
      let margins = self.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.textField.topAnchor.constraint(equalTo: margins.topAnchor),
         self.textField.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.textField.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         
         self.unitLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.unitLabel.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.textField.trailingAnchor, multiplier: 1),
         self.unitLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         
         self.stepper.topAnchor.constraint(equalTo: margins.topAnchor),
         self.stepper.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.unitLabel.trailingAnchor, multiplier: 1),
         self.stepper.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         self.stepper.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.layer.borderColor = UIColor.subtleBorder.withAlphaComponent(0.4).cgColor
   }
   
   // MARK: - Actions
   @objc
   func stepperDidChange() {
      self.setTextFieldValue(stepper.value)
      self.delegate?.measurementInputView(self, didChangeMeasurement: self.measurement)
   }
   
   @objc
   func textFieldDidChange() {
      let valueOrNil = self.formatter.number(from: self.textField.text ?? "") as? Double
      if valueOrNil == nil {
         self.setTextFieldValue(0)
      }
      self.setTextFieldValue(valueOrNil ?? 0)
      self.stepper.value = valueOrNil ?? 0
      self.delegate?.measurementInputView(self, didChangeMeasurement: self.measurement)
   }
   
   // MARK: - Other
   func setTextFieldValue(_ number: Double) {
      self.textField.text = self.formatter.string(from: number)
   }
}

// MARK: - NumberInputViewDelegate
protocol MeasurementInputViewDelegate: AnyObject {
   func measurementInputView(
      _ view: MeasurementInputView,
      didChangeMeasurement measurement: Measurement<Unit>
   )
}
