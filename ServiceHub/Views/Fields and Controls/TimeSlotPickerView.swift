//
//  TimeSlotPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/7/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class TimeSlotPickerView: UIView {
   // MARK: - Properties
   weak var delegate: TimeSlotPickerViewDelegate?
   var timeSlots: [TimeSlot] = [] {
      didSet { self.collectionView.collectionViewLayout.invalidateLayout() }
   }
   private var indexForSelectedTimeSlot: Int?
   private lazy var cellHeight: CGFloat = {
      let string = NSAttributedString(
         string: "AAA",
         attributes: [.font: TimeSlotCollectionViewCell.font]
      )
      return string.size().height * 2 + TimeSlotCollectionViewCell.spacing + 16
   }()
   
   // MARK: - UI
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.preservesSuperviewLayoutMargins = true
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         TimeSlotCollectionViewCell.self,
         forCellWithReuseIdentifier: "TimeSlotCollectionViewCell"
      )
      
      // Extra pixel is needed as a quick fix to a leading constraint offset issue. In particular, the first cell is offset by about 10 pixels and the console shows a warning about the cell's height being greater than that of the collection view. Note that this doesn't happen in the simulator.
      view.heightAnchor.constraint(equalToConstant: self.cellHeight + .pixel).isActive = true
      
      return view
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubview(self.collectionView)
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func select(_ timeSlot: TimeSlot?, animated: Bool = false, scrollToSelection: Bool = false) {
      let scrollPosition: UICollectionView.ScrollPosition = scrollToSelection ?
         .centeredHorizontally : .top
      
      DispatchQueue.main.async {
         if let row = self.timeSlots.firstIndex(of: timeSlot) {
            self.indexForSelectedTimeSlot = row
            self.collectionView.selectItem(
               at: .init(item: row, section: 0),
               animated: animated,
               scrollPosition: scrollPosition
            )
         } else {
            self.indexForSelectedTimeSlot = nil
            self.collectionView.selectItem(
               at: nil,
               animated: animated,
               scrollPosition: scrollPosition
            )
         }
      }
   }
   
   func reload(timeSlots: [TimeSlot], selectedTimeSlot: TimeSlot?) {
      guard timeSlots != self.timeSlots else { return }
      
      // FIXME: Improve animation.
      DispatchQueue.main.async {
         self.indexForSelectedTimeSlot = nil
         self.collectionView.performBatchUpdates({
            self.timeSlots = timeSlots
            self.collectionView.deleteSections([0])
            self.collectionView.insertSections([0])
         }) { _ in
            self.select(selectedTimeSlot, animated: true, scrollToSelection: true)
         }
      }
   }
}

// MARK: - Collection View
extension TimeSlotPickerView: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.timeSlots.count
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "TimeSlotCollectionViewCell", for: indexPath)
         as? TimeSlotCollectionViewCell else
      {
         fatalError("[TimeSlotPickerView] Failed to dequeue TimeSlotCollectionViewCell.")
      }
      cell.setValue(timeSlot: self.timeSlots[indexPath.row])
      cell.isSelected = self.indexForSelectedTimeSlot == indexPath.row
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.delegate?.timeSlotPicker(didSelectTimeSlot: self.timeSlots[indexPath.item])
   }
}

extension TimeSlotPickerView: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      // When the collection view is reloaded, index path is sometimes out of bounds.
      guard indexPath.item < self.timeSlots.count else { return .zero }
      
      return .init(
         TimeSlotCollectionViewCell.width(forTimeSlot: self.timeSlots[indexPath.item]),
         self.cellHeight
      )
   }
}

// MARK: - TimeSlotPickerViewDelegate
protocol TimeSlotPickerViewDelegate: AnyObject {
   func timeSlotPicker(didSelectTimeSlot timeSlot: TimeSlot)
}
