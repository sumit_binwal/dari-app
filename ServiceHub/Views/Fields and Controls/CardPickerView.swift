//
//  CardPickerView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardPickerView: UIView {
   // MARK: - Properties
   weak var delegate: CardPickerViewDelegate?
   var cards: [Card] { didSet { self.collectionView.collectionViewLayout.invalidateLayout() } }
   var indexForSelectedCard: Int?
   
   override var intrinsicContentSize: CGSize {
      .init(super.intrinsicContentSize.width, 65)
   }
   
   // MARK: - UI
   private lazy var collectionView: UICollectionView = {
      let layout = CollectionViewFlowLayout()
      let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.delegate = self
      view.dataSource = self
      view.allowsMultipleSelection = false
      view.preservesSuperviewLayoutMargins = true
      view.backgroundColor = nil
      
      view.showsHorizontalScrollIndicator = false
      view.showsVerticalScrollIndicator = false
      
      view.register(
         CardPickerCollectionViewCell.self,
         forCellWithReuseIdentifier: "CardPickerCollectionViewCell"
      )
      view.register(
         ImageCollectionViewCell.self,
         forCellWithReuseIdentifier: "ImageCollectionViewCell"
      )
      
      return view
   }()
   
   // MARK: - Init
   init(cards: [Card]) {
      self.cards = cards
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      self.cards = []
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.addFillingSubview(self.collectionView)
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      if let layout = self.collectionView.collectionViewLayout as? CollectionViewFlowLayout {
         let inset = self.layoutMargins.left
         layout.sectionInset = .init(top: 0, left: inset, bottom: 0, right: inset)
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
   }
   
   // MARK: - Other
   func select(card: Card?) {
      if let item = self.cards.firstIndex(of: card) {
         self.indexForSelectedCard = item
         self.collectionView.selectItem(
            at: .init(item: item, section: 0),
            animated: false,
            scrollPosition: .top
         )
      } else {
         self.indexForSelectedCard = nil
         self.collectionView.selectItem(at: nil, animated: false, scrollPosition: .top)
      }
   }
   
   func addCard(_ card: Card, select: Bool = true) {
      DispatchQueue.main.async {
         self.collectionView.performBatchUpdates({
            self.cards.append(card)
            self.collectionView.insertItems(at: [.init(item: self.cards.count - 1, section: 0)])
         }) { _ in
            if select { self.select(card: card) }
         }
      }
   }
   
   func removeCards(_ cards: [Card]) {
      let indices: [Int] = cards
         .compactMap { self.cards.firstIndex(of: $0) }
         .sorted { $0 > $1 }
      
      let indexPaths: [IndexPath] = indices.map { IndexPath(item: $0, section: 0) }
      
      DispatchQueue.main.async {
         self.collectionView.performBatchUpdates {
            indexPaths.forEach {
               self.cards.remove(at: $0.item)
               self.collectionView.deleteItems(at: [$0])
            }
         } completion: { _ in
            self.select(card: self.cards.first)
         }
      }
   }
}

// MARK: - Collection View
extension CardPickerView: UICollectionViewDataSource, UICollectionViewDelegate {
   func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int
   ) -> Int {
      self.cards.count + 1
   }
   
   func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
   ) -> UICollectionViewCell {
      guard indexPath.item < self.cards.count else {
         guard let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath)
            as? ImageCollectionViewCell else
         {
            fatalError("[CardPickerView] Failed to dequeue CardPickerCollectionViewCell.")
         }
         cell.imageView.image = UIImage(named: "Add Card")
         return cell
      }
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: "CardPickerCollectionViewCell", for: indexPath)
         as? CardPickerCollectionViewCell else
      {
         fatalError("[CardPickerView] Failed to dequeue CardPickerCollectionViewCell.")
      }
      let card = self.cards[indexPath.item]
      cell.setValues(last4: card.last4, schemeImage: card.templateImage)
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if indexPath.item < self.cards.count {
         self.delegate?.cardPickerView(didSelectCard: self.cards[indexPath.item])
      } else {
         self.delegate?.cardPickerViewDidTapAddCard()
      }
   }
}

extension CardPickerView: UICollectionViewDelegateFlowLayout {
   func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
   ) -> CGSize {
      let height = collectionView.bounds.height
      guard indexPath.item < self.cards.count else { return .init(height, height) }
      
      return .init(100, height)
   }
}

// MARK: - Protocol
protocol CardPickerViewDelegate: AnyObject {
   func cardPickerView(didSelectCard card: Card)
   func cardPickerViewDidTapAddCard()
}
