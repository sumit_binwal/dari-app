//
//  OrderPriceView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/26/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderPriceView: UIView {
   // MARK: - UI
   private lazy var visualEffectView: UIVisualEffectView = {
      let view = UIVisualEffectView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.effect = {
         if #available(iOS 13, *) {
            return UIBlurEffect(
               style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
            )
         } else {
            return UIBlurEffect(style: .prominent)
         }
      }()
      
      view.contentView.addSubviews([self.priceLabel, self.nextButton])
      let margins = view.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.priceLabel.topAnchor.constraint(equalTo: margins.topAnchor),
         self.priceLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.priceLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         
         self.nextButton.topAnchor.constraint(equalTo: margins.topAnchor),
         self.nextButton.leadingAnchor.constraint(
            equalToSystemSpacingAfter: self.priceLabel.trailingAnchor, multiplier: 1),
         self.nextButton.widthAnchor.constraint(equalTo: margins.widthAnchor, multiplier: 0.6),
         self.nextButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         self.nextButton.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
      
      return view
   }()
   lazy var priceLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.textColor = .prominentText
      label.textAlignment = .center
      label.attributedText = {
         let attributedText = NSMutableAttributedString(
            string: "SAR ",
            attributes: [.font: UIFont.mediumFont(ofSize: 13)]
         )
         attributedText.append(.init(
            string: "400",
            attributes: [.font: UIFont.boldFont(ofSize: 17)]
         ))
         return attributedText
      }()
      return label
   }()
   lazy var nextButton: ProminentButton = {
      let button = ProminentButton()
      button.setTitle("next".localized, for: .normal)
      return button
   }()
   private lazy var topBorderLayer: CALayer = {
      let layer = CALayer()
      layer.backgroundColor = UIColor.subtleBorder.cgColor
      return layer
   }()
   
   // MARK: - Init
   override init(frame: CGRect) {
      super.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.backgroundColor = .blurBackground
      self.addFillingSubview(self.visualEffectView)
      self.layer.addSublayer(self.topBorderLayer)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      self.topBorderLayer.frame = CGRect(0, 0, self.bounds.width, .pixel)
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.visualEffectView.effect = UIBlurEffect(
         style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
      )
      self.topBorderLayer.backgroundColor = UIColor.subtleBorder.cgColor
   }
}
