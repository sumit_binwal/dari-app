//
//  AuthOptionsView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/12/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AuthOptionsView: UIView {
   // MARK: - UI
   private lazy var registrationOptionsStackView: UIStackView = {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 4
      
      view.addArrangedSubview(self.registerButton)
      
      return view
   }()
   lazy var registerButton: ProminentButton = {
      let button = ProminentButton()
      button.titleLabel?.font = .boldFont(ofSize: 18)
      button.setTitle("signUp".localized.localizedUppercase, for: .normal)
      return button
   }()
   
   private lazy var logInStackView: UIStackView = {
      let view = UIStackView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .horizontal
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 4
      
      view.addArrangedSubviews([self.logInLabel, self.logInButton])
      
      return view
   }()
   private lazy var logInLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "alreadySignedUp".localized
      label.font = .regularFont(ofSize: 14)
      label.textColor = .subtleText
      return label
   }()
   lazy var logInButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.titleLabel?.font = .regularFont(ofSize: 14)
      button.setTitle("signIn".localized, for: .normal)
      return button
   }()
   lazy var guestButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.titleLabel?.font = .regularFont(ofSize: 13)
      button.setTitle("continueAsGuest".localized, for: .normal)
      return button
   }()
   
   // MARK: - Init
   convenience init() {
      self.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .clear
      
      self.addSubviews([self.registrationOptionsStackView, self.logInStackView, self.guestButton])
      NSLayoutConstraint.activate([
         self.registrationOptionsStackView.topAnchor.constraint(equalTo: self.topAnchor),
         self.registrationOptionsStackView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
         self.registrationOptionsStackView.widthAnchor.constraint(
            equalTo: self.widthAnchor, multiplier: 0.7),
         
         self.logInStackView.topAnchor.constraint(
            equalToSystemSpacingBelow: self.registrationOptionsStackView.bottomAnchor,
            multiplier: 2),
         self.logInStackView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
         
         self.guestButton.topAnchor.constraint(equalTo: self.logInStackView.bottomAnchor),
         self.guestButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
         self.guestButton.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
   }
}
