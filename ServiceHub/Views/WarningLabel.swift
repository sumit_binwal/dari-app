//
//  WarningLabel.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/17/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class WarningLabel: TagLabel {
   override  func tintColorDidChange() {
      self.backgroundColor = self.tintAdjustmentMode == .dimmed ? .dimmedTint : .red
   }
}
