//
//  SchemeIconsView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit
import Frames

class SchemeIconsView: UIStackView {
   // MARK: - Properties
   private var schemes: [CardScheme] = Card.manager.acceptedSchemes
   private lazy var iconImageViews: [UIImageView] = {
      let icons = schemes.compactMap { $0.icon }
       print(schemes)
       print(icons)
      let imageViews = icons.map { UIImageView(image: $0) }
       print(imageViews)
      if schemes.count != icons.count {
         assertionFailure("[SchemeIconsView] Failed to get icon for a scheme.")
      }
      
      return imageViews
   }()
   private var highlightedScheme: CardScheme?
   private let unhighlightedAlpha: CGFloat = 0.7
   private let unhighlightedTransform: CGAffineTransform = .init(scaleX: 0.8, y: 0.8)
   
   // MARK: - Initialization
   override init(frame: CGRect) {
      super.init(frame: .zero)
      setUp()
   }
   
   required init(coder: NSCoder) {
      self.schemes = []
      super.init(coder: coder)
      setUp()
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      axis = .horizontal
      spacing = 4
      backgroundColor = .clear
      
      iconImageViews.forEach {
         addArrangedSubview($0)
         $0.alpha = unhighlightedAlpha
         $0.transform = unhighlightedTransform
      }
      
      addArrangedSubview({
         let view = UIView()
         view.backgroundColor = .clear
         view.setContentHuggingPriority(.defaultLow, for: .horizontal)
         view.translatesAutoresizingMaskIntoConstraints = false
         return view
      }())
   }
   
   public func setHighlightedScheme(forCardNumber cardNumber: String) {
       
      let standardizedCardNumber = Card.manager.standardize(cardNumber: cardNumber)
      setHighlightedScheme(Card.manager.scheme(for: standardizedCardNumber))
   }
   
   private func setHighlightedScheme(_ scheme: CardScheme?) {
      guard scheme != highlightedScheme else { return }
      
      let indexForPreviousHighlightedSchemeOrNil: Int? = {
         guard let highlightedScheme = highlightedScheme else { return nil }
         return schemes.firstIndex(of: highlightedScheme)
      }()
      
      highlightedScheme = scheme
      
      let indexForSchemeOrNil: Int? = {
         guard let scheme = scheme else { return nil }
         return schemes.firstIndex(of: scheme)
      }()
      
      UIView.animate(
         withDuration: .highlightAnimationDuration,
         delay: 0,
         options: .curveEaseInOut,
         animations: {
            if let indexForPreviousHighlightedScheme = indexForPreviousHighlightedSchemeOrNil {
               let imageView = self.iconImageViews[indexForPreviousHighlightedScheme]
               imageView.alpha = self.unhighlightedAlpha
               imageView.transform = self.unhighlightedTransform
            }
            if let indexForScheme = indexForSchemeOrNil {
               let imageView = self.iconImageViews[indexForScheme]
               imageView.alpha = 1
               imageView.transform = .identity
            }
      }, completion: nil)
   }
   
   private func index(for scheme: CardScheme) -> Int? {
      return schemes.firstIndex(of: scheme)
   }
}
