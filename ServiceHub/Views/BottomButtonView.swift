//
//  PlaceOrderView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class BottomButtonView: UIView {
   
   private lazy var visualEffectView: UIVisualEffectView = {
      let view = UIVisualEffectView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.effect = {
         if #available(iOS 13, *) {
            return UIBlurEffect(
               style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
            )
         } else {
            return UIBlurEffect(style: .prominent)
         }
      }()
      
      view.contentView.preservesSuperviewLayoutMargins = true
      view.contentView.addFillingSubviewConstrainedByMargins(self.button)
      
      return view
   }()
   
   private var buttonTitle: String
   
   lazy var button: ProminentButton = {
      let button = ProminentButton()
      button.setTitle(buttonTitle, for: .normal)
      return button
   }()
   private lazy var topBorderLayer: CALayer = {
      let layer = CALayer()
      layer.backgroundColor = UIColor.subtleBorder.cgColor
      return layer
   }()
   
   // MARK: - Init
   
   init(buttonTitle: String) {
      self.buttonTitle = buttonTitle
      super.init(frame: .zero)
      
      translatesAutoresizingMaskIntoConstraints = false
      preservesSuperviewLayoutMargins = true
      backgroundColor = .blurBackground
      addFillingSubview(visualEffectView)
      layer.addSublayer(topBorderLayer)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Life Cycle
   override func layoutSubviews() {
      super.layoutSubviews()
      self.topBorderLayer.frame = CGRect(0, 0, self.bounds.width, .pixel)
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.visualEffectView.effect = UIBlurEffect(
         style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
      )
      self.topBorderLayer.backgroundColor = UIColor.subtleBorder.cgColor
   }
}
