//
//  RatingStar.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/9/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class RatingStar: UIView {
   
   // MARK: - Properties
   
   /// Between 0 and 1
   var rating: CGFloat = 0 { didSet { updateMask() } }
   
   // MARK: UI
   
   private static var outlinedImage: UIImage? {
      if #available(iOS 13, *) {
         return UIImage(
            systemName: "star",
            withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .semibold)
         )
      } else {
         return UIImage(named: "Small Star Outlined")?.scaled(by: 2).withRenderingMode(.alwaysTemplate)
      }
   }
   private static var filledImage: UIImage? {
      if #available(iOS 13, *) {
         return UIImage(
            systemName: "star.fill",
            withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .semibold)
         )
      } else {
         return UIImage(named: "Small Star Filled")?.scaled(by: 2).withRenderingMode(.alwaysTemplate)
      }
   }
   
   var starBackgroundColor: UIColor? {
      didSet {
         backgroundStar.tintColor = starBackgroundColor
      }
   }
   
   private lazy var backgroundStar: UIImageView = {
      let imageView = UIImageView(image: Self.outlinedImage)
      imageView.translatesAutoresizingMaskIntoConstraints = false
      imageView.contentMode = .scaleAspectFit
      return imageView
   }()
   private lazy var foregroundStar: UIImageView = {
      let imageView = UIImageView(image: Self.filledImage)
      imageView.translatesAutoresizingMaskIntoConstraints = false
      imageView.contentMode = .scaleAspectFit
      imageView.tintColor = tintColor
      imageView.mask = foregroundStarMask
      return imageView
   }()
   private lazy var foregroundStarMask: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .black
      view.frame.origin = .zero
      return view
   }()
   
   convenience init(rating: CGFloat) {
      self.init(frame: .zero)
      self.rating = rating
      setUp()
   }
   
   private func setUp() {
      translatesAutoresizingMaskIntoConstraints = false
      addFillingSubview(backgroundStar)
      addFillingSubview(foregroundStar)
   }
   
   private func updateMask() {
      foregroundStarMask.frame = .init(0, 0, rating * bounds.width, bounds.height)
   }
   
   // MARK: - Life Cycle
   
   override func layoutSubviews() {
      super.layoutSubviews()
      updateMask()
   }
   
   override func tintColorDidChange() {
      foregroundStar.tintColor = tintColor
   }
}
