//
//  GreetingView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/8/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class GreetingView: UITableViewHeaderFooterView {
   // MARK: - Properties
   var userName: String = ""
   
   /// Caches animator progress. When this view disappears, the animator fraction complete is reset to 0.
   private var animatorFractionComplete: CGFloat = 0
   lazy var animator: UIViewPropertyAnimator = .init(duration: 1, curve: .linear) {
      self.visualEffectView.effect = {
         if #available(iOS 13, *) {
            return UIBlurEffect(
               style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
            )
         } else {
            return UIBlurEffect(style: .prominent)
         }
      }()
      
      self.contentView.backgroundColor = {
         if #available(iOS 13, *) {
            return UIColor { (traitCollection) -> UIColor in
               let x: CGFloat = traitCollection.userInterfaceStyle == .dark ? 0.03 : 0.97
               return UIColor(red: x, green: x, blue: x, alpha: 0.8)
            }
         } else {
            return UIColor(white: 0.97, alpha: 0.5)
         }
      }()
      self.borderView.alpha = 1
   }

   // MARK: UI
   private lazy var visualEffectView: UIVisualEffectView = {
      let view = UIVisualEffectView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.effect = nil
      view.preservesSuperviewLayoutMargins = true
      
      view.contentView.addSubview(self.stackView)
      NSLayoutConstraint.activate([
         self.stackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
         self.stackView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         self.stackView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
         self.stackView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [self.mainLabel, self.bodyLabel])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      view.alignment = .fill
      view.distribution = .fillProportionally
      view.spacing = 0
      return view
   }()
   private lazy var mainLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 20)
      label.adjustsFontSizeToFitWidth = true
      label.minimumScaleFactor = 0.7
      return label
   }()
   private lazy var bodyLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 14)
      label.textColor = .prominentText
      label.text = "assistanceQuestion".localized
      label.adjustsFontSizeToFitWidth = true
      label.minimumScaleFactor = 0.7
      label.numberOfLines = 3
      return label
   }()
   private lazy var borderView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = UIColor.separator.withAlphaComponent(0.7)
      view.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
      view.alpha = 0
      return view
   }()
   
   // MARK: - Init and Setup
   convenience init(userName: String) {
      self.init(frame: .zero)
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.contentView.backgroundColor = .background
      
      self.contentView.addSubviews([self.visualEffectView, self.borderView])
      NSLayoutConstraint.activate([
         self.visualEffectView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
         self.visualEffectView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.visualEffectView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.visualEffectView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
         
         self.borderView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
         self.borderView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
         self.borderView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
      ])
      
      self.setUserName(userName)
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.reloadVisualEffect),
         name: UIApplication.willEnterForegroundNotification,
         object: nil
      )
   }
   
   func setUserName(_ name: String) {
      self.mainLabel.text =
         "\("greeting".localized)\(name.isEmpty ? "" : "\(",".localized) \(name)")"
   }
   
   deinit {
      self.animator.stopAnimation(true)
      NotificationCenter.default.removeObserver(self)
   }
   
   // MARK: - Life Cycle
   override func tintColorDidChange() {
      super.tintColorDidChange()
      self.mainLabel.textColor = self.tintColor
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      // FIXME: If appearance changes when the app is in the background, this view doesn't change until the app is brought to the foreground; i.e. the change isn't made in the screenshot for the app selector.
      self.reloadVisualEffect()
   }
   
   // MARK: - Other
   @objc
   func reloadVisualEffect() {
      DispatchQueue.main.async {
         self.animator.fractionComplete = 0
         self.animator.stopAnimation(true)
         
         self.visualEffectView.effect = nil
         self.contentView.backgroundColor = .background
         self.borderView.alpha = 0
         
         self.animator = .init(duration: 1, curve: .linear) {
            self.visualEffectView.effect = {
               if #available(iOS 13, *) {
                  return UIBlurEffect(
                     style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
                  )
               } else {
                  return UIBlurEffect(style: .prominent)
               }
            }()
            
            self.contentView.backgroundColor = {
               if #available(iOS 13, *) {
                  return UIColor { (traitCollection) -> UIColor in
                     let x: CGFloat = traitCollection.userInterfaceStyle == .dark ? 0.03 : 0.97
                     return UIColor(red: x, green: x, blue: x, alpha: 0.8)
                  }
               } else {
                  return UIColor(white: 0.97, alpha: 0.5)
               }
            }()
            self.borderView.alpha = 1
         }
         
         self.setAnimatorProgress(self.animatorFractionComplete)
      }
   }
   
   func setAnimatorProgress(_ fractionComplete: CGFloat) {
      animatorFractionComplete = fractionComplete
      animator.fractionComplete = fractionComplete
   }
}
