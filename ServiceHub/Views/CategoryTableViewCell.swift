//
//  CategoryTableViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
@available(*, deprecated)
class CategoryTableViewCell: UITableViewCell {
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var collectionView: UICollectionView!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      titleLabel.textColor = .prominentText
      selectionStyle = .none
      collectionView.collectionViewLayout = DeprecatedCollectionViewFlowLayout()
   }
   
}

fileprivate class DeprecatedCollectionViewFlowLayout: UICollectionViewFlowLayout {
   override var flipsHorizontallyInOppositeLayoutDirection: Bool { return true }
   
   override init() {
      super.init()
      configure()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      configure()
   }
   
   private func configure() {
      scrollDirection = .horizontal
      sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
   }
}
