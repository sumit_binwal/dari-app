//
//  ActivityIndicator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/13/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ActivityIndicator: UIView {
   // MARK: - Properties
   private var circleDiameter: CGFloat
   private var spacing: CGFloat
   private var circles: [CAShapeLayer]
   override var tintColor: UIColor! {
      didSet { circles.forEach { $0.fillColor = tintColor.cgColor } }
   }
   
   // MARK: - Life Cycle
   init(numberOfCircles: Int, diameter: CGFloat, spacing: CGFloat) {
      circleDiameter = diameter
      self.spacing = spacing
      
      circles = []
      super.init(frame: .zero)
      self.tintColor = .foreground
      translatesAutoresizingMaskIntoConstraints = false
      heightAnchor.constraint(equalToConstant: diameter).isActive = true
      widthAnchor.constraint(
         equalToConstant: diameter + CGFloat(numberOfCircles - 1) * (diameter + spacing))
         .isActive = true
      
      for _ in 0 ..< numberOfCircles {
         let layer = CAShapeLayer()
         layer.frame = CGRect(x: 0, y: 0, width: diameter, height: diameter)
         layer.path = UIBezierPath(ovalIn: layer.frame).cgPath
         layer.fillColor = tintColor.withAlphaComponent(0.8).cgColor
         circles.append(layer)
         self.layer.addSublayer(layer)
      }
      
      addAnimationGroups()
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      
      for (index, circle) in circles.enumerated() {
         circle.frame.origin.x = CGFloat(index) * (circleDiameter + spacing)
      }
   }
   
   override func tintColorDidChange() {
      self.circles.forEach { $0.fillColor = self.tintColor.cgColor }
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
         return
      }
      CATransaction.begin()
      CATransaction.setCompletionBlock {
         self.addAnimationGroups()
      }
      self.stopAnimations()
      CATransaction.commit()
   }
   
   // MARK: - Initializers
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Others
   func addAnimationGroups() {
      for (index, circle) in circles.enumerated() {
         var scaleFactorValues: [CGFloat] = .init(repeating: 1, count: circles.count + 2)
         scaleFactorValues.insert(1.4, at: index + 1)
         scaleFactorValues[index + 2] = 0.8
         let duration: CFTimeInterval = 1.5
         let xScaleAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
            animation.valueFunction = CAValueFunction(name: .scaleX)
            animation.values = scaleFactorValues
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = duration
            return animation
         }()
         let yScaleAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.transform))
            animation.valueFunction = CAValueFunction(name: .scaleY)
            animation.values = scaleFactorValues
            animation.isAdditive = true
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = duration
            return animation
         }()
         
         let colorAnimation: CAKeyframeAnimation = {
            let animation = CAKeyframeAnimation(keyPath: #keyPath(CAShapeLayer.fillColor))
            animation.values = scaleFactorValues.compactMap {
               UIColor.foreground?.withAlphaComponent($0 / 1.4).cgColor
            }
            animation.isAdditive = false
            animation.repeatCount = .greatestFiniteMagnitude
            animation.duration = duration
            return animation
         }()
         
         let animationGroup = CAAnimationGroup()
         animationGroup.animations = [xScaleAnimation, yScaleAnimation, colorAnimation]
         animationGroup.isRemovedOnCompletion = false
         animationGroup.repeatCount = .greatestFiniteMagnitude
         animationGroup.duration = duration
         circle.add(animationGroup, forKey: nil)
      }
   }
   
   func stopAnimations() {
      self.circles.forEach {
         $0.removeAllAnimations()
      }
   }
}
