//
//  ServiceCollectionViewCell.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/29/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class DeprecatedServiceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
   @IBOutlet weak var startingPriceLabel: UILabel!
   private lazy var startingSubtitleLabelFont: UIFont = subtitleLabel.font
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var recommendedLabel: UILabel!
    var gradientLayer: CAGradientLayer!
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: .highlightAnimationDuration) {
                self.transform = self.isHighlighted ? .highlightTransformForLargeViews : .identity
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.superview?.backgroundColor = .background
        [titleLabel, subtitleLabel, startingPriceLabel].forEach { $0?.textColor = .white }
        
        imageView.backgroundColor = .offWhite
        layer.borderColor = UIColor.subtleBorder.cgColor
        layer.borderWidth = .pixel
        layer.cornerRadius = .cornerRadius
        layer.masksToBounds = true
        
        gradientLayer = {
            let layer = CAGradientLayer()
            layer.colors = [UIColor.clear.cgColor, UIColor.darkCalypso.withAlphaComponent(0.9).cgColor]
            layer.locations = [0.0, 1]
            return layer
        }()
        
        gradientView.backgroundColor = UIColor.clear
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
        
        recommendedLabel.backgroundColor = .tint
        recommendedLabel.textColor = .white
        recommendedLabel.layer.cornerRadius = 4
        recommendedLabel.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard #available(iOS 13, *) else { return }
        guard traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        layer.borderColor = UIColor.subtleBorder.cgColor
    }
    
    func setStartingPrice(_ price: Double) {
        let attributedString: NSAttributedString = {
            let startingString = "starting".localized
            let priceString = " \(NumberFormatter.currency.string(from: price))"
            
            let adjustedFont = UIFont.systemFont(ofSize: startingSubtitleLabelFont.pointSize * 0.8)
            
            let string = NSMutableAttributedString(string: startingString,
                                                   attributes: [.font: adjustedFont])
            string.append(NSAttributedString(string: priceString, attributes: [.font: startingSubtitleLabelFont]))
            
            return string
        }()
        
        subtitleLabel.attributedText = attributedString
        subtitleLabel.isHidden = false
    }
    
    func setStartingPrice(_ priceString: String) {
        let attributedString: NSAttributedString = {
            let startingString = "starting".localized
            
            let adjustedFont = UIFont.systemFont(ofSize: startingSubtitleLabelFont.pointSize * 0.8)
            
            let string = NSMutableAttributedString(string: startingString,
                                                   attributes: [.font: adjustedFont])
            string.append(NSAttributedString(string: " " + priceString,
                                             attributes: [.font: startingSubtitleLabelFont]))
            
            return string
        }()
        
        startingPriceLabel.attributedText = attributedString
      startingPriceLabel.isHidden = false
    }
    
    func configure(for service: Service) {
        //titleLabel.text = service.title
        //imageView.image = service.image
        
        /*if service.isPackage {
            subtitleLabel.text = service.subtitle
            subtitleLabel.isHidden = service.subtitle == nil
            recommendedLabel.isHidden = false
            
        } else if service.category_english == Service.comingSoon {
            subtitleLabel.text = ""
            subtitleLabel.isHidden = true
            recommendedLabel.isHidden = true
            
        } else {
            setStartingPrice(service.startingPrice)
            recommendedLabel.isHidden = true
        }*/
        
        /*if service.category_english == Service.comingSoon {
            subtitleLabel.text = ""
            subtitleLabel.isHidden = true
         startingPriceLabel.isHidden = true
            recommendedLabel.isHidden = true
            return
        }
        
        if let subtitle = service.subtitle {
            subtitleLabel.text = subtitle
            subtitleLabel.isHidden = false
        } else {
         subtitleLabel.isHidden = true
        }

        setStartingPrice(service.startingPriceString)*/
        recommendedLabel.isHidden = true// !service.isPackage
    }
}
