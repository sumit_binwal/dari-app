//
//  SupportView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SupportView: UIView {
   // MARK: - UI
   private lazy var outerStackView: UIStackView = {
      let stackView = UIStackView()
      stackView.translatesAutoresizingMaskIntoConstraints = false
      stackView.alignment = .fill
      stackView.distribution = .equalSpacing
      stackView.axis = .vertical
      
      stackView.addArrangedSubviews([self.imageView, self.innerStackView])
      
      return stackView
   }()
   private lazy var imageView: UIImageView = {
      let view = UIImageView(image: UIImage(named: "Splash Image"))
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.contentMode = .center
      view.setContentHuggingPriority(UILayoutPriority(rawValue: 200), for: .vertical)
      
      return view
   }()
   private lazy var innerStackView: UIStackView = {
      let stackView = UIStackView()
      stackView.translatesAutoresizingMaskIntoConstraints = false
      stackView.alignment = .center
      stackView.distribution = .fillProportionally
      stackView.axis = .vertical
      stackView.spacing = 20
      
      stackView.addArrangedSubviews([
         self.label,
         self.chatButton,
         self.emailButton,
         self.callButton
      ])
      NSLayoutConstraint.activate([
         self.chatButton.widthAnchor.constraint(equalTo: stackView.widthAnchor, multiplier: 0.7),
         self.emailButton.widthAnchor.constraint(equalTo: self.chatButton.widthAnchor),
         self.callButton.widthAnchor.constraint(equalTo: self.chatButton.widthAnchor)
      ])
      
      return stackView
   }()
   private lazy var label: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "supportInstruction".localized
         .localized
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.numberOfLines = 0
      return label
   }()
   lazy var chatButton: ProminentButton = {
      let button = ProminentButton()
      button.setTitle("chatWithUs".localized, for: .normal)
      return button
   }()
   lazy var emailButton: BigButton = {
      let button = BigButton()
      button.setTitle("emailUs".localized, for: .normal)
      return button
   }()
   lazy var callButton: BigButton = {
      let button = BigButton()
      button.setTitle("callUs".localized, for: .normal)
      return button
   }()
   
   // MARK: - Init and Setup
   init() {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .background
      
      //self.addSubview(self.outerStackView)
      //self.addFillingSubview(self.outerStackView)
      self.addSubviews([self.imageView, self.innerStackView])
      NSLayoutConstraint.activate([
         self.imageView.topAnchor.constraint(equalTo: self.topAnchor),
         self.imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         
         self.innerStackView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor),
         self.innerStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         self.innerStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         self.innerStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
   }
}
