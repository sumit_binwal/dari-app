//
//  ListingsView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ListingsView: BaseView {
   var onListingSelect: ((_ listing: Listing) -> Void)? { get set }
}
