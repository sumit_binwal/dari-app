//
//  OrderDetailView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 18/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: Rename
protocol OrderDetailView: BaseView {
   var onOrderCanceled: ((_ orderID: Int) -> Void)? { get set }
}
