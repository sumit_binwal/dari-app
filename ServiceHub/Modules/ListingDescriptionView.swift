//
//  ListingDescriptionView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ListingDescriptionView: BaseView {
   var onNextButtonTap: (() -> Void)? { get set }
   var onSupportButtonTap: (() -> Void)? { get set }
}
