//
//  AddCardView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 15/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AddCardView: BaseView {
   var onCancelButtonTap: (() -> Void)? { get set }
   var onCardAdded: ((_ card: Card, _ save: Bool) -> Void)? { get set }
}
