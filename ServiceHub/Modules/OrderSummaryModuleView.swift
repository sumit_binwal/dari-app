//
//  OrderSummaryModuleView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation

// TODO: Rename
protocol OrderSummaryModuleView: BaseView {
   var onSupportButtonTap: (() -> Void)? { get set }
   var onAddCardTap: (() -> Void)? { get set }
   var onOrderSuccessful: ((_ order: Order) -> Void)? { get set }
   var onOTPRedirect: ((_ url: URL) -> Void)? { get set }
   
   func refresh()
   func addCard(_ card: Card, save: Bool)
   func removeUnsavedCards()
   func threeDSSuccessCallback(url: URL)
   func threeDSFailureCallback(url: URL)
}
