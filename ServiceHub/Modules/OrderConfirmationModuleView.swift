//
//  OrderConfirmationModuleView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: Rename
protocol OrderConfirmationModuleView: BaseView {
   var onHomeButtonTap: (() -> Void)? { get set }
   var onOrdersButtonTap: (() -> Void)? { get set }
}
