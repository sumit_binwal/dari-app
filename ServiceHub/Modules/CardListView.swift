//
//  CardListView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 18/03/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol CardListView: BaseView {
   var onAddCardButtonTapped: (() -> Void)? { get set }
   func addCard(_ card: Card)
}
