//
//  ResetPasswordView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ResetPasswordView: BaseView {
   var onPasswordResetSuccessful: (() -> Void)? { get set }
}
