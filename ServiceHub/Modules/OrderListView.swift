//
//  OrderListView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 15/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol OrderListView: BaseView {
   var onOrderSelect: ((_ order: Order) -> Void)? { get set }
   func removeOrder(withID id: Int)
}
