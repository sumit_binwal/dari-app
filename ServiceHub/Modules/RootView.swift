//
//  RootView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol RootView: BaseView {
   func setRootModule(_ module: Presentable?, with transition: Transition)
   func requestNotificationAuthorizationIfNeeded()
}
