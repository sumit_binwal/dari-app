//
//  CatalogModuleView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol CatalogModuleView: BaseView {
   var onCityButtonTap: (() -> Void)? { get set }
   var onProfileButtonTap: (() -> Void)? { get set }
   var onListingSelect: ((Listing) -> Void)? { get set }
   
   func reload(completion: (() -> Void)?)
}

extension CatalogModuleView {
   func reload() {
      self.reload(completion: nil)
   }
}
