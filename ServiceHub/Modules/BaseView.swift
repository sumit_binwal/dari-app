//
//  BaseView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 03/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation

protocol BaseView: NSObjectProtocol, Presentable {}
