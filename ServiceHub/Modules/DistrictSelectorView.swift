//
//  DistrictSelectorView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 07/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol DistrictSelectorView: BaseView {
   var showCancelButton: Bool { get set }
   var onSelectDistrict: ((District?) -> Void)? { get set }
}
