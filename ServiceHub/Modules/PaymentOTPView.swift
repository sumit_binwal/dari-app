//
//  PaymentOTPView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/03/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation

protocol PaymentOTPView: BaseView {
   var onCancel: (() -> Void)? { get set }
   var onSuccessRedirect: ((_ url: URL) -> Void)? { get set }
   var onFailureRedirect: ((_ url: URL) -> Void)? { get set }
}
