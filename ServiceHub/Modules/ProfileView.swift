//
//  ProfileView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ProfileView: BaseView {
   var user: User { get set }
   var onNameSelect: (() -> Void)? { get set }
   var onPhoneSelect: (() -> Void)? { get set }
   var onEmailSelect: (() -> Void)? { get set }
   var onChangePasswordSelect: (() -> Void)? { get set }
   var onAddressSelect: (() -> Void)? { get set }
   var onManageCardsSelect: (() -> Void)? { get set }
   var onSignOut: (() -> Void)? { get set }
}
