//
//  SignInView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 07/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol SignInView: BaseView {
   var onSignInComplete: (() -> Void)? { get set }
   var onDistrictUnavailable: (() -> Void)? { get set }
   var onForgotPassword: ((_ email: String?) -> Void)? { get set }
}
