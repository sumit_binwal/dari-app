//
//  SignUpView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 07/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol SignUpView: BaseView {
   var showSignIn: Bool { get set }
   var showCancelButton: Bool { get set }
   var onCancelButtonTap: (() -> Void)? { get set }
   var onSignUpComplete: ((User?) -> Void)? { get set }
   var onSignInButtonTap: (() -> Void)? { get set }
   var onChangeAddressTap: (
      (_ address: String?, _ latitude: Double?, _ longitude: Double?) -> Void
   )? { get set }
   
   func setAddress(_ address: String?, latitude: Double?, longitude: Double?)
}
