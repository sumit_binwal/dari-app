//
//  MainView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

protocol MainView: BaseView {
   var onViewDidLoad: ((UINavigationController) -> Void)? { get set }
   var onCatalogSelect: ((UINavigationController) -> Void)? { get set }
   var onOrdersSelect: ((UINavigationController) -> Void)? { get set }
   var onSupportSelect: ((UINavigationController) -> Void)? { get set }
   var onOrdersTabRemoved: (() -> Void)? { get set }
   
   func showOrders()
}
