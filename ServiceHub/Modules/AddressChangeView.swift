//
//  AddressView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 11/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AddressChangeView: BaseView {
   var onAddressChangeSuccessful: (
      (_ address: String, _ latitude: Double, _ longitude: Double) -> Void
   )? { get set }
}
