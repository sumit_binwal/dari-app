//
//  ProfileChangeView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 11/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ProfileChangeView: BaseView {
   var onProfileChangeSuccessful: (() -> Void)? { get set }
}
