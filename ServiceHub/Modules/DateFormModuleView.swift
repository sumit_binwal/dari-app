//
//  DateFormModuleView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: Rename
protocol DateFormModuleView: BaseView {
   var onSupportButtonTap: (() -> Void)? { get set }
   var onNextButtonTap: (() -> Void)? { get set }
}
