//
//  Presentable.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 03/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

protocol Presentable {
   var presentationController: UIPresentationController? { get }
   func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
   func toPresent() -> UIViewController? {
      self
   }
}
