//
//  FirstLaunchView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol FirstLaunchView: BaseView {
   var shouldAnimate: Bool { get set }
   var onSignUpButtonTap: (() -> Void)? { get set }
   var onSignInButtonTap: (() -> Void)? { get set }
   var onGuestButtonTap: (() -> Void)? { get set }
   var onSupportButtonTap: (() -> Void)? { get set }
}
