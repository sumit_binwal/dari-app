//
//  SupportModuleView.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 16/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: Rename.
protocol SupportModuleView: BaseView {
   var showCancelButton: Bool { get set }
   var onCancelButtonTap: (() -> Void)? { get set }
   var onChatButtonTap: (() -> Void)? { get set}
}
