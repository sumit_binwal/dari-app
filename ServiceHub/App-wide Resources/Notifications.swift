//
//  Notifications.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension User {
    static let userDidChangeNotification = Notification.Name("userDidChange")
}
