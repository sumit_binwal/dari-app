//
//  Colors.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

extension UIColor {
   static let calypso = #colorLiteral(red: 0.2584237456, green: 0.4615902901, blue: 0.5130524635, alpha: 1)
   static let darkCalypso = #colorLiteral(red: 0.03475010023, green: 0.3048779368, blue: 0.3683584929, alpha: 1)
   static let lightCalypso = #colorLiteral(red: 0.2287500799, green: 0.4459409714, blue: 0.5097019672, alpha: 1)
   static let tint: UIColor = {
      guard let color = UIColor(named: Environment.tintColorName) else {
         assertionFailure("[UIColor] Failed to find Tint.")
         return .green
      }
      return color
   }()
   static let dimmedTint: UIColor = .init(white: 0.69, alpha: 0.8)
   static let background = UIColor(named: "Background")
   static let lighterBackground = UIColor(named: "Lighter Background")
   static let unhighlightedBackground: UIColor = {
      guard let color = UIColor(named: "Unhighlighted Background") else {
         assertionFailure("[UIColor] Failed to find Unhighlighted Background.")
         return .calypso
      }
      return color
   }()
   static let highlightedBackground: UIColor = {
      guard let color = UIColor(named: "Highlighted Background") else {
         assertionFailure("[UIColor] Failed to find Highlighted Background.")
         return .darkCalypso
      }
      return color
   }()
   static let bar = UIColor(named: "Bar")
   static let unhighlightedBarItem = UIColor(named: "Bar Item (Unhighlighted)")
   static let prominentText: UIColor = {
      guard let color = UIColor(named: "Prominent Text") else {
         assertionFailure("[UIColor] Failed to find Prominent Text.")
         return .darkCalypso }
      return color
   }()
   static let text: UIColor = {
      guard let color = UIColor(named: "Text") else {
         assertionFailure("[UIColor] Failed to find Text.")
         return .calypso }
      return color
   }()
   static let subtleText: UIColor = {
      guard let color = UIColor(named: "Subtle Text") else {
         assertionFailure("[UIColor] Failed to find Subtle Text.")
         return .lightCalypso
      }
      return color
   }()
   static let subtleButton = UIColor(named: "Subtle Button")
   static let darkBackgroundProminentText = UIColor(named: "Prominent Text With Dark Background")
   static let darkBackgroundText = UIColor(named: "Text With Dark Background")
   
   static let separator: UIColor = {
      guard let color = UIColor(named: "Separator") else {
         assertionFailure("[UIColor] Failed to find Separator.")
         return .calypso
      }
      return color
   }()
   static let subtleBorder: UIColor = {
      guard let color = UIColor(named: "Subtle Border") else {
         assertionFailure("[UIColor] Failed to find Subtle Border.")
         return .lightGray
      }
      return color
   }()
   static let offWhite = UIColor(named: "Off White")
   static let foreground = UIColor(named: "Foreground")
   static var blurBackground: UIColor {
      if #available(iOS 13, *) {
         return UIColor { (traitCollection) -> UIColor in
            let x: CGFloat = traitCollection.userInterfaceStyle == .dark ? 0.03 : 0.97
            return UIColor(red: x, green: x, blue: x, alpha: 0.8)
         }
      } else {
         return UIColor(white: 0.97, alpha: 0.5)
      }
   }
}
