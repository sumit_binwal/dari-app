//
//  TrackingManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 16/01/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation
import AppTrackingTransparency
import Intercom
import Firebase
import FirebaseAnalytics
import FBSDKCoreKit

class TrackingManager {
   static var isTrackingEnabled: Bool { Environment.isTrackingEnabled }
   
   static var shouldCollectAdID: Bool {
      guard #available(iOS 14, *) else { return true }
      return ATTrackingManager.trackingAuthorizationStatus == .authorized
   }
   
   /// The date that the tracking consent view controller was last presented. Note: This is not the date that the ATT api was last used.
   static var dateConsentRequested: Date? {
      get {
         UserDefaults.standard.object(forKey: "dateTrackingConsentRequested") as? Date
      }
      set {
         UserDefaults.standard.setValue(newValue, forKey: "dateTrackingConsentRequested")
      }
   }
   
   static var shouldRequestConsentToTrack: Bool {
      guard #available(iOS 14, *) else { return false }
      guard ATTrackingManager.trackingAuthorizationStatus == .notDetermined else { return false }
      return Self.dateConsentRequested == nil
   }
   
   static func requestConsentToTrackIfNecessary(completionHandler: (() -> Void)?) {
      guard #available(iOS 14, *) else {
         completionHandler?()
         return
      }
      
      ATTrackingManager.requestTrackingAuthorization { (authorizationStatus) in
         self.trackingAuthorizationStatusDidChange()
         DispatchQueue.main.async {
            completionHandler?()
         }
      }
   }
   
   private static func trackingAuthorizationStatusDidChange() {
      // Firebase
      Analytics.setConsent([
         .adStorage: self.shouldCollectAdID ? .granted : .denied,
         .analyticsStorage: self.shouldCollectAdID ? .granted : .denied
      ])
      
      // Facebook
      Settings.isAdvertiserIDCollectionEnabled = self.shouldCollectAdID
      Settings.setAdvertiserTrackingEnabled(self.shouldCollectAdID)
   }
   
   // MARK: - Launch
   static func configureTracking(
      _ application: UIApplication,
      launchOptions: [UIApplication.LaunchOptionsKey: Any]?
   ) {
      guard isTrackingEnabled else { return }
      
      configureFirebase()
      configureIntercom()
      configureFreshchat()
      configureFacebook(application: application, launchOptions: launchOptions)
      
      let user = User.current
      registerUser(user)
      updateUser(user)
   }
   
   private static func configureIntercom() {
      Intercom.setApiKey(Environment.intercomApiKey, forAppId: Environment.intercomAppId)
   }
   
   private static func configureFreshchat() {
//      let config = FreshchatConfig(
//         appID: Environment.freshchatAppID,
//         andAppKey: Environment.freshchatAppKey
//      )
//
//      config.domain = Environment.freschatDomain
//      config.themeName = "FreshchatTheme"
//      config.stringsBundle = "FreshchatLocalization"
//      config.gallerySelectionEnabled = true
//      config.cameraCaptureEnabled = true
//      config.teamMemberInfoVisible = true
//      config.showNotificationBanner = true
//      config.responseExpectationVisible = true
//
//      Freshchat.sharedInstance().initWith(config)
//
//      NotificationCenter.default.addObserver(
//         self,
//         selector: #selector(didReceiveFreshchatRestoreID),
//         name: NSNotification.Name(FRESHCHAT_USER_RESTORE_ID_GENERATED),
//         object: nil
//      )
   }
   
   private static func configureFirebase() {
      FirebaseApp.configure()
      Analytics.setConsent([
         .adStorage: self.shouldCollectAdID ? .granted : .denied,
         .analyticsStorage: self.shouldCollectAdID ? .granted : .denied
      ])
   }
   
   private static func configureFacebook(
      application: UIApplication,
      launchOptions: [UIApplication.LaunchOptionsKey: Any]?
   ) {
      ApplicationDelegate.shared.application(
         application,
         didFinishLaunchingWithOptions: launchOptions
      )
      Settings.setAdvertiserTrackingEnabled(self.shouldCollectAdID)
      Settings.isAdvertiserIDCollectionEnabled = self.shouldCollectAdID
      Settings.isAutoLogAppEventsEnabled = true
   }
   
   // MARK: - Register User
   
   static func registerUser(_ user: User?) {
      guard isTrackingEnabled else { return }
      registerIntercomUser(user)
      registerFreshchatUser(user)
   }
   
   private static func registerIntercomUser(_ user: User?) {
      if let user = user {
         Intercom.registerUser(withUserId: String(user.id))
      } else {
         Intercom.registerUnidentifiedUser()
      }
   }
   
   private static func registerFreshchatUser(_ user: User?) {
//      if let user = user {
//         Freshchat.sharedInstance().identifyUser(
//            withExternalID: String(user.id),
//            restoreID: user.externalID
//         )
//      }
//      #warning("Figure out how to deal with unidentified users.")
   }
   
   static func updateUser(_ user: User? = nil) {
      updateMarketingProfile(user)
      
      guard isTrackingEnabled else { return }
      updateIntercomUser(user)
      updateFreshchatUser(user)
   }
   
   private static func updateIntercomUser(_ user: User? = nil) {
      let attributes = ICMUserAttributes()
      
      if let user = user {
         attributes.name = user.name
         attributes.email = user.email
         attributes.phone = user.phoneNumber
      }
      
      attributes.languageOverride = Environment.language

      if let district = District.current, let city = district.city {
         attributes.customAttributes = [
            "City (Selected)": city.englishName,
            "District": district.englishName
         ]
      }

      Intercom.updateUser(attributes)
   }
   
   static func updateMarketingProfile(_ user: User? = nil) {
      UNUserNotificationCenter.current().getNotificationSettings { (settings) in
         TrackingManager.updateNotificationPermissionStatus(settings.authorizationStatus)
      }
   }
   
   static func updateNotificationPermissionStatus(_ status: UNAuthorizationStatus) {
      var grantedStatuses = [UNAuthorizationStatus.authorized, .provisional]
      if #available(iOS 14, *) {
         grantedStatuses.append(.ephemeral)
      }
      updateNotificationPermissionStatus(permissionGranted: grantedStatuses.contains(status))
   }
   
   static func updateNotificationPermissionStatus(permissionGranted: Bool) {
      
      let urlOrNil: URL? = {
         if let user = User.current {
            return ServerURL.updateProfile.url.appendingPathComponent("\(user.id)")
         } else if let key = User.key {
            return ServerURL.updateAnonymousProfile.url.appendingPathComponent(key)
         } else {
            return nil
         }
      }()
      guard let url = urlOrNil else { return }
      
      var request = URLRequest(url: url)
      request.httpMethod = "PATCH"
      
      struct NotificationPermissionStatus: Encodable {
         let granted: Bool
         
         enum CodingKeys: String, CodingKey {
            case granted = "receive_push_notification"
         }
      }
      let status = NotificationPermissionStatus(granted: permissionGranted)
      
      do {
         request.httpBody = try JSONEncoder().encode(status)
      } catch {
         print("[TrackingManager] \(error.localizedDescription)")
      }
      
      Environment.sessionManager.post(request: request) { data, response, error in
         if let error = error {
            print("[TrackingManager] \(error.localizedDescription)")
         }
      }
   }
   
   private static func updateFreshchatUser(_ user: User? = nil) {
//      let freschatUser = FreshchatUser.sharedInstance()
//
//      freschatUser.firstName = user?.firstName
//      freschatUser.lastName = user?.lastName
//      freschatUser.email = user?.email
//      freschatUser.phoneNumber = user?.phoneNumber
//
//      Freshchat.sharedInstance().setUser(freschatUser)
//
//      if let district = District.current, let city = district.city {
//         Freshchat.sharedInstance().setUserPropertyforKey(
//            "city_selected",
//            withValue: city.englishName
//         )
//         Freshchat.sharedInstance().setUserPropertyforKey(
//            "district",
//            withValue: district.englishName
//         )
//      }
   }
   
   static func logOutUser() {
      guard self.isTrackingEnabled else { return }
      Intercom.logout()
//      Freshchat.sharedInstance().resetUser(completion: nil)
   }
   
   // MARK: - Other
   
   @objc
   private static func didReceiveFreshchatRestoreID() {
//      guard User.current != nil,
//            let externalID = FreshchatUser.sharedInstance().restoreID else { return }
//      User.manager.update(userAttributes: [User.CodingKeys.externalID.rawValue: externalID])
   }
   
   static func sendPushNotificationToken(_ token: Data) {
      let urlOrNil: URL? = {
         if let user = User.current {
            return ServerURL.updateNotificationToken.url.appendingPathComponent("\(user.id)")
         } else if let key = User.key {
            return ServerURL.updateAnonymousNotificationToken.url.appendingPathComponent(key)
         } else {
            return nil
         }
      }()
      guard let url = urlOrNil else { return }
      
      var request = URLRequest(url: url)
      request.httpMethod = "PATCH"
      
      struct DTO: Encodable {
         let token: String
         let deviceID: UUID?
         
         enum CodingKeys: String, CodingKey {
            case token
            case deviceID = "device_id"
         }
      }
      let stringToken = token.map { String(format: "%02x", $0) }.joined()
      let dto = DTO(token: stringToken, deviceID: UIDevice.current.identifierForVendor)
      
      do {
         request.httpBody = try JSONEncoder().encode(dto)
      } catch {
         print("[TrackingManager] \(error.localizedDescription)")
      }
      
      Environment.sessionManager.post(request: request) { data, response, error in
          print(response)
          
          if let data1 = data {
              do {
                  let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String:Any]
                  print(json)
              } catch {
                  print("Something went wrong")
              }
          }

         if let error = error {
             
            print("[TrackingManager] \(error.localizedDescription)")
         }
      }
   }
}
