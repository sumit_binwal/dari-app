//
//  DeepLinkHandler.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 9/12/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class DeepLinkHandler {
    private init() {}
    static let shared = DeepLinkHandler()
    var linkType: DeepLinkType?
    lazy var appDelegate: UIApplicationDelegate? = UIApplication.shared.delegate
    
    func setDeepLink(_ link: URL) {
        linkType = nil
        guard link.scheme == "dariapp" else { return }
        
        switch link.host {
        case "home": linkType = .home
        case "service":
            let queries = link.queries
            guard let serviceIdString = queries["serviceId"],
                let serviceId = Int(serviceIdString) else {
                    print("[DeepLinkHandler] Missing serviceId query for service.")
                    return
            }
            linkType = .service(id: serviceId)
        
        default:
            print("[DeepLinkHandler] Unrecognized url host: \(link.absoluteString)")
        }
    }
    
    @discardableResult func handleDeepLinkIfNeeded() -> Bool {
        return true
        /*guard let linkType = linkType else { return false }
        
        guard let tabBarController = appDelegate?.window??.rootViewController as? UITabBarController else {
            return false
        }
        let homeNavController = TabBar.homeNavigationController
        
        switch linkType {
        case .home:
            defer { self.linkType = nil }
            
            if tabBarController.selectedIndex != TabBar.index(for: .home) {
                tabBarController.selectedIndex = TabBar.index(for: .home)
            }
            if homeNavController.children.count > 1 {
                homeNavController.popToRootViewController(animated: true)
            }
            homeNavController.children.first?.presentedViewController?.dismiss(animated: true, completion: nil)
            
            return true
            
        case .service(let id):
            if tabBarController.selectedIndex != TabBar.index(for: .home) {
                tabBarController.selectedIndex = TabBar.index(for: .home)
            }
            if homeNavController.children.count > 1 {
                homeNavController.popToRootViewController(animated: true)
            }
            guard let homeVC = homeNavController.children.first as? HomeViewController else {
                assertionFailure("[DeepLinkHandler] Failed to get HomeViewController.")
                self.linkType = nil
                return false
            }
            
            homeVC.presentedViewController?.dismiss(animated: true, completion: nil)
            
            if homeVC.isRefreshing {
                homeVC.refreshedByDeeplinkHandler = true
                return false
                
            } else if homeVC.refreshedByDeeplinkHandler {
                homeVC.refreshedByDeeplinkHandler = false
                self.linkType = nil
                let services = homeVC.services.flatMap { $0 }
                
                guard let service = services.first(where: { $0.id == id }) else {
                    let alert = UIAlertController(
                        title: nil,
                        message: "The service is not available in your area.",
                        preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    appDelegate?.window??.rootViewController?.present(alert, animated: true, completion: nil)
                    return false
                }
                
                guard let serviceDetailsVC = homeVC.storyboard?.instantiateViewController(
                    withIdentifier: "ServiceDetailsViewController") as? ServiceDetailsViewController else {
                        assertionFailure("[DeepLinkHandler] Failed to instantiate ServiceDetailsViewController.")
                        return false
                }
                serviceDetailsVC.service = service
                homeNavController.pushViewController(serviceDetailsVC, animated: true)
                
                return true
                
            } else {
                homeVC.refreshedByDeeplinkHandler = true
                homeVC.refreshServices()
                return false
            }
        }*/
    }
}
