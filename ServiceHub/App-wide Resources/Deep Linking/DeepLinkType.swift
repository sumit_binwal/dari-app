//
//  DeepLinkType.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 9/12/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

enum DeepLinkType {
    case home
    case service(id: Int)
}
