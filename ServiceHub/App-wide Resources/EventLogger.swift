//
//  EventLogger.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/22/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import Intercom
import Firebase
import FirebaseAnalytics

struct EventLogger {
   static func log(
      _ event: Event,
      revenue: Double? = nil,
      currency: String? = nil,
      parameters: [Event.Parameter: Any] = [:]
   ) {
      guard TrackingManager.isTrackingEnabled else { return }
      
      let adjustedParameters = Dictionary(
         uniqueKeysWithValues: parameters.map { ($0.rawValue, $1) }
      )
      
      // In-house
      logInhouseEvent(event, parameters: parameters)
      
      // Facebook
      AppEvents.logEvent(.init(event.rawValue), parameters: adjustedParameters)
      if let revenue = revenue, let currency = currency {
         AppEvents.logPurchase(revenue, currency: currency)
      }
      
      // Google
      let firebaseParameters = Dictionary(
         uniqueKeysWithValues: parameters.map { ($0.firebase, $1) }
      )
      Analytics.logEvent(event.firebase, parameters: firebaseParameters)
      
      // Intercom
      if parameters.isEmpty {
         Intercom.logEvent(withName: event.rawValue)
      } else {
         Intercom.logEvent(withName: event.rawValue, metaData: adjustedParameters)
      }
      
      // Freshchat
//      let freshchatParameters = Dictionary(
//         uniqueKeysWithValues: parameters.map { ($0.freschat, $1) }
//      )
//      Freshchat.sharedInstance().trackEvent(event.freschat, withProperties: freshchatParameters)
   }
   
   static func appDidBecomeActive(_ application: UIApplication) {
      guard TrackingManager.isTrackingEnabled else { return }
      AppEvents.activateApp()
      application.registerForRemoteNotifications()
   }
   
   private static func logInhouseEvent(_ event: Event, parameters: [Event.Parameter: Any] = [:]) {
      guard
         TrackingManager.isTrackingEnabled,
         let userID = User.current?.id,
         let inhouseEvent = event.inhouse
      else { return }
      
      let adjustedParameters: [String: AnyEncodable] = {
         let params: [Event.Parameter: AnyEncodable] = parameters.compactMapValues {
            guard let encodableValue = $0 as? Encodable else { return nil }
            return AnyEncodable(encodableValue)
         }
         let adjustedParams: [String: AnyEncodable] = params.reduce([:]) {
            var dictionary = $0
            dictionary[$1.key.inhouse] = $1.value
            return dictionary
         }
         return adjustedParams
      }()
      
      var request = URLRequest(url: ServerURL.logEvent.url)
      request.httpMethod = "POST"
      
      struct EventDTO: Encodable {
         let userID: String
         let event: String
         let parameters: [String: AnyEncodable]
         
         enum CodingKeys: String, CodingKey {
            case userID = "customer"
            case event = "name"
            case parameters = "additional_data"
         }
      }
      
      let eventDTO = EventDTO(
         userID: "\(userID)",
         event: inhouseEvent,
         parameters: adjustedParameters
      )
      
      do {
         request.httpBody = try JSONEncoder().encode(eventDTO)
      } catch {
         print("[EventLogger] Failed to encode event: \(error.localizedDescription)")
      }
      
      Environment.sessionManager.post(request: request) { _, _, error in
         if let error = error {
            print("[EventLogger] \(error.localizedDescription)")
         }
      }
   }
}

enum Event: String {
   case didReachOrderForm = "Reached Order Form"
   case didReachOrderSummary = "Reached Order Summary"
   case didPlaceOrder = "Placed Order"
   case didViewService = "Viewed Service"
   case didReachServiceProviderSelection = "Reached SP Selection"
   case didSignUp = "Signed Up"
   
   fileprivate var firebase: String {
      switch self {
      case .didSignUp: return AnalyticsEventSignUp
      default: return rawValue.lowercased().replacingOccurrences(of: " ", with: "_")
      }
   }
   
   fileprivate var freschat: String {
      rawValue.lowercased().replacingOccurrences(of: " ", with: "_")
   }
   
   fileprivate var inhouse: String? {
      switch self {
      case .didReachOrderForm: return "REACHED_ORDER_FORM"
      case .didReachOrderSummary: return "REACHED_ORDER_SUMMARY"
      case .didViewService: return "VIEWED_SERVICE"
      default: return nil
      }
   }
}

extension Event {
   enum Parameter: String {
      case service = "Service"
      case serviceID = "Service ID"
      case signUpMethod = "Method"
      case serviceProvider = "Service Provider"
      
      fileprivate var firebase: String {
         rawValue.lowercased().replacingOccurrences(of: " ", with: "_")
      }
      
      fileprivate var freschat: String {
         rawValue.lowercased().replacingOccurrences(of: " ", with: "_")
      }
      
      fileprivate var inhouse: String {
         rawValue.lowercased().replacingOccurrences(of: " ", with: "_")
      }
   }
}
