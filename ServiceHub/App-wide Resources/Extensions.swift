//
//  Extensions.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/1/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

extension Double {
   static let timeoutInterval: Double = 60
   static var highlightAnimationDuration: Double = 0.15
   static let defaultUIAnimationDuration: Double = 0.2
   
   func rounded(_ rule: FloatingPointRoundingRule, precision: Int) -> Double {
      let exp = pow(10.0, Double(precision))
      return (self * exp).rounded(rule) / exp
   }
   
   mutating func round(_ rule: FloatingPointRoundingRule, precision: Int) {
      self = self.rounded(rule, precision: precision)
   }
   
   var isWholeNumber: Bool { self.truncatingRemainder(dividingBy: 1) == 0 }
}

extension CGFloat {
   static var pixel: CGFloat { return 1 / UIScreen.main.scale }
   static var cornerRadius: CGFloat { return 10 }
}

extension CGPoint {
   init(_ x: CGFloat, _ y: CGFloat) {
      self.init(x: x, y: y)
   }
}

extension CGRect {
   var center: CGPoint {
      get { return CGPoint(midX, midY) }
      set { origin = CGPoint(newValue.x - width / 2, newValue.y - height / 2) }
   }
   init(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) {
      self.init(x: x, y: y, width: width, height: height)
   }
}

extension CGSize {
   init(_ width: CGFloat, _ height: CGFloat) {
      self.init(width: width, height: height)
   }
}

extension UIView {
   enum Side { case top, bottom, leading, trailing }
   
   func addSubviews(_ subviews: [UIView]) {
      subviews.forEach { addSubview($0) }
   }
   
   /// Adds a subview that fills this view, using layout constraints.
   func addFillingSubview(_ subview: UIView) {
      self.addSubview(subview)
      NSLayoutConstraint.activate([
         subview.topAnchor.constraint(equalTo: self.topAnchor),
         subview.leadingAnchor.constraint(equalTo: self.leadingAnchor),
         subview.trailingAnchor.constraint(equalTo: self.trailingAnchor),
         subview.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
   }
   
   /// Adds a subview that fills this view, stopping short at the safe areas on the side.
   func addFillingSubviewConstrainedBySafeAreaOnSides(_ subview: UIView) {
      self.addSubview(subview)
      NSLayoutConstraint.activate([
         subview.topAnchor.constraint(equalTo: self.topAnchor),
         subview.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
         subview.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
         subview.bottomAnchor.constraint(equalTo: self.bottomAnchor)
      ])
   }
   
   /// Adds a subview that fills this view, stopping short at the margins.
   func addFillingSubviewConstrainedByMargins(_ subview: UIView) {
      let marginsGuide = self.layoutMarginsGuide
      self.addSubview(subview)
      NSLayoutConstraint.activate([
         subview.topAnchor.constraint(equalTo: marginsGuide.topAnchor),
         subview.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         subview.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         subview.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
      ])
   }
   
   func addBorder(to side: Side, color: UIColor = .separator) {
      let isHorizontalBorder = [Side.top, .bottom].contains(side)
      
      let borderView: UIView = {
         let view = UIView()
         view.backgroundColor = color
         view.translatesAutoresizingMaskIntoConstraints = false
         let anchor = isHorizontalBorder ? view.heightAnchor : view.widthAnchor
         anchor.constraint(equalToConstant: .pixel).isActive = true
         return view
      }()
      
      addSubview(borderView)
      if isHorizontalBorder {
         borderView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
         borderView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      } else {
         borderView.topAnchor.constraint(equalTo: topAnchor).isActive = true
         borderView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      }
      
      switch side {
      case .top: borderView.topAnchor.constraint(equalTo: topAnchor).isActive = true
      case .bottom: borderView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      case .leading: borderView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      case .trailing: borderView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      }
      setNeedsLayout()
   }
   
   var firstResponder: UIView? {
      guard !isFirstResponder else { return self }
      
      for subview in subviews {
         if let firstResponder = subview.firstResponder {
            return firstResponder
         }
      }
      return nil
   }
}

extension UIImage {
   func scaled(by scaleFactor: CGFloat) -> UIImage {
      let newSize = CGSize(size.width * scaleFactor, size.height * scaleFactor)
      let rect = CGRect(origin: .zero, size: newSize)
      
      UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
      defer { UIGraphicsEndImageContext() }
      
      draw(in: rect)
      guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
         assertionFailure("[UIImage] Failed to get image from context.")
         return self
      }
      return newImage
   }
}

extension NumberFormatter {
   static let currency: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      
      guard let country = Country.current else {
         assertionFailure("[NumberFormatter] Failed to get country.")
         return formatter
      }
      
      formatter.currencyCode = country.currencyCode
      formatter.currencySymbol = country.currencySymbol
      
      return formatter
   }()
   
   static let shortCurrency: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      
      guard let country = Country.current else {
         assertionFailure("[NumberFormatter] Failed to get country.")
         return formatter
      }
      
      formatter.currencyCode = country.currencyCode
      formatter.currencySymbol = country.currencySymbol
      formatter.maximumFractionDigits = 0
      
      return formatter
   }()
   
   static let integer: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.numberStyle = .none
      return formatter
   }()
   
   static func durationDescription(hours: Int) -> String {
      if Environment.languageIsArabic && hours == 1 {
         return "hour".localized
      } else if Environment.languageIsArabic && hours == 2 {
         return "2hours".localized
      } else {
         return "\(integer.string(from: hours)) \((hours == 1 ? "hour" : "hours").localized)"
      }
   }
   
   static let percent: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.numberStyle = .percent
      formatter.alwaysShowsDecimalSeparator = false
      return formatter
   }()
   
   func string(from double: Double) -> String {
      if let string = string(from: double as NSNumber) {
         return string
      } else {
         assertionFailure("[NumberFormatter] Failed to generate string from double.")
         return String(double)
      }
   }
   
   func string(from int: Int) -> String {
      if let string = string(from: int as NSNumber) {
         return string
      } else {
         assertionFailure("[NumberFormatter] Failed to generate string from integer.")
         return String(int)
      }
   }
}

extension DateFormatter{
   static var timeFormatter: DateFormatter {
      let formatter = DateFormatter()
      formatter.dateStyle = .none
      formatter.timeStyle = .short
      return formatter
   }
   
   static var `default`: DateFormatter {
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      formatter.timeStyle = .none
      return formatter
   }
   
   static func attributeText(
      forHour hour: Int,
      hourFont: UIFont,
      apPmFont: UIFont
   ) -> NSAttributedString {
      guard let date = Calendar.current.date(from: DateComponents(hour: hour)) else {
         assertionFailure("[DateFormatter] Failed to generate date from hour.")
         return NSAttributedString(string: "\(hour)")
      }
      
      let formatter = DateFormatter()
      formatter.dateFormat = "ha"
      let text = formatter.string(from: date) as NSString
      let amRange = text.range(of: formatter.amSymbol)
      let pmRange = text.range(of: formatter.pmSymbol)
      
      
      let attributedText = NSMutableAttributedString(
         string: text as String,
         attributes: [.font: hourFont]
      )
      
      attributedText.addAttributes([.font: apPmFont], range: amRange)
      attributedText.addAttributes([.font: apPmFont], range: pmRange)
      
      return attributedText
   }
   
   static var serverTime: PrecisionDateFormatter = .init()
   
   static var serverDate: ISO8601DateFormatter {
      let formatter = ISO8601DateFormatter()
      formatter.formatOptions = .withFullDate
      formatter.timeZone = Country.current?.timeZone ?? Calendar.current.timeZone
      return formatter
   }
   
   static func string(describing weekdays: [Int]) -> String {
      let symbols = weekdays.map { Calendar.current.shortWeekdaySymbols[$0 - 1] }
      return symbols.reduce("") { $0 + ($0.isEmpty ? "" : ",".localized + " ") + $1 }
   }
}

extension Date {
   var shortMonthSymbol: String {
      let month = Calendar.current.component(.month, from: self) - 1
      return Calendar.current.shortMonthSymbols[month]
   }
   
   var day: String {
      let day = Calendar.current.component(.day, from: self)
      return NumberFormatter.integer.string(from: day)
   }
   
   var weekdaySymbol: String {
      let weekday = Calendar.current.component(.weekday, from: self) - 1
      return Calendar.current.shortWeekdaySymbols[weekday]
   }
   
   var weekday: String {
      let weekday = Calendar.current.component(.weekday, from: self) - 1
      let symbols = Environment.languageIsArabic ?
         Calendar.current.shortWeekdaySymbols : Calendar.current.standaloneWeekdaySymbols
      return symbols[weekday]
   }
   
   var year: String {
      let year = Calendar.current.component(.year, from: self)
      return NumberFormatter.integer.string(from: year)
   }
   
   static func -(lhs: Date, rhs: Date) -> TimeInterval {
      return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
   }
}

extension IndexPath {
   static var zero: IndexPath { return IndexPath(row: 0, section: 0) }
}

extension UICollectionView {
   func centerAlign(spacing: CGFloat = 10, minimumHorizontalInset: CGFloat = 15) {
      guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else { return }
      guard let delegate = self.delegate as? UICollectionViewDelegateFlowLayout else { return }
      
      let contentWidth: CGFloat = {
         let numberOfCells = CGFloat(numberOfItems(inSection: 0))
         var cumulativeCellWidth: CGFloat = 0
         for index in 0 ..< Int(numberOfCells) {
            guard let width = delegate.collectionView?(
               self, layout: layout, sizeForItemAt: IndexPath(item: index, section: 0)).width else {
                  assertionFailure("[CollectionView] Failed to calculate width.")
                  break
            }
            cumulativeCellWidth += width
         }
         let cumulativeSpacing = (numberOfCells - 1) * spacing
         return cumulativeCellWidth + cumulativeSpacing
      }()
      let horizontalInset = max((UIScreen.main.bounds.width - contentWidth) / 2, minimumHorizontalInset)
      layout.sectionInset = UIEdgeInsets(top: 0, left: horizontalInset, bottom: 0, right: horizontalInset)
   }
   
   func smoothlyDeselectItem(alongside transitionCoordinator: UIViewControllerTransitionCoordinator?) {
      guard let indexPath = indexPathsForSelectedItems?.first else { return }
      guard let transitionCoordinator = transitionCoordinator else {
         deselectItem(at: indexPath, animated: false)
         return
      }
      
      transitionCoordinator.animate(alongsideTransition: { (context) in
         self.deselectItem(at: indexPath, animated: context.isAnimated)
      }) { (context) in
         if context.isCancelled {
            self.selectItem(at: indexPath, animated: false, scrollPosition: .left)
         }
      }
   }
}

extension UIViewController {
   var firstResponder: UIView? { return view.firstResponder }
   var viewIsInHierarchy: Bool { self.viewIfLoaded?.window != nil }
   
   func smoothlyDeselectRows(tableView: UITableView) {
      guard let indexPath = tableView.indexPathForSelectedRow else { return }
      
      if let coordinator = transitionCoordinator {
         coordinator.animate(alongsideTransition: { (context) in
            tableView.deselectRow(at: indexPath, animated: context.isAnimated)
            
         }) { (context) in
            if context.isCancelled {
               tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
         }
      } else {
         tableView.deselectRow(at: indexPath, animated: false)
      }
   }
   
   func addBackgroundPattern() {
      guard let backgroundImageTile = UIImage(named: "Background Tile") else {
         assertionFailure("[UIViewController] Background tile image is nil.")
         return
      }
      let enclosingView: UIView = {
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.backgroundColor = self.view.backgroundColor
         view.clipsToBounds = true
         view.tag = 1000
         if traitCollection.userInterfaceStyle == .dark {
            view.alpha = 0.2
         }
         return view
      }()
      view.insertSubview(enclosingView, at: 0)
      enclosingView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
      enclosingView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
      enclosingView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      enclosingView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      
      let backgroundView: UIView = {
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.backgroundColor = UIColor(patternImage: backgroundImageTile)
         
         let tileHeight = backgroundImageTile.size.height
         let tileWidth = backgroundImageTile.size.width
         let side = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
         view.heightAnchor.constraint(equalToConstant: side + tileHeight / 2).isActive = true
         view.widthAnchor.constraint(equalToConstant: side + tileWidth / 2).isActive = true
         
         return view
      }()
      enclosingView.addSubview(backgroundView)
      backgroundView.centerXAnchor.constraint(equalTo: enclosingView.centerXAnchor).isActive = true
      backgroundView.centerYAnchor.constraint(equalTo: enclosingView.centerYAnchor).isActive = true
      backgroundView.transform = CGAffineTransform(rotationAngle: -.pi / 8)
   }
   
   func dismissActivityIndicatorOrAlert(completion: (() -> Void)? = nil) {
      guard let presentedViewController = presentedViewController else { return }
      if presentedViewController is ActivityIndicatorViewController ||
         presentedViewController is UIAlertController {
         presentedViewController.dismiss(animated: true, completion: completion)
      }
   }
   
   func presentActivityIndicatorController() {
      guard presentedViewController == nil else { return }
      DispatchQueue.main.async {
         self.present(ActivityIndicatorViewController(), animated: true, completion: nil)
      }
   }
}

extension UITextField {
   func setPlaceholderColor(_ color: UIColor) {
      attributedPlaceholder = NSAttributedString(
         string: placeholder ?? "", attributes: [.foregroundColor: color])
   }
   
   func toolbar(in view: UIView) -> UIToolbar {
      let toolbar: UIToolbar = {
         let toolbar = UIToolbar()
         toolbar.sizeToFit()
         toolbar.tintColor = .tint
         return toolbar
      }()
      
      let previousTextField = self.previousTextField(in: view)
      let nextTextField = self.nextTextField(in: view)
      
      let previousButton = UIBarButtonItem(
         image: UIImage(named: "Upward Chevron"),
         style: .plain,
         target: previousTextField,
         action: #selector(UITextField.becomeFirstResponder))
      previousButton.isEnabled = previousTextField != nil
      
      let nextButton = UIBarButtonItem(
         image: UIImage(named: "Downward Chevron"),
         style: .plain,
         target: nextTextField,
         action: #selector(UITextField.becomeFirstResponder))
      nextButton.isEnabled = nextTextField != nil
      
      let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
      
      toolbar.setItems([space, previousButton, nextButton], animated: false)
      
      return toolbar
   }
   
   func nextTextField(in view: UIView) -> UITextField? {
      return view.viewWithTag(tag + 1) as? UITextField
   }
   
   func previousTextField(in view: UIView) -> UITextField? {
      return view.viewWithTag(tag - 1) as? UITextField
   }
   
   var isEmpty: Bool {
      guard let text = text else { return true }
      return text.isEmpty
   }
}

extension String {
   var trimmed: String { return self.trimmingCharacters(in: .whitespacesAndNewlines) }
   
   var localized: String { return NSLocalizedString(self, comment: "") }
   
   func contains(_ set: CharacterSet) -> Bool {
      let setForThisString = CharacterSet(charactersIn: self)
      return !setForThisString.isDisjoint(with: set)
   }
}

extension UIAlertController {
   @available(*, deprecated)
   static var signInActionTitle: String { return "signIn".localized }
   @available(*, deprecated)
   static func signInAlertController(
      title: String? = nil,
      message: String? = "signInRequired".localized,
      textFieldTarget: Any?,
      textFieldSelector: Selector,
      signInHandler: ((String, String) -> Void)?) -> UIAlertController {
      
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      alert.addTextField { textField in
         textField.placeholder = "email".localized
         textField.keyboardType = .emailAddress
         textField.autocapitalizationType = .none
         textField.textContentType = .username
         textField.addTarget(textFieldTarget, action: textFieldSelector, for: .editingChanged)
         textField.text = User.current?.email
         textField.returnKeyType = .next
      }
      
      alert.addTextField { textField in
         textField.placeholder = "password".localized
         textField.keyboardType = .default
         textField.autocapitalizationType = .none
         textField.isSecureTextEntry = true
         textField.textContentType = .password
         textField.addTarget(textFieldTarget, action: textFieldSelector, for: .editingChanged)
         textField.returnKeyType = .go
         textField.enablesReturnKeyAutomatically = true
      }
      
      let signInAction = UIAlertAction(title: UIAlertController.signInActionTitle, style: .default, handler: { _ in
         guard let emailTextField = alert.textFields?[0],
            let passwordTextField = alert.textFields?[1] else {
               fatalError("[OrderSummaryVC] Failed to get email or password text fields.")
         }
         guard let email = emailTextField.text, !email.isEmpty else {
            fatalError("[OrderSummaryVC] email is empty.")
         }
         guard let password = passwordTextField.text, !password.isEmpty else {
            fatalError("[OrderSummaryVC] password is empty.")
         }
         signInHandler?(email, password)
      })
      signInAction.isEnabled = false
      alert.addAction(signInAction)
      
      alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
      
      return alert
   }
   
   static func logInAlertController(
      presentingController: UIViewController,
      textFieldSelector selector: Selector)
      -> (alert: UIAlertController, action: UIAlertAction)?
   {
      guard let user = User.current else { return nil }
      
      let alert = UIAlertController(
         title: "signInRequired".localized,
         message: user.email,
         preferredStyle: .alert
      )
      alert.addTextField { (textField) in
         textField.placeholder = "password".localized
         textField.keyboardType = .default
         textField.autocapitalizationType = .none
         textField.isSecureTextEntry = true
         textField.textContentType = .password
         textField.returnKeyType = .go
         textField.enablesReturnKeyAutomatically = false
         textField.addTarget(presentingController, action: selector, for: .editingChanged)
      }
      
      let signInAction = UIAlertAction(title: "signIn".localized, style: .default, handler: { _ in
         guard let password = alert.textFields?.first?.text, !password.trimmed.isEmpty else {
            return
         }
         
         presentingController.presentActivityIndicatorController()
         User.manager.logIn(email: user.email, password: password)
      })
      signInAction.isEnabled = false
      
      alert.addAction(signInAction)
      alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
      
      return (alert, signInAction)
   }
}

extension UIRefreshControl {
   @available(*, deprecated)
   func beginRefreshing(in tableView: UITableView) {
      beginRefreshing()
      tableView.setContentOffset(CGPoint(0, -frame.height), animated: true)
   }
   
   /// Call when superview is a scroll view.
   func startRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      DispatchQueue.main.async {
         if let scrollView = self.superview as? UIScrollView {
            let safeAreaInset: CGFloat = scrollView.safeAreaInsets.top
            scrollView.setContentOffset(
               CGPoint(0, -self.frame.height - safeAreaInset),
               animated: animate
            )
         }
         self.beginRefreshing()
      }
   }
   
   /// Call when superview is a scroll view.
   func stopRefreshing(animateScrollViewContentOffset animate: Bool = true) {
      guard self.isRefreshing else { return }
      
      DispatchQueue.main.async {
         if let scrollView = self.superview as? UIScrollView {
            scrollView.setContentOffset(.zero, animated: animate)
         }
         self.endRefreshing()
      }
   }
}

extension UIScrollView {
   func endScroll() {
      var offset = contentOffset
      offset.x -= 1
      offset.y -= 1
      setContentOffset(offset, animated: false)
      offset.x += 1
      offset.y += 1
      setContentOffset(offset, animated: false)
   }
}

extension UIGestureRecognizer {
   func didTapAttributedText(inLabel label: UILabel, inRange targetRange: NSRange) -> Bool {
      guard let attributedText = label.attributedText else { return false }
      
      // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
      let layoutManager = NSLayoutManager()
      let textContainer = NSTextContainer(size: CGSize.zero)
      let textStorage = NSTextStorage(attributedString: attributedText)
      
      // Configure layoutManager and textStorage
      layoutManager.addTextContainer(textContainer)
      textStorage.addLayoutManager(layoutManager)
      
      // Configure textContainer
      textContainer.lineFragmentPadding = 0.0
      textContainer.lineBreakMode = label.lineBreakMode
      textContainer.maximumNumberOfLines = label.numberOfLines
      let labelSize = label.bounds.size
      textContainer.size = labelSize
      
      // Find the tapped character location and compare it to the specified range
      let locationOfTouchInLabel = self.location(in: label)
      let textBoundingBox = layoutManager.usedRect(for: textContainer)
      
      let alignmentFactor: CGFloat = {
         switch label.textAlignment {
         case .left: return 0
         case .center: return 0.5
         case .right: return 1
         case .justified: return 0.5
         case .natural:
            let direction = UIView.userInterfaceLayoutDirection(for: label.semanticContentAttribute)
            return direction == .leftToRight ? 0 : 1
         @unknown default:
            assertionFailure("[UIGestureRecognizer] Unrecognized text alignment case.")
            return 0
         }
      }()
      
      let textContainerOffset = CGPoint(
         (labelSize.width - textBoundingBox.size.width) * alignmentFactor
            - textBoundingBox.origin.x,
         (labelSize.height - textBoundingBox.size.height) * alignmentFactor
            - textBoundingBox.origin.y
      )
      let locationOfTouchInTextContainer = CGPoint(
         locationOfTouchInLabel.x - textContainerOffset.x,
         locationOfTouchInLabel.y - textContainerOffset.y
      )
      let indexOfCharacter = layoutManager.characterIndex(
         for: locationOfTouchInTextContainer,
         in: textContainer,
         fractionOfDistanceBetweenInsertionPoints: nil)
      
      return NSLocationInRange(indexOfCharacter, targetRange)
   }
}

extension CGAffineTransform {
   static var zeroScale: CGAffineTransform {
      .init(scaleX: .leastNormalMagnitude, y: .leastNormalMagnitude)
   }
   static var highlightTransformForLargeViews: CGAffineTransform { .init(scaleX: 0.98, y: 0.98) }
   static var highlightTransformForSmallViews: CGAffineTransform { .init(scaleX: 0.95, y: 0.95) }
}

extension UIView.AnimationOptions {
   static var highlightAnimationCurve: UIView.AnimationOptions { .curveEaseOut }
}

extension URL {
   var queries: [String: String] {
      guard let queryString = query else { return [:] }
      
      let queryTokens = queryString.split(separator: "&")
      var queries: [String: String] = [:]
      for queryToken in queryTokens {
         let tokens = queryToken.split(separator: "=", maxSplits: 1, omittingEmptySubsequences: false)
         guard tokens.count == 2 else { continue }
         let key = String(tokens[0])
         let value = String(tokens[1])
         queries[key] = value
      }
      return queries
   }
}

extension UITableView {
   private var naturalDeleteAnimation: RowAnimation {
      UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .leftToRight ?
         .right : .left
   }
   private var naturalInsertAnimation: RowAnimation {
      UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .leftToRight ?
         .left : .right
   }
   
   func insertRows(at indexPaths: [IndexPath]) {
      insertRows(at: indexPaths, with: self.naturalInsertAnimation)
   }
   
   func deleteRows(at indexPaths: [IndexPath]) {
      deleteRows(at: indexPaths, with: self.naturalDeleteAnimation)
   }
   
   func deleteSections(_ sections: IndexSet) {
      self.deleteSections(sections, with: self.naturalDeleteAnimation)
   }
   
   func insertSections(_ sections: IndexSet) {
      self.insertSections(sections, with: self.naturalInsertAnimation)
   }
   
   /// Reloads the table, keeping the selected cell highlighted and the content offset.
   func reloadKeepingSelectionAndContentOffsetState() {
      let selectedIndexPath = self.indexPathForSelectedRow
      let offset = self.contentOffset
      self.reloadData()
      self.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
      self.contentOffset = offset
   }
}

extension UIVisualEffect {
   static var defaultBlur: UIBlurEffect {
      return {
         if #available(iOS 13, *) {
            return UIBlurEffect(style: .systemMaterial)
         } else {
            return UIBlurEffect(style: .extraLight)
         }
         }()
   }
}

extension UIStackView {
   public func addArrangedSubviews(_ subviews: [UIView]) {
      subviews.forEach { self.addArrangedSubview($0) }
   }
   
   public func insertArrangedSubviews(_ views: [UIView], at index: Int) {
      views.reversed().forEach { self.insertArrangedSubview($0, at: index) }
   }
   
   public func insertArrangedSubviews(_ views: [UIView], belowSubview siblingSubview: UIView) {
      guard let index = self.arrangedSubviews.firstIndex(of: siblingSubview) else { return }
      self.insertArrangedSubviews(views, at: index + 1)
   }
}

extension Encodable {
   var dictionary: [String: Any]? {
      guard let data = try? JSONEncoder().encode(self) else { return nil }
      return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments))
         .flatMap { $0 as? [String: Any] }
   }
}

extension Array where Element: Equatable {
   func firstIndex(of element: Element?) -> Int? {
       guard let element = element else { return nil }
       return self.firstIndex(of: element)
   }
   
   func contains(_ element: Element?) -> Bool {
      guard let element = element else { return false }
      return self.contains(element)
   }
}

extension NSMutableAttributedString {
   func setAttributesForNumericCharacters(_ attributes: [NSAttributedString.Key: Any]) {
      let stringLength = self.string.count
      
      let numericalCharacters: Set<Character> = Set(self.string.filter {
         $0.isNumber ||
            NumberFormatter.currency.currencyDecimalSeparator.contains($0) ||
            NumberFormatter.currency.currencyGroupingSeparator.contains($0)
            
      })
      
      numericalCharacters.forEach {
         let searchString = String($0)
         let searchStringLength = 1
         var range = NSRange(location: 0, length: self.length)
         
         while range.location != NSNotFound {
            range = (self.string as NSString).range(of: searchString, options: [], range: range)
            if range.location != NSNotFound {
               self.setAttributes(
                  attributes,
                  range: NSRange(location: range.location, length: searchStringLength)
               )
               range = NSRange(
                  location: range.location + range.length,
                  length: stringLength - (range.location + range.length)
               )
            }
         }
      }
   }
}

extension UIDevice {
   var modelName: String {
      var systemInfo = utsname()
      uname(&systemInfo)
      let machineMirror = Mirror(reflecting: systemInfo.machine)
      let identifier = machineMirror.children.reduce("") { identifier, element in
         guard let value = element.value as? Int8, value != 0 else { return identifier }
         return identifier + String(UnicodeScalar(UInt8(value)))
      }
      
      func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
         #if os(iOS)
         switch identifier {
         case "iPod5,1": return "iPod touch (5th generation)"
         case "iPod7,1": return "iPod touch (6th generation)"
         case "iPod9,1": return "iPod touch (7th generation)"
         case "iPhone3,1", "iPhone3,2", "iPhone3,3": return "iPhone 4"
         case "iPhone4,1": return "iPhone 4s"
         case "iPhone5,1", "iPhone5,2": return "iPhone 5"
         case "iPhone5,3", "iPhone5,4": return "iPhone 5c"
         case "iPhone6,1", "iPhone6,2": return "iPhone 5s"
         case "iPhone7,2": return "iPhone 6"
         case "iPhone7,1": return "iPhone 6 Plus"
         case "iPhone8,1": return "iPhone 6s"
         case "iPhone8,2": return "iPhone 6s Plus"
         case "iPhone8,4": return "iPhone SE"
         case "iPhone9,1", "iPhone9,3": return "iPhone 7"
         case "iPhone9,2", "iPhone9,4": return "iPhone 7 Plus"
         case "iPhone10,1", "iPhone10,4": return "iPhone 8"
         case "iPhone10,2", "iPhone10,5": return "iPhone 8 Plus"
         case "iPhone10,3", "iPhone10,6": return "iPhone X"
         case "iPhone11,2": return "iPhone XS"
         case "iPhone11,4", "iPhone11,6": return "iPhone XS Max"
         case "iPhone11,8": return "iPhone XR"
         case "iPhone12,1": return "iPhone 11"
         case "iPhone12,3": return "iPhone 11 Pro"
         case "iPhone12,5": return "iPhone 11 Pro Max"
         case "iPhone12,8": return "iPhone SE (2nd generation)"
         case "iPhone13,1": return "iPhone 12 mini"
         case "iPhone13,2": return "iPhone 12"
         case "iPhone13,3": return "iPhone 12 Pro"
         case "iPhone13,4": return "iPhone 12 Pro Max"
         case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return "iPad 2"
         case "iPad3,1", "iPad3,2", "iPad3,3": return "iPad (3rd generation)"
         case "iPad3,4", "iPad3,5", "iPad3,6": return "iPad (4th generation)"
         case "iPad6,11", "iPad6,12": return "iPad (5th generation)"
         case "iPad7,5", "iPad7,6": return "iPad (6th generation)"
         case "iPad7,11", "iPad7,12": return "iPad (7th generation)"
         case "iPad11,6", "iPad11,7": return "iPad (8th generation)"
         case "iPad4,1", "iPad4,2", "iPad4,3": return "iPad Air"
         case "iPad5,3", "iPad5,4": return "iPad Air 2"
         case "iPad11,3", "iPad11,4": return "iPad Air (3rd generation)"
         case "iPad13,1", "iPad13,2": return "iPad Air (4th generation)"
         case "iPad2,5", "iPad2,6", "iPad2,7": return "iPad mini"
         case "iPad4,4", "iPad4,5", "iPad4,6": return "iPad mini 2"
         case "iPad4,7", "iPad4,8", "iPad4,9": return "iPad mini 3"
         case "iPad5,1", "iPad5,2": return "iPad mini 4"
         case "iPad11,1", "iPad11,2": return "iPad mini (5th generation)"
         case "iPad6,3", "iPad6,4": return "iPad Pro (9.7-inch)"
         case "iPad7,3", "iPad7,4": return "iPad Pro (10.5-inch)"
         case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4": return "iPad Pro (11-inch) (1st generation)"
         case "iPad8,9", "iPad8,10": return "iPad Pro (11-inch) (2nd generation)"
         case "iPad6,7", "iPad6,8": return "iPad Pro (12.9-inch) (1st generation)"
         case "iPad7,1", "iPad7,2": return "iPad Pro (12.9-inch) (2nd generation)"
         case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8": return "iPad Pro (12.9-inch) (3rd generation)"
         case "iPad8,11", "iPad8,12": return "iPad Pro (12.9-inch) (4th generation)"
         case "AppleTV5,3": return "Apple TV"
         case "AppleTV6,2": return "Apple TV 4K"
         case "AudioAccessory1,1": return "HomePod"
         case "AudioAccessory5,1": return "HomePod mini"
         case "i386", "x86_64":
            return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
         default: return identifier
         }
         #elseif os(tvOS)
         switch identifier {
         case "AppleTV5,3": return "Apple TV 4"
         case "AppleTV6,2": return "Apple TV 4K"
         case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
         default: return identifier
         }
         #endif
      }
      
      return mapToDevice(identifier: identifier)
   }
}
