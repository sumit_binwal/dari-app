//
//  PrecisionDateFormatter.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/3/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

public final class PrecisionDateFormatter: DateFormatter {
   private let microsecondsPrefix = "."
   
   override public init() {
      super.init()
      self.locale = Locale(identifier: "en_US_POSIX")
      self.timeZone = TimeZone(secondsFromGMT: 0)
   }
   
   required public init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override public func string(from date: Date) -> String {
      self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
      let components = self.calendar.dateComponents([.nanosecond], from: date)
      
      let nanosecondsInMicrosecond: Double = 1000
      let microseconds = lrint(Double(components.nanosecond ?? 0) / nanosecondsInMicrosecond)
      
      // Subtract nanoseconds from date to ensure string(from: Date) doesn't attempt faulty rounding.
      let updatedDate = self.calendar.date(
         byAdding: .nanosecond,
         value: -(components.nanosecond ?? 0),
         to: date
      ) ?? date
      
      let dateTimeString = super.string(from: updatedDate)
      
      return String(format: "%@.%06ldZ", dateTimeString, microseconds)
   }
   
   override public func date(from string: String) -> Date? {
      self.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
      
      guard let microsecondsPrefixRange = string.range(of: self.microsecondsPrefix) else {
         return nil
      }
      let microsecondsWithTimeZoneString = String(
         string.suffix(from: microsecondsPrefixRange.upperBound)
      )
      
      let nonDigitsCharacterSet = CharacterSet.decimalDigits.inverted
      guard let timeZoneRangePrefixRange = microsecondsWithTimeZoneString.rangeOfCharacter(
         from: nonDigitsCharacterSet) else { return nil }
      
      let microsecondsString = String(
         microsecondsWithTimeZoneString.prefix(upTo: timeZoneRangePrefixRange.lowerBound)
      )
      guard let microsecondsCount = Double(microsecondsString) else { return nil }
      
      let dateStringExludingMicroseconds = string
         .replacingOccurrences(of: microsecondsString, with: "")
         .replacingOccurrences(of: microsecondsPrefix, with: "")
      
      guard let date = super.date(from: dateStringExludingMicroseconds) else { return nil }
      let microsecondsInSecond = Double(1000000)
      let dateWithMicroseconds = date + microsecondsCount / microsecondsInSecond
      
      return dateWithMicroseconds
   }
}
