//
//  Environment.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 9/18/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation
import UIKit
import Frames

struct Environment {
   #if DEVELOPMENT
   static let tintColorName: String = "Tint-Stage"
   static let serverBaseURL: String = "http://127.0.0.1:8000/"
   static let fileURLsAreRelative: Bool = true
   static let isTrackingEnabled: Bool = false
   static let intercomAppId: String = "duo0vz8d"
   static let intercomApiKey: String = "ios_sdk-b827f36f378e66cfefebf9b7613ab7cefdd23778"
   static let checkoutPublicKey: String = "pk_test_97bc08d0-bdf5-405a-8459-3c462b920132"
   static let checkoutEnvironment: Frames.Environment = .sandbox
   static let freshchatAppID = "e6be637c-f30d-4c73-aa4b-31c3a5912bf1"
   static let freshchatAppKey = "56046758-441a-489c-bd7b-b278202bbfbb"
   static let freschatDomain = "msdk.freshchat.com"
   
   #elseif STAGING
   static let tintColorName: String = "Tint-Stage"
   static let serverBaseURL: String = "https://staging.dariapp.com/"
   static let fileURLsAreRelative: Bool = false
   static let isTrackingEnabled: Bool = true
   static let intercomAppId: String = "duo0vz8d"
   static let intercomApiKey: String = "ios_sdk-b827f36f378e66cfefebf9b7613ab7cefdd23778"
   static let checkoutPublicKey: String = "pk_test_97bc08d0-bdf5-405a-8459-3c462b920132"
   static let checkoutEnvironment: Frames.Environment = .sandbox
   static let freshchatAppID = "e6be637c-f30d-4c73-aa4b-31c3a5912bf1"
   static let freshchatAppKey = "56046758-441a-489c-bd7b-b278202bbfbb"
   static let freschatDomain = "msdk.freshchat.com"
   
   #else
   static let tintColorName: String = "Tint"
   static let serverBaseURL: String = "https://app.dariapp.com/"
   static let fileURLsAreRelative: Bool = false
   static let isTrackingEnabled: Bool = true
   static let intercomAppId: String = "wru6zdzp"
   static let intercomApiKey: String = "ios_sdk-f0d0c374bf8fc5a3ce759f201349919f4dc059f7"
   static let checkoutPublicKey: String = "pk_5c07fcd0-aacd-4ec3-b68b-154f96cb556e"
   static let checkoutEnvironment: Frames.Environment = .live
   #warning("Use production values")
   static let freshchatAppID = "e6be637c-f30d-4c73-aa4b-31c3a5912bf1"
   static let freshchatAppKey = "56046758-441a-489c-bd7b-b278202bbfbb"
   static let freschatDomain = "msdk.freshchat.com"
   
   #endif
   
   static var language: String? {
      get {
         guard
            let languages = UserDefaults.standard.array(forKey: "AppleLanguages") as? [String],
            let languageType = languages.first,
            let language = languageType.components(separatedBy: "-").first
         else {
            return Locale.current.languageCode
         }
         
         return language
      }
      
      set {
         guard let newValue = newValue else {
            UserDefaults.standard.removeObject(forKey: "AppleLanguages")
            return
         }
         UserDefaults.standard.set([newValue], forKey: "AppleLanguages")
      }
   }
   
   static var languageIsArabic: Bool = language == "ar"
   
   static var sessionManager: SessionManager { .default }
   
   static var imagesArchiveKey: String { "images" }
   
   static var termsAndConditionsURL: URL? { URL(string: "https://dariapp.com/terms-and-conditions") }
}
