//
//  Fonts.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/5/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

extension UIFontDescriptor.AttributeName {
   static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

extension UIFont {
   
   struct Montserrat: FontFamily {
      let regular = "Montserrat"
      let bold = "Montserrat-Bold"
      let light = "Montserrat-Light"
      let italic = "Montserrat-Italic"
      let medium = "Montserrat-Medium"
   }
   
   struct Tajawal: FontFamily {
      let regular = "Tajawal"
      let bold = "Tajawal-Bold"
      let light = "Tajawal-Light"
      let italic = "Tajawal"
      let medium = "Tajawal-Medium"
   }
   
   fileprivate class var fontFamily: FontFamily {
      Environment.languageIsArabic ? Tajawal() : Montserrat()
   }
   
   class func regularFont(ofSize size: CGFloat) -> UIFont {
      guard let font = UIFont(name: fontFamily.regular, size: size) else {
         assertionFailure("[UIFont] Failed to get regular font.")
         return systemFont(ofSize: size)
      }
      return font
   }
   
   @objc private class func _regularFont(ofSize size: CGFloat) -> UIFont {
      return regularFont(ofSize: size)
   }
   
   class func mediumFont(ofSize size: CGFloat) -> UIFont {
      guard let font = UIFont(name: fontFamily.medium, size: size) else {
         assertionFailure("[UIFont] Failed to get medium font.")
         return boldSystemFont(ofSize: size)
      }
      return font
   }
   
   class func boldFont(ofSize size: CGFloat) -> UIFont {
      guard let font = UIFont(name: fontFamily.bold, size: size) else {
         assertionFailure("[UIFont] Failed to get bold font.")
         return boldSystemFont(ofSize: size)
      }
      return font
   }
   
   @objc private class func _boldFont(ofSize size: CGFloat) -> UIFont {
      return boldFont(ofSize: size)
   }
   
   class func lightFont(ofSize size: CGFloat) -> UIFont {
      guard let font = UIFont(name: fontFamily.light, size: size) else {
         assertionFailure("[UIFont] Failed to get light font.")
         return systemFont(ofSize: size)
      }
      return font
   }
   
   @objc private class func _lightFont(ofSize size: CGFloat) -> UIFont {
      return lightFont(ofSize: size)
   }
   
   class func italicFont(ofSize size: CGFloat) -> UIFont {
      guard let font = UIFont(name: fontFamily.italic, size: size) else {
         assertionFailure("[UIFont] Failed to get italic font.")
         return italicSystemFont(ofSize: size)
      }
      return font
   }
   
   @objc private class func _italicFont(ofSize size: CGFloat) -> UIFont {
      return italicFont(ofSize: size)
   }
   
   @objc convenience init(myCoder aDecoder: NSCoder) {
      guard let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
         self.init(myCoder: aDecoder)
         return
      }
      var fontName = ""
      switch fontAttribute {
      case "CTFontRegularUsage": fontName = Self.fontFamily.regular
      case "CTFontEmphasizedUsage", "CTFontBoldUsage": fontName = Self.fontFamily.bold
      case "CTFontObliqueUsage": fontName = Self.fontFamily.italic
      default: fontName = Self.fontFamily.regular
      }
      self.init(name: fontName, size: fontDescriptor.pointSize)!
   }
   
   class func overrideInitialize() {
      guard self == UIFont.self else { return }
      
      if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
         let mySystemFontMethod = class_getClassMethod(self, #selector(_regularFont(ofSize:))) {
         method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
      }
      
      if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
         let myBoldSystemFontMethod = class_getClassMethod(self, #selector(_boldFont(ofSize:))) {
         method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
      }
      
      if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))),
         let myItalicSystemFontMethod = class_getClassMethod(self, #selector(_italicFont(ofSize:))) {
         method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
      }
      
      if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))), // Trick to get over the lack of UIFont.init(coder:))
         let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
         method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
      }
   }
}

fileprivate protocol FontFamily {
   var regular: String { get }
   var bold: String { get }
   var light: String { get }
   var italic: String { get }
   var medium: String { get }
}
