//
//  PlaceOrderCoordinatorInput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol PlaceOrderCoordinatorInput {
   var data: Order.FormData { get set }
}
