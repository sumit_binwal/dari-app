//
//  Coordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol Coordinator: AnyObject {
   func start()
   func start(with option: DeepLinkOption?)
}
