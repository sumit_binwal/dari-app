//
//  SupportCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol SupportCoordinatorOutput {
   var onDismiss: (() -> Void)? { get set }
}
