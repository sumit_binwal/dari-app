//
//  AppCoordinatorInput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 03/06/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AppCoordinatorInput: AnyObject {
   func showForceUpdate()
}
