//
//  CatalogCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol CatalogCoordinatorOutput {
   var navigateToOrders: (() -> Void)? { get set }
}
