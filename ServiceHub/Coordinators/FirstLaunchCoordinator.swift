//
//  FirstLaunchCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class FirstLaunchCoordinator: BaseCoordinator, FirstLaunchCoordinatorOutput {
   
   var shouldAnimateStart: Bool = true
   var finishFlow: (() -> Void)?
   
   private let router: Router
   private let factory: FirstLaunchModuleFactory & AuthModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   init(router: Router, factory: FirstLaunchModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      let firstLaunchView = self.factory.makeFirstLaunchView()
      firstLaunchView.shouldAnimate = self.shouldAnimateStart
      firstLaunchView.onSignUpButtonTap = { [weak self] in
         self?.showSignUp()
      }
      firstLaunchView.onSignInButtonTap = { [weak self] in
         self?.showSignIn()
      }
      firstLaunchView.onGuestButtonTap = { [weak self] in
         self?.showDistrictSelection(showCancelButton: false) { [weak self] _ in
            self?.finishFlow?()
         }
      }
      firstLaunchView.onSupportButtonTap = { [weak self] in
         self?.showSupport()
      }
      
      self.router.setRootModule(firstLaunchView, hideBar: true)
   }
   
   private func showSignUp() {
      let coordinator = self.coordinatorFactory.makeAuthCoordinator(router: self.router)
      coordinator.startWithSignIn = false
      coordinator.showSignInButtonInSignUp = false
      coordinator.showDistrictSelectionForSignUp = true
      coordinator.finishFlow = { [weak self, weak coordinator] in
         self?.finishFlow?()
         self?.removeDependency(coordinator)
      }
      self.addDependency(coordinator)
      coordinator.start()
   }
   
   private func showSignIn() {
      let coordinator = self.coordinatorFactory.makeAuthCoordinator(router: self.router)
      coordinator.startWithSignIn = true
      coordinator.showSignInButtonInSignUp = false
      coordinator.finishFlow = { [weak self, weak coordinator] in
         self?.finishFlow?()
         self?.removeDependency(coordinator)
      }
      self.addDependency(coordinator)
      coordinator.start()
   }
   
   private func showDistrictSelection(showCancelButton: Bool, completion: ((District?) -> Void)?) {
      let view = self.factory.makeDistrictSelectorView()
      view.showCancelButton = showCancelButton
      view.onSelectDistrict = completion
      self.router.push(view)
   }
   
   private func showSupport() {
      var (coordinator, module) = self.coordinatorFactory.makeSupportCoordinator()
      coordinator.isDismissible = true
      coordinator.onDismiss = { [weak self] in
         self?.router.dismissModule()
      }
      self.addDependency(coordinator)
      self.router.present(module, onDismiss: { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      })
      coordinator.start()
   }
}
