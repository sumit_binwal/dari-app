//
//  AddCardCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class AddCardCoordinator: BaseCoordinator, AddCardCoordinatorInput, AddCardCoordinatorOutput {
   
   var isForPayment: Bool = true
   var animateStart: Bool = true
   var onDismiss: (() -> Void)?
   var onCardAdded: ((_ card: Card, _ saveCard: Bool) -> Void)?
   
   private let router: Router
   private let factory: CardModuleFactory
   
   init(router: Router, factory: CardModuleFactory) {
      self.router = router
      self.factory = factory
   }
   
   override func start() {
      let view = self.factory.makeAddCardView(isForPayment: self.isForPayment)
      
      view.onCancelButtonTap = { [weak self] in
         self?.onDismiss?()
      }
      view.onCardAdded = { [weak self] card, save in
         self?.onCardAdded?(card, save)
      }
      
      if self.animateStart {
         self.router.push(view)
      } else {
         self.router.setRootModule(view)
      }
   }
}
