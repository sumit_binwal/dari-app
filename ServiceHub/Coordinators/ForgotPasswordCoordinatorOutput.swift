//
//  ForgotPasswordCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ForgotPasswordCoordinatorOutput: AnyObject {
   var email: String? { get set }
   var onDismiss: (() -> Void)? { get set }
   var finishFlow: (() -> Void)? { get set }
}
