//
//  SupportCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Intercom

final class SupportCoordinator: BaseCoordinator, SupportCoordinatorInput, SupportCoordinatorOutput {
   
   var isDismissible: Bool = false
   var onDismiss: (() -> Void)?
   
   private let router: Router
   private let factory: SupportModuleFactory
   
   init(router: Router, factory: SupportModuleFactory) {
      self.router = router
      self.factory = factory
   }
   
   override func start() {
      let view = self.factory.makeSupportView()
      view.showCancelButton = self.isDismissible
      view.onCancelButtonTap = { [weak self] in
         self?.onDismiss?()
      }
      view.onChatButtonTap = { [weak self] in
         self?.showChat()
      }
      self.router.setRootModule(view)
   }
   
   private func showChat() {
      guard Environment.isTrackingEnabled else { return }
      guard let vc = router.toPresent() else { return }
      Intercom.presentMessenger()
//      Freshchat.sharedInstance().showConversations(vc)
   }
}
