//
//  MainCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class MainCoordinator: BaseCoordinator {
   
   private let router: Router
   private let mainView: MainView
   private let factory: ModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   // MARK: - Initial Config
   
   init(router: Router, factory: ModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
      
      self.mainView = factory.makeMainView()
      
      super.init()
      
      // The main view is loaded before the coordinator starts.
      router.setRootModule(mainView, hideBar: true)
      self.mainView.onViewDidLoad = runCatalogFlow()
   }
   
   override func start() {
//      showRating()
      mainView.onCatalogSelect = runCatalogFlow()
      mainView.onOrdersSelect = runOrdersFlow()
      mainView.onSupportSelect = runSupportFlow()
   }
   
   
   // MARK: - Flows
   
   private func runCatalogFlow() -> ((UINavigationController) -> Void)? {
      { [weak self] navigationController in
         if navigationController.viewControllers.isEmpty {
            let router = RouterImp(rootViewController: navigationController)
            
            guard var coordinator = self?.coordinatorFactory.makeCatalogCoordinator(router: router)
            else { return }
            
            coordinator.navigateToOrders = { [weak self] in
               self?.mainView.showOrders()
            }
            
            self?.addDependency(coordinator)
            coordinator.start()
         }
      }
   }
   
   private func runOrdersFlow() -> ((UINavigationController) -> Void)? {
      { [weak self] navigationController in
         if navigationController.viewControllers.isEmpty {
            let router = RouterImp(rootViewController: navigationController)
            
            guard let coordinator = self?.coordinatorFactory.makeOrdersCoordinator(router: router)
            else { return }
            
            self?.mainView.onOrdersTabRemoved = { [weak self, weak coordinator] in
               self?.removeDependency(coordinator)
            }
            
            self?.addDependency(coordinator)
            coordinator.start()
         }
      }
   }
   
   private func runSupportFlow() -> ((UINavigationController) -> Void)? {
      { [weak self] navigationController in
         let router = RouterImp(rootViewController: navigationController)
         
         guard let coordinator = self?.coordinatorFactory.makeSupportCoordinator(router: router)
         else { return }
         
         self?.addDependency(coordinator)
         coordinator.start()
      }
   }
   
   private func showRating() {
      let view = factory.makeRatingView()
      router.present(view)
   }
}
