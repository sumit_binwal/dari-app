//
//  ProfileCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ProfileCoordinatorOutput {
   var onDismiss: (() -> Void)? { get set }
   var onProfileChange: (() -> Void)? { get set }
   var onUserSignOut: (() -> Void)? { get set }
}
