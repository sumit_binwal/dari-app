//
//  AddCardCoordinatorInput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AddCardCoordinatorInput {
   var isForPayment: Bool { get set }
}
