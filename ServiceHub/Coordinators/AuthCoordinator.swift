//
//  AuthCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class AuthCoordinator: BaseCoordinator, AuthCoordinatorInput, AuthCoordinatorOutput {
   
   var startWithSignIn: Bool = false
   var showSignInButtonInSignUp: Bool = true
   var showDistrictSelectionForSignUp: Bool = true
   var onDismiss: (() -> Void)?
   var finishFlow: (() -> Void)?
   
   private let router: Router
   private let factory: AuthModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   private lazy var signUpView: SignUpView = self.factory.makeSignUpView()
   
   init(router: Router, factory: AuthModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      if self.startWithSignIn {
         self.showSignIn()
      } else {
         self.showSignUp()
      }
   }
   
   private func showSignUp() {
      let view = self.signUpView
      view.showSignIn = self.showSignInButtonInSignUp
      
      view.onSignInButtonTap = { [weak self] in
         self?.showSignIn()
      }
      view.onChangeAddressTap = { [weak self] (address, latitude, longitude) in
         self?.showAddress(address: address, latitude: latitude, longitude: longitude)
      }
      view.onSignUpComplete = { [weak self] user in
         guard user != nil else { return }
         self?.finishFlow?()
      }
      
      if self.showDistrictSelectionForSignUp {
         view.showCancelButton = false
         self.showDistrictSelection(showCancelButton: false) { [weak self] district in
            guard district != nil else { return }
            self?.router.push(view)
         }
      } else {
         view.showCancelButton = true
         view.onCancelButtonTap = { [weak self] in
            self?.onDismiss?()
         }
         self.router.setRootModule(view)
      }
   }
   
   private func showSignIn() {
      let view = self.factory.makeSignInView()
      
      view.onForgotPassword = { [weak self] email in
         self?.showForgotPassword(email: email)
      }
      view.onDistrictUnavailable = { [weak self] in
         self?.showDistrictSelection(showCancelButton: true, completion: { [weak self] district in
            if district != nil {
               self?.finishFlow?()
            }
         })
      }
      view.onSignInComplete = { [weak self] in
         self?.finishFlow?()
      }
      
      self.router.push(view)
   }
   
   private func showDistrictSelection(showCancelButton: Bool, completion: ((District?) -> Void)?) {
      let view = self.factory.makeDistrictSelectorView()
      view.showCancelButton = showCancelButton
      view.onSelectDistrict = completion
      self.router.push(view)
   }
   
   private func showAddress(address: String?, latitude: Double?, longitude: Double?) {
      let view = self.factory.makeAddressView(
         address: address,
         latitude: latitude,
         longitude: longitude
      )
      
      
      if latitude == nil {
         view.onAddressChangeSuccessful = { [weak self] (address, latitude, longitude) in
            self?.signUpView.setAddress(address, latitude: latitude, longitude: longitude)
            self?.router.dismissModule()
         }
         self.router.present(view, embedded: true)
         
      } else {
         view.onAddressChangeSuccessful = { [weak self] (address, latitude, longitude) in
            self?.signUpView.setAddress(address, latitude: latitude, longitude: longitude)
            self?.router.popModule()
         }
         self.router.push(view)
      }
   }
   
   private func showForgotPassword(email: String?) {
      let coordinator = self.coordinatorFactory.makeForgotPasswordCoordinator(router: self.router)
      coordinator.email = email
      
      coordinator.onDismiss = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      }
      coordinator.finishFlow = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         self?.finishFlow?()
      }
      self.addDependency(coordinator)
      coordinator.start()
   }
}
