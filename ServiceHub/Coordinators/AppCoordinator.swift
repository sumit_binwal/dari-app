//
//  AppCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

fileprivate enum LaunchInstructor {
   case splash
   case firstLaunch
   case trackingConsent
   case main
   
   static func configure(
      showSplash: Bool,
      isFirstLaunch: Bool,
      shouldRequestTrackingConsent: Bool
   ) -> LaunchInstructor {
      switch (showSplash, isFirstLaunch, shouldRequestTrackingConsent) {
      case (true, _, _): return .splash
      case (_, _, true): return .trackingConsent
      case (_, true, false): return .firstLaunch
      case (_, false, false): return .main
      }
   }
}

final class AppCoordinator: BaseCoordinator, AppCoordinatorInput {
   private var showSplash: Bool = true
   
   /// Indicates whether consent was requested during this session.
   private var didRequestConsent: Bool = false
   
   /// Indicates whether first launch flow was presented this session.
   private var didPresentFirstLaunchFlow: Bool = false
   
   private let window: UIWindow
   private let root: RootView
   private let coordinatorFactory: CoordinatorFactory
   
   private var instructor: LaunchInstructor {
      let isFirstLaunch = District.current == nil
      let shouldRequestTrackingConsent = TrackingManager.shouldRequestConsentToTrack
      
      return LaunchInstructor.configure(
         showSplash: self.showSplash,
         isFirstLaunch: isFirstLaunch,
         shouldRequestTrackingConsent: shouldRequestTrackingConsent
      )
   }
   
   init(window: UIWindow, root: RootView, coordinatorFactory: CoordinatorFactory) {
      self.window = window
      self.root = root
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start(with option: DeepLinkOption?) {
      if self.window.rootViewController !== self.root.toPresent() {
         self.window.rootViewController = self.root.toPresent()
         self.window.makeKeyAndVisible()
      }
      
      // Start with deep link
      if let option = option {
         switch option {
         case .login: break
         }
         
      } else {
         switch self.instructor {
         case .splash: self.runSplashFlow()
         case .firstLaunch: self.runFirstLaunchFlow()
         case .trackingConsent: self.runTrackingConsentRequestFlow()
         case .main: self.runMainFlow()
         }
      }
   }
   
   private func runSplashFlow() {
      let splashView = ModuleFactory().makeSplashView()
      splashView.completion = { [weak self] in
         self?.showSplash = false
         self?.start()
      }
      self.root.setRootModule(splashView, with: EmptyTransition())
   }
   
   private func runFirstLaunchFlow() {
      self.didPresentFirstLaunchFlow = true
      let (coordinator, module) = self.coordinatorFactory.makeFirstLaunchCoordinator()
      
      coordinator.shouldAnimateStart = !self.didRequestConsent
      coordinator.finishFlow = { [weak self, weak coordinator] in
         // Current district, which is used to determine first launch, is set during the first launch flow.
         self?.start()
         self?.removeDependency(coordinator)
      }
      
      self.addDependency(coordinator)
      self.root.setRootModule(
         module,
         with: coordinator.shouldAnimateStart ? FadeInTransition() : FadeAndZoomInTransition()
      )
      coordinator.start()
   }
   
   private func runTrackingConsentRequestFlow() {
      self.didRequestConsent = true
      let trackingConsentView = ModuleFactory().makeTrackingConsentView()
      trackingConsentView.completionHandler = { [weak self] in
         // TrackingManager handles whether or not consent should be requested.
         self?.start()
      }
      self.root.setRootModule(trackingConsentView, with: FadeAndZoomInTransition())
   }
   
   private func runMainFlow() {
      let (coordinator, module) = self.coordinatorFactory.makeMainCoordinator()
      
      let shouldPush: Bool = self.didPresentFirstLaunchFlow || self.didRequestConsent
      let transition: Transition = shouldPush ? PushTransition() : FadeAndZoomInTransition()
      
      self.addDependency(coordinator)
      
      self.root.setRootModule(module, with: transition)
      coordinator.start()
      
      // TODO: Find a better place to ask for notification permission.
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
         self.root.requestNotificationAuthorizationIfNeeded()
      }
   }
   
   func showForceUpdate() {
      let alert = UIAlertController(
         title: "forceUpdateTitle".localized,
         message: "forceUpdateText".localized,
         preferredStyle: .alert
      )
      alert.addAction(
         .init(
            title: "update".localized,
            style: .default,
            handler: { _ in
               self.goToAppStore()
            }
         )
      )
      root.toPresent()?.present(alert, animated: true, completion: nil)
   }
   
   private func goToAppStore() {
      guard
         let url = URL(string: "itms-apps://itunes.apple.com/app/bars/id1478552076"),
         UIApplication.shared.canOpenURL(url)
      else { return }
      
      UIApplication.shared.open(url)
   }
}
