//
//  PlaceOrderCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation

final class PlaceOrderCoordinator:
   BaseCoordinator,
   PlaceOrderCoordinatorInput,
   PlaceOrderCoordinatorOutput
{
   var data: Order.FormData
   var onDismiss: (() -> Void)?
   var finishFlow: (() -> Void)?
   var finishFlowAndNavigateToMyOrders: (() -> Void)?
   
   private let router: Router
   private let factory: OrderPlacementModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   init(
      data: Order.FormData,
      router: Router,
      factory: OrderPlacementModuleFactory,
      coordinatorFactory: CoordinatorFactory
   ) {
      self.data = data
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      // If there's a description show it first
      if !self.data.listing.description.isEmpty {
         showDescription { [weak self] in
            self?.initiateOrderFlow(onDismiss: nil)
            
         } onDismiss: { [weak self] in
            self?.onDismiss?()
         }
         
      } else {
         initiateOrderFlow { [weak self] in
            self?.onDismiss?()
         }
      }
   }
   
   private func initiateOrderFlow(onDismiss: (() -> Void)?) {
      if self.data.listing.requiresServiceProviderSelection {
         self.showServiceProviderSelector(onDismiss: onDismiss)
      } else {
         self.showOrderForm(onDismiss: onDismiss)
      }
   }
   
   private func showServiceProviderSelector(onDismiss: (() -> Void)?) {
      let view = self.factory.makeServiceProviderSelectorView(data: self.data)
      view.onServiceProviderSelect = { [weak self] in
         self?.showOrderForm(onDismiss: nil)
      }
      router.push(view, animated: true, hideBottomBar: true, completion: onDismiss)
   }
   
   private func showOrderForm(onDismiss: (() -> Void)?) {
      let view = self.factory.makeOrderFormView(data: self.data)
      
      view.onSupportButtonTap = { [weak self] in
         self?.showSupport()
      }
      view.onNextButtonTap = { [weak self] in
         guard let data = self?.data else { return }
         if data.requiresMultipleDates {
            self?.showDateForm()
         } else {
            self?.showOrderSummary()
         }
      }
      
      router.push(view, animated: true, hideBottomBar: true, completion: onDismiss)
   }
   
   private func showDateForm() {
      let view = self.factory.makeDateFormView(data: self.data)
      view.onSupportButtonTap = { [weak self] in
         self?.showSupport()
      }
      view.onNextButtonTap = { [weak self] in
         self?.showOrderSummary()
      }
      self.router.push(view)
   }
   
   private func showOrderSummary() {
      let view = self.factory.makeOrderSummaryView(data: self.data)
      view.onSupportButtonTap = { [weak self] in
         self?.showSupport()
      }
      view.onAddCardTap = { [weak self] in
         self?.showAddCard(onSuccess: { [weak view] (card, save) in
            view?.addCard(card, save: save)
         }, onDismiss: { [weak view] in
            view?.refresh()
         })
      }
      view.onOTPRedirect = { [weak self, weak view] url in
         self?.showPaymentOTPView(
            url: url,
            successCallback: { (url) in
               self?.router.dismissModule()
               view?.threeDSSuccessCallback(url: url)
               
            }, failureCallback: { (url) in
               view?.threeDSFailureCallback(url: url)
               self?.router.dismissModule()
            }, onCancel: {
               view?.removeUnsavedCards()
            }
         )
      }
      view.onOrderSuccessful = { [weak self] order in
         self?.showOrderConfirmation(order)
      }
      self.router.push(view)
   }
   
   private func showDescription(completion: (() -> Void)?, onDismiss: (() -> Void)?) {
      let listing = data.listing
      let view = self.factory.makeListingDescriptionView(listing: listing)
      view.onNextButtonTap = completion
      view.onSupportButtonTap = { [weak self] in
         self?.showSupport()
      }
      router.push(view, animated: true, hideBottomBar: true, completion: onDismiss)
      
      let service = listing.service
      
      EventLogger.log(
         .didViewService,
         revenue: nil,
         currency: nil,
         parameters: [
            .service: service?.englishName ?? "",
            .serviceID: service?.id ?? 0
         ]
      )
   }
   
   private func showSupport() {
      var (coordinator, module) = self.coordinatorFactory.makeSupportCoordinator()
      coordinator.isDismissible = true
      coordinator.onDismiss = { [weak self] in
         self?.router.dismissModule()
      }
      self.addDependency(coordinator)
      self.router.present(module, onDismiss: { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      })
      coordinator.start()
   }
   
   private func showOrderConfirmation(_ order: Order) {
      let view = self.factory.makeOrderConfirmationView(order: order)
      view.onHomeButtonTap = { [weak self] in
         self?.finishFlow?()
      }
      view.onOrdersButtonTap = { [weak self] in
         self?.finishFlowAndNavigateToMyOrders?()
      }
      self.router.present(view)
   }
   
   private func showAddCard(
      onSuccess: ((_ card: Card, _ saveCard: Bool) -> Void)?,
      onDismiss: (() -> Void)?
   ) {
      var (coordinator, view) = self.coordinatorFactory.makeAddCardCoordinator()
      
      coordinator.onDismiss = { [weak self] in
         self?.router.dismissModule()
      }
      coordinator.onCardAdded = { [weak self, weak coordinator] card, saveCard in
         self?.removeDependency(coordinator)
         self?.router.dismissModule(animated: true, completion: {
            onSuccess?(card, saveCard)
         })
      }
      
      self.addDependency(coordinator)
      self.router.present(view, onDismiss: { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         onDismiss?()
      })
      coordinator.start()
   }
   
   private func showPaymentOTPView(
      url: URL,
      successCallback: @escaping ((_ url: URL) -> Void),
      failureCallback: @escaping ((_ url: URL) -> Void),
      onCancel: (() -> Void)?
   ) {
      let paymentOTPView = factory.makePaymentOTPController(url: url)
      paymentOTPView.onSuccessRedirect = successCallback
      paymentOTPView.onFailureRedirect = failureCallback
      paymentOTPView.onCancel = { [weak self] in
         onCancel?()
         self?.router.dismissModule()
      }
      router.present(paymentOTPView, embedded: true, animated: true, completion: nil) {
         onCancel?()
      }
   }
}
