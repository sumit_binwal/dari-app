//
//  BaseCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

class BaseCoordinator: Coordinator {
   var childCoordinators: [Coordinator] = []
   
   func start() {
      self.start(with: nil)
   }
   
   func start(with option: DeepLinkOption?) {}
   
   func addDependency(_ coordinator: Coordinator) {
      guard !self.childCoordinators.contains(where: { $0 === coordinator }) else { return }
      self.childCoordinators.append(coordinator)
   }
   
   func removeDependency(_ coordinator: Coordinator?) {
      guard
         !self.childCoordinators.isEmpty,
         let coordinator = coordinator
      else { return }
      
      // Clear child coordinators recursively
      if let coordinator = coordinator as? BaseCoordinator, !coordinator.childCoordinators.isEmpty {
         coordinator.childCoordinators
            .filter { $0 !== coordinator }
            .forEach { coordinator.removeDependency($0) }
      }
      
      for (index, element) in self.childCoordinators.enumerated() where element === coordinator {
         self.childCoordinators.remove(at: index)
         break
      }
   }
}
