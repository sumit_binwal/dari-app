//
//  AuthCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 03/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AuthCoordinatorOutput: AnyObject {
   var onDismiss: (() -> Void)? { get set }
   var finishFlow: (() -> Void)? { get set }
}
