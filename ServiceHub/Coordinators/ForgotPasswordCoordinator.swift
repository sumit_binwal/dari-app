//
//  ForgotPasswordCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class ForgotPasswordCoordinator: BaseCoordinator, ForgotPasswordCoordinatorOutput {
   
   var email: String?
   var onDismiss: (() -> Void)?
   var finishFlow: (() -> Void)?
   
   private let router: Router
   private let factory: AuthModuleFactory
   
   init(router: Router, factory: AuthModuleFactory) {
      self.router = router
      self.factory = factory
   }
   
   override func start() {
      let forgotPasswordView = self.factory.makeForgotPasswordView(email: self.email)
      forgotPasswordView.onRequestOTP = { [weak self] email in
         self?.showResetPassword(email: email)
      }
      self.router.push(forgotPasswordView, animated: true) { [weak self] in
         self?.onDismiss?()
      }
   }
   
   private func showResetPassword(email: String?) {
      let resetPasswordView = self.factory.makeResetPasswordView(email: email)
      resetPasswordView.onPasswordResetSuccessful = { [weak self] in
         self?.finishFlow?()
      }
      self.router.push(resetPasswordView)
   }
}
