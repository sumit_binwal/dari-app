//
//  DeepLinkOption.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

struct DeepLinkURLConstants {
   static let login = "login"
}

enum DeepLinkOption {
   case login
}
