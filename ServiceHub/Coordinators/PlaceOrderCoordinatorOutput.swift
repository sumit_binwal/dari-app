//
//  PlaceOrderCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol PlaceOrderCoordinatorOutput {
   var onDismiss: (() -> Void)? { get set }
   var finishFlow: (() -> Void)? { get set }
   var finishFlowAndNavigateToMyOrders: (() -> Void)? { get set }
}
