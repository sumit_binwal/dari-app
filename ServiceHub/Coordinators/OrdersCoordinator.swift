//
//  OrdersCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 15/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class OrdersCoordinator: BaseCoordinator {
   
   private let router: Router
   private let factory: OrderModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   private lazy var orderListView: OrderListView = self.factory.makeOrderListView()
   
   init(router: Router, factory: OrderModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      self.showOrderList()
   }
   
   private func showOrderList() {
      self.orderListView.onOrderSelect = { [weak self] order in
         self?.showOrderDetails(order)
      }
      self.router.setRootModule(self.orderListView)
   }
   
   private func showOrderDetails(_ order: Order) {
      let view = self.factory.makeOrderDetailView(order)
      view.onOrderCanceled = { [weak self] orderID in
         self?.router.popModule()
         self?.deleteOrder(withID: orderID)
      }
      self.router.push(view)
   }
   
   private func deleteOrder(withID orderID: Int) {
      self.orderListView.removeOrder(withID: orderID)
   }
}
