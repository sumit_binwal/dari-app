//
//  CatalogCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class CatalogCoordinator: BaseCoordinator, CatalogCoordinatorOutput {
   
   var navigateToOrders: (() -> Void)?
   
   private let router: Router
   private let factory: CatalogModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   private lazy var catalogView: CatalogModuleView = self.factory.makeCatalogView()
   
   init(router: Router, factory: CatalogModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      self.showCatalog()
   }
   
   private func showCatalog() {
      self.catalogView.onCityButtonTap = { [weak self] in
         self?.showDistrictSelector()
      }
      self.catalogView.onProfileButtonTap = { [weak self] in
         self?.showProfileOrAuth()
      }
      self.catalogView.onListingSelect = { [weak self] listing in
         self?.handleListingSelection(listing)
      }
      self.router.setRootModule(self.catalogView)
   }
   
   private func showDistrictSelector() {
      let view = self.factory.makeDistrictSelectorView()
      view.showCancelButton = true
      view.onSelectDistrict = { [weak self] district in
         self?.router.dismissModule()
         
         if district != nil {
            self?.catalogView.reload()
         }
      }
      self.router.present(view, embedded: true)
   }
   
   private func showProfileOrAuth() {
      if User.current == nil {
         self.showAuth()
      } else {
         self.showProfile()
      }
   }
   
   private func showProfile() {
      var coordinator = self.coordinatorFactory.makeProfileCoordinator(router: self.router)
      coordinator.onDismiss = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      }
      coordinator.onProfileChange = { [weak self] in
         self?.catalogView.reload()
      }
      coordinator.onUserSignOut = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         self?.router.popToRootModule(animated: true) { [weak self] in
            self?.catalogView.reload()
         }
      }
      
      self.addDependency(coordinator)
      coordinator.start()
   }
   
   private func showAuth(completion: (() -> Void)? = nil) {
      let (coordinator, module) = self.coordinatorFactory.makeAuthCoordinator()
      
      coordinator.startWithSignIn = false
      coordinator.showSignInButtonInSignUp = true
      coordinator.showDistrictSelectionForSignUp = false
      
      coordinator.onDismiss = { [weak self] in
         self?.router.dismissModule()
      }
      coordinator.finishFlow = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         self?.router.dismissModule(animated: true, completion: { [weak self] in
            self?.catalogView.reload(completion: completion)
         })
      }
      
      self.addDependency(coordinator)
      self.router.present(module, onDismiss: { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      })
      coordinator.start()
   }
   
   private func handleListingSelection(_ listing: Listing) {
      guard User.current != nil else {
         let listingID = listing.id
         self.showAuth() { [weak self] in
            if let listing = Listing.listing(withID: listingID) {
               self?.handleListingSelection(listing)
            }
         }
         return
      }
      
      if let orderFormData = Order.FormData(listing: listing) {
         self.startPlaceOrderFlow(orderFormData)
      } else if !listing.sublistings.isEmpty {
         self.showSublistings(for: listing)
      }
   }
   
   private func showSublistings(for listing: Listing) {
      let view = self.factory.makeListingsView(listing)
      view.onListingSelect = { [weak self] listing in
         self?.handleListingSelection(listing)
      }
      self.router.push(view)
   }
   
   private func startPlaceOrderFlow(_ orderData: Order.FormData) {
      var coordinator = self.coordinatorFactory.makePlaceOrderCoordinator(
         data: orderData,
         router: self.router
      )
      coordinator.onDismiss = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      }
      coordinator.finishFlow = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         self?.router.popToRootModule(animated: false, completion: {
            self?.router.dismissModule()
         })
      }
      coordinator.finishFlowAndNavigateToMyOrders = { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
         self?.router.popToRootModule(animated: false, completion: {
            self?.navigateToOrders?()
            self?.router.dismissModule()
         })
      }
      self.addDependency(coordinator)
      coordinator.start()
   }
}
