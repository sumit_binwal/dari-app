//
//  SupportCoordinatorInput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol SupportCoordinatorInput {
   var isDismissible: Bool { get set }
}
