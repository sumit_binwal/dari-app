//
//  AuthCoordinatorInput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AuthCoordinatorInput: AnyObject {
   var startWithSignIn: Bool { get set }
   var showSignInButtonInSignUp: Bool { get set }
   var showDistrictSelectionForSignUp: Bool { get set }
}
