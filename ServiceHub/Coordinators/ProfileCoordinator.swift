//
//  ProfileCoordinator.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

final class ProfileCoordinator: BaseCoordinator, ProfileCoordinatorOutput {
   
   var onDismiss: (() -> Void)?
   var onProfileChange: (() -> Void)?
   var onUserSignOut: (() -> Void)?
   
   private let router: Router
   private let factory: ProfileModuleFactory
   private let coordinatorFactory: CoordinatorFactory
   
   init(router: Router, factory: ProfileModuleFactory, coordinatorFactory: CoordinatorFactory) {
      self.router = router
      self.factory = factory
      self.coordinatorFactory = coordinatorFactory
   }
   
   override func start() {
      guard let user = User.current else { return }
      let view = self.factory.makeProfileView(user: user)
      
      view.onNameSelect = { [weak self] in
         self?.showName()
      }
      view.onPhoneSelect = { [weak self] in
         self?.showPhone()
      }
      view.onEmailSelect = { [weak self] in
         self?.showEmail()
      }
      view.onChangePasswordSelect = { [weak self] in
         self?.showChangePassword()
      }
      view.onAddressSelect = { [weak self] in
         self?.showAddress()
      }
      view.onManageCardsSelect = { [weak self] in
         self?.showCardList()
      }
      view.onSignOut = { [weak self] in
         self?.onUserSignOut?()
      }
      
      self.router.push(view, animated: true) { [weak self] in
         self?.onDismiss?()
      }
   }
   
   private func showName() {
      let view = self.factory.makeNameChangeView()
      view.onProfileChangeSuccessful = { [weak self] in
         self?.router.popModule()
         self?.onProfileChange?()
      }
      self.router.push(view)
   }
   
   private func showPhone() {
      let view = self.factory.makePhoneChangeView()
      view.onProfileChangeSuccessful = { [weak self] in
         self?.router.popModule()
      }
      self.router.push(view)
   }
   
   private func showEmail() {
      let view = self.factory.makeEmailChangeView()
      view.onProfileChangeSuccessful = { [weak self] in
         self?.router.popModule()
      }
      self.router.push(view)
   }
   
   private func showChangePassword() {
      let view = self.factory.makePasswordChangeView()
      view.onProfileChangeSuccessful = { [weak self] in
         self?.router.popModule()
      }
      self.router.push(view)
   }
   
   private func showAddress() {
      let user = User.current
      let view = self.factory.makeAddressView(
         address: user?.address,
         latitude: user?.latitude,
         longitude: user?.longitude
      )
      view.onAddressChangeSuccessful = { [weak self] _, _, _ in
         self?.router.popModule()
      }
      self.router.push(view)
   }
   
   private func showCardList() {
      let view = factory.makeCardListView()
      view.onAddCardButtonTapped = { [weak self] in
         self?.showAddCard(completion: { card in
            view.addCard(card)
         })
      }
      router.push(view)
   }
   
   private func showAddCard(completion: ((_ card: Card) -> Void)?) {
      var (coordinator, module) = coordinatorFactory.makeAddCardCoordinator()
      
      coordinator.isForPayment = false
      coordinator.onCardAdded = { [weak self, weak coordinator] card, _ in
         self?.router.dismissModule()
         self?.removeDependency(coordinator)
         completion?(card)
      }
      coordinator.onDismiss = { [weak self, weak coordinator] in
         self?.router.dismissModule()
         self?.removeDependency(coordinator)
      }
      
      addDependency(coordinator)
      router.present(module) { [weak self, weak coordinator] in
         self?.removeDependency(coordinator)
      }
      coordinator.start()
   }
}
