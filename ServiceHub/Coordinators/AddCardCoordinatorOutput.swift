//
//  AddCardCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 14/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AddCardCoordinatorOutput {
   var onDismiss: (() -> Void)? { get set }
   var onCardAdded: ((_ card: Card, _ saveCard: Bool) -> Void)? { get set }
}
