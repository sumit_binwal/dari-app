//
//  FirstLaunchCoordinatorOutput.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: Think of a better name
protocol FirstLaunchCoordinatorOutput: AnyObject {
   var shouldAnimateStart: Bool { get set }
   var finishFlow: (() -> Void)? { get set }
}
