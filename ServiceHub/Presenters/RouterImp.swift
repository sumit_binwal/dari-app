//
//  RouterImp.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 04/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class RouterImp: NSObject, Router {
   
   private weak var rootViewController: UINavigationController?
   private var completions: [UIViewController: () -> Void]
   
   init(rootViewController: UINavigationController) {
      self.rootViewController = rootViewController
      self.completions = [:]
      super.init()
      rootViewController.delegate = self
   }
   
   var presentationController: UIPresentationController? {
      self.toPresent()?.presentationController
   }
   
   func toPresent() -> UIViewController? {
      self.rootViewController
   }
   
   func runCompletion(for controller: UIViewController?) {
      guard let controller = controller else { return }
      let completion = self.completions.removeValue(forKey: controller)
      completion?()
   }
   
   func present(
      _ module: Presentable?,
      embedded: Bool,
      animated: Bool,
      completion: (() -> Void)?,
      onDismiss: (() -> Void)?
   ) {
      guard let controller = module?.toPresent() else { return }
      
      let viewController: UIViewController = embedded ?
         UINavigationController(rootViewController: controller) : controller
      
      if let onDismiss = onDismiss {
         self.completions[viewController] = onDismiss
      }
      
      viewController.presentationController?.delegate = self
      
      self.rootViewController?.present(viewController, animated: animated, completion: completion)
   }
   
   func dismissModule(animated: Bool, completion: (() -> Void)?) {
      let controller = self.rootViewController?.presentedViewController
      self.rootViewController?.dismiss(animated: animated, completion: {
         completion?()
         self.runCompletion(for: controller)
      })
   }
   
   func push(
      _ module: Presentable?,
      animated: Bool,
      hideBottomBar: Bool,
      completion: (() -> Void)?
   ) {
      guard let controller = module?.toPresent() else { return }
      guard !(controller is UINavigationController) else {
         assertionFailure("Attempted to push UINavigaitonController.")
         return
      }
      
      if let completion = completion {
         self.completions[controller] = completion
      }
      
      controller.hidesBottomBarWhenPushed = hideBottomBar
      self.rootViewController?.pushViewController(controller, animated: animated)
   }
   
   func popModule(animated: Bool) {
      if let controller = self.rootViewController?.popViewController(animated: animated) {
         self.runCompletion(for: controller)
      }
   }
   
   func setRootModule(_ module: Presentable?, hideBar: Bool) {
      guard let controller = module?.toPresent() else { return }
      self.rootViewController?.setViewControllers([controller], animated: false)
      self.rootViewController?.isNavigationBarHidden = hideBar
   }
   
   func popToRootModule(animated: Bool, completion: (() -> Void)?) {
      if let controllers = self.rootViewController?.popToRootViewController(animated: animated) {
         controllers.forEach {
            self.runCompletion(for: $0)
         }
         completion?()
      }
   }
}

extension RouterImp: UINavigationControllerDelegate {
   func navigationController(
      _ navigationController: UINavigationController,
      didShow viewController: UIViewController,
      animated: Bool
   ) {
      guard
         let fromVC = navigationController.transitionCoordinator?.viewController(forKey: .from)
      else { return }
      
      // If fromVC is not in viewControllers, then it's being dismissed
      guard !navigationController.viewControllers.contains(fromVC) else { return }
      
      self.runCompletion(for: fromVC)
   }
}

extension RouterImp: UIPopoverPresentationControllerDelegate {
   func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
      let viewController = presentationController.presentedViewController
      self.runCompletion(for: viewController)
   }
}
