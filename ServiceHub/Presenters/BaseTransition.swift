//
//  BaseTransition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

class BaseTransition: Transition {
   
   let duration: TimeInterval
   
   internal var fromViewStartingAlpha: CGFloat { 1 }
   internal var fromViewEndingAlpha: CGFloat { 1 }
   
   internal var toViewStartingAlpha: CGFloat { 1 }
   internal var toViewEndingAlpha: CGFloat { 1 }
   
   init(duration: TimeInterval = 0.3) {
      self.duration = duration
   }
   
   func open(
      _ viewController: UIViewController,
      from fromViewController: UIViewController?,
      in container: UIView,
      containerViewController: UIViewController,
      completion: (() -> Void)?
   ) {
      let fromVC = fromViewController
      let fromView = fromVC?.view
      
      containerViewController.addChild(viewController)
      fromVC?.willMove(toParent: nil)
      
      viewController.view.alpha = self.toViewStartingAlpha
      viewController.view.transform = self.getToViewStartingTransform(container)
      
      fromView?.alpha = self.fromViewStartingAlpha
      fromView?.transform = self.getFromViewStartingTransform(container)
      
      container.addFillingSubview(viewController.view)
      
      UIView.animate(
         withDuration: self.duration,
         delay: 0,
         options: .curveEaseInOut) {
         
         viewController.view.alpha = self.toViewEndingAlpha
         viewController.view.transform = self.getToViewEndingTransform(container)
         
         fromView?.alpha = self.fromViewEndingAlpha
         fromView?.transform = self.getFromViewEndingTransform(container)
         
      } completion: { _ in
         viewController.didMove(toParent: containerViewController)
         fromView?.removeFromSuperview()
         fromVC?.removeFromParent()
         completion?()
      }
   }
   
   
   internal func getFromViewStartingTransform(_ containerView: UIView) -> CGAffineTransform {
      .identity
   }
   
   internal func getFromViewEndingTransform(_ containerView: UIView) -> CGAffineTransform {
      .identity
   }
   
   internal func getToViewStartingTransform(_ containerView: UIView) -> CGAffineTransform {
      .identity
   }
   
   internal func getToViewEndingTransform(_ containerView: UIView) -> CGAffineTransform {
      .identity
   }
   
   internal func layoutDirection(for view: UIView) -> UIUserInterfaceLayoutDirection {
      UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute)
   }
}
