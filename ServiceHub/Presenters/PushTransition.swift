//
//  PushTransition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class PushTransition: BaseTransition {
   
   override internal func getFromViewEndingTransform(_ containerView: UIView) -> CGAffineTransform {
      let pushFromLeft = self.layoutDirection(for: containerView) == .rightToLeft
      
      return CGAffineTransform(
         translationX: (pushFromLeft ? 1 : -1) / 3 * containerView.bounds.width,
         y: 0
      )
   }
   
   override internal func getToViewStartingTransform(_ containerView: UIView) -> CGAffineTransform {
      let pushFromLeft = self.layoutDirection(for: containerView) == .rightToLeft
      
      return CGAffineTransform(
         translationX: (pushFromLeft ? -1 : 1) * containerView.bounds.width,
         y: 0
      )
   }
}
