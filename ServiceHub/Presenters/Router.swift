//
//  Router.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol Router: Presentable {
   func present(
      _ module: Presentable?,
      embedded: Bool,
      animated: Bool,
      completion: (() -> Void)?,
      onDismiss: (() -> Void)?
   )
   func dismissModule(animated: Bool, completion: (() -> Void)?)
   func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?)
   func popModule(animated: Bool)
   func setRootModule(_ module: Presentable?, hideBar: Bool)
   func popToRootModule(animated: Bool, completion: (() -> Void)?)
}

extension Router {
   func present(_ module: Presentable?) {
      self.present(module, animated: true)
   }
   
   func present(_ module: Presentable?, onDismiss: (() -> Void)?) {
      self.present(module, embedded: false, animated: true, completion: nil, onDismiss: onDismiss)
   }
   
   func present(_ module: Presentable?, embedded: Bool) {
      self.present(module, embedded: embedded, animated: true, completion: nil, onDismiss: nil)
   }
   
   func present(_ module: Presentable?, animated: Bool) {
      self.present(module, animated: animated, completion: nil)
   }
   
   func present(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
      self.present(
         module,
         embedded: false,
         animated: animated,
         completion: completion,
         onDismiss: nil
      )
   }
   
   func dismissModule() {
      self.dismissModule(animated: true, completion: nil)
   }
}

extension Router {
   func push(_ module: Presentable?) {
      self.push(module, animated: true)
   }
   
   func push(_ module: Presentable?, hideBottomBar: Bool) {
      self.push(module, animated: true, hideBottomBar: hideBottomBar, completion: nil)
   }
   
   func push(_ module: Presentable?, animated: Bool) {
      self.push(module, animated: animated, completion: nil)
   }
   
   func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
      self.push(module, animated: animated, hideBottomBar: false, completion: completion)
   }
   
   func popModule() {
      self.popModule(animated: true)
   }
   
   func popToRootModule(animated: Bool) {
      self.popToRootModule(animated: animated, completion: nil)
   }
}

extension Router {
   func setRootModule(_ module: Presentable?) {
      self.setRootModule(module, hideBar: false)
   }
}
