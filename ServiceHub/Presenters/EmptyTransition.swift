//
//  EmptyTransition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class EmptyTransition: Transition {
   func open(
      _ viewController: UIViewController,
      from fromViewController: UIViewController?,
      in container: UIView,
      containerViewController: UIViewController,
      completion: (() -> Void)?
   ) {
      let fromVC = fromViewController
      let fromView = fromVC?.view
      
      containerViewController.addChild(viewController)
      fromVC?.willMove(toParent: nil)
      
      container.addFillingSubview(viewController.view)
      
      viewController.didMove(toParent: containerViewController)
      fromView?.removeFromSuperview()
      fromVC?.removeFromParent()
      completion?()
   }
}
