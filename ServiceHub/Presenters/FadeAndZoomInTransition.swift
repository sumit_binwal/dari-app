//
//  FadeAndZoomInTransition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

class FadeAndZoomInTransition: BaseTransition {
   
   override internal var toViewStartingAlpha: CGFloat { 0 }
   
   override internal func getFromViewEndingTransform(_ containerView: UIView) -> CGAffineTransform {
      self.getToViewStartingTransform(containerView).inverted()
   }
   
   override internal func getToViewStartingTransform(_ containerView: UIView) -> CGAffineTransform {
      .init(scaleX: 1.1, y: 1.1)
   }
}
