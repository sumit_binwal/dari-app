//
//  FadeInTransition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 07/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

class FadeInTransition: BaseTransition {
   override internal var toViewStartingAlpha: CGFloat { 0 }
}
