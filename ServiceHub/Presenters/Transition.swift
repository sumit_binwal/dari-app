//
//  Transition.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 04/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

protocol Transition {
   func open(
      _ viewController: UIViewController,
      from fromViewController: UIViewController?,
      in container: UIView,
      containerViewController: UIViewController,
      completion: (() -> Void)?
   )
}

extension Transition {
   func open(
      _ viewController: UIViewController,
      from fromViewController: UIViewController?,
      in container: UIView,
      containerViewController: UIViewController
   ) {
      self.open(
         viewController,
         from: fromViewController,
         in: container,
         containerViewController: containerViewController,
         completion: nil
      )
   }
}
