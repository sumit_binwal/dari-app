//
//  WebController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/03/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit
import WebKit

class WebController: UIViewController, WebView {
   
   // MARK: - Properties
   
   private let url: URL
   
   private lazy var progressView: UIProgressView = {
      let view = UIProgressView(progressViewStyle: .bar)
      view.translatesAutoresizingMaskIntoConstraints = false
      return view
   }()
   
   internal lazy var webView: WKWebView = {
      let view = WKWebView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.navigationDelegate = self
      return view
   }()
   
   // MARK: - Init
   
   init(url: URL) {
      self.url = url
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      view.addSubviews([webView, progressView])
      NSLayoutConstraint.activate([
         progressView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
         progressView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         progressView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         
         webView.topAnchor.constraint(equalTo: view.topAnchor),
         webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      [#keyPath(WKWebView.estimatedProgress), #keyPath(WKWebView.isLoading)].forEach {
         webView.addObserver(
            self,
            forKeyPath: $0,
            options: .new,
            context: nil
         )
      }
      
      
      webView.load(URLRequest(url: url))
   }
   
   // MARK: - Other
   
   override func observeValue(
      forKeyPath keyPath: String?,
      of object: Any?,
      change: [NSKeyValueChangeKey : Any]?,
      context: UnsafeMutableRawPointer?
   ) {
      switch keyPath {
      case #keyPath(WKWebView.estimatedProgress):
         progressView.progress = Float(webView.estimatedProgress)
         
      case #keyPath(WKWebView.isLoading):
         progressView.isHidden = !webView.isLoading
         
      default: break
      }
   }
}

// MARK: - Navigation Delegate

extension WebController: WKNavigationDelegate {
   func webView(
      _ webView: WKWebView,
      didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!
   ) {
   }
}
