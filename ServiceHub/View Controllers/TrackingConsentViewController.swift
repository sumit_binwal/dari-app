//
//  TrackingConsentViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 12/01/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

class TrackingConsentViewController: UIViewController, TrackingConsentView {
   var completionHandler: (() -> Void)?
   // MARK: - UI
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "dataConsentTitle".localized
      label.numberOfLines = 0
      label.font = .boldFont(ofSize: 17)
      label.textColor = .text
      label.textAlignment = .center
      return label
   }()
   private lazy var textLabel: TappableLabel = {
      let label = TappableLabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.numberOfLines = 0
      label.textAlignment = .center
      
      label.set(
         text: "dataConsent".localized,
         tappableTexts: ["please".localized, "now".localized, "later".localized],
         tapActions: [nil, nil, nil],
         font: .mediumFont(ofSize: 20),
         tappableFont: .boldFont(ofSize: 20)
      )
      
      label.setContentHuggingPriority(.init(249), for: .vertical)
      return label
   }()
   private lazy var tapLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "dataConsentInstruction".localized
      label.numberOfLines = 0
      label.font = .boldFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      return label
   }()
   private lazy var alertView: AlertView = .init()
   private lazy var instructionView: UIView = {
      let v = UIView()
      v.translatesAutoresizingMaskIntoConstraints = false
      v.addSubviews([self.tapLabel, self.alertView])
      
      NSLayoutConstraint.activate([
         self.tapLabel.topAnchor.constraint(equalTo: v.topAnchor),
         self.tapLabel.leadingAnchor.constraint(equalTo: v.leadingAnchor),
         self.tapLabel.trailingAnchor.constraint(equalTo: v.trailingAnchor),
         
         self.alertView.topAnchor.constraint(equalTo: self.tapLabel.bottomAnchor, constant: 20),
         self.alertView.leadingAnchor.constraint(equalTo: v.leadingAnchor),
         self.alertView.trailingAnchor.constraint(equalTo: v.trailingAnchor),
         self.alertView.bottomAnchor.constraint(equalTo: v.bottomAnchor)
      ])
      
      return v
   }()
   
   private lazy var imageView: UIImageView = {
      let iv = UIImageView(image: UIImage(named: "handsHoldingPhone"))
      iv.translatesAutoresizingMaskIntoConstraints = false
      iv.contentMode = .scaleAspectFit
      return iv
   }()
   
   // MARK: Arrow
   private var arrowStartPoint: CGPoint {
      let x: CGFloat = Environment.languageIsArabic ? self.tapLabel.bounds.maxX - 40 : 20
      let point = CGPoint(x, self.tapLabel.font.ascender + 8)
      let adjustedPoint = self.tapLabel.convert(point, to: self.view)
      return adjustedPoint
   }
   private var arrowEndPoint: CGPoint {
      let point = self.alertView.arrowAnchorPoint
      return self.alertView.convert(point, to: self.view)
   }
   private lazy var arrowLayer: CAShapeLayer = {
      let l = CAShapeLayer()
      
      l.fillColor = nil
      l.strokeColor = UIColor.calypso.cgColor
      l.lineWidth = 4
      l.lineCap = .round
      
      return l
   }()
   
   private lazy var buttonToolbar: ButtonToolbar = .init()
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.view.backgroundColor = .background
      self.setUpViews()
      
      self.buttonToolbar.laterButton.addTarget(
         self,
         action: #selector(self.didTapLaterButton),
         for: .touchUpInside
      )
      self.buttonToolbar.allowButton.addTarget(
         self,
         action: #selector(self.didTapAllowButton),
         for: .touchUpInside
      )
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      DispatchQueue.main.async {
         let arrow = ArrowPath(start: self.arrowStartPoint, end: self.arrowEndPoint)
         self.arrowLayer.path = arrow.cgPath
      }
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      
      TrackingManager.dateConsentRequested = Date()
   }
   
   private func setUpViews() {
      let margins = self.view.layoutMarginsGuide
      
      self.view.addSubviews([
         self.textLabel,
         self.instructionView,
         self.imageView,
         self.buttonToolbar
      ])
      
      NSLayoutConstraint.activate([
         self.textLabel.topAnchor.constraint(equalTo: margins.topAnchor, constant: 0),
         self.textLabel.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
         self.textLabel.widthAnchor.constraint(equalTo: self.alertView.widthAnchor),
         self.textLabel.bottomAnchor.constraint(
            equalTo: self.instructionView.topAnchor, constant: -8),
         
         self.instructionView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
         self.instructionView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
         
         self.imageView.topAnchor.constraint(
            greaterThanOrEqualToSystemSpacingBelow: self.instructionView.bottomAnchor,
            multiplier: 2),
         self.imageView.bottomAnchor.constraint(equalTo: self.buttonToolbar.topAnchor),
         self.imageView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
         self.imageView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
         
         self.buttonToolbar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
         self.buttonToolbar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
         self.buttonToolbar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
      ])
      
      self.view.layer.addSublayer(self.arrowLayer)
   }
   
   // MARK: - Actions
   @objc
   private func didTapLaterButton() {
      self.completionHandler?()
   }
   
   @objc
   private func didTapAllowButton() {
      TrackingManager.requestConsentToTrackIfNecessary(completionHandler: self.completionHandler)
   }
}

// MARK: - Alert View
/// Simulates alert view button
fileprivate class AlertView: UIView {
   var arrowAnchorPoint: CGPoint {
      let x: CGFloat = Environment.languageIsArabic ?
         self.allowButton.label.frame.maxX + 8 : self.allowButton.label.frame.minX - 8
      let point = CGPoint(x, self.allowButton.label.frame.midY)
      return self.allowButton.convert(point, to: self)
   }
   private lazy var allowButton = CellView(buttonText: "allowTracking".localized)
   private lazy var disallowButton = CellView(buttonText: "disallowTracking".localized)
   private lazy var separatorView: UIView = {
      let v = UIView()
      v.translatesAutoresizingMaskIntoConstraints = false
      v.backgroundColor = .separator
      v.heightAnchor.constraint(equalToConstant: .pixel).isActive = true
      return v
   }()
   private lazy var stackView: UIStackView = {
      let v = UIStackView(
         arrangedSubviews: [self.disallowButton, self.separatorView, self.allowButton]
      )
      v.translatesAutoresizingMaskIntoConstraints = false
      v.spacing = 0
      v.distribution = .equalSpacing
      v.alignment = .fill
      v.axis = .vertical
      
      NSLayoutConstraint.activate([
         self.allowButton.heightAnchor.constraint(equalToConstant: 44),
         self.disallowButton.heightAnchor.constraint(equalToConstant: 44)
      ])
      return v
   }()
   
   init() {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .lighterBackground
      
      self.addFillingSubview(self.stackView)
      
      self.widthAnchor.constraint(equalToConstant: 270).isActive = true
      
      self.layer.borderWidth = .pixel
      self.layer.borderColor = UIColor.separator.cgColor
      self.layer.masksToBounds = true
      self.layer.cornerRadius = 10
   }
}

// MARK: - Cell View
fileprivate class CellView: UIView {
   private var buttonText: String
   
   lazy var label: UILabel = {
      let l = UILabel()
      l.translatesAutoresizingMaskIntoConstraints = false
      l.text = self.buttonText
      return l
   }()
   
   override var intrinsicContentSize: CGSize { .init(super.intrinsicContentSize.width, 44) }
   
   init(buttonText: String) {
      self.buttonText = buttonText
      super.init(frame: .zero)
      
      self.translatesAutoresizingMaskIntoConstraints = false
      self.backgroundColor = .clear
      
      self.addSubview(self.label)
      let margins = self.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.label.topAnchor.constraint(equalTo: margins.topAnchor),
         self.label.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
         self.label.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
         self.label.widthAnchor.constraint(lessThanOrEqualTo: margins.widthAnchor, multiplier: 1)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}

// MARK: - Arrow
fileprivate class ArrowPath: UIBezierPath {
   init(start: CGPoint, end: CGPoint) {
      super.init()
      
      // Flip if Arabic
      let m: CGFloat = Environment.languageIsArabic ? -1 : 1
      
      // Arrow Body
      self.move(to: start)
      let cp1 = CGPoint(start.x, start.y + (end.y - start.y) / 2)
      let cp2 = CGPoint(start.x + m * abs(end.x - start.x) / 3, end.y)
      self.addCurve(to: end, controlPoint1: cp1, controlPoint2: cp2)
      
      // Arrow Head: Right Side
      let arrowHeadLengthRight: CGFloat = 12
      let rightArrowHeadAngle: CGFloat = .pi / 7
      let endOfRightSide = CGPoint(
         end.x - m * arrowHeadLengthRight * cos(rightArrowHeadAngle),
         end.y + arrowHeadLengthRight * sin(rightArrowHeadAngle)
      )
      self.move(to: endOfRightSide)
      let rcp = CGPoint(
         endOfRightSide.x + (end.x - endOfRightSide.x) / 2,
         end.y + (endOfRightSide.y - end.y) / 4
      )
//      self.addQuadCurve(to: end, controlPoint: rcp)
      self.addLine(to: end)
      
      // Arrow Head: Left Side
      //self.move(to: end)
      let arrowHeadLengthLeft: CGFloat = 14
      let leftArrowHeadAngle: CGFloat = .pi / 4
      let endOfLeftSide = CGPoint(
         end.x - m * arrowHeadLengthLeft * cos(leftArrowHeadAngle),
         end.y - arrowHeadLengthLeft * sin(leftArrowHeadAngle)
      )
      let lcp = CGPoint(
         endOfLeftSide.x + m * abs(end.x - endOfLeftSide.x) / 3,
         end.y - (end.y - endOfLeftSide.y) / 3
      )
      self.addQuadCurve(to: endOfLeftSide, controlPoint: lcp)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}

// MARK: - Button Toolbar
fileprivate class ButtonToolbar: UIView {
   private lazy var visualEffectView: UIVisualEffectView = {
      let view = UIVisualEffectView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.contentView.preservesSuperviewLayoutMargins = true
      view.effect = {
         if #available(iOS 13, *) {
            return UIBlurEffect(
               style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
            )
         } else {
            return UIBlurEffect(style: .prominent)
         }
      }()
      
      view.contentView.addFillingSubviewConstrainedByMargins(allowButton)
      return view
   }()
   
   lazy var allowButton: ProminentButton = {
      let b = ProminentButton()
      b.setTitle("next".localized, for: .normal)
      return b
   }()
   lazy var laterButton: ProminentButton = {
      let b = ProminentButton()
      b.setTitle("later".localized.localizedCapitalized, for: .normal)
      return b
   }()
   private lazy var buttonStack: UIStackView = {
      let s = UIStackView(arrangedSubviews: [allowButton])
      s.translatesAutoresizingMaskIntoConstraints = false
      s.axis = .horizontal
      s.spacing = 16
      s.alignment = .fill
      s.distribution = .fillEqually
      return s
   }()
   private lazy var topBorderLayer: CALayer = {
      let layer = CALayer()
      layer.backgroundColor = UIColor.subtleBorder.cgColor
      return layer
   }()
   
   init() {
      super.init(frame: .zero)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.setUp()
   }
   
   private func setUp() {
      self.translatesAutoresizingMaskIntoConstraints = false
      self.preservesSuperviewLayoutMargins = true
      self.backgroundColor = .blurBackground
      self.addFillingSubview(self.visualEffectView)
      self.layer.addSublayer(self.topBorderLayer)
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      self.topBorderLayer.frame = CGRect(0, 0, self.bounds.width, .pixel)
   }
   
   override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
      super.traitCollectionDidChange(previousTraitCollection)
      
      guard #available(iOS 13, *) else { return }
      guard self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
         else
      {
         return
      }
      self.visualEffectView.effect = UIBlurEffect(
         style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .prominent
      )
      self.topBorderLayer.backgroundColor = UIColor.subtleBorder.cgColor
   }
}
