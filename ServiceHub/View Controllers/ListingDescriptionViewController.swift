//
//  ListingDescriptionViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ListingDescriptionViewController: UIViewController, ListingDescriptionView {
   
   var onNextButtonTap: (() -> Void)?
   var onSupportButtonTap: (() -> Void)?
   
   private var listing: Listing
   
   private lazy var textView: UITextView = {
      let view = UITextView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.isEditable = false
      view.contentInset = .init(top: 20, left: 15, bottom: 20, right: 15)
      view.isSelectable = false
      view.showsVerticalScrollIndicator = false
      return view
   }()
   private lazy var buttonView: BottomButtonView = {
      let view = BottomButtonView(buttonTitle: "next".localized)
      view.button.addTarget(
         self,
         action: #selector(self.didTapNextButton),
         for: .touchUpInside
      )
      return view
   }()
   
   // MARK: - Init
   
   init(_ listing: Listing) {
      self.listing = listing
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      title = listing.title
      navigationItem.rightBarButtonItem = UIBarButtonItem(
         image: UIImage(named: "Support"),
         style: .plain,
         target: self,
         action: #selector(didTapSupportButton)
      )
      
      view.backgroundColor = .background
      
      view.addFillingSubviewConstrainedBySafeAreaOnSides(textView)
      view.addSubview(buttonView)
      NSLayoutConstraint.activate([
         buttonView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         buttonView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         buttonView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      textView.attributedText = getAttributedText()
      
      // FIXME: This is a workaround to a content offset issue. Find a better solution.
      textView.contentOffset.y = 0
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      let bottomInset = buttonView.frame.height - view.safeAreaInsets.bottom
      
      textView.textContainerInset.bottom = bottomInset
      textView.scrollIndicatorInsets.bottom = bottomInset
      
   }
   
   // MARK: - Actions
   
   @objc
   private func didTapNextButton() {
      onNextButtonTap?()
   }
   
   @objc
   private func didTapSupportButton() {
      onSupportButtonTap?()
   }
   
   // MARK: - Other
   
   private func getAttributedText() -> NSAttributedString {
      let text = NSMutableAttributedString()
      
      let imageText = getAttributedString(for: listing.image)
      text.append(imageText)
      
      let descriptionText = getAttributedString(for: listing.description)
      text.append(descriptionText)
      
      return text
   }
   
   private func getAttributedString(for image: UIImage?) -> NSAttributedString {
      
      guard let image = image else { return .init() }
      
      let textAttachment = NSTextAttachment()
      
      let insets = textView.adjustedContentInset
      let width = min(
         image.size.width,
         UIScreen.main.bounds.width - insets.left - insets.right - 10
      )
      let scaleFactor = width / image.size.width
      let scaledImage = image.scaled(by: scaleFactor)
      
      textAttachment.image = scaledImage
      
      let imageString = NSMutableAttributedString()
      imageString.append(NSAttributedString(attachment: textAttachment))
      
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.alignment = .center
      imageString.addAttribute(
          .paragraphStyle,
         value: paragraphStyle,
         range: NSRange(location: 0, length: imageString.length)
      )
      imageString.append(NSAttributedString(string: "\n\n"))
      
      return imageString
   }
   
   private func getAttributedString(for description: String) -> NSAttributedString {
      NSAttributedString(
         string: description,
         attributes: [
            .font: UIFont.mediumFont(ofSize: 18),
            .foregroundColor: UIColor.prominentText
         ]
      )
   }
}
