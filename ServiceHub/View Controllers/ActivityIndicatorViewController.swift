//
//  ActivityIndicatorViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/20/19.
//  Copyright © 2019 Venture X. All rights reserved.
//
import UIKit

class ActivityIndicatorViewController: UIViewController {
   // MARK: - Properties
   lazy private var overlayView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = UIColor(white: 0, alpha: 0.2)
      view.alpha = 0
      return view
   }()
   
   lazy private var alertView: UIVisualEffectView = {
      let view = UIVisualEffectView(effect: .defaultBlur)
      view.translatesAutoresizingMaskIntoConstraints = false
      view.layer.cornerRadius = 20
      view.layer.masksToBounds = true
      view.transform = .init(scaleX: 1.2, y: 1.2)
      view.alpha = 0
      
      indicatorView.tintColor = .foreground
      view.contentView.addSubview(indicatorView)
      indicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
      indicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
      view.widthAnchor.constraint(equalTo: indicatorView.widthAnchor, constant: 30).isActive = true
      view.heightAnchor.constraint(equalTo: view.widthAnchor).isActive = true
      
      return view
   }()
   
   private var indicatorView: ActivityIndicator = ActivityIndicator(
      numberOfCircles: 3, diameter: 12, spacing: 15)
   var windowTint: UIColor?
   
   // MARK: - Initializers
   override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
      super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
      modalPresentationStyle = .overFullScreen
      modalTransitionStyle = .crossDissolve
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      modalPresentationStyle = .overFullScreen
      modalTransitionStyle = .crossDissolve
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      view.addSubview(overlayView)
      view.addSubview(alertView)
      
      overlayView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
      overlayView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      overlayView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      overlayView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
      
      alertView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
      alertView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
      
      view.setNeedsLayout()
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      windowTint = (UIApplication.shared.delegate as? AppDelegate)?.window?.tintColor
      
      DispatchQueue.main.async {
         
         UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear, animations: {
            self.overlayView.alpha = 1
            (UIApplication.shared.delegate as? AppDelegate)?.window?.tintColor = .unhighlightedBarItem
         }, completion: nil)
         UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.alertView.alpha = 1
            self.alertView.transform = .identity
         }, completion: nil)
      }
   }
   
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      DispatchQueue.main.async {
         UIView.animate(withDuration: 0.15, delay: 0.1, options: .curveLinear, animations: {
            self.overlayView.alpha = 0
            (UIApplication.shared.delegate as? AppDelegate)?.window?.tintColor = self.windowTint
         }, completion: nil)
         
         UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.alertView.alpha = 0
            self.alertView.transform = .init(scaleX: 1.2, y: 1.2)
         }, completion: nil)
      }
   }
}
