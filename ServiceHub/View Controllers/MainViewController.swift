//
//  MainViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class MainViewController: UITabBarController, MainView {
   
   // MARK: - Properties
   
   var onViewDidLoad: ((UINavigationController) -> Void)?
   var onCatalogSelect: ((UINavigationController) -> Void)?
   var onOrdersSelect: ((UINavigationController) -> Void)?
   var onSupportSelect: ((UINavigationController) -> Void)?
   var onOrdersTabRemoved: (() -> Void)?
   
   private var shouldShowOrdersTab: Bool {
      User.current != nil
   }
   private var isShowingOrdersTab: Bool {
      self.viewControllers?.count == 3
   }
   /// Index for orders tab, if it should be shown. Note that his does not reflect the actual index of the orders tab.
   private var indexForOrdersTab: Int { 1 }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.delegate = self
      
      let catalogNavigationController = self.makeCatalogNavigationController()
      let supportNavigationController = self.makeSupportNavigationController()
      
      var viewControllers: [UINavigationController] = [
         catalogNavigationController,
         supportNavigationController
      ]
      
      if self.shouldShowOrdersTab {
         let ordersNavigationController = self.makeOrdersNavigationController()
         viewControllers.insert(ordersNavigationController, at: self.indexForOrdersTab)
      }
      
      self.setViewControllers(viewControllers, animated: false)
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleUserDidChangeNotification),
         name: User.userDidChangeNotification,
         object: nil
      )
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      if let controller = self.viewControllers?.first as? UINavigationController {
         self.onViewDidLoad?(controller)
      }
   }
   
   // MARK: - Other
   
   func showOrders() {
      guard self.isShowingOrdersTab else { return }
      guard
         let controller = self.viewControllers?[self.indexForOrdersTab] as? UINavigationController
      else { return }
      
      self.selectedIndex = self.indexForOrdersTab
      self.onOrdersSelect?(controller)
   }
   
   private func makeCatalogNavigationController() -> UINavigationController {
      let navigationController = UINavigationController()
      navigationController.tabBarItem = UITabBarItem(
         title: "home".localized,
         image: UIImage(named: "House"),
         selectedImage: nil
      )
      return navigationController
   }
   
   private func makeOrdersNavigationController() -> UINavigationController {
      let navigationController = UINavigationController()
      navigationController.tabBarItem = UITabBarItem(
         title: "myOrders".localized,
         image: UIImage(named: "Orders"),
         selectedImage: nil
      )
      return navigationController
   }
   
   private func makeSupportNavigationController() -> UINavigationController {
      let navigationController = UINavigationController()
      navigationController.tabBarItem = UITabBarItem(
         title: "support".localized,
         image: UIImage(named: "Support"),
         selectedImage: nil
      )
      return navigationController
   }
   
   @objc
   private func handleUserDidChangeNotification() {
      guard var viewControllers = self.viewControllers else { return }
      
      switch (self.shouldShowOrdersTab, self.isShowingOrdersTab) {
      case (true, false):
         let ordersNavigationController = self.makeOrdersNavigationController()
         viewControllers.insert(ordersNavigationController, at: self.indexForOrdersTab)
         DispatchQueue.main.async {
            self.setViewControllers(viewControllers, animated: true)
         }
      
      case (false, true):
         viewControllers.remove(at: self.indexForOrdersTab)
         DispatchQueue.main.async {
            self.setViewControllers(viewControllers, animated: true)
            self.onOrdersTabRemoved?()
            self.onOrdersTabRemoved = nil
         }
         
      default: break
      }
   }
}

extension MainViewController: UITabBarControllerDelegate {
   
   func tabBarController(
      _ tabBarController: UITabBarController,
      didSelect viewController: UIViewController
   ) {
      guard
         let controller = self.viewControllers?[self.selectedIndex] as? UINavigationController
      else { return }
      
      switch self.selectedIndex {
      case 0:
         self.onCatalogSelect?(controller)
         
      case 1:
         if self.isShowingOrdersTab {
            self.onOrdersSelect?(controller)
         } else {
            fallthrough
         }
         
      default:
         self.onSupportSelect?(controller)
      }
   }
}
