//
//  CardFormViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardFormViewController: UIViewController, AddCardView {
   
   // MARK: - Properties
   
   var onCancelButtonTap: (() -> Void)?
   var onCardAdded: ((_ card: Card, _ save: Bool) -> Void)?
   
   private let inputInstructionSpacing: CGFloat = 5
   @available(*, deprecated)
   weak var delegate: CardFormViewControllerDelegate?
   let purpose: CardFormView.Purpose
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   
   // MARK: UI
   
   lazy var scrollView: UIScrollView = {
      let view = UIScrollView()
      view.translatesAutoresizingMaskIntoConstraints = false
      return view
   }()
   lazy var cardView: CardFormView = .init(purpose: self.purpose)
   lazy var addCardButton: ProminentButton = {
      let button = ProminentButton()
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setTitle((self.purpose == .add ? "addCard" : "done").localized, for: .normal)
      button.titleLabel?.font = button.titleLabel?.font.withSize(18)
      return button
   }()
   
   // MARK: - Initialization
   
   init(purpose: CardFormView.Purpose) {
      self.purpose = purpose
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      self.purpose = .add
      super.init(coder: coder)
   }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      view.backgroundColor = .background
      
      if self == navigationController?.children.first {
         navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(cancelButtonWasTapped))
      }
      
      setUpViews()
      setDelegatesAndActions()
      refreshAddCardButton()
      
      NotificationCenter.default.addObserver(
          self,
          selector: #selector(handleKeyboardWillChange(notification:)),
          name: UIResponder.keyboardWillChangeFrameNotification,
          object: nil)
      NotificationCenter.default.addObserver(
          self,
          selector: #selector(handleKeyboardWillHideNotification),
          name: UIResponder.keyboardWillHideNotification,
          object: nil)
   }
   
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      resignFirstResponder()
   }
   
   // MARK: - Initial Configuration
   private func setUpViews() {
      // Card View
      view.addSubview(scrollView)
      scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
      scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
      
      let contentView = UIView()
      contentView.translatesAutoresizingMaskIntoConstraints = false
      contentView.backgroundColor = .background
      scrollView.addSubview(contentView)
      contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
      contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
      contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
      contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
      contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
      
      // Enclosing View
      let enclosingView = UIView()
      enclosingView.translatesAutoresizingMaskIntoConstraints = false
      enclosingView.backgroundColor = .lighterBackground
      enclosingView.layer.masksToBounds = false
      enclosingView.layer.cornerRadius = .cornerRadius
      contentView.addSubview(enclosingView)
      enclosingView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
      enclosingView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
                                             constant: 15).isActive = true
      enclosingView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
                                              constant: -15).isActive = true
      enclosingView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                            constant: -20).isActive = true
      
      // Card View
      enclosingView.addSubview(cardView)
      cardView.topAnchor.constraint(equalTo: enclosingView.topAnchor, constant: 20).isActive = true
      cardView.leadingAnchor.constraint(equalTo: enclosingView.leadingAnchor,
                                         constant: 15).isActive = true
      cardView.trailingAnchor.constraint(equalTo: enclosingView.trailingAnchor,
                                          constant: -15).isActive = true
      
      
      // Button
      enclosingView.addSubview(addCardButton)
      addCardButton.topAnchor.constraint(equalTo: cardView.bottomAnchor, constant: 20).isActive = true
      addCardButton.bottomAnchor.constraint(equalTo: enclosingView.bottomAnchor,
                                            constant: -15).isActive = true
      addCardButton.widthAnchor.constraint(equalTo: enclosingView.widthAnchor,
                                           multiplier: 0.7).isActive = true
      addCardButton.centerXAnchor.constraint(equalTo: enclosingView.centerXAnchor).isActive = true
   }
   
   private func setDelegatesAndActions() {
      cardView.delegate = self
      addCardButton.addTarget(self, action: #selector(addCardButtonWasTapped), for: .touchUpInside)
   }
   
   // MARK: - Actions
   @objc private func cancelButtonWasTapped() {
      self.view.firstResponder?.resignFirstResponder()
      self.onCancelButtonTap?()
   }
   
   @objc private func addCardButtonWasTapped() {
      guard cardView.validate() else { return }
      Card.manager.delegate = self
      
      DispatchQueue.main.async {
         self.firstResponder?.resignFirstResponder()
         self.presentActivityIndicatorController()
      }
      
      switch self.purpose {
      case .add:
         Card.manager.addCard(
            cardNumber: cardView.standardizedCardNumber,
            expiryMonth: cardView.expiryMonth,
            expiryYear: cardView.expiryYear,
            cvv: cardView.cvv,
            name: cardView.name
         )
         
      case .pay:
         Card.manager.tokenizeCard(
            cardNumber: cardView.standardizedCardNumber,
            expiryMonth: cardView.expiryMonth,
            expiryYear: cardView.expiryYear,
            cvv: cardView.cvv
         ) { response in
            
            let card = Card(
               id: 0,
               expiryMonth: response.expiryMonth,
               expiryYear: response.expiryYear,
               last4: response.last4,
               bin: response.bin,
               token: response.token
            )
            DispatchQueue.main.async {
               self.presentedViewController?.dismiss(animated: true)
            }
            self.onCardAdded?(card, self.cardView.saveCard)
         }
      }
   }
   
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   // MARK: - Other
   private func refreshAddCardButton() {
      addCardButton.isEnabled = !cardView.hasEmptyRequiredField
   }
   
   @objc private func handleKeyboardWillChange(notification: Notification) {
       if let userInfo = notification.userInfo,
           let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]
               as? NSValue)?.cgRectValue.height {
           scrollView.contentInset.bottom = keyboardHeight - view.safeAreaInsets.bottom
       }
   }
   
   @objc private func handleKeyboardWillHideNotification() {
       scrollView.contentInset.bottom = 0
   }
}

// MARK: - CardViewDelegate

extension CardFormViewController: CardViewDelegate {
   func cardDetailsDidChange() {
      refreshAddCardButton()
   }
}

// MARK: - CardManagerDelegate

extension CardFormViewController: CardManagerDelegate {
   func cardManager(
      didAddCard card: Card?,
      completedWithError error: Error?,
      errorResponse: Card.Manager.ErrorResponse?
   ) {
      DispatchQueue.main.async {
         if let error = error {
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               
               self.presentedViewController?.dismiss(animated: true, completion: {
                   self.present(signInAlert, animated: true, completion: nil)
               })
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: "cardDeclined".localized,
               message: errorResponse?.description ?? error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         guard let card = card else {
            assertionFailure("[VXCardVC] Uncaught error.")
            return
         }
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onCardAdded?(card, self.cardView.saveCard)
         })
      }
   }
   
   func cardManager(didDeleteCard card: Card, completedWithError error: Error?) {}
}

// MARK: - Protocol

@available(*, deprecated)
protocol CardFormViewControllerDelegate: AnyObject {
   func cardViewController(_ controller: CardFormViewController, didAddCard card: Card)
   func cardViewController(
      _ controller: CardFormViewController,
      didTokenizeCard card: Card,
      saveCard: Bool
   )
   func cardViewControllerDidCancel()
}
