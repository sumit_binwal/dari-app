//
//  DistrictSelectorViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/4/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DistrictSelectorViewController: UIViewController, DistrictSelectorView {
   
   // MARK: - Properties
   
   var showCancelButton: Bool = false
   var onSelectDistrict: ((District?) -> Void)?
   
   private weak var signInAction: UIAlertAction?
   private lazy var tableView: UITableView = {
      let tableView = UITableView(frame: .zero, style: .grouped)
      tableView.translatesAutoresizingMaskIntoConstraints = false
      tableView.register(
         UINib(nibName: "AreaSectionHeaderView", bundle: nil),
         forHeaderFooterViewReuseIdentifier: "AreaSectionHeaderView"
      )
      tableView.register(
         DistrictTableViewCell.self,
         forCellReuseIdentifier: "DistrictTableViewCell"
      )
      tableView.rowHeight = UITableView.automaticDimension
      tableView.estimatedRowHeight = 200
      tableView.sectionHeaderHeight = UITableView.automaticDimension
      tableView.estimatedSectionHeaderHeight = 100
      tableView.sectionFooterHeight = 0
      tableView.refreshControl = self.refreshControl
      tableView.backgroundColor = .clear
      
      tableView.dataSource = self
      tableView.delegate = self
      
      return tableView
   }()
   private lazy var refreshControl: UIRefreshControl = {
      let control = UIRefreshControl()
      control.tintColor = .calypso
      control.sizeToFit()
      control.addTarget(self, action: #selector(refreshControlWasPulled), for: .valueChanged)
      return control
   }()
   private var expandedSection: Int?
   let citySection: Int = 0
   var cities: [City] = []
   var districts: [District] = []
   var locations: [[Area]] = []
   
   private var selectedCityIndex: Int? {
      didSet {
         let selectedCityIndex = self.selectedCityIndex ?? -1
         let header = tableView.headerView(forSection: 0) as? AreaSectionHeaderView
         header?.titleLabel.text = selectedCityIndex < 0 ?
            "selectCity".localized : cities[selectedCityIndex].name
      }
   }
   private var selectedCity: City? {
      guard let index = selectedCityIndex else { return nil }
      return cities[index]
   }
   @available(*, deprecated)
   private var selectedDistrictIndex: Int?
   private var selectedDistrict: District?
   private var searchController: UISearchController?
   private var isFirstLaunch: Bool = District.current == nil
   @available(*, deprecated)
   weak var delegate: DistrictSelectorViewControllerDelegate?
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      view.backgroundColor = .background
      if self.showCancelButton {
         navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(cancelButtonWasTapped))
      }
      
      setUpViews()
      setUpSearchBar()
      
      self.edgesForExtendedLayout = .all
      if let date = AreaManager.dateOfLastSuccessfulAreaFetch, Date() - date < 3600.0 * 24.0 {
         self.cities = City.all
         self.locations = [self.cities]
      } else {
         self.fetchAreas()
      }
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      navigationController?.setNavigationBarHidden(false, animated: animated)
      self.smoothlyDeselectRows(tableView: tableView)
   }
   
   // MARK: - Initial Configuration
   private func fetchAreas(isUserInitiated: Bool = false) {
      AreaManager.default.delegate = self
      
      if !isUserInitiated {
         self.refreshControl.startRefreshing(animateScrollViewContentOffset: false)
      }
      
      AreaManager.default.getAreas()
   }
   
   private func setUpViews() {
      let bouncePreventerView: UIView = {
         let view = UIView()
         view.backgroundColor = .clear
         view.translatesAutoresizingMaskIntoConstraints = false
         view.heightAnchor.constraint(equalToConstant: 0).isActive = true
         return view
      }()
      
      self.view.addSubviews([bouncePreventerView, self.tableView])
      NSLayoutConstraint.activate([
         bouncePreventerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
         bouncePreventerView.leadingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
         bouncePreventerView.trailingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
         
         self.tableView.topAnchor.constraint(equalTo: bouncePreventerView.topAnchor),
         self.tableView.leadingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
         self.tableView.trailingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
         self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
      ])
   }
   
   private func setUpSearchBar() {
      let searchResultsVC = SearchResultsViewController()
      searchResultsVC.delegate = self
      
      let searchController: UISearchController = {
         let searchController = UISearchController(searchResultsController: searchResultsVC)
         searchController.searchBar.searchBarStyle = .minimal
         searchController.searchResultsUpdater = searchResultsVC
         return searchController
      }()
      self.searchController = searchController
      searchController.searchBar.delegate = self
      
      if isFirstLaunch {
         searchController.hidesNavigationBarDuringPresentation = false
         navigationItem.titleView = searchController.searchBar
      } else {
         self.navigationItem.searchController = searchController
      }
      
      definesPresentationContext = true
   }
   
   // MARK: - Actions
   @objc private func cancelButtonWasTapped() {
      self.onSelectDistrict?(nil)
   }
   
   @objc private func refreshControlWasPulled() {
      DispatchQueue.main.async { self.tableView.endScroll() }
      self.fetchAreas(isUserInitiated: true)
   }
   
   // MARK: - Other
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   private func expandCitySection(
      _ expand: Bool,
      includeSectionHeader: Bool = true,
      animated: Bool = true)
   {
      guard (expand && locations[citySection].isEmpty) ||
         (!expand && !locations[citySection].isEmpty) else { return }
      
      let indexPaths = (0 ..< cities.count).map { IndexPath(row: $0, section: citySection) }
      
      guard let headerView = tableView.headerView(forSection: citySection) as? AreaSectionHeaderView else {
         fatalError("[AreaSelectionVC] Failed to access section header view.")
      }
      
      DispatchQueue.main.async {
         if includeSectionHeader { headerView.setIsExpanded(expand, animated: animated) }
         
         if expand {
            self.locations[self.citySection] = self.cities
            self.tableView.insertRows(at: indexPaths, with: animated ? .fade : .none)
            self.searchController?.searchBar.placeholder = "search".localized
         } else {
            self.locations[0].removeAll()
            self.tableView.deleteRows(at: indexPaths, with: animated ? .fade : .none)
            self.searchController?.searchBar.placeholder =
               "\("searchIn".localized) \(self.selectedCity?.name ?? "")"
         }
      }
      
   }
   
   private func expandDistrictSection(
      _ expand: Bool,
      includeSectionHeader: Bool = true,
      animated: Bool = true)
   {
      let districtSection: Int = 1
      let sectionExists: Bool = locations.count > 1
      guard (expand && (sectionExists && self.locations[districtSection].isEmpty || !sectionExists)) ||
         (!expand && sectionExists && !self.locations[districtSection].isEmpty) else { return }
      
      DispatchQueue.main.async {
         if includeSectionHeader && sectionExists,
            let headerView = self.tableView.headerView(forSection: districtSection) as? AreaSectionHeaderView {
            headerView.setIsExpanded(expand, animated: animated)
         }
         
         if expand {
            if sectionExists {
               let indexPaths = (0 ..< self.districts.count).map {
                  IndexPath(row: $0, section: districtSection)
               }
               self.locations[1] = self.districts
               self.tableView.insertRows(at: indexPaths, with: animated ? .fade : .none)
            } else {
               self.locations.append(self.districts)
               self.tableView.insertSections([districtSection], with: .fade)
            }
         } else {
            let indexPaths = (0 ..< self.locations[1].count).map {
               IndexPath(row: $0, section: districtSection)
            }
            self.locations[1].removeAll()
            self.tableView.deleteRows(at: indexPaths, with: animated ? .fade : .none)
         }
      }
   }
   
   private func updateUser(district: District) {
      DispatchQueue.main.async {
         self.presentActivityIndicatorController()
      }
      
      User.manager.delegate = self
      User.manager.update(userAttributes: [
         User.CodingKeys.profile.rawValue: [
            User.Profile.CodingKeys.districtID.rawValue: district.id
         ]
      ])
   }
   
   private func setSelectedDistrict(_ district: District) {
      self.selectedDistrict = district
      District.current = district
      
      if User.current != nil {
         self.updateUser(district: district)
         
      } else {
         User.manager.postUserChange()
         TrackingManager.updateUser()
         self.onSelectDistrict?(district)
      }
   }
}

// MARK: - Table view data source
extension DistrictSelectorViewController: UITableViewDelegate, UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int { return self.locations.count }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return locations[section].count
   }
   
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell
   {
      let location = locations[indexPath.section][indexPath.row]
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "DistrictTableViewCell", for: indexPath)
         as? DistrictTableViewCell else {
            fatalError("[AreaSelectionVC] Failed to dequeue DistrictTableViewCell.")
      }
      cell.textLabel?.text = location.name
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      switch indexPath.section {
      case 0:
         if selectedCityIndex != indexPath.row { self.selectedDistrict = nil }
         selectedCityIndex = indexPath.row
         districts = District.getDistricts(inCity: cities[indexPath.row])
         expandCitySection(false)
         expandDistrictSection(true)
         
      case 1:
         self.setSelectedDistrict(self.districts[indexPath.row])
         
         
      default:
         assertionFailure("[AreaSelectionVC] Unrecognized section selected.")
      }
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int)
      -> UIView?
   {
      guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AreaSectionHeaderView")
         as? AreaSectionHeaderView else {
            fatalError("[AreaSelectionVC] Failed to dequeue AreaSectionHeaderView.")
      }
      switch section {
      case 0:
         header.imageView.image = UIImage(named: "City")
         let selectedCityIndex = self.selectedCityIndex ?? -1
         header.titleLabel.text = selectedCityIndex < 0 ? "selectCity".localized : cities[selectedCityIndex].name
         
      case 1:
         header.imageView.image = UIImage(named: "City")
         header.titleLabel.text = "selectDistrict".localized
         
      default: return nil
      }
      
      header.sectionNumber = section
      header.delegate = self
      header.setIsExpanded(!locations[section].isEmpty, animated: false)
      
      return header
   }
}

extension DistrictSelectorViewController: AreaSectionHeaderViewDelegate {
   func sectionHeaderExpanded(_ expanded: Bool, section: Int) {
      switch section {
      case 0:
         expandCitySection(expanded, includeSectionHeader: false)
         if expanded {
            expandDistrictSection(false)
         }
         
      case 1:
         expandDistrictSection(expanded, includeSectionHeader: false)
         if expanded {
            expandCitySection(false)
         }
         
      default:
         assertionFailure("[AreaSelectionVC] Invalid section \(section).")
      }
   }
}

// MARK: - Search
extension DistrictSelectorViewController: SearchResultsViewControllerDelegate {
   func searchResultsViewController(_ controller: UIViewController, didSelectItem item: Any?) {
      guard let district = item as? District else {
         assertionFailure("[DistrictSelectorVC] Unrecognized selected search item.")
         return
      }
      self.setSelectedDistrict(district)
   }
}

extension DistrictSelectorViewController: UISearchBarDelegate {
   func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      guard let searchResultsVC = self.searchController?.searchResultsController
         as? SearchResultsViewController else
      {
         fatalError("[DistrictSelectorVC] Failed to get SearchResultsViewController.")
      }
      
      if self.locations[citySection].isEmpty, let selectedCity = self.selectedCity {
         searchResultsVC.items = District.getDistricts(inCity: selectedCity)
      } else {
         searchResultsVC.items = City.all.flatMap { District.getDistricts(inCity: $0) }
      }
   }
}

// MARK: - AreaManagerDelegate
extension DistrictSelectorViewController: AreaManagerDelegate {
   func areaManagerDidReceiveAreas(completedWithError error: Error?) {
      DispatchQueue.main.async {
         self.refreshControl.stopRefreshing()
         
         // Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
         }
         
         // Success
         self.tableView.beginUpdates()
         
         self.cities.removeAll()
         let oldSections = IndexSet(integersIn: 0..<self.locations.count)
         self.locations.removeAll()
         self.tableView.deleteSections(oldSections)
         
         self.cities = City.all
         self.locations = [self.cities]
         self.expandedSection = nil
         self.selectedCityIndex = nil
         self.tableView.insertSections(IndexSet(integersIn: 0..<self.locations.count))
         
         self.tableView.endUpdates()
      }
   }
}

// MARK: - UserManagerDelegate

// TODO: Move authentication to the coordinator.
extension  DistrictSelectorViewController: UserManagerDelegate {
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               self.presentedViewController?.dismiss(animated: true, completion: {
                   self.present(signInAlert, animated: true, completion: nil)
               })
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            guard let district = self.selectedDistrict else {
               assertionFailure("[DistrictSelectorVC] Selected district index is nil.")
               return
            }
            self.onSelectDistrict?(district)
         })
      }
   }
   
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   {
      // Errors
      if let error = error {
         let alert = UIAlertController(
            title: nil,
            message: errorMessages?.nonFieldErrors?.first ?? error.localizedDescription,
            preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
         
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.present(alert, animated: true, completion: nil)
         })
         return
      }
      
      // Success
      guard let district = self.selectedDistrict else {
         assertionFailure("[DistrictSelectorVC] Selected district index is null.")
         return
      }
      self.updateUser(district: district)
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}

// MARK: - Protocol

@available(*, deprecated)
protocol DistrictSelectorViewControllerDelegate: AnyObject {
   func districtSelector(
      _ selector: DistrictSelectorViewController,
      didSelectDistrict district: District
   )
   
   func districtSelectorDidCancel()
}
