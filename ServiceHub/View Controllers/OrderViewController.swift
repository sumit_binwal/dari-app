//
//  OrderViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/17/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController, OrderDetailView {
   
   // MARK: - Properties
   
   var onOrderCanceled: ((_ orderID: Int) -> Void)?
   
   @available(*, deprecated)
   weak var delegate: OrderViewControllerDelegate?
   let order: Order
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   
   // MARK: - UI
   private lazy var orderView: OrderView = {
      let view = OrderView(self.order)
      view.orderViewDelegate = self
      return view
   }()
   
   // MARK: - Init
   init(_ order: Order) {
      self.order = order
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.backgroundColor = .background
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.orderView)
   }
   
   // MARK: - Actions
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   // MARK: - Other
   private func confirmOrderCancellation(_ order: Order) {
      let alert = UIAlertController(
         title: nil,
         message: "confirmOrderCancellation".localized,
         preferredStyle: .alert
      )
      alert.addAction(
         .init(title: "yes".localized, style: .destructive, handler: { _ in
            self.presentActivityIndicatorController()
            Order.manager.historyDelegate = self
            Order.manager.cancelOrder(withID: order.id)
         })
      )
      alert.addAction(.init(title: "no".localized, style: .cancel, handler: nil))
      
      DispatchQueue.main.async {
         self.present(alert, animated: true, completion: nil)
      }
   }
}

// MARK: - OrderViewDelegate
extension OrderViewController: OrderViewDelegate {
   func orderView(didCancelOrder order: Order) {
      self.confirmOrderCancellation(order)
   }
}

// MARK: - OrderManagerHistoryDelegate
extension OrderViewController: OrderManagerHistoryDelegate {
   func orderManager(didCancelOrderWithID id: Int?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            //Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               
               self.present(signInAlert, animated: true, completion: nil)
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(.init(title: "ok".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
         }
         
         // Success
         guard let id = id else {
            assertionFailure("[OrdersVC] Uncaught error.")
            return
         }
         self.dismissActivityIndicatorOrAlert {
            self.onOrderCanceled?(id)
         }
      }
   }
   
   func orderManager(didGetOrders orders: [Order.Summary]?, completedWithError error: Error?) {}
   func orderManager(didGetOrder order: Order?, completedWithError error: Error?) {}
   func orderManager(didPlaceOrderWithSummary summary: Order.Summary) {}
}


// MARK: - Protocol

@available(*, deprecated)
protocol OrderViewControllerDelegate: AnyObject {
   func orderViewController(_ controller: UIViewController, didCancelOrderWithID id: Int)
}
