//
//  ServiceDetailsViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/30/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import FBSDKCoreKit

@available(*, deprecated)
class ServiceDetailsViewController: UIViewController {
    // MARK: - Properties
    var service: Service!
   var textView: UITextView = {
      let textView = UITextView()
      textView.translatesAutoresizingMaskIntoConstraints = false

      textView.isScrollEnabled = false
      textView.textContainer.lineFragmentPadding = 0
      textView.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
      textView.isEditable = false
      textView.isSelectable = false
      textView.showsVerticalScrollIndicator = false
      
      return textView
   }()
    private var imageSize: CGSize?
    var imageFrame: CGRect {
        guard let size = imageSize else { return .zero }
        let insets = textView.textContainerInset
        let x: CGFloat = (view.bounds.width - size.width) / 2
        let y: CGFloat = insets.top - textView.contentOffset.y
        let rawRect = CGRect(x, y + textView.frame.origin.y, size.width, size.height)
        return rawRect
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
        
      //EventLogger.log(.didViewService, parameters: [.service: service.title, .serviceID: service.id])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: animated)
    }
    
    // MARK: - Initial Configuration
    private func load() {
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        //title = service.title
        navigationItem.backBarButtonItem?.title = " "
        view.backgroundColor = .background
        
        setUpTextView()
        setUpNextButton()
    }
    
    private func setUpTextView() {
      view.addSubview(textView)
      textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
      textView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
      textView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
      textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        /*let (text, size) = service.attributedDescription(forTextView: textView)
        textView.attributedText = text
        imageSize = size*/
        textView.contentOffset.y = 0
    }
    
    private func setUpNextButton(animated: Bool = true) {
        let nextBarButtonItem = UIBarButtonItem(
            title: "continue".localized,
            style: .plain,
            target: self,
            action: #selector(nextButtonTapped))
        setToolbarItems(
            [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
             nextBarButtonItem,
             UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)],
            animated: animated)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ShowOrderForm":
         break
            /*guard let vc = segue.destination as? DeprecatedOrderFormViewController else {
                fatalError("[ServiceDetailsVC] Segue destination is not ServiceOrderFormViewController.")
            }
            vc.orderData = Order.Data(primaryService: service)*/
            
        default: fatalError("[ServiceDetailsVC] Unrecognized segue identifier \(segue.identifier ?? "").")
        }
    }
    
    // MARK: - Actions
    @objc private func nextButtonTapped() {
        performSegue(withIdentifier: "ShowOrderForm", sender: nil)
    }
    
    // MARK: - Other
    func maskImage(_ mask: Bool) {
        if mask {
            let maskingView = UIView(frame: imageFrame)
            maskingView.backgroundColor = textView.backgroundColor
            maskingView.tag = 500
            view.addSubview(maskingView)
        } else {
            view.viewWithTag(500)?.removeFromSuperview()
        }
    }
}
