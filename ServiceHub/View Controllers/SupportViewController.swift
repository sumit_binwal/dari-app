//
//  SupportViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: UIViewController, SupportModuleView {
   
   let email = "support@dariapp.com"
   let phoneNumber = "920033501"
   
   var showCancelButton: Bool = false
   var onCancelButtonTap: (() -> Void)?
   var onChatButtonTap: (() -> Void)?
   
   // MARK: - UI
   private lazy var supportView: SupportView = {
      let view = SupportView()
      
      view.chatButton.addTarget(
         self,
         action: #selector(self.chatButtonWasTapped),
         for: .touchUpInside
      )
      view.emailButton.addTarget(
         self,
         action: #selector(self.emailButtonWasTapped),
         for: .touchUpInside
      )
      view.callButton.addTarget(
         self,
         action: #selector(self.callButtonWasTapped),
         for: .touchUpInside
      )
      
      return view
   }()
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      if self.showCancelButton {
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(self.cancelButtonWasTapped)
         )
      }
      
      self.view.backgroundColor = .background
      
      self.view.addSubview(self.supportView)
      let marginsGuide = self.view.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.supportView.topAnchor.constraint(equalTo: marginsGuide.topAnchor),
         self.supportView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.supportView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         self.supportView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor, constant: -20)
      ])
   }
   
   // MARK: - Actions
   @objc
   private func cancelButtonWasTapped() {
      self.onCancelButtonTap?()
   }
   
   @objc
   private func chatButtonWasTapped() {
      self.onChatButtonTap?()
   }
   
   @objc
   private func emailButtonWasTapped() {
      guard MFMailComposeViewController.canSendMail() else {
         let alert = UIAlertController(
            title: "emailComposerLaunchFailure".localized,
            message: "\("youCanReachUsAt".localized) \(email)",
            preferredStyle: .alert)
         alert.addAction(UIAlertAction(
            title: "copyToClipboard".localized, style: .default, handler: { _ in
               UIPasteboard.general.string = self.email
         }))
         alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
         self.present(alert, animated: true, completion: nil)
         return
      }
      
      let composer = MFMailComposeViewController()
      composer.mailComposeDelegate = self
      composer.setToRecipients([email])
      composer.setSubject("helpSubject".localized)
      composer.view.tintColor = .tint
      present(composer, animated: true, completion: nil)
   }
   
   @objc
   private func callButtonWasTapped() {
      guard let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) else {
         let alert = UIAlertController(
            title: "callInitiationFailure".localized,
            message: "\("youCanReachUsAt".localized) \(phoneNumber)",
            preferredStyle: .alert)
         alert.addAction(UIAlertAction(
            title: "copyToClipboard".localized, style: .default, handler: { _ in
               UIPasteboard.general.string = self.phoneNumber
         }))
         alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
         self.present(alert, animated: true, completion: nil)
         return
      }
      UIApplication.shared.open(url)
   }
}

// MARK: - MFMailComposeViewControllerDelegate
extension SupportViewController: MFMailComposeViewControllerDelegate {
   func mailComposeController(
      _ controller: MFMailComposeViewController,
      didFinishWith result: MFMailComposeResult,
      error: Error?) {
      if error != nil {
         controller.dismiss(animated: true, completion: nil)
      }
      
      let completion: (() -> Void)? = {
         switch result {
         case .sent:
            return { [weak self] in
               let alert = UIAlertController(
                  title: "thankYouForYourEmail".localized,
                  message: "getBackToYouSoon".localized,
                  preferredStyle: .alert
               )
               self?.present(alert, animated: true, completion: {
                  DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                     alert.dismiss(animated: true, completion: nil)
                  })
               })
            }
         case .cancelled, .saved, .failed:
            return nil
         @unknown default:
            return nil
         }
      }()
      
      controller.dismiss(animated: true, completion: completion)
   }
}
