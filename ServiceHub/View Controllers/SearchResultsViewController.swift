//
//  SearchViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/15/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SearchResultsViewController: UITableViewController {
   // MARK: - Properties
   weak var delegate: SearchResultsViewControllerDelegate?
   var items: [Searchable] = []
   var results: [Searchable] = []
   
   // MARK: - Init
   init() {
      super.init(style: .plain)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.tableView.tableFooterView = UIView()
      self.tableView.rowHeight = UITableView.automaticDimension
      self.tableView.estimatedRowHeight = 44
      self.tableView.register(
         SearchResultTableViewCell.self,
         forCellReuseIdentifier: "SearchResultTableViewCell"
      )
   }
   
   // MARK: Table View Data Source
   override func numberOfSections(in tableView: UITableView) -> Int { 1 }
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.results.count
   }
   
   override func tableView(
      _ tableView: UITableView,
      cellForRowAt indexPath: IndexPath
   ) -> UITableViewCell {
      
      guard let cell = tableView.dequeueReusableCell(
         withIdentifier: "SearchResultTableViewCell", for: indexPath)
         as? SearchResultTableViewCell else
      {
         fatalError("[SearchResultsVC] Failed to dequeue SearchResultTableViewCell.")
      }
      
      cell.configure(forItem: self.results[indexPath.row])
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.delegate?.searchResultsViewController(self, didSelectItem: self.results[indexPath.row])
   }
}

extension SearchResultsViewController: UISearchResultsUpdating {
   func updateSearchResults(for searchController: UISearchController) {
      guard let searchText = searchController.searchBar.text?.lowercased() else { return }
      DispatchQueue.main.async {
         self.results = self.items.filter { $0.contains(searchText: searchText) }
         self.tableView.reloadData()
      }
   }
}

// MARK: - Protocols
protocol Searchable {
   var title: String? { get }
   var subtitle: String? { get }
   var image: UIImage? { get }
   func contains(searchText: String) -> Bool
}

protocol SearchResultsViewControllerDelegate: class {
   func searchResultsViewController(_ controller: UIViewController, didSelectItem item: Any?)
}

@available(*, deprecated)
protocol SearchResultsControllerDelegate: class {
    func searchResultsViewController(didSelectItem item: Any?)
}
