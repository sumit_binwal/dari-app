//
//  AuthOptionsViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/12/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AuthOptionsViewController: UIViewController, FirstLaunchView {
   /// Set to true when this view controller is presented immediately after the splash screen.
   var shouldAnimate: Bool = true
   var onSignUpButtonTap: (() -> Void)?
   var onSignInButtonTap: (() -> Void)?
   var onGuestButtonTap: (() -> Void)?
   var onSupportButtonTap: (() -> Void)?
   
   @available(*, deprecated)
   weak var delegate: AuthOptionsViewControllerDelegate?
   @available(*, deprecated)
   private enum Intent {
      case register
      case continueAsGuest
      case none
   }
   @available(*, deprecated)
   private var intent: Intent = .none
   /// Set to true when this view controller is presented immediately after the splash screen.
   @available(*, deprecated)
   var shouldAnimateOptions: Bool = false
   
   // MARK: - UI
   
   private lazy var languageButton: UIButton = {
      let button = UIButton(type: .system)
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setTitle("changeLanguageButton".localized, for: .normal)
      button.titleLabel?.font = .regularFont(ofSize: 18)
      button.addTarget(
         self,
         action: #selector(self.languageButtonWasTapped),
         for: .touchUpInside
      )
      return button
   }()
   private lazy var supportButton: UIButton = {
      let button = UIButton()
      button.translatesAutoresizingMaskIntoConstraints = false
      button.setImage(UIImage(named: "Support"), for: .normal)
      button.addTarget(
         self,
         action: #selector(self.supportButtonWasTapped),
         for: .touchUpInside
      )
      return button
   }()
   private lazy var imageView: UIImageView = {
      let view = UIImageView(image: UIImage(named: "Splash Image"))
      view.translatesAutoresizingMaskIntoConstraints = false
      return view
   }()
   private lazy var authOptionsView: AuthOptionsView = {
      let view = AuthOptionsView()
      view.registerButton.addTarget(
         self,
         action: #selector(self.registerButtonWasTapped),
         for: .touchUpInside
      )
      view.logInButton.addTarget(
         self,
         action: #selector(self.logInButtonWasTapped),
         for: .touchUpInside
      )
      view.guestButton.addTarget(
         self,
         action: #selector(self.guestButtonWasTapped),
         for: .touchUpInside
      )
      return view
   }()
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationController?.setNavigationBarHidden(true, animated: false)
      self.view.backgroundColor = .background
      self.setUpViews()
      
      if self.shouldAnimate {
         self.addBackgroundPattern()
         
         self.authOptionsView.transform = CGAffineTransform(
            translationX: 0,
            y: self.view.bounds.maxY - self.authOptionsView.frame.origin.y
         )
      }
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.setNavigationBarHidden(true, animated: animated)
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      self.navigationItem.searchController = nil
      
      if self.shouldAnimate {
         UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options:
            .curveEaseOut,
            animations: {
               self.authOptionsView.transform = .identity
         }, completion: nil)
      }
   }
   
   // MARK: - Setup
   private func setUpViews() {
      self.view.addSubviews([
         self.languageButton,
         self.supportButton,
         self.imageView,
         self.authOptionsView
      ])
      NSLayoutConstraint.activate([
         self.languageButton.topAnchor.constraint(
            equalToSystemSpacingBelow: self.view.layoutMarginsGuide.topAnchor, multiplier: 2),
         self.languageButton.leadingAnchor.constraint(
            equalTo: self.view.layoutMarginsGuide.leadingAnchor),
         
         self.supportButton.topAnchor.constraint(equalTo: self.languageButton.topAnchor),
         self.supportButton.trailingAnchor.constraint(
            equalTo: self.view.layoutMarginsGuide.trailingAnchor),
         
         self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
         self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20),
         
         self.authOptionsView.leadingAnchor.constraint(
            equalTo: self.view.layoutMarginsGuide.leadingAnchor),
         self.authOptionsView.trailingAnchor.constraint(
            equalTo: self.view.layoutMarginsGuide.trailingAnchor),
         self.authOptionsView.bottomAnchor.constraint(
            equalTo: self.view.layoutMarginsGuide.bottomAnchor)
      ])
   }
   
   // MARK: - Actions
   @objc
   private func registerButtonWasTapped() {
      self.onSignUpButtonTap?()
   }
   
   @objc
   private func logInButtonWasTapped() {
      self.onSignInButtonTap?()
   }
   
   @objc
   private func guestButtonWasTapped() {
      self.onGuestButtonTap?()
   }
   
   @objc
   private func languageButtonWasTapped() {
      let alert: UIAlertController = {
         let languageIsArabic: Bool = Environment.languageIsArabic
         
         let alert = UIAlertController(
            title: "changeLanguageQuery".localized,
            message: "changeLanguageEffect".localized,
            preferredStyle: .actionSheet
         )
         
         alert.addAction(UIAlertAction(
            title: "yes".localized,
            style: .default,
            handler: { [weak self] _ in
               Environment.language = languageIsArabic ? "en" : "ar"
               
               let restartAlert = UIAlertController(
                  title: nil,
                  message: "changeLanguageNotification".localized,
                  preferredStyle: .alert)
               
               self?.present(restartAlert, animated: true, completion: nil)
         }))
         alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil))
         
         return alert
      }()
      
      DispatchQueue.main.async {
         self.present(alert, animated: true, completion: nil)
      }
   }
   
   @objc
   private func supportButtonWasTapped() {
      self.onSupportButtonTap?()
   }
}

@available(*, deprecated)
protocol AuthOptionsViewControllerDelegate: AnyObject {
   func authOptionsViewControllerDidCompleteProcess()
}
