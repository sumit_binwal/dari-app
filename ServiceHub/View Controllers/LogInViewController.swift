//
//  LogInViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, SignInView {
   
   var onSignInComplete: (() -> Void)?
   var onDistrictUnavailable: (() -> Void)?
   var onForgotPassword: ((_ email: String?) -> Void)?
   
   @available(*, deprecated)
   weak var delegate: LogInViewControllerDelegate?
   
   // MARK: - UI
   private lazy var scrollView: UIScrollView = {
      let view = UIScrollView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.backgroundColor = .background
      view.showsVerticalScrollIndicator = false
      view.showsHorizontalScrollIndicator = false
      view.keyboardDismissMode = .interactive
      
      view.addSubview(self.contentView)
      NSLayoutConstraint.activate([
         self.contentView.topAnchor.constraint(equalTo: view.topAnchor),
         self.contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         self.contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
         self.contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var contentView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.preservesSuperviewLayoutMargins = true
      view.backgroundColor = .background
      
      view.addSubview(self.logInFormView)
      let marginsGuide = view.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.logInFormView.topAnchor.constraint(
            equalToSystemSpacingBelow: view.topAnchor, multiplier: 3),
         self.logInFormView.leadingAnchor.constraint(equalTo: marginsGuide.leadingAnchor),
         self.logInFormView.trailingAnchor.constraint(equalTo: marginsGuide.trailingAnchor),
         self.logInFormView.bottomAnchor.constraint(equalTo: marginsGuide.bottomAnchor)
      ])
      
      return view
   }()
   private lazy var logInFormView: LogInFormView = {
      let view = LogInFormView()
      view.delegate = self
      return view
   }()
   
   // MARK: - Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.backgroundColor = .background
      
      self.view.addSubview(self.scrollView)
      NSLayoutConstraint.activate([
         self.scrollView.topAnchor.constraint(equalTo: self.view.topAnchor),
         self.scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
         self.scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
         self.scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
         self.contentView.widthAnchor.constraint(equalTo: self.view.widthAnchor)
      ])
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillChange(notification:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil
      )
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillHideNotification),
         name: UIResponder.keyboardWillHideNotification,
         object: nil
      )
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.setNavigationBarHidden(false, animated: animated)
   }
   
   deinit { NotificationCenter.default.removeObserver(self) }
   
   // MARK: - Other
   @objc
   private func handleKeyboardWillChange(notification: Notification) {
      if let userInfo = notification.userInfo,
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue)?.cgRectValue.height {
         self.scrollView.contentInset.bottom = keyboardHeight - view.safeAreaInsets.bottom
      }
   }
   
   @objc
   private func handleKeyboardWillHideNotification() {
      self.scrollView.contentInset.bottom = 0
   }
}

// MARK: - UserFormViewDelegate
extension LogInViewController: UserFormViewDelegate {
   func userForm(didSubmitWithEmail email: String, password: String) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.logIn(email: email, password: password)
   }
   
   func userFormDidTapForgotPassword(email: String?) {
      self.onForgotPassword?(email)
   }
   
   func userFormDidRequestAddress(currentAddress: String?, latitude: Double?, longitude: Double?) {}
   func userFormDidRequestPhoneNumberCountry(currentCountry country: ISOCountry?) {}
   func userForm(didSubmitWithUser user: User, password: String) {}
   func userForm(didSubmitWithKeyValuePairs pairs: [String : Codable]) {}
   func userForm(didSubmitPasswordReset password: String, confirmPassword: String) {}
   func userForm(didRequestOTPWithEmail email: String) {}
   func userForm(
      didSubmitOTP otp: String,
      email: String,
      password: String,
      confirmPassword: String) {}
}

// MARK: - UserManagerDelegate
extension LogInViewController: UserManagerDelegate {
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   {
      DispatchQueue.main.async {
         // Field Errors
         if let errorMessages = errorMessages {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            self.logInFormView.displayErrorMessages(errorMessages)
            return
         }
         
         // Other Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // If the district isn't saved, get the district list.
         if District.current == nil {
            AreaManager.default.delegate = self
            AreaManager.default.getAreas()
            return
         
         }
         
         // Success
         // TODO: Find a better way to present loading view
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onSignInComplete?()
         })
      }
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}

// MARK: - AreaManagerDelegate
extension LogInViewController: AreaManagerDelegate {
   func areaManagerDidReceiveAreas(completedWithError error: Error?) {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            
            // Log out user
            User.manager.logOut()
            return
         }
         
         // Check if the user's district is still not available
         if District.current == nil {
            let alert = UIAlertController(
               title: nil,
               message: "districtNoLongerAvailable".localized,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(
               title: "selectAnotherDistrict".localized,
               style: .default,
               handler: { _ in
                  self.onDistrictUnavailable?()
                  
            }))
            alert.addAction(UIAlertAction(
               title: "cancel".localized,
               style: .destructive,
               handler: { _ in
                  User.manager.logOut()
            }))
            
            DispatchQueue.main.async {
               self.presentedViewController?.dismiss(animated: true, completion: {
                  self.present(alert, animated: true, completion: nil)
               })
            }
            
            return
         }
         
         // Success
         // TODO: Find a better way to show loading indicator
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onSignInComplete?()
         })
      }
   }
}

@available(*, deprecated)
protocol LogInViewControllerDelegate: AnyObject {
   func logInViewControllerDidLogInUser()
}
