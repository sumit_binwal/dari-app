//
//  CountryListViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/24/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class CountryListViewController: UITableViewController {
   // MARK: - Properties
   private lazy var countries = ISOCountry.load()
   var currentCountry: ISOCountry!
   weak var delegate: CountrySelectorDelegate?
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationItem.leftBarButtonItem = .init(
         barButtonSystemItem: .cancel,
         target: self,
         action: #selector(self.cancelButtonWasTapped(_:))
      )
      
      self.tableView.backgroundColor = .background
      self.tableView.separatorInset.left = 57
      self.tableView.rowHeight = UITableView.automaticDimension
      self.tableView.estimatedRowHeight = 44
      
      self.tableView.register(
         CountryTableViewCell.self,
         forCellReuseIdentifier: "CountryTableViewCell"
      )
      setUpSearchBar()
      DispatchQueue.main.async {
         if let country = self.currentCountry, let row = self.countries.firstIndex(of: country) {
            self.tableView.scrollToRow(at: IndexPath(row: row, section: 0), at: .top, animated: false)
         }
      }
      
   }
   
   // MARK: Initial Configuration
   private func setUpSearchBar() {
      let searchResultsVC = SearchResultsViewController()
      searchResultsVC.items = self.countries
      searchResultsVC.delegate = self
      
      navigationItem.searchController = {
         let searchController = UISearchController(searchResultsController: searchResultsVC)
         searchController.searchBar.searchBarStyle = .minimal
         searchController.searchResultsUpdater = searchResultsVC
         return searchController
      }()
      navigationItem.hidesSearchBarWhenScrolling = false
      definesPresentationContext = true
   }
   
   // MARK: - Actions
   @objc
   func cancelButtonWasTapped(_ sender: Any) {
      dismiss(animated: true, completion: nil)
   }
   
   // MARK: - Table View
   override func numberOfSections(in tableView: UITableView) -> Int { 1 }
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.countries.count
   }
   
   override func tableView(
      _ tableView: UITableView,
      cellForRowAt indexPath: IndexPath
   ) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(
         withIdentifier: "CountryTableViewCell", for: indexPath) as? CountryTableViewCell else
      {
         fatalError("[CountryListVC] Failed to dequeue CountryTableViewCell.")
      }
      
      let country = self.countries[indexPath.row]
      cell.configure(forItem: country)
      cell.accessoryType = country == self.currentCountry ? .checkmark : .none
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let newCountry = countries[indexPath.row]
      guard newCountry != currentCountry else {
         tableView.deselectRow(at: indexPath, animated: true)
         return
      }
      delegate?.didSelectCountry(newCountry)
      dismiss(animated: true, completion: nil)
   }
   
}

// MARK: - SearchResultsControllerDelegate
extension CountryListViewController: SearchResultsViewControllerDelegate {
   func searchResultsViewController(_ controller: UIViewController, didSelectItem item: Any?) {
      guard let country = item as? ISOCountry else {
         fatalError("[CountryVC] Unrecognized item sent from search countroller.")
      }
      self.delegate?.didSelectCountry(country)
      DispatchQueue.main.async {
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion: nil)
         })
      }
   }
}

protocol CountrySelectorDelegate: class {
   func didSelectCountry(_ country: ISOCountry)
}
