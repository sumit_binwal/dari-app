//
//  CatalogViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/6/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController, CatalogModuleView {
   
   // MARK: - Properties
   
   var onCityButtonTap: (() -> Void)?
   var onProfileButtonTap: (() -> Void)?
   var onListingSelect: ((Listing) -> Void)?
   
   var reloadCompletion: (() -> Void)?
   
   private var state: State = .shouldReloadData
   private enum State {
      case isLoaded
      case isLoading
      case shouldReloadData
      case shouldReloadUI
   }
   var cachedError: Error?
   var cachedCatalog: Catalog?
   
   // MARK: - UI
   
   private lazy var cityButton: UIBarButtonItem = .init(
      title: District.current?.city?.name,
      style: .plain,
      target: self,
      action: #selector(self.cityButtonWasTapped)
   )
   private lazy var profileButton: UIBarButtonItem = .init(
      image: UIImage(named: "Profile"),
      style: .plain,
      target: self,
      action: #selector(self.profileButtonWasTapped)
   )
   private lazy var catalogView: CatalogView = .init()
   
   // MARK: - Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationItem.leftBarButtonItem = self.cityButton
      self.navigationItem.rightBarButtonItem = self.profileButton
      self.navigationItem.titleView = {
         let imageView = UIImageView(image: UIImage(named: "Logo"))
         imageView.contentMode = .scaleAspectFit
         return imageView
      }()
      
      self.view.backgroundColor = .background
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.catalogView)
      
      self.catalogView.catalogDelegate = self
      self.handleState(animated: false)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.handleState(animated: animated)
      catalogView.reloadGreetingView()
   }
   
   // MARK: - Actions
   @objc
   private func cityButtonWasTapped() {
      self.onCityButtonTap?()
   }
   
   @objc
   private func profileButtonWasTapped() {
      self.onProfileButtonTap?()
   }
   
   // MARK: - Other
   
   private func getCatalog() {
      self.state = .isLoading
      Service.manager.delegate = self
      Service.manager.getCatalog()
   }
   
   func reload(completion: (() -> Void)?) {
      self.reloadCompletion = completion
      self.cityButton.title = District.current?.city?.name
      
      if self.viewIsInHierarchy {
         self.catalogView.startRefreshing()
      }
      self.getCatalog()
   }
   
   private func handleState(animated: Bool = true) {
      switch self.state {
      case .isLoaded:
         break
         
      case .isLoading:
         if !self.catalogView.isRefreshing {
            self.catalogView.startRefreshing(animateScrollViewContentOffset: animated)
         }
         
      case .shouldReloadData:
         if self.viewIsInHierarchy {
            self.catalogView.startRefreshing(animateScrollViewContentOffset: animated)
         }
         self.getCatalog()
         
      case .shouldReloadUI:
         self.state = .isLoaded
         self.catalogView.endRefreshing(animateScrollViewContentOffset: animated)
         if let error = self.cachedError {
            self.cachedError = nil
            self.catalogView.displayError(error, animated: animated)
            
         } else if let catalog = self.cachedCatalog {
            self.cachedCatalog = nil
            self.catalogView.setCatalog(catalog, animated: animated)
            
         } else {
            self.catalogView.reloadData()
         }
      }
   }
   
   private func showListing(_ listing: Listing) {
      self.onListingSelect?(listing)
   }
}

// MARK: - CatalogViewDelegate

extension CatalogViewController: CatalogViewDelegate {
   func catalogView(didSelectListing listing: Listing) {
      self.showListing(listing)
   }
   
   func catalogViewRefreshControlWasPulled() {
      self.getCatalog()
   }
}

// MARK: - ServiceManagerDelegate

extension CatalogViewController: ServiceManagerDelegate {
   func serviceManager(didGetCatalog catalog: Catalog?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         guard self.viewIsInHierarchy else {
            self.state = .shouldReloadUI
            self.cachedError = error
            self.cachedCatalog = catalog
            return
         }
         
         self.state = .isLoaded
         
         self.catalogView.endRefreshing()
         
         // Errors
         if let error = error {
            self.catalogView.displayError(error)
            return
         }
         
         // Success
         guard let catalog = catalog else {
            assertionFailure("[CatalogVC] Uncaught error.")
            return
         }
         self.catalogView.setCatalog(catalog) { [weak self] in
            self?.reloadCompletion?()
            self?.reloadCompletion = nil
         }
      }
   }
}

// MARK: - AuthViewControllerDelegate
extension CatalogViewController: AuthViewControllerDelegate {
   func authViewControllerDidLogInUser(authViewController: AuthViewController) {
      DispatchQueue.main.async {
         authViewController.dismiss(animated: true)
      }
   }
   
   func authViewControllerDidRegisterUser(authViewController: AuthViewController) {
      DispatchQueue.main.async {
         authViewController.dismiss(animated: true)
      }
   }
}
