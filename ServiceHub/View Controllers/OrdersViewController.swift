//
//  OrdersViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/21/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController, OrderListView {
   
   // MARK: - Properties
   
   var onOrderSelect: ((Order) -> Void)?
   
   private var state: State = .shouldReloadData
   private enum State {
      case isLoaded
      case isLoading
      case shouldReloadData
      case shouldReloadUI
   }
   var cachedError: Error?
   var cachedOrders: [Order.Summary]?
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   
   // MARK: - UI
   private lazy var ordersView: OrdersView = {
      let view = OrdersView()
      view.ordersViewDelegate = self
      return view
   }()
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "myOrders".localized
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.ordersView)
      self.handleState(animated: false)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.handleState(animated: animated)
   }
   
   // MARK: - Actions
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   @objc
   func didTapCancelationPolicyButton() {
      let navController = UINavigationController(
         rootViewController: CancelationPolicyViewController(
            gracePeriod: Order.gracePeriodInMinutes,
            cancelationRefundRates: Order.cancellationRefundRates
         )
      )
      self.present(navController, animated: true, completion: nil)
   }
   
   // MARK: - Other
   private func getOrders() {
      self.state = .isLoading
      Order.manager.historyDelegate = self
      Order.manager.getOrderSummaries()
   }
   
   private func handleState(animated: Bool = true) {
      switch self.state {
      case .isLoaded:
         break
         
      case .isLoading:
         if !self.ordersView.isRefreshing {
            self.ordersView.startRefreshing(animateScrollViewContentOffset: animated)
         }
         
      case .shouldReloadData:
         if self.viewIsInHierarchy {
            self.ordersView.startRefreshing(animateScrollViewContentOffset: animated)
         }
         self.getOrders()
         
      case .shouldReloadUI:
         self.state = .isLoaded
         self.ordersView.endRefreshing(animateScrollViewContentOffset: animated)
         if let error = self.cachedError {
            self.cachedError = nil
            self.ordersView.displayError(error, animated: animated)
            
         } else if let orders = self.cachedOrders {
            self.cachedOrders = nil
            self.ordersView.setOrders(orders, animated: animated)
            
         } else {
            self.ordersView.reloadData()
         }
      }
   }
   
   private func confirmOrderCancellation(_ order: Order.Summary) {
      let refundText: String? = {
         guard order.paymentMethod == .card else { return nil }
         
         let refundAmount = order.refundIfCancelledNow
         
         return Order.refundDescription(
            refundAmount: refundAmount,
            total: order.total,
            currencyFormatter: order.currencyFormatter
         )
      }()
      let title: String? = refundText == nil ? nil : "confirmOrderCancellation".localized
      let message: String = refundText ?? "confirmOrderCancellation".localized
      
      let alert = UIAlertController(
         title: title,
         message: message,
         preferredStyle: .alert
      )
      
      alert.addAction(
         .init(title: "yes".localized, style: .destructive, handler: { _ in
            self.presentActivityIndicatorController()
            Order.manager.historyDelegate = self
            Order.manager.cancelOrder(withID: order.id)
         })
      )
      alert.addAction(
         .init(title: "cancelationPolicy".localized, style: .default, handler: { _ in
            self.didTapCancelationPolicyButton()
         })
      )
      alert.addAction(.init(title: "no".localized, style: .cancel, handler: nil))
      
      self.present(alert, animated: true, completion: nil)
   }
   
   func removeOrder(withID id: Int) {
      self.ordersView.delete(orderWithID: id)
   }
}

// MARK: - OrdersViewDelegate

extension OrdersViewController: OrdersViewDelegate {
   func ordersView(didSelectOrder order: Order.Summary) {
      Order.manager.historyDelegate = self
      Order.manager.getOrder(id: order.id)
   }
   
   func ordersView(didCancelOrder order: Order.Summary) {
      self.confirmOrderCancellation(order)
   }
   
   func ordersViewRefreshControlWasPulled() {
      self.getOrders()
   }
}

// MARK: - OrderManagerHistoryDelegate

extension OrdersViewController: OrderManagerHistoryDelegate {
   func orderManager(didGetOrder order: Order?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         if let error = error {
            self.ordersView.clearOrdersBeingFetched()
            
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               self.present(signInAlert, animated: true, completion: nil)
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(.init(title: "ok".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
         }
         
         guard let order = order else { return }
         self.ordersView.setOrderAsFetched(id: order.id)
         self.onOrderSelect?(order)
      }
   }
   
   func orderManager(didGetOrders orders: [Order.Summary]?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         guard self.viewIsInHierarchy else {
            self.state = .shouldReloadUI
            self.cachedError = error
            self.cachedOrders = orders
            return
         }
         
         self.state = .isLoaded
         self.ordersView.endRefreshing()
         
         if let error = error {
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               self.present(signInAlert, animated: true, completion: nil)
               return
            }
            
            // Other Errors
            self.ordersView.displayError(error)
            return
         }
         
         // Success
         guard let orders = orders else {
            assertionFailure("[OrdersVC] Uncaught error.")
            return
         }
         self.ordersView.setOrders(orders)
      }
   }
   
   func orderManager(didCancelOrderWithID id: Int?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            //Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               
               self.dismissActivityIndicatorOrAlert {
                  self.present(signInAlert, animated: true, completion: nil)
               }
               return
            }
            
            // Other Errors
            self.dismissActivityIndicatorOrAlert {
               self.ordersView.displayError(error)
            }
            return
         }
         
         // Success
         guard let id = id else {
            assertionFailure("[OrdersVC] Uncaught error.")
            return
         }
         self.dismissActivityIndicatorOrAlert {
            self.ordersView.delete(orderWithID: id)
         }
      }
   }
   
   func orderManager(didPlaceOrderWithSummary summary: Order.Summary) {
      // It's assumed the view isn't in the heirarchy when this function is called.
      DispatchQueue.main.async {
         if !self.viewIsInHierarchy {
            var orders = self.cachedOrders ?? self.ordersView.orders
            orders.insert(summary, at: 0)
            
            self.state = .shouldReloadUI
            self.cachedError = nil
            self.cachedOrders = orders
            
         } else {
            assertionFailure(
               "[OrdersVC] orderManager(didPlaceOrderWithSummary:) called when view is in heirarchy."
            )
         }
      }
   }
}
