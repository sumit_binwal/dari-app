//
//  PaymentOTPController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 17/03/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit
import WebKit

class PaymentOTPController: WebController, PaymentOTPView {
   
   var successURL: URL
   var failureURL: URL
   var onSuccessRedirect: ((URL) -> Void)?
   var onFailureRedirect: ((URL) -> Void)?
   var onCancel: (() -> Void)?
   
   init(url: URL, successURL: URL, failureURL: URL) {
      self.successURL = successURL
      self.failureURL = failureURL
      super.init(url: url)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      webView.allowsBackForwardNavigationGestures = false
      
      navigationItem.leftBarButtonItem = UIBarButtonItem(
         title: "cancel".localized,
         style: .plain,
         target: self,
         action: #selector(didTapCancelButton)
      )
   }
   
   override func webView(
      _ webView: WKWebView,
      didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!
   ) {
      guard let redirectURL = webView.url else { return }
      
      if redirectURL.contains(successURL) {
         onSuccessRedirect?(redirectURL)
         webView.stopLoading()
      } else if redirectURL.contains(failureURL) {
         onFailureRedirect?(redirectURL)
         webView.stopLoading()
      }
   }
   
   @objc
   private func didTapCancelButton() {
      onCancel?()
   }
}

fileprivate extension URL {
   func contains(_ potentialSubURL: URL) -> Bool {
      let components = Set(pathComponents)
      let potentialSubComponents = Set(potentialSubURL.pathComponents)
      return components.isSuperset(of: potentialSubComponents)
   }
}
