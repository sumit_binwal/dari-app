//
//  UserChangeViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/1/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

// TODO: Refactor into different view controllers for each function
class UserChangeViewController:
   UIViewController,
   ProfileChangeView,
   ForgotPasswordView,
   ResetPasswordView
{
   
   var onProfileChangeSuccessful: (() -> Void)?
   
   // ForgotPasswordView
   var onRequestOTP: ((_ email: String?) -> Void)?
   
   // ResetPasswordView
   var onPasswordResetSuccessful: (() -> Void)?
   
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   
   // MARK: UI
   private lazy var scrollView: UIScrollView = {
      let scrollView = UIScrollView()
      scrollView.translatesAutoresizingMaskIntoConstraints = false
      scrollView.isScrollEnabled = true
      scrollView.bounces = true
      scrollView.maximumZoomScale = 1
      scrollView.minimumZoomScale = 1
      scrollView.backgroundColor = .clear
      scrollView.keyboardDismissMode = .onDrag
      
      scrollView.addSubview(contentView)
      NSLayoutConstraint.activate([
         contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
         contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
         contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
         contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
      ])
      
      return scrollView
   }()
   private lazy var contentView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .clear
      
      view.addSubview(self.formView)
      NSLayoutConstraint.activate([
         self.formView.topAnchor.constraint(equalTo: view.topAnchor, constant: 15),
         self.formView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
         self.formView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
         self.formView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -15)
      ])
      return view
   }()
   private var formView: RegisterFormView
   
   // MARK: Initializers
   init(formView: RegisterFormView) {
      self.formView = formView
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   deinit { NotificationCenter.default.removeObserver(self) }
   
   // MARK: - Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.formView.delegate = self
      self.addViews()
      self.setAppearance()
      
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillChange(notification:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil
      )
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillHideNotification),
         name: UIResponder.keyboardWillHideNotification,
         object: nil
      )
   }
   
   private func addViews() {
      self.view.addSubview(self.scrollView)
      NSLayoutConstraint.activate([
         self.scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
         self.scrollView.leadingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
         self.scrollView.trailingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
         self.scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
         
         self.contentView.widthAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.widthAnchor)
      ])
   }
   
   private func setAppearance() {
      self.view.backgroundColor = .background
   }
   
   // MARK: - Actions
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   // MARK: Other
   @objc
   private func handleKeyboardWillChange(notification: Notification) {
      if let userInfo = notification.userInfo,
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue)?.cgRectValue.height {
         self.scrollView.contentInset.bottom = keyboardHeight - view.safeAreaInsets.bottom
      }
   }
   
   @objc
   private func handleKeyboardWillHideNotification() {
      self.scrollView.contentInset.bottom = 0
   }
}

// MARK: - UserFormViewDelegate
extension UserChangeViewController: UserFormViewDelegate {
   func userForm(didSubmitWithKeyValuePairs pairs: [String : Codable]) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.update(userAttributes: pairs)
   }
   
   func userForm(didSubmitPasswordReset password: String, confirmPassword: String) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.changePassword(password, confirmPassword: confirmPassword)
   }
   
   func userFormDidRequestPhoneNumberCountry(currentCountry: ISOCountry?) {
      let countryListVC = CountryListViewController()
      countryListVC.currentCountry = currentCountry
      countryListVC.delegate = self.formView
      
      DispatchQueue.main.async {
         self.present(
            UINavigationController(rootViewController: countryListVC),
            animated: true,
            completion: nil
         )
      }
   }
   
   func userForm(didRequestOTPWithEmail email: String) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.resetPassword(email: email)
   }
   
   func userForm(
      didSubmitOTP otp: String,
      email: String,
      password: String,
      confirmPassword: String
   ) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.submit(
         otp: otp,
         email: email,
         password: password,
         confirmPassword: confirmPassword
      )
   }
   
   func userFormDidTapForgotPassword(email: String?) {}
   func userForm(didSubmitWithUser user: User, password: String) {}
   func userFormDidRequestAddress(currentAddress: String?, latitude: Double?, longitude: Double?) {}
   func userForm(didSubmitWithEmail email: String, password: String) {}
}

// MARK: - UserManagerDelegate
extension UserChangeViewController: UserManagerDelegate {
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Authentication
         if let error = error as? APIError, error == .notAuthenticated,
            let (signInAlert, signInAction) = UIAlertController.logInAlertController(
               presentingController: self,
               textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
         {
            self.signInAction = signInAction
            
            self.presentedViewController?.dismiss(animated: true, completion: {
                self.present(signInAlert, animated: true, completion: nil)
            })
            return
         }
         
         // Field Errors
         if let errorMessages = errorMessages {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            self.formView.displayErrorMessages(errorMessages)
            return
         }

         // Other Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onProfileChangeSuccessful?()
         })
      }
   }
   
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Authentication
         if let error = error as? APIError, error == .notAuthenticated,
            let (signInAlert, signInAction) = UIAlertController.logInAlertController(
               presentingController: self,
               textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
         {
            self.signInAction = signInAction
            
            self.presentedViewController?.dismiss(animated: true, completion: {
                self.present(signInAlert, animated: true, completion: nil)
            })
            return
         }
         
         // Field Errors
         if let errorMessages = errorMessages {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            self.formView.displayErrorMessages(errorMessages)
            return
         }
         
         // Other Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         if self.formView is OTPSubmissionFormView {
            self.onPasswordResetSuccessful?()
            
         } else {
            self.formView.resubmit()
         }
      }
   }
   
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Error
         if let error = error {
            // Field Errors
            if let errorMessages = errorMessages {
               self.presentedViewController?.dismiss(animated: true, completion: nil)
               self.formView.displayErrorMessages(errorMessages)
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onRequestOTP?(self.formView.email)
         })
      }
   }
   
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Error
         if let error = error {
            // Field Errors
            if let errorMessages = errorMessages {
               self.presentedViewController?.dismiss(animated: true, completion: nil)
               self.formView.displayErrorMessages(errorMessages)
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: nil)
      }
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}
