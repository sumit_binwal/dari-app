//
//  OrderConfirmationViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/23/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import StoreKit

class OrderConfirmationViewController: UIViewController, OrderConfirmationModuleView {
   
   // MARK: - Properties
   
   var onHomeButtonTap: (() -> Void)?
   var onOrdersButtonTap: (() -> Void)?
   
   let order: Order
   
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   
   // MARK: - UI
   
   lazy var orderConfirmationView: OrderConfirmationView = {
      let view = OrderConfirmationView(order: self.order)
      view.delegate = self
      return view
   }()
   
   // MARK: Init
   
   init(order: Order) {
      self.order = order
      super.init(nibName: nil, bundle: nil)
      self.modalPresentationStyle = .fullScreen
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      self.view.backgroundColor = .background
      self.view.addFillingSubviewConstrainedByMargins(self.orderConfirmationView)
      
      EventLogger.log(
         .didPlaceOrder,
         revenue: order.total,
         currency: order.englishCurrencySymbol,
         parameters: [.service: order.serviceParameters.englishServiceName]
      )
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      self.promptToRate()
   }
   
   // MARK: - Actions
   @objc
   func didTapCancelationPolicyButton() {
      let navController = UINavigationController(
         rootViewController: CancelationPolicyViewController(
            gracePeriod: Order.gracePeriodInMinutes,
            cancelationRefundRates: Order.cancellationRefundRates
         )
      )
      self.present(navController, animated: true, completion: nil)
   }
   
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   private func promptToRate() {
      // Get the current build
      let currentBuildAsString = Bundle.main.object(
         forInfoDictionaryKey: kCFBundleVersionKey as String
      ) as? String ?? "0"
      let currentBuild = Int(currentBuildAsString) ?? 0
      
      // Get the last build prompted for rating
      let lastBuildPromptedForRating = UserDefaults.standard.integer(
         forKey: "lastBuildPromptedForRating"
      )
      
      guard lastBuildPromptedForRating < currentBuild else { return }
      
      // Prompt after a delay
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
         SKStoreReviewController.requestReview()
         UserDefaults.standard.set(currentBuild, forKey: "lastBuildPromptedForRating")
      }
   }
}

// MARK: - OrderConfirmationViewDelegate
extension OrderConfirmationViewController: OrderConfirmationViewDelegate {
   func orderConfirmationViewDidUnwindToCatalog() {
      self.onHomeButtonTap?()
   }
   
   func orderConfirmationViewDidTapMyOrders() {
      self.onOrdersButtonTap?()
   }
   
   func orderConfirmationView(didCancelOrder order: Order) {
      let refundText: String? = {
         guard order.paymentMethod == .card else { return nil }
         
         let refundAmount = order.refundIfCancelledNow
         
         return Order.refundDescription(
            refundAmount: refundAmount,
            total: order.total,
            currencyFormatter: order.currencyFormatter
         )
      }()
      let title: String? = refundText == nil ? nil : "confirmOrderCancellation".localized
      let message: String = refundText ?? "confirmOrderCancellation".localized
      
      let alert = UIAlertController(
         title: title,
         message: message,
         preferredStyle: .alert
      )
      
      alert.addAction(
         .init(title: "yes".localized, style: .destructive, handler: { _ in
            self.presentActivityIndicatorController()
            Order.manager.historyDelegate = self
            Order.manager.cancelOrder(withID: order.id)
         })
      )
      alert.addAction(
         .init(title: "cancelationPolicy".localized, style: .default, handler: { _ in
            self.didTapCancelationPolicyButton()
         })
      )
      alert.addAction(.init(title: "no".localized, style: .cancel, handler: nil))
      
      self.present(alert, animated: true, completion: nil)
   }
}

// MARK: - OrderManagerHistoryDelegate
extension OrderConfirmationViewController: OrderManagerHistoryDelegate {
   func orderManager(didCancelOrderWithID id: Int?, completedWithError error: Error?) {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            //Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               
               self.dismissActivityIndicatorOrAlert {
                  self.present(signInAlert, animated: true, completion: nil)
               }
               return
            }
            
            // Other Errors
            self.dismissActivityIndicatorOrAlert {
               let alert = UIAlertController(
                  title: nil,
                  message: error.localizedDescription,
                  preferredStyle: .alert
               )
               alert.addAction(.init(title: "ok".localized, style: .default, handler: nil))
               self.present(alert, animated: true, completion: nil)
            }
            return
         }
         
         // Success
         self.dismissActivityIndicatorOrAlert {
            self.onHomeButtonTap?()
         }
      }
   }
   
   func orderManager(didGetOrders orders: [Order.Summary]?, completedWithError error: Error?) {}
   func orderManager(didGetOrder order: Order?, completedWithError error: Error?) {}
   func orderManager(didPlaceOrderWithSummary summary: Order.Summary) {}
}
