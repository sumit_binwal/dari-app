//
//  MapSearchResultsViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/9/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import MapKit

class MapSearchResultsViewController: SearchResultsViewController {
   lazy var localSearchRequest: MKLocalSearch.Request = .init()
   var localSearch: MKLocalSearch!
   var localSearchResponse: MKLocalSearch.Response!
   var error: NSError!
   
   override func updateSearchResults(for searchController: UISearchController) {
      let searchBar = searchController.searchBar
      localSearchRequest.naturalLanguageQuery = searchBar.text
      localSearch = MKLocalSearch(request: localSearchRequest)
      localSearch.start { (localSearchResponse, error) in
          self.results = localSearchResponse?.mapItems ?? [MKMapItem]()
          self.tableView.reloadData()
      }
   }
}

extension MKMapItem: Searchable {
   var title: String? { self.placemark.name }
   
   var subtitle: String? { self.placemark.locality }
   
   var image: UIImage? { nil }
   
   func contains(searchText: String) -> Bool { false }
}
