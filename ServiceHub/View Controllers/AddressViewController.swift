//
//  AddressViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/8/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import MapKit

class AddressViewController: UIViewController, AddressChangeView {
   
   var onAddressChangeSuccessful: (
      (_ address: String, _ latitude: Double, _ longitude: Double) -> Void
   )?
   
   @available(*, deprecated)
   weak var delegate: AddressViewControllerDelegate?
   private weak var signInAction: UIAlertAction?
   
   // MARK: - Properties
   private var beginningAddress: String?
   private var beginningLatitude: Double?
   private var beginningLongitude: Double?
   
   // MARK: UI
   private lazy var setButton: UIBarButtonItem = .init(
      title: "Set".localized,
      style: .plain,
      target: self,
      action: #selector(self.setButtonWasTapped)
   )
   private lazy var addressView: AddressView = {
      let view = AddressView(
         address: self.beginningAddress,
         latitude: self.beginningLatitude,
         longitude: self.beginningLongitude
      )
      view.delegate = self
      return view
   }()
   
   // MARK: - Init
   init(address: String?, latitude: Double?, longitude: Double?) {
      self.beginningAddress = address
      self.beginningLatitude = latitude
      self.beginningLongitude = longitude
      super.init(nibName: nil, bundle: nil)
      
      self.hidesBottomBarWhenPushed = true
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      if self === self.navigationController?.children.first {
         self.navigationItem.leftBarButtonItem = .init(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(self.cancelButtonWasTapped)
         )
      }
      
      self.navigationItem.rightBarButtonItem = self.setButton
      
      self.setUpSearchBar()
      self.view.addFillingSubview(self.addressView)
      
   }
   
   // MARK: - Actions
   @objc
   private func cancelButtonWasTapped() {
      DispatchQueue.main.async {
         self.firstResponder?.resignFirstResponder()
         self.dismiss(animated: true, completion: nil)
      }
   }
   
   @objc
   private func setButtonWasTapped() {
      let address = self.addressView.currentAddress
      guard !address.isEmpty else {
         self.addressView.addressTextField.flash(andBecomeFirstResponder: true)
         return
      }
      
      let latitude = self.addressView.currentLatitude
      let longitude = self.addressView.currentLongitude
      
      if User.current != nil {
         DispatchQueue.main.async {
            self.view.firstResponder?.resignFirstResponder()
            self.presentActivityIndicatorController()
         }
         
         User.manager.delegate = self
         
         User.manager.update(userAttributes: [
            User.CodingKeys.profile.rawValue: [
               User.Profile.CodingKeys.address.rawValue: address,
               User.Profile.CodingKeys._latitude.rawValue: String(latitude),
               User.Profile.CodingKeys._longitude.rawValue: String(longitude)
            ]
         ])
      } else {
         self.onAddressChangeSuccessful?(address, latitude, longitude)
      }
   }
   
   // MARK: - Other
   private func setUpSearchBar() {
      let searchResultsVC = MapSearchResultsViewController()
      searchResultsVC.delegate = self
      navigationItem.searchController = {
         let searchController = UISearchController(searchResultsController: searchResultsVC)
         searchController.searchBar.searchBarStyle = .minimal
         return searchController
      }()
      navigationItem.searchController?.searchResultsUpdater = searchResultsVC
      navigationItem.hidesSearchBarWhenScrolling = false
      definesPresentationContext = true
   }
   
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
}

// MARK: - SearchResultsControllerDelegate
extension AddressViewController: SearchResultsViewControllerDelegate {
   func searchResultsViewController(_ controller: UIViewController, didSelectItem item: Any?) {
      guard let mapItem = item as? MKMapItem else {
         assertionFailure("[AddressVC] Failed to retrieve map item.")
         return
      }
      
      self.navigationItem.searchController?.isActive = false
      let coordinate = mapItem.placemark.coordinate
      self.addressView.centerMapOnLocation(CLLocation(
         latitude: coordinate.latitude,
         longitude: coordinate.longitude
      ))
   }
}

// MARK: - AddressViewDelegate
extension AddressViewController: AddressViewDelegate {
   func addressView(didChangeAddress address: String, latitude: Double, longitude: Double) {
      self.setButton.isEnabled =
         self.beginningAddress != address ||
         self.beginningLatitude != latitude ||
         self.beginningLongitude != longitude
   }
}

// MARK: - UserManagerDelegate
extension AddressViewController: UserManagerDelegate {
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               self.presentedViewController?.dismiss(animated: true, completion: {
                   self.present(signInAlert, animated: true, completion: nil)
               })
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onAddressChangeSuccessful?(
               self.addressView.currentAddress,
               self.addressView.currentLatitude,
               self.addressView.currentLongitude
            )
         })
      }
   }
   
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   {
      // Errors
      if let error = error {
         // Authentication
         if let error = error as? APIError, error == .notAuthenticated,
            let (signInAlert, signInAction) = UIAlertController.logInAlertController(
               presentingController: self,
               textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
         {
            self.signInAction = signInAction
            
            self.presentedViewController?.dismiss(animated: true, completion: {
                self.present(signInAlert, animated: true, completion: nil)
            })
            return
         }
         
         // Other Errors
         let alert = UIAlertController(
            title: nil,
            message: errorMessages?.nonFieldErrors?.first ?? error.localizedDescription,
            preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
         
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.present(alert, animated: true, completion: nil)
         })
         return
      }
      
      // Success
      self.setButtonWasTapped()
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}

// MARK: - Delegate Protocol

@available(*, deprecated)
protocol AddressViewControllerDelegate: AnyObject {
   func addressViewController(didSetAddress address: String, latitude: Double, longitude: Double)
}
