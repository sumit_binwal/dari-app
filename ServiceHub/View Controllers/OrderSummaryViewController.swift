//
//  OrderSummaryViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/13/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class OrderSummaryViewController: UIViewController, OrderSummaryModuleView {
   
   var onSupportButtonTap: (() -> Void)?
   var onAddCardTap: (() -> Void)?
   var onOTPRedirect: ((_ url: URL) -> Void)?
   var onOrderSuccessful: ((_ order: Order) -> Void)?
  
   let formData: Order.FormData
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   /// If true, the user attempted to place an order with payment by card, while no card was selected.
   var didTapSubmitWithNilCard: Bool = false
   
   // MARK: - UI
   
   private lazy var orderSummaryView: OrderSummaryView = {
      let view = OrderSummaryView(formData: self.formData)
      view.orderSummaryDelegate = self
      return view
   }()
   private lazy var buttonView: BottomButtonView = {
      let view = BottomButtonView(buttonTitle: "placeOrder".localized)
      view.button.addTarget(
         self,
         action: #selector(self.didTapPlaceOrderButton),
         for: .touchUpInside
      )
      return view
   }()
   
   // MARK: - Init
   
   init(data: Order.FormData) {
      self.formData = data
      super.init(nibName: nil, bundle: nil)
      
      // Remove any previously existing promo codes. This is to close the loop hole of validating a promo code for certain parameters, going back to the order form, and changing the parameters.
      self.formData.promoCode = nil
   }
   
   required init?(coder: NSCoder) {
      fatalError("init?(coder:) not implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationItem.rightBarButtonItem = UIBarButtonItem(
         image: UIImage(named: "Support"),
         style: .plain,
         target: self,
         action: #selector(self.didTapSupportButton)
      )
      self.view.backgroundColor = .background
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.orderSummaryView)
      self.view.addSubview(self.buttonView)
      NSLayoutConstraint.activate([
         self.buttonView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
         self.buttonView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
         self.buttonView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
      ])
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleKeyboardFrameWillChange(_:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil
      )
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleKeyboardWillHide(_:)),
         name: UIResponder.keyboardWillHideNotification,
         object: nil
      )
      
      let eventParameters: [Event.Parameter: Any] = {
         var dictionary: [Event.Parameter: Any] = [
            .service: self.formData.primaryService.englishName,
            .serviceID: self.formData.primaryService.id
         ]
         if let serviceProvider = self.formData.serviceProvider {
            dictionary[.serviceProvider] =  serviceProvider.englishCompanyName
         }
         return dictionary
      }()
      EventLogger.log(.didReachOrderSummary, parameters: eventParameters)
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      self.orderSummaryView.contentInset.bottom =
         self.buttonView.bounds.height - self.view.safeAreaInsets.bottom
   }
   
   // MARK: - Actions
   
   @objc
   private func didTapSupportButton() {
      self.onSupportButtonTap?()
   }
   
   @objc
   private func didTapPlaceOrderButton() {
      DispatchQueue.main.async { self.firstResponder?.resignFirstResponder() }
      
      if self.formData.paymentMethod == .card {
         
         if formData.card == nil {
            didTapSubmitWithNilCard = true
            onAddCardTap?()
            return
            
         } else if formData.card?.isSaved ?? false && formData.cvv == nil {
            showCVVForm {
               DispatchQueue.main.async { self.presentActivityIndicatorController() }
               self.submitOrder()
            }
            return
         }
      }
      
      DispatchQueue.main.async { self.presentActivityIndicatorController() }
      self.submitOrder()
   }
   
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   // MARK: - Other
   
   private func submitOrder() {
      guard let submissionDTO = self.formData.submissionDTO else {
         assertionFailure("[OrderSummaryVC] Submitted order with nil dto.")
         return
      }
      Order.manager.delegate = self
      Order.manager.placeOrder(submissionDTO: submissionDTO)
   }
   
   private func showCVVForm(completion: (() -> Void)?) {
      let alert = UIAlertController(title: nil, message: "enterCVV".localized, preferredStyle: .alert)
      
      alert.addTextField { textField in
         textField.keyboardType = .numberPad
      }
      
      alert.addAction(.init(title: "ok".localized, style: .default, handler: {
         [weak self, weak alert] _ in
         self?.formData.cvv = alert?.textFields?.first?.text
         completion?()
      }))
      alert.addAction(.init(title: "cancel".localized, style: .cancel, handler: nil))
      
      present(alert, animated: true, completion: nil)
   }
   
   @objc
   private func handleKeyboardFrameWillChange(_ notification: Notification) {
      guard let userInfo = notification.userInfo else {
         assertionFailure("[OrderSummaryVC] Failed to get userInfo from keyboard change notification.")
         return
      }
      DispatchQueue.main.async {
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
         let duration =
            userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
         let animationCurve: UIView.AnimationOptions =
            userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UIView.AnimationOptions ?? .curveEaseOut
         
         self.additionalSafeAreaInsets.bottom += keyboardHeight - self.view.safeAreaInsets.bottom
         
         UIView.animate(
            withDuration: duration,
            delay: 0,
            options: animationCurve,
            animations: {
               self.view.layoutIfNeeded()
         })
      }
      
   }
   
   @objc
   private func handleKeyboardWillHide(_ notification: Notification) {
      guard let userInfo = notification.userInfo else {
         assertionFailure("[OrderSummaryVC] Failed to get userInfo from keyboard change notification.")
         return
      }
      
      DispatchQueue.main.async {
         let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
         let animationCurve: UIView.AnimationOptions =
            userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UIView.AnimationOptions ?? .curveEaseOut
         
         self.additionalSafeAreaInsets.bottom = 0
         UIView.animate(
            withDuration: duration,
            delay: 0,
            options: animationCurve,
            animations: {
               self.view.layoutIfNeeded()
         })
      }
   }
   
   private func showTermsAndConditions() {
      guard let url = Environment.termsAndConditionsURL else { return }
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
   }
   
   func refresh() {
      self.didTapSubmitWithNilCard = false
      self.orderSummaryView.refreshCardSelection()
   }
   
   func addCard(_ card: Card, save: Bool) {
      self.formData.saveCard = save
      self.formData.cards.append(card)
      self.formData.card = card
      self.orderSummaryView.addCard(card)
      
      if self.didTapSubmitWithNilCard {
         self.didTapPlaceOrderButton()
      }
   }
   
   func threeDSSuccessCallback(url: URL) {
      presentActivityIndicatorController()
      Order.manager.delegate = self
      Order.manager.threeDSSuccessCallback(url: url)
   }
   
   func threeDSFailureCallback(url: URL) {
      Order.manager.delegate = self
      Order.manager.threeDSFailureCallback(url: url)
   }
   
   func removeUnsavedCards() {
      let unsavedCards = formData.removeUnsavedCards()
      orderSummaryView.removeCards(unsavedCards)
   }
}

// MARK: - OrderSummaryViewDelegate

extension OrderSummaryViewController: OrderSummaryViewDelegate {
   func orderSummaryDidTapCancelationPolicy() {
      let navController = UINavigationController(
         rootViewController: CancelationPolicyViewController(
            gracePeriod: Order.gracePeriodInMinutes,
            cancelationRefundRates: Order.cancellationRefundRates
         )
      )
      self.present(navController, animated: true, completion: nil)
   }
   
   func orderSummaryViewDidTapAddCard() {
      self.onAddCardTap?()
   }
   
   func orderSummaryDidTapTermsAndConditions() {
      self.showTermsAndConditions()
   }
}

// MARK: - OrderManagerDelegate
extension OrderSummaryViewController: OrderManagerDelegate {
   func orderManager(
      didValidatePromoCode promoCode: Order.PromoCode?,
      completedWithError error: Error?,
      errorMessage: String?
   ) {
      // FIXME: Order summary view also handles this, which could cause an issue if user places order before receiving promo response
   }
   
   func orderManager(didRedirectTo url: URL) {
      DispatchQueue.main.async {
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onOTPRedirect?(url)
         })
      }
   }
   
   func orderManager(
      didPlaceOrder order: Order?,
      completedWithError error: Error?,
      cardTransaction: CardTransaction?
   ) {
      DispatchQueue.main.async {
         if let error = error {
            
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               User.manager.delegate = self
               
               self.presentedViewController?.dismiss(animated: true, completion: {
                   self.present(signInAlert, animated: true, completion: nil)
               })
               return
            }
            
            // Listing Modified
            if let error = error as? APIError, error == .resourceChanged {
               self.handleListingModification()
               return
            }
            
            // Card Payment
            if let error = error as? APIError,
               [APIError.paymentFailure, .cardDeclined].contains(error)
            {
               let alert = UIAlertController(
                  title: error.localizedDescription,
                  message: nil,
                  preferredStyle: .alert
               )
               alert.addAction(.init(title: "ok".localized, style: .default, handler: nil))
               
               if let presentedVC = self.presentedViewController {
                  presentedVC.dismiss(animated: true, completion: {
                     self.present(alert, animated: true, completion: nil)
                  })
               } else {
                  self.present(alert, animated: true, completion: nil)
               }
               
               self.removeUnsavedCards()
               return
            }
            
            // Other Errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            if let presentedVC = self.presentedViewController {
               presentedVC.dismiss(animated: true, completion: {
                  self.present(alert, animated: true, completion: nil)
               })
            } else {
               self.present(alert, animated: true, completion: nil)
            }
            
            self.removeUnsavedCards()
            return
         }
         
         guard let order = order else {
            assertionFailure("[OrderSummaryVC] Uncaught error.")
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            return
         }
         
         // Success
         if let presentedViewController = self.presentedViewController {
            presentedViewController.dismiss(animated: true, completion: {
               self.onOrderSuccessful?(order)
            })
         } else {
            self.onOrderSuccessful?(order)
         }
      }
   }
   
   func orderManager(didGetOrders orders: [Order], completedWithError error: Error?) {}
   
   private func handleListingModification() {
      guard let changedField = self.formData.refresh() else {
         self.submitOrder()
         return
      }
      
      switch changedField {
      case .listing: self.handleDestructiveModification(message: "listingUnavailable".localized)
      case .serviceProvider:
         self.handleDestructiveModification(message: "serviceProviderUnlisted".localized)
      case .service: self.handleDestructiveModification(message: "invalidatedSelection".localized)
      case .price: self.handlePriceModification()
      default:
         assertionFailure("[OrderSummaryVC] Uncaught error.")
         self.presentedViewController?.dismiss(animated: true, completion: nil)
      }
   }
   
   private func handleDestructiveModification(message: String) {
      let alert = UIAlertController(
         title: "sorry".localized,
         message: message,
         preferredStyle: .alert
      )
      alert.addAction(.init(title: "ok".localized, style: .default, handler: { _ in
         self.navigationController?.popToRootViewController(animated: true)
      }))
      
      self.presentedViewController?.dismiss(animated: true, completion: {
         self.present(alert, animated: true, completion: nil)
      })
   }
   
   private func handlePriceModification() {
      let message: String = "\("priceChanged".localized) \(self.formData.totalString ?? "")."
      let alert = UIAlertController(
         title: "sorry".localized,
         message: message,
         preferredStyle: .alert
      )
      
      alert.addAction(
         .init(
            title: "orderAnyway".localized,
            style: .default,
            handler: { _ in self.didTapPlaceOrderButton() }
         )
      )
      
      alert.addAction(
         .init(
            title: "reviewOrder".localized,
            style: .default,
            handler: { _ in self.orderSummaryView.reload() })
      )
      
      self.presentedViewController?.dismiss(animated: true, completion: {
         self.present(alert, animated: true, completion: nil)
      })
   }
}

// MARK: - UserManagerDelegate
extension OrderSummaryViewController: UserManagerDelegate {
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Error
         if let error = error {
            // Authentication
            if let error = error as? APIError, error == .notAuthenticated,
               let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                  presentingController: self,
                  textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
            {
               self.signInAction = signInAction
               User.manager.delegate = self
               
               self.presentedViewController?.dismiss(animated: true, completion: {
                  self.present(signInAlert, animated: true, completion: nil)
               })
               return
            }
            
            // Other errors
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true) {
            self.didTapPlaceOrderButton()
         }
      }
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}
