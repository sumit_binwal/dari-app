//
//  UserViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/31/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, ProfileView {
   
   var user: User
   var onNameSelect: (() -> Void)?
   var onPhoneSelect: (() -> Void)?
   var onEmailSelect: (() -> Void)?
   var onChangePasswordSelect: (() -> Void)?
   var onAddressSelect: (() -> Void)?
   var onManageCardsSelect: (() -> Void)?
   var onSignOut: (() -> Void)?
   
   lazy var userView: UserView = .init(user: self.user)
   
   // MARK: - Init and Deinit
   
   init(user: User) {
      self.user = user
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   deinit { NotificationCenter.default.removeObserver(self) }
   
   // MARK: View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.backgroundColor = .background
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.userView)
      self.userView.userViewDelegate = self
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleUserDidChangeNotification),
         name: User.userDidChangeNotification,
         object: nil
      )
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.smoothlyDeselectRows(tableView: self.userView)
   }
   
   // MARK: - Actions and Notifications
   
   @objc
   private func handleUserDidChangeNotification() {
      guard let user = User.current else { return }
      
      self.user = user
      self.userView.user = user
   }
}

// MARK: - UserViewDelegate
extension UserViewController: UserViewDelegate {
   func userView(didRequestChangeOf field: UserView.Field) {
      switch field {
      case .name: self.onNameSelect?()
      case .phone: self.onPhoneSelect?()
      case .email: self.onEmailSelect?()
      case .changePassword: self.onChangePasswordSelect?()
      case .address: self.onAddressSelect?()
      default: break
      }
   }
   
   func userView(didRequestChangeUsingFormView formView: RegisterFormView) {
      let viewController = UserChangeViewController(formView: formView)
      DispatchQueue.main.async {
         self.navigationController?.pushViewController(viewController, animated: true)
      }
   }
   
   func userViewDidRequestCardManagement() {
      onManageCardsSelect?()
   }
   
   func userViewDidRequestLanguageChange(indexPath: IndexPath) {
      self.userView.deselectRow(at: indexPath, animated: true)
      
      let alert: UIAlertController = {
         let languageIsArabic: Bool = Environment.languageIsArabic
         
         let alert = UIAlertController(
            title: "changeLanguageQuery".localized,
            message: "changeLanguageEffect".localized,
            preferredStyle: .actionSheet
         )
         
         alert.addAction(UIAlertAction(
            title: "yes".localized,
            style: .destructive,
            handler: { [weak self] _ in
               Environment.language = languageIsArabic ? "en" : "ar"
               
               let restartAlert = UIAlertController(
                  title: nil,
                  message: "changeLanguageNotification".localized,
                  preferredStyle: .alert
               )
               
               self?.present(restartAlert, animated: true, completion: nil)
         }))
         alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil))
         
         return alert
      }()
      
      DispatchQueue.main.async {
         self.present(alert, animated: true, completion: nil)
      }
   }
   
   func userViewDidRequestLogOut(indexPath: IndexPath) {
      self.userView.deselectRow(at: indexPath, animated: true)
      
      let alert: UIAlertController = {
         let alert = UIAlertController(
            title: nil,
            message: "confirmSignOut".localized,
            preferredStyle: .actionSheet
         )
         alert.addAction(UIAlertAction(
            title: "yes".localized,
            style: .destructive,
            handler: {
               _ in
               DispatchQueue.main.async {
                  self.presentActivityIndicatorController()
               }
               User.manager.delegate = self
               User.manager.logOut()
         }))
         
         alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil))
         return alert
      }()
      
      DispatchQueue.main.async {
         self.present(alert, animated: true, completion: nil)
      }
   }
}

// MARK: - UserManagerDelegate
extension UserViewController: UserManagerDelegate {
   func userManagerDidLogOutUser(completedWithError error: Error?) {
      DispatchQueue.main.async {
         // Errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onSignOut?()
         })
      }
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogInUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?, errorMessages: User.ErrorMessages?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}
