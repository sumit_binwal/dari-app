//
//  CancelationPolicyViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/16/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation
import UIKit

class CancelationPolicyViewController: UIViewController {
   // MARK: - Properties
   /// A list of (hour, refund rate) tuples. For example, a tuple of (1, 0.5) means that if the order is cancelled more than an hour before the delivery time, then 50% of the order value is refunded. Note that this list must be sorted by descending hours.
   private let cancelationRefundRates: [(Double, Double)]
   private let gracePeriod: Int
   private lazy var cases: [String] = {
      var cases = [self.text(forGracePeriodOf: self.gracePeriod)]
      
      for (index, (hour, _)) in self.cancelationRefundRates.enumerated() {
         if index == 0 {
            cases.append(self.caseText(forPeriodBetween: nil, and: hour))
         } else if index == self.cancelationRefundRates.count - 1 {
            cases.append(self.caseText(forPeriodBetween: hour, and: nil))
            continue
         }
         let earlierHour = self.cancelationRefundRates[index + 1].0
         cases.append(self.caseText(forPeriodBetween: earlierHour, and: hour))
      }
      
      return cases
   }()
   private lazy var results: [String] = {
      var results = [self.resultText(forRefundRateOf: 1)]
      
      for (index, (_, refundRate)) in self.cancelationRefundRates.enumerated() {
         results.append(self.resultText(forRefundRateOf: refundRate))
         if index == self.cancelationRefundRates.count - 1 {
            results.append(self.resultText(forRefundRateOf: 0))
         }
      }
      
      return results
   }()
   
   // MARK: - UI
   private lazy var tableView: UITableView = {
      let view = UITableView(frame: .zero, style: .grouped)
      view.translatesAutoresizingMaskIntoConstraints = false
      view.rowHeight = UITableView.automaticDimension
      view.estimatedRowHeight = 80
      
      view.delegate = self
      view.dataSource = self
      
      view.register(Cell.self, forCellReuseIdentifier: "tableViewCell")
      view.register(HeaderView.self, forHeaderFooterViewReuseIdentifier: "headerView")
      return view
   }()
   
   // MARK: - Init
   init(gracePeriod: Int, cancelationRefundRates: [(Double, Double)]) {
      self.gracePeriod = gracePeriod
      self.cancelationRefundRates = cancelationRefundRates
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.title = "cancelationPolicy".localized
      self.navigationItem.leftBarButtonItem = .init(
         title: "close".localized,
         style: .plain,
         target: self,
         action: #selector(self.didTapDoneButton)
      )
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.tableView)
   }
   
   // MARK: - Actions
   @objc
   private func didTapDoneButton() {
      self.dismiss(animated: true, completion: nil)
   }
   
   private func text(forGracePeriodOf gracePeriod: Int) -> String {
      let durationMeasurement = Measurement(value: Double(gracePeriod), unit: UnitDuration.minutes)
      let formatter = MeasurementFormatter()
      formatter.unitStyle = .long
      formatter.unitOptions = .providedUnit
      formatter.numberFormatter = .integer
      
      let duration = formatter.string(from: durationMeasurement)
      return "\("within".localized) \(duration) \("ofPlacingOrder".localized)"
   }
   
   private func caseText(
      forPeriodBetween smallerValue: Double?,
      and largerValue: Double?
   ) -> String {
      
      let formatter = MeasurementFormatter()
      formatter.unitStyle = .long
      formatter.unitOptions = .providedUnit
      formatter.numberFormatter = .integer
      
      if let smallerValue = smallerValue, let largerValue = largerValue {
         let sm = Measurement(value: smallerValue, unit: UnitDuration.hours)
         let lm = Measurement(value: largerValue, unit: UnitDuration.hours)
         
         let ss = formatter.string(from: sm)
         let ls = formatter.string(from: lm)
         
         let ssContainsDecimals = ss.contains(.decimalDigits)
         let ssAdjusted = ssContainsDecimals ?
            NumberFormatter.integer.string(from: sm.value) : ss
         
         return "\(ssAdjusted) \("to".localized) \(ls) \("beforeService".localized)"
         
      } else if let smallerValue = smallerValue {
         let sm = Measurement(value: smallerValue, unit: UnitDuration.hours)
         let ss = formatter.string(from: sm)
         
         return "\("lessThan".localized) \(ss) \("beforeService".localized)"
         
      } else if let largerValue = largerValue {
         let lm = Measurement(value: largerValue, unit: UnitDuration.hours)
         let ls = formatter.string(from: lm)
         
         return "\("moreThan".localized) \(ls) \("beforeService".localized)"
      }
      return ""
   }
   
   private func resultText(forRefundRateOf rate: Double) -> String {
      if rate == 1 {
         return "free".localized
      }
      return "\(NumberFormatter.percent.string(from: 1 - rate)) \("ofInvoice".localized)"
   }
}

// MARK: Table View
extension CancelationPolicyViewController: UITableViewDataSource, UITableViewDelegate {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.cases.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(
         withIdentifier: "tableViewCell",
         for: indexPath
      ) as! Cell
      
      cell.caseLabel.text = self.cases[indexPath.row]
      cell.feeLabel.text = self.results[indexPath.row]
      return cell
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      let view = tableView.dequeueReusableHeaderFooterView(
         withIdentifier: "headerView"
      ) as? HeaderView ?? HeaderView(reuseIdentifier: "headerView")
      view.caseLabel.text = "ifCanceling".localized
      view.feeLabel.text = "fee".localized
      return view
   }
}

// MARK: - Cell
fileprivate class Cell: UITableViewCell {
   lazy var caseLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .prominentText
      label.numberOfLines = 3
      return label
   }()
   lazy var feeLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 15)
      label.textColor = .text
      label.numberOfLines = 3
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      return label
   }()
   
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      self.selectionStyle = .none
      self.backgroundColor = .lighterBackground
      
      self.contentView.addSubviews([self.caseLabel, self.feeLabel])
      let margin = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.caseLabel.topAnchor.constraint(equalTo: margin.topAnchor),
         self.caseLabel.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         self.caseLabel.widthAnchor.constraint(equalTo: margin.widthAnchor, multiplier: 0.5),
         self.caseLabel.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
         
         self.feeLabel.topAnchor.constraint(equalTo: margin.topAnchor),
         self.feeLabel.leadingAnchor.constraint(equalTo: self.caseLabel.trailingAnchor),
         self.feeLabel.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         self.feeLabel.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}

// MARK: - Header
fileprivate class HeaderView: UITableViewHeaderFooterView {
   lazy var caseLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 13)
      label.textColor = .subtleText
      return label
   }()
   lazy var feeLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.font = .regularFont(ofSize: 13)
      label.textColor = .subtleText
      label.textAlignment = {
         let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)
         return direction == .leftToRight ? .right : .left
      }()
      return label
   }()
   
   override init(reuseIdentifier: String?) {
      super.init(reuseIdentifier: reuseIdentifier)
      
      self.contentView.addSubviews([self.caseLabel, self.feeLabel])
      self.contentView.layoutMargins.top = 34
      let margin = self.contentView.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.caseLabel.topAnchor.constraint(equalTo: margin.topAnchor),
         self.caseLabel.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         self.caseLabel.widthAnchor.constraint(equalTo: margin.widthAnchor, multiplier: 0.5),
         self.caseLabel.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
         
         self.feeLabel.topAnchor.constraint(equalTo: margin.topAnchor),
         self.feeLabel.leadingAnchor.constraint(equalTo: self.caseLabel.trailingAnchor),
         self.feeLabel.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         self.feeLabel.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      ])
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}
