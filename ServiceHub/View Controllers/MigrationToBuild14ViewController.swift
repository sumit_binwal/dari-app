//
//  MigrationToBuild14ViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/12/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

@available(*, deprecated)
class MigrationToBuild14ViewController: UIViewController {
   // MARK: - Properties
   let email: String
   let password: String
   var isLoading: Bool = false
   var completion: ((_ successful: Bool) -> ())?
   var migrationSuccessful: Bool = false
   
   // MARK: UI
   private var indicatorView: ActivityIndicator = ActivityIndicator(
      numberOfCircles: 3,
      diameter: 15,
      spacing: 15
   )
   private lazy var activityLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "updating".localized
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      return label
   }()
   private lazy var responseLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "pastOrdersInfo".localized
      label.font = .regularFont(ofSize: 17)
      label.textColor = .prominentText
      label.textAlignment = .center
      label.numberOfLines = 0
      label.alpha = 0
      return label
   }()
   private lazy var activityStackView: UIStackView = {
      let stack = UIStackView(arrangedSubviews: [self.indicatorView, self.activityLabel])
      stack.translatesAutoresizingMaskIntoConstraints = false
      stack.axis = .vertical
      stack.alignment = .center
      stack.distribution = .fill
      stack.spacing = 20
      return stack
   }()
   private lazy var continueButton: ProminentButton = {
      let button = ProminentButton()
      button.setTitle("continue".localized, for: .normal)
      button.addTarget(self, action: #selector(self.continueButtonWasTapped), for: .touchUpInside)
      button.isHidden = true
      return button
   }()
   
   // MARK: - Init
   init(email: String, password: String, completion: ((_ successful: Bool) ->())? = nil) {
      self.email = email
      self.password = password
      self.completion = completion
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.backgroundColor = .background
      self.addBackgroundPattern()
      
      self.view.addSubviews([self.activityStackView, self.responseLabel, self.continueButton])
      let margin = self.view.layoutMarginsGuide
      NSLayoutConstraint.activate([
         self.activityStackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
         self.activityStackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
         
         self.responseLabel.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
         self.responseLabel.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
         self.responseLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
         
         self.continueButton.bottomAnchor.constraint(equalTo: margin.bottomAnchor, constant: -20),
         self.continueButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
         self.continueButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.7)
      ])
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      guard !self.isLoading else { return }
      self.getAreas()
   }
   
   // MARK: - Actions
   @objc
   private func continueButtonWasTapped() {
      User.deletePreBuild14User()
      self.completion?(self.migrationSuccessful)
   }
   
   // MARK: - Other
   private func showResponse(message: String) {
      DispatchQueue.main.async {
         self.responseLabel.text = message
         self.responseLabel.transform = .init(
            translationX: 0,
            y: abs(self.activityStackView.frame.maxY - self.responseLabel.frame.origin.y)
         )
         
         self.continueButton.transform = .init(
            translationX: 0,
            y: self.view.frame.maxY - self.continueButton.frame.origin.y
         )
         self.continueButton.isHidden = false
         
         CATransaction.begin()
         CATransaction.setCompletionBlock {
            UIView.animate(
               withDuration: 0.25,
               delay: 0,
               options: .curveEaseInOut,
               animations: {
                  self.activityStackView.transform = .init(
                     translationX: 0,
                     y: -abs(
                        self.activityStackView.frame.maxY - self.responseLabel.frame.origin.y
                     )
                  )
                  self.activityStackView.alpha = 0
                  
                  self.responseLabel.transform = .identity
                  self.responseLabel.alpha = 1
            },
               completion: nil
            )
            
            UIView.animate(
               withDuration: 0.25,
               delay: 1.5,
               options: .curveEaseInOut,
               animations: {
                  self.continueButton.transform = .identity
            },
               completion: nil
            )
         }
         self.indicatorView.stopAnimations()
         CATransaction.commit()
      }
   }
   
   private func getAreas() {
      AreaManager.default.delegate = self
      AreaManager.default.getAreas()
   }
   
   private func logIn() {
      User.manager.delegate = self
      User.manager.logIn(email: self.email, password: self.password)
   }
}

// MARK: - UserManagerDelegate
extension MigrationToBuild14ViewController: UserManagerDelegate {
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         if error != nil {
            self.migrationSuccessful = false
            self.showResponse(message: "autoLoginFailure".localized)
            return
         }
         
         self.migrationSuccessful = true
         self.showResponse(message: "pastOrdersInfo".localized)
      }
   }
   
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidRegisterUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}

// MARK: - AreaManagerDelegate
extension MigrationToBuild14ViewController: AreaManagerDelegate {
   func areaManagerDidReceiveAreas(completedWithError error: Error?) {
      if let error = error {
         let alert = UIAlertController(
            title: nil,
            message: error.localizedDescription,
            preferredStyle: .alert
         )
         alert.addAction(
            .init(
               title: "retry".localized,
               style: .default
            ) { _ in
               self.getAreas()
            }
         )
         
         DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
         }
         return
      }
      
      self.logIn()
   }
}
