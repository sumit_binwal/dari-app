//
//  AuthViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController, SignUpView {
   
   var showSignIn: Bool = true
   var showCancelButton: Bool = false
   var onSignUpComplete: ((User?) -> Void)?
   var onSignInButtonTap: (() -> Void)?
   var onCancelButtonTap: (() -> Void)?
   var onChangeAddressTap: (
      (_ address: String?, _ latitude: Double?, _ longitude: Double?) -> Void
   )?
   
   @available(*, deprecated)
   weak var delegate: AuthViewControllerDelegate?
   private lazy var authView: AuthView = .init(showLogInView: self.showSignIn)
   
   deinit {
      NotificationCenter.default.removeObserver(self)
   }
   
   // MARK: - Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      if self.showCancelButton {
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(self.cancelButtonWasTapped))
      }
      
      self.view.backgroundColor = .background
      
      self.view.addSubview(self.authView)
      NSLayoutConstraint.activate([
         self.authView.topAnchor.constraint(equalTo: self.view.topAnchor),
         self.authView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
         self.authView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
         self.authView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
      ])
      
      self.authView.registerFormViewDelegate = self
      self.authView.logInButton.addTarget(
         self,
         action: #selector(self.logInButtonWasTapped),
         for: .touchUpInside
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillChange(notification:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil
      )
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(handleKeyboardWillHideNotification),
         name: UIResponder.keyboardWillHideNotification,
         object: nil
      )
   }
   
   // MARK: - Actions
   
   @objc
   private func cancelButtonWasTapped() {
      DispatchQueue.main.async {
         self.view.firstResponder?.resignFirstResponder()
         self.onCancelButtonTap?()
      }
   }
   
   @objc
   private func logInButtonWasTapped() {
      DispatchQueue.main.async {
         self.view.firstResponder?.resignFirstResponder()
         self.onSignInButtonTap?()
      }
   }
      
   // MARK: - Other
   
   @objc
   private func handleKeyboardWillChange(notification: Notification) {
      if let userInfo = notification.userInfo,
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue)?.cgRectValue.height {
         self.authView.contentInset.bottom = keyboardHeight - view.safeAreaInsets.bottom
      }
   }
   
   @objc
   private func handleKeyboardWillHideNotification() {
      self.authView.contentInset.bottom = 0
   }
   
   func setAddress(_ address: String?, latitude: Double?, longitude: Double?) {
      self.authView.setAddress(address, latitude: latitude, longitude: longitude)
   }
}

// MARK: - UserFormViewDelegate
extension AuthViewController: UserFormViewDelegate {
   func userFormDidRequestAddress(currentAddress: String?, latitude: Double?, longitude: Double?) {
      DispatchQueue.main.async {
         self.view.firstResponder?.resignFirstResponder()
      }
      self.onChangeAddressTap?(currentAddress, latitude, longitude)
   }
   
   func userFormDidRequestPhoneNumberCountry(currentCountry: ISOCountry?) {
      let countryListVC = CountryListViewController()
      countryListVC.currentCountry = currentCountry
      countryListVC.delegate = self.authView.registerFormView
      
      DispatchQueue.main.async {
         self.present(
            UINavigationController(rootViewController: countryListVC),
            animated: true,
            completion: nil
         )
      }
   }
   
   func userForm(didSubmitWithUser user: User, password: String) {
      self.presentActivityIndicatorController()
      User.manager.delegate = self
      User.manager.register(user: user, password: password, passwordConfirmation: password)
   }
   
   func userFormDidTapForgotPassword(email: String?) {}
   func userForm(didSubmitWithEmail email: String, password: String) {}
   func userForm(didSubmitWithKeyValuePairs pairs: [String: Codable]) {}
   func userForm(didSubmitPasswordReset password: String, confirmPassword: String) {}
   func userForm(didRequestOTPWithEmail email: String) {}
   func userForm(
      didSubmitOTP otp: String,
      email: String,
      password: String,
      confirmPassword: String
   ) {}
}

// MARK: - UserManagerDelegate
extension AuthViewController: UserManagerDelegate {
   func userManagerDidRegisterUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {
      DispatchQueue.main.async {
         // Field errors
         if let errorMessages = errorMessages {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            self.authView.registerFormView.displayErrorMessages(errorMessages)
            return
         }
         
         // Other errors
         if let error = error {
            let alert = UIAlertController(
               title: nil,
               message: error.localizedDescription,
               preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.present(alert, animated: true, completion: nil)
            })
            return
         }
         
         // Success
         self.presentedViewController?.dismiss(animated: true, completion: {
            self.onSignUpComplete?(User.current)
         })
      }
   }
   
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?) {}
   func userManagerDidGetUser(completedWithError error: Error?) {}
}

protocol AuthViewControllerDelegate: AnyObject {
   func authViewControllerDidRegisterUser(authViewController: AuthViewController)
   func authViewControllerDidLogInUser(authViewController: AuthViewController)
}
