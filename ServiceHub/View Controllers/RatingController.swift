//
//  RatingController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 15/04/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

class RatingController: UIViewController, RatingView {
   
   // MARK: - UI
   
   private lazy var stackView: UIStackView = {
      let view = UIStackView(arrangedSubviews: [titleLabel, feedbackLabel, ratingWrapper])
      view.translatesAutoresizingMaskIntoConstraints = false
      view.axis = .vertical
      return view
   }()
   private lazy var titleLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "hopeToSeeYouSoon".localized
      label.textColor = .prominentText
      label.font = .mediumFont(ofSize: 18)
      label.textAlignment = .center
      label.numberOfLines = 0
      return label
   }()
   private lazy var feedbackLabel: UILabel = {
      let label = UILabel()
      label.translatesAutoresizingMaskIntoConstraints = false
      label.text = "hopeToSeeYouSoon".localized
      label.textColor = .prominentText
      label.font = .mediumFont(ofSize: 18)
      label.textAlignment = .center
      label.numberOfLines = 0
      return label
   }()
   private lazy var ratingWrapper: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview(ratingControl)
      NSLayoutConstraint.activate([
         ratingControl.topAnchor.constraint(equalTo: view.topAnchor),
         ratingControl.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         ratingControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         ratingControl.widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor, multiplier: 1)
      ])
      return view
   }()
   private lazy var ratingControl: StarRating = {
      StarRating(3, outOf: 5)
   }()
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      view.backgroundColor = .background
      setUpViews()
   }
   
   private func setUpViews() {
      view.addSubview(stackView)
      NSLayoutConstraint.activate([
         stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
         stackView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
         stackView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
      ])
   }
}
