//
//  CardsViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CardsViewController: UIViewController, CardListView {
   
   // MARK: - Properties
   
   var onAddCardButtonTapped: (() -> Void)?
   
   private lazy var tableView: UITableView = {
      let view = UITableView(frame: .zero, style: .grouped)
      
      view.translatesAutoresizingMaskIntoConstraints = false
      view.rowHeight = UITableView.automaticDimension
      view.estimatedRowHeight = 75
      view.delegate = self
      view.dataSource = self
      
      view.register(CardTableViewCell.self, forCellReuseIdentifier: "CardTableViewCell")
      
      return view
   }()
   
   /// Used to facilitate enabling and disabling the sign in action of a log in alert controller.
   private weak var signInAction: UIAlertAction?
   var cards: [Card] = Card.all
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      self.navigationItem.title = "cards".localized
      self.view.backgroundColor = .background
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.tableView)
      
      self.navigationItem.rightBarButtonItem = UIBarButtonItem(
         barButtonSystemItem: .add,
         target: self,
         action: #selector(self.addButtonWasTapped)
      )
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.smoothlyDeselectRows(tableView: tableView)
   }
   
   // MARK: - Actions
   
   @objc
   private func addButtonWasTapped() {
      onAddCardButtonTapped?()
   }
   
   @objc
   func logInAlertTextFieldDidChange(_ textField: UITextField) {
      self.signInAction?.isEnabled = !(textField.text?.trimmed ?? "").isEmpty
   }
   
   // MARK: - Other
   
   func addCard(_ card: Card) {
      self.cards.append(card)
      self.tableView.insertRows(at: [.init(row: self.cards.count - 1, section: 0)])
   }
}

// MARK: - Table View

extension CardsViewController: UITableViewDataSource, UITableViewDelegate {
   func numberOfSections(in tableView: UITableView) -> Int { 1 }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      cards.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(
         withIdentifier: "CardTableViewCell", for: indexPath)
         as? CardTableViewCell else
      {
         fatalError("[CardsVC] Failed to dequeue CardTableViewCell.")
      }
      cell.configure(with: cards[indexPath.row])
      return cell
   }
   
   func tableView(
      _ tableView: UITableView,
      commit editingStyle: UITableViewCell.EditingStyle,
      forRowAt indexPath: IndexPath
   ) {
      
      guard editingStyle == .delete else { return }
      
      self.presentActivityIndicatorController()
      
      Card.manager.delegate = self
      Card.manager.deleteCard(self.cards[indexPath.row])
   }
}


// MARK: - CardFormViewControllerDelegate
extension CardsViewController: CardFormViewControllerDelegate {
   func cardViewController(_ controller: CardFormViewController, didAddCard card: Card) {
//      DispatchQueue.main.async {
//         self.navigationController?.popToViewController(self, animated: true)
//
//         self.cards.append(card)
//         //self.collectionView.insertItems(at: [.init(item: self.cards.count - 1, section: 0)])
//         self.tableView.insertRows(at: [.init(row: self.cards.count - 1, section: 0)])
//      }
   }
   
   func cardViewController(
      _ controller: CardFormViewController,
      didTokenizeCard card: Card, saveCard: Bool
   ) {}
   
   func cardViewControllerDidCancel() {}
}

// MARK: - CardManagerDelegate
extension CardsViewController: CardManagerDelegate {
   func cardManager(
      didAddCard card: Card?,
      completedWithError error: Error?,
      errorResponse: Card.Manager.ErrorResponse?
   ) {}
   
   func cardManager(didDeleteCard card: Card, completedWithError error: Error?) {
         DispatchQueue.main.async {
            if let error = error {
               // Authentication
               if let error = error as? APIError, error == .notAuthenticated,
                  let (signInAlert, signInAction) = UIAlertController.logInAlertController(
                     presentingController: self,
                     textFieldSelector: #selector(self.logInAlertTextFieldDidChange(_:)))
               {
                  self.signInAction = signInAction
                  
                  self.presentedViewController?.dismiss(animated: true, completion: {
                      self.present(signInAlert, animated: true, completion: nil)
                  })
                  return
               }
               
               // Other Errors
               let alert = UIAlertController(
                  title: nil,
                  message: error.localizedDescription,
                  preferredStyle: .alert
               )
               alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
               
               self.presentedViewController?.dismiss(animated: true, completion: {
                  self.present(alert, animated: true, completion: nil)
               })
               return
            }
            
            // Success
            guard let row = self.cards.firstIndex(of: card) else {
               assertionFailure("[CardsVC] Failed to get index of card.")
               return
            }
            
            self.presentedViewController?.dismiss(animated: true, completion: {
               self.cards.remove(at: row)
               self.tableView.deleteRows(at: [.init(row: row, section: 0)])
            })
         }
      }
}
