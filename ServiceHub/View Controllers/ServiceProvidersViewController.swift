//
//  ServiceProvidersViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 8/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class ServiceProvidersViewController: UIViewController, ServiceProviderSelectorView {
   let data: Order.FormData
   var onServiceProviderSelect: (() -> Void)?
   
   // MARK: - UI
   private lazy var serviceProvidersView: ServiceProvidersView = {
      let view = ServiceProvidersView(data: self.data)
      view.serviceProviderViewDelegate = self
      return view
   }()
   
   // MARK: - Init
   init(data: Order.FormData) {
      self.data = data
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.serviceProvidersView)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.smoothlyDeselectRows(tableView: self.serviceProvidersView)
   }
}

extension ServiceProvidersViewController: ServiceProvidersViewDelegate {
   func serviceProvidersView(didSelectServiceProvider serviceProvider: ServiceProvider) {
      self.onServiceProviderSelect?()
   }
}
