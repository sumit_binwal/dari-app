//
//  SplashScreenViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController, SplashView {
   private var isLoadingUser: Bool = false
   private let minimumLoadDuration: Double = 2
   private var timeLoadingStarted: Date?
   var originalBackgroundAlpha: CGFloat = 0
   var completion: (() -> Void)?
   lazy var imageView: UIImageView = {
      let view = UIImageView(image: UIImage(named: "Splash Image"))
      view.translatesAutoresizingMaskIntoConstraints = false
      return view
   }()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.translatesAutoresizingMaskIntoConstraints = false
      self.view.backgroundColor = .background
      
      self.view.addSubview(self.imageView)
      NSLayoutConstraint.activate([
         self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
         self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20)
      ])
      
      self.imageView.alpha = 0
      self.imageView.transform = CGAffineTransform(translationX: 0, y: -5)
         .concatenating(CGAffineTransform(scaleX: 0.95, y: 0.95))
      
      self.addBackgroundPattern()
      let background = view.viewWithTag(1000)
      originalBackgroundAlpha = background?.alpha ?? 0
      background?.alpha = 0
      
      loadUserIfNecessary()
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      UIView.animateKeyframes(
         withDuration: 2,
         delay: 0,
         options: .calculationModeCubicPaced,
         animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.4, animations: {
                self.view.viewWithTag(1000)?.alpha = self.originalBackgroundAlpha
            })
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.8, animations: {
                self.imageView.alpha = 1
                self.imageView.transform = .identity
            })
            
         }) { _ in
         if !self.isLoadingUser {
            self.completion?()
         }
      }
      
      UIView.animateKeyframes(
         withDuration: 1,
         delay: 2,
         options: [.calculationModeCubicPaced, .autoreverse, .repeat]) {
         self.imageView.transform = CGAffineTransform(scaleX: 0.97, y: 0.97)
      }

   }
   
   private func loadUserIfNecessary() {
      guard User.current != nil else { return }
      isLoadingUser = true
      timeLoadingStarted = Date()
      User.manager.getUser()
      User.manager.delegate = self
   }
}

extension SplashScreenViewController: UserManagerDelegate {
   
   func userManagerDidGetUser(completedWithError error: Error?) {
      // If the splash screen isn't shown for long enough, delay completion
      let delay: Double = {
         let timeStarted = timeLoadingStarted ?? Date(timeIntervalSinceNow: -minimumLoadDuration)
         let timeToLoad = Date().timeIntervalSince(timeStarted)
         return max(minimumLoadDuration - timeToLoad, 0)
      }()
      
      DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
         self.completion?()
      }
   }
   
   func userManagerDidRegisterUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidLogOutUser(completedWithError error: Error?) {}
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?
   ) {}
}
