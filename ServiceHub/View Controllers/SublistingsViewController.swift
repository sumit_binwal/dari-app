//
//  SublistingsViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/12/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class SublistingsViewController: UIViewController, ListingsView {
   
   let listing: Listing
   var sublistings: [Listing]
   var onListingSelect: ((_ listing: Listing) -> Void)?
   
   private lazy var tableView: UITableView = {
      let view = UITableView(frame: .zero, style: .grouped)
      view.translatesAutoresizingMaskIntoConstraints = false
      view.showsVerticalScrollIndicator = false
      view.rowHeight = UITableView.automaticDimension
      view.estimatedRowHeight = 60
      view.separatorInset = .zero
      view.tableFooterView = UIView()
      
      view.delegate = self
      view.dataSource = self
      view.register(SublistingTableViewCell.self, forCellReuseIdentifier: "SublistingTableViewCell")
      
      return view
   }()

   // MARK: - Init
   
   init(listing: Listing) {
      self.listing = listing
      self.sublistings = listing.sublistings
      super.init(nibName: nil, bundle: nil)
      self.title = listing.title
   }

   required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }

   // MARK: View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()

      self.view.addFillingSubview(self.tableView)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.smoothlyDeselectRows(tableView: self.tableView)
   }
}

// MARK: - Table View

extension SublistingsViewController: UITableViewDelegate, UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      self.sublistings.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView
         .dequeueReusableCell(withIdentifier: "SublistingTableViewCell", for: indexPath)
         as? SublistingTableViewCell else
      {
         fatalError("[SublistingsView] Failed to dequeue SublistingTableViewCell.")
      }
      cell.configure(for: self.sublistings[indexPath.row])
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let listing = self.sublistings[indexPath.row]
      self.onListingSelect?(listing)
   }
}
