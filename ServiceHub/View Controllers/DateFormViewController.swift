//
//  DateFormViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/19/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class DateFormViewController: UIViewController, DateFormModuleView {
   
   let data: Order.FormData
   var onSupportButtonTap: (() -> Void)?
   var onNextButtonTap: (() -> Void)?
   
   // MARK: - UI
   
   private lazy var dateFormView: DateFormView = {
      let view = DateFormView(data: self.data)
      view.dateFormViewDelegate = self
      return view
   }()
   private lazy var orderPriceView: OrderPriceView = {
      let view = OrderPriceView()
      view.nextButton.addTarget(self, action: #selector(self.didTapNextButton), for: .touchUpInside)
      view.priceLabel.attributedText = self.data.attributedStringForTotal(
         currencyFont: .mediumFont(ofSize: 13),
         numberFont: .boldFont(ofSize: 17)
      )
      return view
   }()
   
   // MARK: - Init
   
   init(data: Order.FormData) {
      self.data = data
      super.init(nibName: nil, bundle: nil)
      self.setUp()
   }
   
   required init?(coder: NSCoder) {
      fatalError("init from coder not implemented")
   }
   
   private func setUp() {
      self.hidesBottomBarWhenPushed = true
      self.title =  {
         let title = self.data.selectedService.titleAsVariant
         return title.isEmpty ? self.data.selectedService.name : title
      }()
   }
   
   deinit { NotificationCenter.default.removeObserver(self) }
   
   // MARK: - View Life Cycle
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationItem.rightBarButtonItem = UIBarButtonItem(
         image: UIImage(named: "Support"),
         style: .plain,
         target: self,
         action: #selector(self.didTapSupportButton)
      )
      self.view.backgroundColor = .background
      
      self.view.addFillingSubviewConstrainedBySafeAreaOnSides(self.dateFormView)
      self.view.addSubview(self.orderPriceView)
      NSLayoutConstraint.activate([
         self.orderPriceView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
         self.orderPriceView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
         self.orderPriceView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
      ])
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleKeyboardFrameWillChange(_:)),
         name: UIResponder.keyboardWillChangeFrameNotification,
         object: nil
      )
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.handleKeyboardWillHide(_:)),
         name: UIResponder.keyboardWillHideNotification,
         object: nil
      )
   }
   
   override func viewDidDisappear(_ animated: Bool) {
      super.viewDidDisappear(animated)
      NotificationCenter.default.removeObserver(self)
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      self.dateFormView.contentInset.bottom =
         self.orderPriceView.bounds.height - self.view.safeAreaInsets.bottom
   }
   
   // MARK: - Actions
   @objc
   private func didTapSupportButton() {
      self.onSupportButtonTap?()
   }
   
   @objc
   private func didTapNextButton() {
      guard self.data.weekdaySelectionCountIsValid else {
         self.dateFormView.showWeekdaySelectionInvalidWarning()
         return
      }
      self.onNextButtonTap?()
   }
   
   // MARK: Other
   
   @objc
   private func handleKeyboardFrameWillChange(_ notification: Notification) {
      guard let userInfo = notification.userInfo else {
         assertionFailure("[OrderSummaryVC] Failed to get userInfo from keyboard change notification.")
         return
      }
      DispatchQueue.main.async {
         let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
         let duration =
            userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
         let animationCurve: UIView.AnimationOptions =
            userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UIView.AnimationOptions ?? .curveEaseOut
         
         self.additionalSafeAreaInsets.bottom += keyboardHeight - self.view.safeAreaInsets.bottom
         
         UIView.animate(
            withDuration: duration,
            delay: 0,
            options: animationCurve,
            animations: {
               self.view.layoutIfNeeded()
         })
      }
      
   }
   
   @objc
   private func handleKeyboardWillHide(_ notification: Notification) {
      guard let userInfo = notification.userInfo else {
         assertionFailure("[OrderSummaryVC] Failed to get userInfo from keyboard change notification.")
         return
      }
      
      DispatchQueue.main.async {
         let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
         let animationCurve: UIView.AnimationOptions =
            userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UIView.AnimationOptions ?? .curveEaseOut
         
         self.additionalSafeAreaInsets.bottom = 0
         UIView.animate(
            withDuration: duration,
            delay: 0,
            options: animationCurve,
            animations: {
               self.view.layoutIfNeeded()
         })
      }
   }
}

// MARK: - DateFormViewDelegate
extension DateFormViewController: DateFormViewDelegate {}
