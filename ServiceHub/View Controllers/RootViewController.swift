//
//  RootViewController.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/11/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, RootView {
   private lazy var containerView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.backgroundColor = .background
      return view
   }()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.addFillingSubview(self.containerView)
   }
   
   func setRootModule(_ module: Presentable?, with transition: Transition) {
      guard let viewController = module?.toPresent() else { return }
      let fromViewController = self.children.first
      
      transition.open(
         viewController,
         from: fromViewController,
         in: self.containerView,
         containerViewController: self
      )
   }
   
   public func requestNotificationAuthorizationIfNeeded() {
      let notificationCenter = UNUserNotificationCenter.current()
      notificationCenter.getNotificationSettings { (settings) in
         
         guard settings.authorizationStatus == .notDetermined else { return }
         
         notificationCenter.requestAuthorization(
            options: [.badge, .sound, .alert], completionHandler: { (granted, error) in
               if let error = error {
                  print("[AppDelegate] Error requesting notification authorization: \(error.localizedDescription)")
               }
               TrackingManager.updateNotificationPermissionStatus(permissionGranted: granted)
         })
      }
   }
}

// MARK: - Migration
extension RootViewController {
   // Remove in build 20.
   private func presentMigrationViewController(
      email: String,
      password: String,
      completion: ((_ successful: Bool) ->())?
   ) {
      guard let oldVC = self.children.first,
         let oldView = self.containerView.subviews.first else { return }
      
      let vc = MigrationToBuild14ViewController(
         email: email,
         password: password,
         completion: completion
      )
      
      self.addChild(vc)
      oldVC.willMove(toParent: nil)
      
      guard let newView = vc.view else { return }
      newView.translatesAutoresizingMaskIntoConstraints = false
      newView.alpha = 0
      newView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
      
      self.containerView.addFillingSubview(newView)
      
      UIView.animate(
         withDuration: 0.3,
         delay: 0,
         options: .curveEaseInOut,
         animations: {
            oldView.transform = newView.transform.inverted()
            newView.alpha = 1
            newView.transform = .identity
      }) { _ in
         vc.didMove(toParent: self)
         oldView.removeFromSuperview()
         oldVC.removeFromParent()
      }
   }
}
