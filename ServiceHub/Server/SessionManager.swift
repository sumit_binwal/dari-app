//
//  SessionManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/23/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation
import UIKit

typealias TaskCompletionHandler = (Data?, URLResponse?, Error?) -> ()
typealias DownloadCompletionHandler = (URL?) -> ()

class SessionManager: NSObject {
   
   // MARK: - Properties
   
   let configuration: URLSessionConfiguration
   lazy var session: URLSession = {
      return URLSession(
         configuration: self.configuration,
         delegate: SessionManagerDelegate(),
         delegateQueue: .main
      )
   }()
   
   lazy var downloadsSession: URLSession = .init(
      configuration: .ephemeral,
      delegate: SessionManagerDownloadDelegate(),
      delegateQueue: nil
   )
   
   static var `default`: SessionManager = .init()
   
   // MARK: - Init and Deinit
   
   init(configuration: URLSessionConfiguration) {
      self.configuration = configuration
      super.init()
   }
   
   convenience override init() {
      let configuration: URLSessionConfiguration = .default
      
      if let token = Self.token {
         configuration.httpAdditionalHeaders = ["Authorization": "Token \(token)"]
      }
      
      self.init(configuration: configuration)
   }
   
   deinit {
      self.session.invalidateAndCancel()
      self.downloadsSession.invalidateAndCancel()
   }
   
   // MARK: Token
   
   private static let secureStore = SecureStore(
      secureStoreQueryable: GenericPasswordQueryable(service: "Dari")
   )
   
   // CSRF might not be necessary.
   private class var csrfTokenHeader: String { "X-CSRFToken" }
   private static var csrfToken: String? {
      get { try? self.secureStore.getValue(for: self.csrfTokenHeader) }
      set {
         if let value = newValue {
            try? self.secureStore.setValue(value, for: self.csrfTokenHeader)
         } else {
            try? self.secureStore.removeValue(for: self.csrfTokenHeader)
         }
      }
   }
   private class var tokenKey: String { "authToken" }
   private class var token: String? {
      get { try? self.secureStore.getValue(for: self.tokenKey) }
      set {
         if let value = newValue {
            try? self.secureStore.setValue(value, for: self.tokenKey)
         } else {
            try? self.secureStore.removeValue(for: self.tokenKey)
         }
      }
   }
   
   func saveCSRFToken() {
      guard let url = URL(string: ServerURL.baseURLString),
         let storage = HTTPCookieStorage.shared.cookies(for: url),
         let cookie = storage.first(where: { $0.name == "csrftoken" })
         else {
            return
      }
      Self.csrfToken = cookie.value
   }
   
   func removeCSRFToken() {
      Self.csrfToken = nil
      
      if let url = URL(string: ServerURL.baseURLString),
         var storage = HTTPCookieStorage.shared.cookies(for: url),
         let cookieIndex = storage.firstIndex(where: { $0.name == "csrftoken" }) {
         storage.remove(at: cookieIndex)
      }
   }
   
   // MARK: - Other
   
   @discardableResult
   func get(
      request: URLRequest,
      completionHandler handler: @escaping TaskCompletionHandler
   ) -> URLSessionTask {
      var request = request
       request.timeoutInterval = 60
      addGetHeaders(to: &request)
      
      let task = self.session.dataTask(with: request)
      (self.session.delegate as? SessionManagerDelegate)?.appendHandler(handler, task: task)
      task.resume()
      
      return task
   }
   
   @discardableResult
   func post(
      request: URLRequest,
      completionHandler handler: @escaping TaskCompletionHandler
   ) -> URLSessionTask {
      var request = request
      addPostHeaders(to: &request)
      return self.get(request: request, completionHandler: handler)
   }
   
   @discardableResult
   func download(
      url: URL,
      completionHandler handler: @escaping DownloadCompletionHandler
   ) -> URLSessionDownloadTask {
      let task = self.downloadsSession.downloadTask(with: url)
      (self.downloadsSession.delegate as? SessionManagerDownloadDelegate)?.appendHandler(
         handler,
         task: task
      )
      task.resume()
      
      return task
   }
   
   private func addGetHeaders(to request: inout URLRequest) {
      if let token = Self.csrfToken {
         request.addValue(token, forHTTPHeaderField: Self.csrfTokenHeader)
      }
      if let language = Environment.language {
         request.addValue(language, forHTTPHeaderField: "Accept-Language")
      }
      if let userKey = User.key {
         request.addValue(userKey, forHTTPHeaderField: "User-Key")
      }
      if let district = District.current {
         request.addValue(String(district.id), forHTTPHeaderField: "district")
      }
      if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
         request.addValue(appVersion, forHTTPHeaderField: "App-Version")
      }
      
      let device = UIDevice.current
      request.addValue(device.modelName, forHTTPHeaderField: "Device-Name")
      request.addValue(device.systemName, forHTTPHeaderField: "Device-Os")
      request.addValue(device.systemVersion, forHTTPHeaderField: "Device-Os-Version")
   }
   
   private func addPostHeaders(to request: inout URLRequest) {
      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      request.addValue("application/json", forHTTPHeaderField: "Accept")
   }
   
   func setToken(_ token: String?) {
      Self.token = token
      
      if let token = token {
         self.configuration.httpAdditionalHeaders = ["Authorization": "Token \(token)"]
      } else {
         self.configuration.httpAdditionalHeaders = nil
      }
      
      let delegate = self.session.delegate
      
      self.session = .init(
         configuration: self.configuration,
         delegate: delegate,
         delegateQueue: .main
      )
   }
   
   func setUserKey(_ userKey: String?) {
      let adjustedKey = userKey?.lowercased() == "none" ? nil : userKey
      User.key = adjustedKey
   }
}

// MARK: - Delegate

class SessionManagerDelegate: NSObject, URLSessionDelegate, URLSessionDataDelegate {
   private var handlers = [Int: TaskCompletionHandler]()
   private var receivedData = [Int: Data]()
   
   func appendHandler(_ handler: @escaping TaskCompletionHandler, task: URLSessionTask) {
      self.handlers[task.taskIdentifier] = handler
   }
   
   func urlSession(
      _ session: URLSession,
      dataTask: URLSessionDataTask,
      didReceive response: URLResponse,
      completionHandler: @escaping (URLSession.ResponseDisposition) -> Void
   ) {
      
      if let response = response as? HTTPURLResponse {
         
         // Remove token if it isn't valid.
         if response.statusCode == 401 {
            Environment.sessionManager.setToken(nil)
            Environment.sessionManager.removeCSRFToken()
         }
         
         // Update user key
         let key = response.allHeaderFields["User-Key"] as? String
         Environment.sessionManager.setUserKey(key)
      }
      
      completionHandler(.allow)
   }
   
   func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
      self.receivedData[dataTask.taskIdentifier, default: Data()].append(data)
   }
   
   func urlSession(
      _ session: URLSession,
      task: URLSessionTask,
      didCompleteWithError error: Error?
   ) {
      let id = task.taskIdentifier
      let data = self.receivedData[id]
      self.receivedData[id] = nil
      let handler = self.handlers[id]
      self.handlers[id] = nil
      handler?(data, task.response, error)
   }
}

class SessionManagerDownloadDelegate: NSObject, URLSessionDownloadDelegate {
   private var handlers = [Int: DownloadCompletionHandler]()
   
   func appendHandler(
      _ handler: @escaping DownloadCompletionHandler,
      task: URLSessionDownloadTask
   ) {
      self.handlers[task.taskIdentifier] = handler
   }
   
   func urlSession(
      _ session: URLSession,
      downloadTask: URLSessionDownloadTask,
      didFinishDownloadingTo location: URL
   ) {
      let id = downloadTask.taskIdentifier
      let handler = self.handlers[id]
      self.handlers[id] = nil
      
      handler?(location)
   }
   
   func urlSession(
      _ session: URLSession,
      task: URLSessionTask,
      didCompleteWithError error: Error?
   ) {
      let id = task.taskIdentifier
      let handler = self.handlers[id]
      self.handlers[id] = nil
      
      if error != nil { handler?(nil) }
   }
}
