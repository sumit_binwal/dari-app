//
//  Errors.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/24/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

public enum APIError: Error, LocalizedError, Equatable {
   case unknown
   case nilResponse
   case nilData
   case dataDeserialization
   case objectSerialization
   case cardTokenization(codes: [String])
   case paymentFailure
   case cardDeclined
   case resourceChanged
   case notFound
   case notAuthenticated
   case nilDistrict
   case server
   
   public var errorDescription: String? {
      switch self {
      case .unknown: return "unknownError".localized
      case .nilResponse: return "nilResponseError".localized
      case .nilData: return "nilDataError".localized
      case .dataDeserialization: return "dataDeserializationError".localized
      case .objectSerialization: return "objectSerializationError".localized
      case .cardTokenization: return "cardTokenizationError".localized
      case .paymentFailure: return "paymentFailure".localized
      case .cardDeclined: return "cardDeclined".localized
      case .resourceChanged: return "resourceChangedError".localized
      case .notFound: return "notFoundError".localized
      case .notAuthenticated: return "notAuthenticatedError".localized
      case .nilDistrict: return "nilDistrictError".localized
      case .server: return "serverError".localized
      }
   }
   
   public static func ==(lhs: Self, rhs: Self) -> Bool {
      switch (lhs, rhs) {
      case (.unknown, .unknown): return true
      case (.nilResponse, .nilResponse): return true
      case (.nilData, .nilData): return true
      case (.dataDeserialization, .dataDeserialization): return true
      case (.objectSerialization, .objectSerialization): return true
      case (.cardTokenization(let leftStrings), .cardTokenization(let rightStrings)):
         return leftStrings == rightStrings
      case (.paymentFailure, .paymentFailure): return true
      case (.cardDeclined, .cardDeclined): return true
      case (.resourceChanged, .resourceChanged): return true
      case (.notFound, .notFound): return true
      case (.notAuthenticated, .notAuthenticated): return true
      case (.nilDistrict, .nilDistrict): return true
      case (.server, .server): return true
      default: return false
      }
   }
}
