//
//  Order+ItemParameters.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

extension Order {
   struct ItemParametersDTO: Codable {
      let itemID: Int
      let serviceOptionID: Int?
      let quantity: Int
      let flatDetails: FlatItemParametersDTO?
      let agentDependentDetails: AgentDependentItemParametersDTO?
      let residenceDependentDetails: ResidenceDependentItemParametersDTO?
      let areaDetails: AreaItemParametersDTO?
      
      private enum CodingKeys: String, CodingKey {
         case itemID = "item"
         case serviceOptionID = "service_option"
         case quantity = "item_count"
         case flatDetails = "flat_item_details"
         case agentDependentDetails = "agent_time_item_details"
         case residenceDependentDetails = "residence_item_details"
         case areaDetails = "area_item_details"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard let item = invoiceItem.parameters[.item] as? Item else { return nil }
         
         self.itemID = item.id
         self.serviceOptionID = (invoiceItem.parameters[.serviceOption] as? Item.ServiceOption)?.id
         self.quantity = invoiceItem.quantity
         self.flatDetails = FlatItemParametersDTO(from: invoiceItem)
         self.agentDependentDetails = AgentDependentItemParametersDTO(from: invoiceItem)
         self.residenceDependentDetails = ResidenceDependentItemParametersDTO(from: invoiceItem)
         self.areaDetails = AreaItemParametersDTO(from: invoiceItem)
      }
   }
   
   struct FlatItemParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.item] is Item,
            let pricePoint = invoiceItem.pricePointValue as? FlatPricePoint else { return nil }
         self.pricePointMetadata = pricePoint.metadata
      }
   }
   
   struct AgentDependentItemParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      let agentCount: Int
      let duration: Int
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
         case agentCount = "agent_count"
         case duration
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.item] is Item,
            let pricePoint = invoiceItem.pricePointValue as? AgentTimePricePoint,
            let agentCount = invoiceItem.parameters[.agentCount] as? Int,
            let duration = invoiceItem.parameters[.duration] as? Int else { return nil }
         
         self.pricePointMetadata = pricePoint.metadata
         self.agentCount = agentCount
         self.duration = duration
      }
   }
   
   struct ResidenceDependentItemParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      let residenceType: ResidenceType
      let roomCount: Int
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
         case residenceType = "residence_type"
         case roomCount = "room_count"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.item] is Item,
            let pricePoint = invoiceItem.pricePointValue as? ResidencePricePoint,
            let residenceType = invoiceItem.parameters[.residenceType] as? ResidenceType,
            let roomCount = invoiceItem.parameters[.roomCount] as? Int else { return nil }
         
         self.pricePointMetadata = pricePoint.metadata
         self.residenceType = residenceType
         self.roomCount = roomCount
      }
   }
   
   struct AreaItemParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      let areaAsString: String
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
         case areaAsString = "area"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.item] is Item,
            let pricePoint = invoiceItem.pricePointValue as? AreaPricePoint,
            let area = invoiceItem.parameters[.area] as? Double else { return nil }
         
         self.pricePointMetadata = pricePoint.metadata
         self.areaAsString = String(area)
      }
   }
}
