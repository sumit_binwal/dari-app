//
//  UserManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/30/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation
import Security
import FBSDKCoreKit

extension User {
   class Manager {
      static let `default` = Manager()
      weak var delegate: UserManagerDelegate?
   }
}

// MARK: - Register
extension User.Manager{
   func register(user: User, password: String, passwordConfirmation: String) {
      var request = URLRequest(url: ServerURL.registerUser.url)
      request.httpMethod = "POST"
      
      guard var json = user.dictionary else {
         assertionFailure("[UserManager] Failed to generate dictionary from user.")
         self.delegate?.userManagerDidRegisterUser(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      json[User.CodingKeys.id.rawValue] = nil
      json["password1"] = password
      json["password2"] = passwordConfirmation
      
      do {
         request.httpBody = try JSONSerialization.data(
            withJSONObject: json,
            options: .fragmentsAllowed)
      } catch {
         self.delegate?.userManagerDidRegisterUser(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidRegisterUser(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidRegisterUser(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.userManagerDidRegisterUser(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         do {
            switch response.statusCode {
            case 200...299:
               struct ResponseData: Codable {
                  let key: String
                  let user: User
               }
               
               let responseData = try JSONDecoder().decode(ResponseData.self, from: data)
               Environment.sessionManager.setToken(responseData.key)
               self?.saveUser(responseData.user, isNewlyRegistered: true)
               self?.delegate?.userManagerDidRegisterUser(
                  completedWithError: nil,
                  errorMessages: nil
               )
               
            case 400...499:
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidRegisterUser(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
               
            case 500...599:
               self?.delegate?.userManagerDidRegisterUser(
                  completedWithError: APIError.server,
                  errorMessages: nil
               )
               
            default:
               self?.delegate?.userManagerDidRegisterUser(
                  completedWithError: APIError.unknown,
                  errorMessages: nil
               )
            }
            
         } catch {
            self?.delegate?.userManagerDidRegisterUser(
               completedWithError: APIError.dataDeserialization,
               errorMessages: nil
            )
         }
      }
   }
}

// MARK: - Log In

extension User.Manager {
   func logIn(email: String, password: String) {
      var request = URLRequest(url: ServerURL.logIn.url)
      request.httpMethod = "POST"
      
      struct Credentials: Codable {
         let email: String
         let password: String
      }
      
      let credentials = Credentials(email: email, password: password)
      do {
         request.httpBody = try JSONEncoder().encode(credentials)
      } catch {
         self.delegate?.userManagerDidLogInUser(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidLogInUser(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidLogInUser(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.userManagerDidLogInUser(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         do {
            switch response.statusCode {
            case 200...299:
               struct ResponseData: Codable {
                  let key: String
                  let user: User
               }
               
               let responseData = try JSONDecoder().decode(ResponseData.self, from: data)
               Environment.sessionManager.setToken(responseData.key)
               self?.saveUser(responseData.user, isNewlyRegistered: false)
               self?.delegate?.userManagerDidLogInUser(
                  completedWithError: nil,
                  errorMessages: nil
               )
               
            case 400...499:
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidLogInUser(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
               
            case 500...599:
               self?.delegate?.userManagerDidLogInUser(
                  completedWithError: APIError.server,
                  errorMessages: nil
               )
               
            default:
               self?.delegate?.userManagerDidLogInUser(
                  completedWithError: APIError.unknown,
                  errorMessages: nil
               )
            }
         } catch {
            self?.delegate?.userManagerDidLogInUser(
               completedWithError: APIError.dataDeserialization,
               errorMessages: nil
            )
            return
         }
      }
   }
}

// MARK: - Change Password

extension User.Manager {
   func changePassword(_ password: String, confirmPassword: String) {
      var request = URLRequest(url: ServerURL.changePassword.url)
      request.httpMethod = "POST"
      
      do {
         request.httpBody = try JSONSerialization.data(
            withJSONObject: ["new_password1": password, "new_password2": confirmPassword],
            options: .fragmentsAllowed)
      } catch {
         self.delegate?.userManagerDidUpdateUser(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: nil,
               errorMessages: nil
            )
            
         case 401, 403:
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.notAuthenticated,
               errorMessages: nil
            )
            
         case 400...499:
            do {
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
            } catch {
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.dataDeserialization,
                  errorMessages: nil
               )
            }
            
         case 500...599:
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.server,
               errorMessages: nil
            )
            
         default:
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.unknown,
               errorMessages: nil
            )
         }
      }
   }
}

// MARK: - Reset Password
extension User.Manager {
   func resetPassword(email: String) {
      var request = URLRequest(url: ServerURL.resetPassword.url)
      request.httpMethod = "POST"
      
      struct DTO: Encodable {
         let email: String
      }
      
      let dto = DTO(email: email)
      do {
         request.httpBody = try JSONEncoder().encode(dto)
      } catch {
         self.delegate?.userManagerDidSubmitPasswordReset(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         
         guard let data = data else {
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: nil,
               errorMessages: nil
            )
            
         case 400...499:
            do {
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
            } catch {
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.notFound,
                  errorMessages: nil
               )
            }
            
         case 500...599:
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: APIError.server,
               errorMessages: nil
            )
            
         default:
            self?.delegate?.userManagerDidSubmitPasswordReset(
               completedWithError: APIError.unknown,
               errorMessages: nil
            )
         }
      }
   }
   
   func submit(otp: String, email: String, password: String, confirmPassword: String) {
      var request = URLRequest(url: ServerURL.passwordResetConfirm.url)
      request.httpMethod = "POST"
      
      struct DTO: Encodable {
         let otp: String
         let email: String
         let password: String
         let confirmPassword: String
         
         enum CodingKeys: String, CodingKey {
            case otp = "password"
            case email
            case password = "new_password1"
            case confirmPassword = "new_password2"
         }
      }
      
      let dto = DTO(otp: otp, email: email, password: password, confirmPassword: confirmPassword)
      do {
         request.httpBody = try JSONEncoder().encode(dto)
      } catch {
         self.delegate?.userManagerDidResetPassword(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidResetPassword(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidResetPassword(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.userManagerDidResetPassword(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            self?.logIn(email: email, password: password)
            
         case 400...499:
            do {
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidResetPassword(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
            } catch {
               self?.delegate?.userManagerDidResetPassword(
                  completedWithError: APIError.notFound,
                  errorMessages: nil
               )
            }
            
         case 500...599:
            self?.delegate?.userManagerDidResetPassword(
               completedWithError: APIError.server,
               errorMessages: nil
            )
            
         default:
            self?.delegate?.userManagerDidResetPassword(
               completedWithError: APIError.unknown,
               errorMessages: nil
            )
         }
      }
   }
}

// MARK: - Get User

extension User.Manager {
   func getUser() {
      guard User.current != nil else {
         delegate?.userManagerDidGetUser(completedWithError: nil)
         return
      }
      
      let request = URLRequest(url: ServerURL.user.url)
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidGetUser(completedWithError: error)
            return
         }
         
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidGetUser(completedWithError: APIError.nilResponse)
            return
         }
         
         guard let data = data else {
            self?.delegate?.userManagerDidGetUser(completedWithError: APIError.nilData)
            return
         }
         
         do {
            switch response.statusCode {
            case 200...299:
               let user = try JSONDecoder().decode(User.self, from: data)
               self?.saveUser(user, isNewlyRegistered: false)
               self?.delegate?.userManagerDidGetUser(completedWithError: nil)
               
            case 401, 403:
               self?.delegate?.userManagerDidGetUser(completedWithError: APIError.notAuthenticated)
               
            case 400...499:
               self?.delegate?.userManagerDidGetUser(completedWithError: APIError.notFound)
               
            case 500...599:
               self?.delegate?.userManagerDidGetUser(completedWithError: APIError.server)
               
            default:
               self?.delegate?.userManagerDidGetUser(completedWithError: APIError.unknown)
            }
         } catch {
            self?.delegate?.userManagerDidGetUser(completedWithError: APIError.dataDeserialization)
            return
         }
      }
   }
}

// MARK: - Update User

extension User.Manager {
   func update(userAttributes attributes: [String: Codable]) {
      var request = URLRequest(
         url: ServerURL.user.url
      )
      request.httpMethod = "PATCH"
      
      do {
         request.httpBody = try JSONSerialization.data(
            withJSONObject: attributes,
            options: .fragmentsAllowed)
      } catch {
         self.delegate?.userManagerDidUpdateUser(
            completedWithError: APIError.objectSerialization,
            errorMessages: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: error,
               errorMessages: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.nilResponse,
               errorMessages: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.nilData,
               errorMessages: nil
            )
            return
         }
         
         do {
            switch response.statusCode {
            case 200...299:
               let user = try JSONDecoder().decode(User.self, from: data)
               self?.saveUser(user, isNewlyRegistered: false)
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: nil,
                  errorMessages: nil
               )
               
            case 401, 403:
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.notAuthenticated,
                  errorMessages: nil
               )
               
            case 400...499:
               let errorMessages = try JSONDecoder().decode(User.ErrorMessages.self, from: data)
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.notFound,
                  errorMessages: errorMessages
               )
               
            case 500...599:
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.server,
                  errorMessages: nil
               )
               
            default:
               self?.delegate?.userManagerDidUpdateUser(
                  completedWithError: APIError.unknown,
                  errorMessages: nil
               )
            }
         } catch {
            self?.delegate?.userManagerDidUpdateUser(
               completedWithError: APIError.dataDeserialization,
               errorMessages: nil
            )
            return
         }
      }
   }
}

// MARK: - Log Out
extension User.Manager {
   func logOut() {
      var request = URLRequest(url: ServerURL.logOut.url)
      request.httpMethod = "POST"
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.userManagerDidLogOutUser(completedWithError: error)
            return
         }
         
         // Store the current district to make sure there's a current district.
         if let districtID = User.current?.profile.districtID,
            let district = District.getDistrict(withID: districtID)
         {
            District.current = district
         }
         
         User.current = nil
         Environment.sessionManager.setToken(nil)
         Environment.sessionManager.removeCSRFToken()
         self?.postUserChange()
         
         TrackingManager.logOutUser()
         self?.delegate?.userManagerDidLogOutUser(completedWithError: nil)
      }
   }
   
   // MARK: - Helper Functions
   func postUserChange(object: Any? = nil) {
      DispatchQueue.main.async {
         NotificationCenter.default.post(name: User.userDidChangeNotification, object: object)
      }
   }
   
   private func saveUser(_ user: User, isNewlyRegistered: Bool) {
      User.current = user
      self.postUserChange()
      Environment.sessionManager.saveCSRFToken()
      
      TrackingManager.registerUser(user)
      TrackingManager.updateUser(user)
      
      if isNewlyRegistered {
         EventLogger.log(.didSignUp, parameters: [.signUpMethod: "Email"])
      }
   }
}

// MARK: - Protocol
protocol UserManagerDelegate: AnyObject {
   func userManagerDidRegisterUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   func userManagerDidLogInUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   func userManagerDidGetUser(completedWithError error: Error?)
   func userManagerDidLogOutUser(completedWithError error: Error?)
   func userManagerDidUpdateUser(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   func userManagerDidSubmitPasswordReset(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
   func userManagerDidResetPassword(
      completedWithError error: Error?,
      errorMessages: User.ErrorMessages?)
}
