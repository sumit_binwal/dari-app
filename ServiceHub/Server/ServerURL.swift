//
//  ServerURL.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

enum ServerURL: String {
   static let baseURLString: String = Environment.serverBaseURL
   private static let api: String = "api/"
   
   // User
   case getDistricts = "v1/areas/"
   case registerUser = "v1/auth/registration/"
   case user = "v1/auth/user/"
   case changePassword = "v1/auth/password/change/"
   case resetPassword = "v1/auth/password/reset/"
   case passwordResetConfirm = "v1/auth/password/reset/confirm/"
   case logIn = "v1/auth/login/"
   case logOut = "v1/auth/logout/"
   case addCard = "v1/payments/add_card/"
   case deleteCard = "v1/payments/delete_card/"
   
   // Service and Order
   case getCatalog = "v1/services/get_catalog/"
   case validatePromoCode = "v1/orders/validate_promo_code/"
   case orders = "v1/orders/"
   case placeOrder = "v2/orders/"
   case getOrderSummaries = "v1/orders/get_summaries/"
   case successfulPaymentCallback = "v1/payments/callback/success/"
   case failedPaymentCallback = "v1/payments/callback/failed/"
   
   // Marketing
   case updateProfile = "marketing/customer/update/"
   case updateAnonymousProfile = "marketing/anonymous_user/update/"
   case logEvent = "marketing/event/create"
   case updateNotificationToken = "marketing/customer/token/update/"
   case updateAnonymousNotificationToken = "marketing/anonymous_user/token/update/"
   
   // Other
   case checkVersion = "users/app/version/check/"
   
   var url: URL {
      let path: String = {
         switch self {
         case .updateProfile,
              .updateAnonymousProfile,
              .updateNotificationToken,
              .updateAnonymousNotificationToken: return Self.baseURLString + rawValue
         case .logEvent: return Self.baseURLString + rawValue
         case .checkVersion: return Self.baseURLString + rawValue
         default: return Self.baseURLString + Self.api + rawValue
         }
      }()
      guard let url = URL(string: path) else {
         fatalError("[ServerURL] Failed to generate URL for \(self).")
      }
      return url
   }
   
   static func getURL(fromRelativeURLString urlString: String) -> URL? {
      URL(string: urlString, relativeTo: URL(string: self.baseURLString))
   }
}
