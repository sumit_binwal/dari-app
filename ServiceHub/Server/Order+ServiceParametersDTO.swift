//
//  Order+ServiceParameters.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   /// Holds the parameters for the service of an order. Use in the order placement API.
   struct ServiceParametersDTO: Codable {
      let serviceID: Int
      let flatDetails: FlatServiceParametersDTO?
      let agentDependentDetails: AgentDependentServiceParametersDTO?
      let residenceDependentDetails: ResidenceDependentServiceParametersDTO?
      
      private enum CodingKeys: String, CodingKey {
         case serviceID = "service"
         case flatDetails = "flat_service_details"
         case agentDependentDetails = "agent_time_service_details"
         case residenceDependentDetails = "residence_service_details"
      }
      
      init(serviceID: Int) {
         self.serviceID = serviceID
         self.flatDetails = nil
         self.agentDependentDetails = nil
         self.residenceDependentDetails = nil
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard let service = invoiceItem.parameters[.service] as? Service else { return nil }
         
         self.serviceID = service.id
         self.flatDetails = FlatServiceParametersDTO(from: invoiceItem)
         self.agentDependentDetails = AgentDependentServiceParametersDTO(from: invoiceItem)
         self.residenceDependentDetails = ResidenceDependentServiceParametersDTO(from: invoiceItem)
      }
   }
   
   struct FlatServiceParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.service] is Service,
            let pricePoint = invoiceItem.pricePointValue as? FlatPricePoint else { return nil }
         self.pricePointMetadata = pricePoint.metadata
      }
   }
   
   struct AgentDependentServiceParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      let agentCount: Int
      let duration: Int
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
         case agentCount = "agent_count"
         case duration
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.service] is Service,
            let pricePoint = invoiceItem.pricePointValue as? AgentTimePricePoint,
            let agentCount = invoiceItem.parameters[.agentCount] as? Int,
            let duration = invoiceItem.parameters[.duration] as? Int else { return nil }
         
         self.pricePointMetadata = pricePoint.metadata
         self.agentCount = agentCount
         self.duration = duration
      }
   }
   
   struct ResidenceDependentServiceParametersDTO: Codable {
      let pricePointMetadata: PricePoint.Metadata
      let residenceType: ResidenceType
      let roomCount: Int
      
      private enum CodingKeys: String, CodingKey {
         case pricePointMetadata = "price_point"
         case residenceType = "residence_type"
         case roomCount = "room_count"
      }
      
      init?(from invoiceItem: InvoiceItem) {
         guard invoiceItem.parameters[.service] is Service,
            let pricePoint = invoiceItem.pricePointValue as? ResidencePricePoint,
            let residenceType = invoiceItem.parameters[.residenceType] as? ResidenceType,
            let roomCount = invoiceItem.parameters[.roomCount] as? Int else { return nil }
         
         self.pricePointMetadata = pricePoint.metadata
         self.residenceType = residenceType
         self.roomCount = roomCount
      }
   }
}
