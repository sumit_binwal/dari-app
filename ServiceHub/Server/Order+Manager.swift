//
//  OrderManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/22/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension Order {
   class Manager {
      static let `default` = Manager()
      weak var delegate: OrderManagerDelegate?
      weak var historyDelegate: OrderManagerHistoryDelegate?
   }
}

// MARK: - Promo Code Validation
extension Order.Manager {
   func validate(
      promoCode: String,
      listing: Listing,
      serviceProvider: ServiceProvider,
      serviceDate: Date,
      timeSlot: TimeSlot?
   ) {
      let request: URLRequest = {
         var urlComponents = URLComponents(
            url: ServerURL.validatePromoCode.url,
            resolvingAgainstBaseURL: false
         )
         urlComponents?.queryItems = [
            URLQueryItem(name: "promo_code", value: promoCode),
            URLQueryItem(name: "listing", value: String(listing.id)),
            URLQueryItem(name: "service_provider", value: String(serviceProvider.id)),
            URLQueryItem(
               name: "service_delivery_date",
               value: DateFormatter.serverDate.string(from: serviceDate)
            )
         ]
         
         if let timeSlotID = timeSlot?.id {
            urlComponents?.queryItems?.append(.init(name: "time_slot", value: String(timeSlotID)))
         }
         
         guard let url = urlComponents?.url else {
            fatalError("[Order.Manager] Failed to get url.")
         }
         return URLRequest(url: url)
      }()
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: error,
               errorMessage: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: APIError.nilResponse,
               errorMessage: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: APIError.nilData,
               errorMessage: nil
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            do {
               let promoCode = try JSONDecoder().decode(Order.PromoCode.self, from: data)
               self?.delegate?.orderManager(
                  didValidatePromoCode: promoCode,
                  completedWithError: nil,
                  errorMessage: nil
               )
            } catch {
               self?.delegate?.orderManager(
                  didValidatePromoCode: nil,
                  completedWithError: APIError.dataDeserialization,
                  errorMessage: nil
               )
            }
            
         case 400...499:
            struct ErrorResponse: Decodable {
               let promoCode: [String]?
               let nonFieldErrors: [String]?
               
               enum CodingKeys: String, CodingKey {
                  case promoCode = "promo_code"
                  case nonFieldErrors = "non_field_errors"
               }
               
               var firstErrorMessage: String? {
                  self.promoCode?.first ?? self.nonFieldErrors?.first
               }
            }
            
            let errorResponse = try? JSONDecoder().decode(ErrorResponse.self, from: data)
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: APIError.notFound,
               errorMessage: errorResponse?.firstErrorMessage
            )
            
         case 500...599:
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: APIError.server,
               errorMessage: nil
            )
            
         default:
            self?.delegate?.orderManager(
               didValidatePromoCode: nil,
               completedWithError: APIError.unknown,
               errorMessage: nil
            )
         }
      }
   }
}

// MARK: - Order Placement
extension Order.Manager {
   func placeOrder(submissionDTO: Order.SubmissionDTO) {
      var request = URLRequest(url: ServerURL.placeOrder.url)
      request.httpMethod = "POST"
      
      do {
         request.httpBody = try JSONEncoder().encode(submissionDTO)
      } catch {
         self.delegate?.orderManager(
            didPlaceOrder: nil,
            completedWithError: APIError.objectSerialization,
            cardTransaction: nil
         )
         return
      }
      
      Environment.sessionManager.post(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: error,
               cardTransaction: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.nilResponse,
               cardTransaction: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.nilData,
               cardTransaction: nil
            )
            return
         }
         
         switch response.statusCode {
         
         case 202:
            enum PaymentStatus: String, Decodable {
               case pending = "PENDING"
               case ok = "OK"
            }
            struct DTO: Decodable {
               let order: Order?
               let status: PaymentStatus
               let redirectURL: URL?
               
               enum CodingKeys: String, CodingKey {
                  case order
                  case status
                  case redirectURL = "redirect_uri"
               }
            }
            do {
            let dto = try JSONDecoder().decode(DTO.self, from: data)
            
            switch dto.status {
            case .pending:
               guard let url = dto.redirectURL else { fatalError("missing url") }
               self?.delegate?.orderManager(didRedirectTo: url)
            
            case .ok:
               fatalError("ok")
            }
            } catch {
               fatalError("uncaught")
            }
            
         
         case 200...299:
            do {
               struct DTO: Decodable {
                  let order: Order
                  let card: Card?
               }
               
               let dto = try JSONDecoder().decode(DTO.self, from: data)
               let order = dto.order
               if let card = dto.card { Card.manager.addCard(card) }
               
               self?.delegate?.orderManager(
                  didPlaceOrder: order,
                  completedWithError: nil,
                  cardTransaction: nil
               )
               self?.historyDelegate?.orderManager(didPlaceOrderWithSummary: .init(from: order))
               
            } catch {
               self?.delegate?.orderManager(
                  didPlaceOrder: nil,
                  completedWithError: APIError.dataDeserialization,
                  cardTransaction: nil
               )
            }
            
         case 401, 403:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.notAuthenticated,
               cardTransaction: nil
            )
            
         case 402:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.cardDeclined,
               cardTransaction: nil
            )
         
         case 410:
            guard let catalog = try? JSONDecoder().decode(Catalog.self, from: data) else {
               self?.delegate?.orderManager(
                  didPlaceOrder: nil,
                  completedWithError: APIError.unknown,
                  cardTransaction: nil
                  
               )
               return
            }
            Service.manager.refreshCatalog(catalog)
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.resourceChanged,
               cardTransaction: nil
            )
            
         case 400...499:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.notFound,
               cardTransaction: nil
            )
            
         case 500...599:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.server,
               cardTransaction: nil
            )
            
         default:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.unknown,
               cardTransaction: nil
            )
         }
      }
   }
}

// MARK: - 3DS Success

extension Order.Manager {
   func threeDSSuccessCallback(url: URL) {
      let request = URLRequest(url: url)
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: error,
               cardTransaction: nil
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.nilResponse,
               cardTransaction: nil
            )
            return
         }
         guard let data = data else {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.nilData,
               cardTransaction: nil
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            do {
               struct DTO: Decodable {
                  let order: Order
                  let card: Card?
               }
               
               let dto = try JSONDecoder().decode(DTO.self, from: data)
               let order = dto.order
               if let card = dto.card { Card.manager.addCard(card) }
               
               self?.delegate?.orderManager(
                  didPlaceOrder: order,
                  completedWithError: nil,
                  cardTransaction: nil
               )
               self?.historyDelegate?.orderManager(didPlaceOrderWithSummary: .init(from: order))
               
            } catch {
               self?.delegate?.orderManager(
                  didPlaceOrder: nil,
                  completedWithError: APIError.dataDeserialization,
                  cardTransaction: nil
               )
            }
            
         default:
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: APIError.unknown,
               cardTransaction: nil
            )
         }
      }
   }
}

// MARK: - 3DS Failure

extension Order.Manager {
   func threeDSFailureCallback(url: URL) {
      let request = URLRequest(url: url)
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.orderManager(
               didPlaceOrder: nil,
               completedWithError: error,
               cardTransaction: nil
            )
            return
         }
         
         self?.delegate?.orderManager(
            didPlaceOrder: nil,
            completedWithError: APIError.cardDeclined,
            cardTransaction: nil
         )
      }
   }
}

// MARK: - Order History
extension Order.Manager {
   func getOrderSummaries() {
      let request = URLRequest(url: ServerURL.getOrderSummaries.url)
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.historyDelegate?.orderManager(didGetOrders: [], completedWithError: error)
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.nilResponse
            )
            return
         }
         guard let data = data else {
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.nilData
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            do {
               let orders = try JSONDecoder().decode([Order.Summary].self, from: data)
               self?.historyDelegate?.orderManager(didGetOrders: orders, completedWithError: nil)
            } catch {
               self?.historyDelegate?.orderManager(
                  didGetOrders: [],
                  completedWithError: APIError.dataDeserialization
               )
            }
            
         case 401, 403:
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.notAuthenticated
            )
            
         case 400...499:
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.notFound
            )
         
         case 500...599:
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.server
            )
            
         default:
            self?.historyDelegate?.orderManager(
               didGetOrders: [],
               completedWithError: APIError.unknown
            )
         }
      }
   }
   
   func cancelOrder(withID id: Int) {
      var request = URLRequest(url: ServerURL.orders.url.appendingPathComponent("\(id)"))
      request.httpMethod = "DELETE"
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: error
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.nilResponse
            )
            return
         }
         guard let data = data else {
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.nilData
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            struct CancelledOrder: Decodable {
               let id: Int
            }
            do {
               let cancelledOrder = try JSONDecoder().decode(CancelledOrder.self, from: data)
               self?.historyDelegate?.orderManager(
                  didCancelOrderWithID: cancelledOrder.id,
                  completedWithError: nil
               )
            } catch {
               self?.historyDelegate?.orderManager(
                  didCancelOrderWithID: nil,
                  completedWithError: APIError.dataDeserialization
               )
            }
            
         case 401, 403:
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.notAuthenticated
            )
            
         case 400...499:
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.notFound
            )
            
         case 500...599:
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.server
            )
            
         default:
            self?.historyDelegate?.orderManager(
               didCancelOrderWithID: nil,
               completedWithError: APIError.unknown
            )
         }
      }
   }
   
   func getOrder(id: Int) {
      let request = URLRequest(url: ServerURL.orders.url.appendingPathComponent("\(id)"))
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.historyDelegate?.orderManager(didGetOrder: nil, completedWithError: error)
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.historyDelegate?.orderManager(
               didGetOrder: nil,
               completedWithError: APIError.nilResponse
            )
            return
         }
         guard let data = data else {
            self?.historyDelegate?.orderManager(
               didGetOrder: nil,
               completedWithError: APIError.nilData
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            do {
               let order = try JSONDecoder().decode(Order.self, from: data)
               self?.historyDelegate?.orderManager(didGetOrder: order, completedWithError: nil)
            } catch {
               self?.historyDelegate?.orderManager(
                  didGetOrder: nil,
                  completedWithError: APIError.dataDeserialization
               )
            }
         
         default:
            self?.historyDelegate?.orderManager(
               didGetOrder: nil,
               completedWithError: APIError.unknown
            )
         }
      }
   }
}

// MARK: - Protocols
protocol OrderManagerDelegate: AnyObject {
   func orderManager(
      didValidatePromoCode promoCode: Order.PromoCode?,
      completedWithError error: Error?,
      errorMessage: String?
   )
   func orderManager(
      didPlaceOrder order: Order?,
      completedWithError error: Error?,
      cardTransaction: CardTransaction?
   )
   func orderManager(didRedirectTo url: URL)
}

protocol OrderManagerHistoryDelegate: AnyObject {
   func orderManager(didGetOrders orders: [Order.Summary]?, completedWithError error: Error?)
   func orderManager(didCancelOrderWithID id: Int?, completedWithError error: Error?)
   func orderManager(didGetOrder order: Order?, completedWithError error: Error?)
   func orderManager(didPlaceOrderWithSummary summary: Order.Summary)
}
