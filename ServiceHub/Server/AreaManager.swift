//
//  AreaManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/22/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

class AreaManager {
   static let `default` = AreaManager()
   static var dateOfLastSuccessfulAreaFetch: Date? {
      get { UserDefaults.standard.object(forKey: "dateOfLastSuccessfulAreaFetch") as? Date }
      set { UserDefaults.standard.set(newValue, forKey: "dateOfLastSuccessfulAreaFetch")}
   }
   weak var delegate: AreaManagerDelegate?
   private init() {}
   
   func getAreas() {
      var request = URLRequest(url: ServerURL.getDistricts.url)
      request.httpMethod = "GET"
      
      Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
         if let error = error {
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: error)
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: APIError.nilResponse)
            return
         }
         guard let data = data else {
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: APIError.nilData)
            return
         }
         
         switch response.statusCode {
         case 200...299:
            struct AreaLists: Codable {
               let countries: [Country]
               let cities: [City]
               let districts: [District]
            }
            do {
               let areaLists = try JSONDecoder().decode(AreaLists.self, from: data)
               Country.all = areaLists.countries
               City.all = areaLists.cities
               District.all = areaLists.districts
               Self.dateOfLastSuccessfulAreaFetch = Date()
               self?.delegate?.areaManagerDidReceiveAreas(completedWithError: nil)
               
            } catch {
               self?.delegate?.areaManagerDidReceiveAreas(
                  completedWithError: APIError.dataDeserialization)
            }
            
         case 400...499:
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: APIError.notFound)
            
         case 500...599:
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: APIError.server)
            
         default:
            self?.delegate?.areaManagerDidReceiveAreas(completedWithError: APIError.unknown)
         }
      }
   }
}

protocol AreaManagerDelegate: AnyObject {
   func areaManagerDidReceiveAreas(completedWithError error: Error?)
}
