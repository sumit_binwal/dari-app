//
//  ServiceManager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 7/17/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import Foundation

extension Service {
   class Manager {
      static let `default` = Manager()
      weak var delegate: ServiceManagerDelegate?
      private init() {}
      
      func getCatalog() {
         guard let district = District.current else {
            assertionFailure("[ServiceManager] District is nil.")
            self.delegate?.serviceManager(
               didGetCatalog: nil,
               completedWithError: APIError.nilDistrict
            )
            return
         }
         
         let request: URLRequest = {
            var urlComponents = URLComponents(
               url: ServerURL.getCatalog.url,
               resolvingAgainstBaseURL: false
            )
            urlComponents?.queryItems = [
               URLQueryItem(name: "district_id", value: "\(district.id)")
            ]
            guard let url = urlComponents?.url else {
               fatalError("[Service.Manager] Failed to get url")
            }
            return URLRequest(url: url)
         }()
         
         Environment.sessionManager.get(request: request) { [weak self] (data, response, error) in
            if let error = error {
               self?.delegate?.serviceManager(didGetCatalog: nil, completedWithError: error)
               return
            }
            guard let response = response as? HTTPURLResponse else {
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.nilResponse
               )
               return
            }
            guard let data = data else {
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.nilData
               )
               return
            }
            
            switch response.statusCode {
            case 200...299:
               do {
                  let catalog = try JSONDecoder().decode(Catalog.self, from: data)
                  self?.refreshCatalog(catalog)
                  
               } catch {
                  self?.delegate?.serviceManager(
                     didGetCatalog: nil,
                     completedWithError: APIError.dataDeserialization
                  )
               }
            
            case 401, 403:
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.notAuthenticated
               )
               
            case 400...499:
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.notFound
               )
               
            case 500...599:
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.server
               )
               
            default:
               self?.delegate?.serviceManager(
                  didGetCatalog: nil,
                  completedWithError: APIError.unknown
               )
            }
         }
      }
      
      func refreshCatalog(_ catalog: Catalog) {
         catalog.save()
         self.delegate?.serviceManager(didGetCatalog: catalog, completedWithError: nil)
      }
   }
}

// MARK: - Protocol
protocol ServiceManagerDelegate: AnyObject {
   func serviceManager(didGetCatalog catalog: Catalog?, completedWithError error: Error?)
}
