//
//  Order+SubmissionDTO.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 6/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import Foundation

extension Order {
   struct SubmissionDTO: Encodable {
      private let listingMetadata: ListingMetadata
      let serviceProviderID: Int
      let deliveryDate: Date
      let timeSlotID: Int?
      let hour: Int?
      let paymentMethod: PaymentMethod
      let promoCodeID: Int?
      let weekdays: [Int]
      let serviceParameters: ServiceParametersDTO
      let itemParameters: [ItemParametersDTO]
      let notes: String
      let card: Card?
      let cvv: String?
      let saveCard: Bool?
      private let cardPayment: CardPayment?
      
      private enum CodingKeys: String, CodingKey {
         case listingMetadata = "listing"
         case serviceProviderID = "service_provider"
         case deliveryDate = "delivery_date"
         case timeSlotID = "time_slot"
         case hour
         case paymentMethod = "payment_method"
         case promoCodeID = "promo_code"
         case weekdays
         case serviceParameters = "ordered_service"
         case itemParameters = "ordered_items"
         case notes
         case cardPayment = "card_payment"
      }
      
      func encode(to encoder: Encoder) throws {
         var container = encoder.container(keyedBy: CodingKeys.self)
         
         try container.encode(self.listingMetadata, forKey: .listingMetadata)
         try container.encode(self.serviceProviderID, forKey: .serviceProviderID)
         
         let dateString = DateFormatter.serverDate.string(from: self.deliveryDate)
         try container.encode(dateString, forKey: .deliveryDate)
         
         try container.encode(self.timeSlotID, forKey: .timeSlotID)
         try container.encode(self.hour, forKey: .hour)
         try container.encode(self.paymentMethod, forKey: .paymentMethod)
         try container.encode(self.promoCodeID, forKey: .promoCodeID)
         try container.encode(self.weekdays, forKey: .weekdays)
         try container.encode(self.serviceParameters, forKey: .serviceParameters)
         try container.encode(self.itemParameters, forKey: .itemParameters)
         try container.encode(self.notes, forKey: .notes)
         try container.encode(self.cardPayment, forKey: .cardPayment)
      }
      
      init(
         listing: Listing,
         serviceProviderID: Int,
         deliveryDate: Date,
         timeSlotID: Int?,
         hour: Int?,
         paymentMethod: PaymentMethod,
         card: Card? = nil,
         cvv: String? = nil,
         saveCard: Bool?,
         promoCodeID: Int?,
         weekdays: [Int],
         serviceParameters: ServiceParametersDTO,
         itemParameters: [ItemParametersDTO],
         notes: String
      ) {
         self.listingMetadata = .init(id: listing.id, dateModified: listing.dateModified)
         self.serviceProviderID = serviceProviderID
         self.deliveryDate = deliveryDate
         self.timeSlotID = timeSlotID
         self.hour = hour
         self.paymentMethod = paymentMethod
         self.card = card
         self.cvv = cvv
         self.saveCard = saveCard
         self.promoCodeID = promoCodeID
         self.weekdays = weekdays
         self.serviceParameters = serviceParameters
         self.itemParameters = itemParameters
         self.notes = notes
         
         if paymentMethod == .card {
            guard let card = card else {
               assertionFailure("[SubmissionDTO] Payment method is card, but card is missing.")
               self.cardPayment = nil
               return
            }
            if let token = card.token {
               self.cardPayment = .init(
                  cardID: nil,
                  cvv: nil,
                  isMada: card.isMada,
                  token: token,
                  saveCard: self.saveCard
               )
            } else {
               self.cardPayment = .init(
                  cardID: card.id,
                  cvv: cvv ?? "",
                  isMada: card.isMada,
                  token: nil,
                  saveCard: nil
               )
            }
         } else {
            self.cardPayment = nil
         }
      }
   }
}

extension Order.FormData {
   var submissionDTO: Order.SubmissionDTO? {
      guard let serviceProvider = self.serviceProvider,
         let deliveryDate = self.selectedDate,
         let paymentMethod = self.paymentMethod else { return nil }
      
      let invoiceItems = self.aggregatedInvoiceItems
      
      let serviceParameters: Order.ServiceParametersDTO = {
         for invoiceItem in invoiceItems {
            if let parameters = Order.ServiceParametersDTO(from: invoiceItem) {
               return parameters
            }
         }
         return .init(serviceID: self.selectedService.id)
      }()
      let itemParameters = invoiceItems.compactMap { Order.ItemParametersDTO(from: $0) }
      
      return Order.SubmissionDTO(
         listing: self.listing,
         serviceProviderID: serviceProvider.id,
         deliveryDate: deliveryDate,
         timeSlotID: self.selectedTimeSlot?.id,
         hour: self.selectedHour,
         paymentMethod: paymentMethod,
         card: self.card,
         cvv: cvv,
         saveCard: self.saveCard,
         promoCodeID: self.promoCode?.id,
         weekdays: self.selectedWeekdays,
         serviceParameters: serviceParameters,
         itemParameters: itemParameters,
         notes: self.notes
      )
   }
}

fileprivate struct ListingMetadata: Encodable {
   let id: Int
   let dateModified: Date
   
   private enum CodingKeys: String, CodingKey {
      case id = "listing"
      case dateModified = "date_modified"
   }
   
   func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(self.id, forKey: .id)
      
      let dateString = DateFormatter.serverTime.string(from: self.dateModified)
      try container.encode(dateString, forKey: .dateModified)
   }
}

fileprivate struct CardPayment: Encodable {
   let cardID: Int?
   let cvv: String?
   let isMada: Bool
   let token: String?
   let saveCard: Bool?
   
   private enum CodingKeys: String, CodingKey {
      case cardID = "card"
      case cvv
      case isMada = "is_mada"
      case token
      case saveCard = "save_card"
   }
}
