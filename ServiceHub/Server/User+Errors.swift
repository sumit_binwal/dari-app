//
//  User+Errors.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 4/29/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

extension User {
   struct ErrorMessages: Codable {
      let email: [String]?
      let otp: [String]?
      let password: [String]?
      let newPassword: [String]?
      let firstName: [String]?
      let lastName: [String]?
      let phoneNumber: [String]?
      let profileErrorMessages: Profile.ErrorMessages?
      let nonFieldErrors: [String]?
      
      enum CodingKeys: String, CodingKey {
         case email
         case otp = "password"
         case password = "password1"
         case newPassword = "new_password1"
         case firstName = "first_name"
         case lastName = "last_name"
         case phoneNumber = "phone_number"
         case profileErrorMessages = "customer_profile"
         case nonFieldErrors = "non_field_errors"
      }
      
      var name: [String]? {
         var messages: [String] = []
         messages.append(contentsOf: self.firstName ?? [])
         messages.append(contentsOf: self.lastName ?? [])
         return messages.isEmpty ? nil : messages
      }
      
      var addressAndOthers: [String]? {
         let messages: [String] = [self.profileErrorMessages?.all, self.nonFieldErrors]
            .compactMap { $0 }
            .flatMap { $0 }
         return messages.isEmpty ? nil : messages
      }
      
      var all: [String]? {
         let messages: [String] = [
            self.email,
            self.otp,
            self.password,
            self.firstName,
            self.lastName,
            self.phoneNumber,
            self.profileErrorMessages?.all,
            self.nonFieldErrors]
            .compactMap { $0 }
            .flatMap { $0 }
         
         return messages.isEmpty ? nil : messages
      }
   }
}

extension User.Profile {
   struct ErrorMessages: Codable {
      let districtID: [String]?
      let address: [String]?
      let latitude: [String]?
      let longitude: [String]?
      let nonFieldErrors: [String]?
      
      enum CodingKeys: String, CodingKey {
         case districtID = "district"
         case address, latitude, longitude
         case nonFieldErrors = "non_field_errors"
      }
      
      var all: [String]? {
         let messages: [String] = [
            self.districtID,
            self.address,
            self.latitude,
            self.longitude,
            self.nonFieldErrors
         ]
         .compactMap { $0 }
         .flatMap { $0 }
         
         return messages.isEmpty ? nil : messages
      }
   }
}
