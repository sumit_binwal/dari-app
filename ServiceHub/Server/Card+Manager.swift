//
//  Card+Manager.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/14/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit
import Frames

extension Card {
   class Manager: NSObject {
      static let `default` = Manager()
      weak var delegate: CardManagerDelegate?
      
      private lazy var checkoutAPIClient: CheckoutAPIClient = .init(
         publicKey: Environment.checkoutPublicKey,
         environment: Environment.checkoutEnvironment
      )
      lazy var cardUtils: CardUtils = .init()
      var maximumCVVLength: Int { 4 }
      var acceptedSchemes: [CardScheme] {
         [
            .visa,
            .mastercard,
            .mada/*,
            .maestro,
            .americanExpress,
            .jcb,
            .dinersClub,
            .discover*/
         ]
      }
      override private init() { super.init() }
   }
}

// MARK: - Card Utilities
extension Card.Manager {
   func standardize(cardNumber: String) -> String {
      return self.cardUtils.standardize(cardNumber: cardNumber)
   }
   
   /// Returns nil if card number doesn't have a type.
   func format(cardNumber: String) -> String? {
      guard let cardType = self.cardUtils.getTypeOf(cardNumber: cardNumber) else { return nil }
      return self.cardUtils.format(cardNumber: cardNumber, cardType: cardType)
   }
   
   func scheme(for cardNumber: String) -> CardScheme? {
      let standardNumber = self.standardize(cardNumber: cardNumber)
      return self.cardUtils.getTypeOf(cardNumber: standardNumber)?.scheme
   }
   
   func schemeName(for bin: String) -> String? {
      self.cardUtils.getTypeOf(cardNumber: bin)?.name
   }
   
   func schemeIcon(for bin: String) -> UIImage? {
      self.cardUtils.getTypeOf(cardNumber: bin)?.scheme.icon
   }
   
   func schemeTemplateIcon(for bin: String) -> UIImage? {
       self.cardUtils.getTypeOf(cardNumber: bin)?.scheme.iconTemplate ?? schemeIcon(for: bin)
   }
}

// MARK: - Validation
extension Card.Manager {
   func maximumValidLength(for cardNumber: String) -> Int? {
      let standardizedCardNumber = self.standardize(cardNumber: cardNumber)
      return self.cardUtils.getTypeOf(cardNumber: standardizedCardNumber)?.validLengths.last
   }
   
   func maximumValidCVVLength(for cardNumber: String) -> Int {
      let standardCardNumber = self.standardize(cardNumber: cardNumber)
      return self.cardUtils.getTypeOf(
         cardNumber: standardCardNumber)?.validCvvLengths.last ?? maximumCVVLength
   }
   
   func isValid(cvv: String, cardNumber: String) -> Bool {
      let standardizedCardNumber = self.standardize(cardNumber: cardNumber)
      guard let cardType = cardUtils.getTypeOf(cardNumber: standardizedCardNumber) else {
         return false
      }
      return self.cardUtils.isValid(cvv: cvv, cardType: cardType)
   }
   
   func isValid(expiryMonth: String, expiryYear: String) -> Bool {
      self.cardUtils.isValid(expirationMonth: expiryMonth, expirationYear: expiryYear)
   }
   
   func isValid(cardNumber: String) -> Bool {
      let standardNumber = self.standardize(cardNumber: cardNumber)
      return self.cardUtils.isValid(cardNumber: standardNumber)
   }
   
   func cardTypeIsAccepted(for cardNumber: String) -> Bool {
      let standardizedNumber = self.standardize(cardNumber: cardNumber)
      guard let cardType = self.cardUtils.getTypeOf(cardNumber: standardizedNumber) else {
         return false
      }
      return self.acceptedSchemes.contains(cardType.scheme)
   }
}

// MARK: - Tokenization, Saving, and Deletion
extension Card.Manager {
   func addCard(_ card: Card) {
      User.current?.cards.append(card)
      self.delegate?.cardManager(
         didAddCard: card,
         completedWithError: nil,
         errorResponse: nil
      )
   }
   
   func addCard(
      cardNumber: String,
      expiryMonth: String,
      expiryYear: String,
      cvv: String,
      name: String?
   ) {
      self.tokenizeCard(
         cardNumber: cardNumber,
         expiryMonth: expiryMonth,
         expiryYear: expiryYear,
         cvv: cvv) { response in
            var request = URLRequest(url: ServerURL.addCard.url)
            request.httpMethod = "POST"
            
            struct TokenDTO: Codable {
               let token: String
            }
            
            do {
               request.httpBody = try JSONEncoder().encode(TokenDTO(token: response.token))
            } catch {
               self.delegate?.cardManager(
                  didAddCard: nil,
                  completedWithError: APIError.objectSerialization,
                  errorResponse: nil
               )
               return
            }
            
            Environment.sessionManager.post(request: request) {
               [weak self] (data, response, error) in
               
               if let error = error {
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: error,
                     errorResponse: nil
                  )
                  return
               }
               guard let response = response as? HTTPURLResponse else {
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.nilResponse,
                     errorResponse: nil
                  )
                  return
               }
               guard let data = data else {
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.nilData,
                     errorResponse: nil
                  )
                  return
               }
               
               switch response.statusCode {
               case 200...299:
                  do {
                     let card = try JSONDecoder().decode(Card.self, from: data)
                     self?.addCard(card)
                     
                  } catch {
                     self?.delegate?.cardManager(
                        didAddCard: nil,
                        completedWithError: APIError.dataDeserialization,
                        errorResponse: nil
                     )
                  }
                  
               case 401, 403:
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.notAuthenticated,
                     errorResponse: nil
                  )
                  
               case 400...499:
                  let errorResponse = try? JSONDecoder().decode(
                     ErrorResponse.self,
                     from: data
                  )
                  
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.notFound,
                     errorResponse: errorResponse
                  )
                  
               case 500...599:
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.server,
                     errorResponse: nil
                  )
                  
               default:
                  self?.delegate?.cardManager(
                     didAddCard: nil,
                     completedWithError: APIError.unknown,
                     errorResponse: nil
                  )
                  
               }
            }
      }
   }
   
   func tokenizeCard(
      cardNumber: String,
      expiryMonth: String,
      expiryYear: String,
      cvv: String,
      name: String? = nil,
      billingAddress: CkoAddress? = nil,
      phone: CkoPhoneNumber? = nil,
      completion: ((CkoCardTokenResponse) -> Void)?
   ) {
      let standardizedNumber = standardize(cardNumber: cardNumber)
      let cardTokenRequest = CkoCardTokenRequest(
         number: standardizedNumber,
         expiryMonth: expiryMonth,
         expiryYear: expiryYear,
         cvv: cvv,
         name: name,
         billingAddress: nil,
         phone: nil
      )
      
      self.checkoutAPIClient.createCardToken(
         card: cardTokenRequest,
         successHandler: { response in
            completion?(response)
            
      }) { error in
         
         self.delegate?.cardManager(
            didAddCard: nil,
            completedWithError: APIError.cardTokenization(codes: error.errorCodes),
            errorResponse: nil
         )
      }
   }
   
   func deleteCard(_ card: Card) {
      var request = URLRequest(url: ServerURL.deleteCard.url)
      request.httpMethod = "POST"
      
      struct CardDTO: Codable {
         let id: Int
      }
      
      do {
         request.httpBody = try JSONEncoder().encode(CardDTO(id: card.id))
      } catch {
         self.delegate?.cardManager(
            didDeleteCard: card,
            completedWithError: APIError.objectSerialization
         )
         return
      }
      
      Environment.sessionManager.post(request: request) {
         [weak self] (_, response, error) in
         
         if let error = error {
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: error
            )
            return
         }
         guard let response = response as? HTTPURLResponse else {
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: APIError.nilResponse
            )
            return
         }
         
         switch response.statusCode {
         case 200...299:
            User.current?.cards.removeAll { $0 == card }
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: nil
            )
            
         case 401, 403:
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: APIError.notAuthenticated
            )
            
         case 400...499:
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: APIError.notFound
            )
            
         case 500...599:
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: APIError.server
            )
            
         default:
            self?.delegate?.cardManager(
               didDeleteCard: card,
               completedWithError: APIError.unknown
            )
         }
      }
   }
}

// MARK: - Protocol
protocol CardManagerDelegate: AnyObject {
   func cardManager(
      didAddCard card: Card?,
      completedWithError error: Error?,
      errorResponse: Card.Manager.ErrorResponse?
   )
   func cardManager(didDeleteCard card: Card, completedWithError error: Error?)
}

extension CardScheme {
   var icon: UIImage? {
       print(rawValue)
       if rawValue == "mada"
       {
           return UIImage(named: "mada")
       }
       return UIImage(named: rawValue)
       
   }
   var iconTemplate: UIImage? {
       print(rawValue)
       return UIImage(named: "\(rawValue).template")
       
   }
}

// MARK: - URLSessionDelegate
extension Card.Manager: URLSessionDelegate {}

// MARK: - Card Tokenization Error Response
extension Card.Manager {
   struct ErrorResponse: Codable, Equatable {
      let code: String
      let summary: String
      
      private enum CodingKeys: String, CodingKey {
         case code = "response_code"
         case summary = "response_summary"
      }
      
      var description: String { "\(summary) (\(code))." }
      
      static func ==(lhs: ErrorResponse, rhs: ErrorResponse) -> Bool { lhs.code == rhs.code }
   }
}
