//
//  TrackingConsentModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 04/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol TrackingConsentModuleFactory {
   func makeTrackingConsentView() -> TrackingConsentView
}
