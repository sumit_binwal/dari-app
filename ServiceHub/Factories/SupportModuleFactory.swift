//
//  SupportModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 16/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol SupportModuleFactory {
   func makeSupportView() -> SupportModuleView
}
