//
//  CoordinatorFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 02/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol CoordinatorFactory {
   func makeFirstLaunchCoordinator() -> (
      coordinator: Coordinator & FirstLaunchCoordinatorOutput,
      toPresent: Presentable
   )
   
   func makeAuthCoordinator(
      router: Router
   ) -> Coordinator & AuthCoordinatorInput & AuthCoordinatorOutput
   
   func makeAuthCoordinator() -> (
      coordinator: Coordinator & AuthCoordinatorInput & AuthCoordinatorOutput,
      toPresent: Presentable
   )
   
   func makeForgotPasswordCoordinator(router: Router) -> Coordinator & ForgotPasswordCoordinatorOutput
   
   func makeProfileCoordinator(router: Router) -> Coordinator & ProfileCoordinatorOutput
   
   func makeMainCoordinator() -> (coordinator: Coordinator, toPresent: Presentable)
   
   func makeCatalogCoordinator(router: Router) -> Coordinator & CatalogCoordinatorOutput
   
   func makePlaceOrderCoordinator(
      data: Order.FormData,
      router: Router
   ) -> Coordinator & PlaceOrderCoordinatorInput & PlaceOrderCoordinatorOutput
   
   func makeAddCardCoordinator() -> (
      coordinator: Coordinator & AddCardCoordinatorInput & AddCardCoordinatorOutput,
      toPresent: Presentable
   )
   
   func makeOrdersCoordinator(router: Router) -> Coordinator
   
   func makeSupportCoordinator(
      router: Router
   ) -> Coordinator & SupportCoordinatorInput & SupportCoordinatorOutput
   
   func makeSupportCoordinator() -> (
      coordinator: Coordinator & SupportCoordinatorInput & SupportCoordinatorOutput,
      toPresent: Presentable
   )
}
