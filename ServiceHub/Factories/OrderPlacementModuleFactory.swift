//
//  OrderPlacementModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 13/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import Foundation

protocol OrderPlacementModuleFactory {
   func makeServiceProviderSelectorView(data: Order.FormData) -> ServiceProviderSelectorView
   func makeOrderFormView(data: Order.FormData) -> OrderFormModuleView
   func makeListingDescriptionView(listing: Listing) -> ListingDescriptionView
   func makeDateFormView(data: Order.FormData) -> DateFormModuleView
   func makeOrderSummaryView(data: Order.FormData) -> OrderSummaryModuleView
   func makeOrderConfirmationView(order: Order) -> OrderConfirmationModuleView
   func makePaymentOTPController(url: URL) -> PaymentOTPView
}
