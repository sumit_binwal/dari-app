//
//  ProfileModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 10/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol ProfileModuleFactory {
   func makeProfileView(user: User) -> ProfileView
   func makeNameChangeView() -> ProfileChangeView
   func makePhoneChangeView() -> ProfileChangeView
   func makeEmailChangeView() -> ProfileChangeView
   func makePasswordChangeView() -> ProfileChangeView
   func makeAddressView(address: String?, latitude: Double?, longitude: Double?) -> AddressChangeView
   func makeCardListView() -> CardListView
}
