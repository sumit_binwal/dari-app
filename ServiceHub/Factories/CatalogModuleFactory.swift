//
//  CatalogModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 08/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol CatalogModuleFactory {
   func makeCatalogView() -> CatalogModuleView
   func makeListingsView(_ listing: Listing) -> ListingsView
   func makeDistrictSelectorView() -> DistrictSelectorView
}
