//
//  FirstLaunchModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol FirstLaunchModuleFactory: AuthModuleFactory {
   func makeFirstLaunchView() -> FirstLaunchView
}
