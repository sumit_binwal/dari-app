//
//  CoordinatorFactoryImp.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 04/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class CoordinatorFactoryImp: CoordinatorFactory {
   
   func makeFirstLaunchCoordinator() -> (
      coordinator: Coordinator & FirstLaunchCoordinatorOutput,
      toPresent: Presentable
   ) {
      let navigationController = UINavigationController()
      let router = RouterImp(rootViewController: navigationController)
      let coordinator = FirstLaunchCoordinator(
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
      return (coordinator: coordinator, toPresent: navigationController)
   }
   
   func makeAuthCoordinator() -> (
      coordinator: Coordinator & AuthCoordinatorInput & AuthCoordinatorOutput,
      toPresent: Presentable
   ) {
      let navigationController = UINavigationController()
      let router = RouterImp(rootViewController: navigationController)
      let coordinator = self.makeAuthCoordinator(router: router)
      return (coordinator: coordinator, toPresent: navigationController)
   }
   
   func makeAuthCoordinator(
      router: Router
   ) -> Coordinator & AuthCoordinatorInput & AuthCoordinatorOutput {
      AuthCoordinator(
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   func makeForgotPasswordCoordinator(
      router: Router
   ) -> Coordinator & ForgotPasswordCoordinatorOutput {
      ForgotPasswordCoordinator(router: router, factory: ModuleFactory())
   }
   
   func makeMainCoordinator() -> (coordinator: Coordinator, toPresent: Presentable) {
      let navigationController = UINavigationController()
      let router = RouterImp(rootViewController: navigationController)
      
      let coordinator = MainCoordinator(
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
      return (coordinator: coordinator, toPresent: navigationController)
   }
   
   func makeCatalogCoordinator(router: Router) -> Coordinator & CatalogCoordinatorOutput {
      CatalogCoordinator(
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   func makeProfileCoordinator(router: Router) -> Coordinator & ProfileCoordinatorOutput {
      ProfileCoordinator(
         router: router, 
         factory: ModuleFactory(), 
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   func makePlaceOrderCoordinator(
      data: Order.FormData,
      router: Router
   ) -> Coordinator & PlaceOrderCoordinatorInput & PlaceOrderCoordinatorOutput {
      PlaceOrderCoordinator(
         data: data,
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   func makeAddCardCoordinator() -> (
      coordinator: Coordinator & AddCardCoordinatorInput & AddCardCoordinatorOutput,
      toPresent: Presentable
   ) {
      let navigationController = UINavigationController()
      let router = RouterImp(rootViewController: navigationController)
      let coordinator = AddCardCoordinator(router: router, factory: ModuleFactory())
      coordinator.animateStart = false
      return (coordinator: coordinator, toPresent: navigationController)
   }
   
   func makeOrdersCoordinator(router: Router) -> Coordinator {
      OrdersCoordinator(
         router: router,
         factory: ModuleFactory(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   func makeSupportCoordinator(
      router: Router
   ) -> Coordinator & SupportCoordinatorInput & SupportCoordinatorOutput {
      SupportCoordinator(router: router, factory: ModuleFactory())
   }
   
   func makeSupportCoordinator() -> (
      coordinator: Coordinator & SupportCoordinatorInput & SupportCoordinatorOutput,
      toPresent: Presentable
   ) {
      let navigationController = UINavigationController()
      let router = RouterImp(rootViewController: navigationController)
      let coordinator = self.makeSupportCoordinator(router: router)
      return (coordinator: coordinator, toPresent: navigationController)
   }
}
