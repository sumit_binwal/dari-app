//
//  ModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 04/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

import UIKit

final class ModuleFactory:
   SplashModuleFactory,
   TrackingConsentModuleFactory,
   FirstLaunchModuleFactory,
   AuthModuleFactory
{
   
   func makeSplashView() -> SplashView {
      SplashScreenViewController()
   }
   
   func makeTrackingConsentView() -> TrackingConsentView {
      TrackingConsentViewController()
   }
   
   func makeFirstLaunchView() -> FirstLaunchView {
      AuthOptionsViewController()
   }
   
   func makeMainView() -> MainView {
      MainViewController()
   }
   
   func makeDistrictSelectorView() -> DistrictSelectorView {
      DistrictSelectorViewController()
   }
   
   func makeSignUpView() -> SignUpView {
      AuthViewController()
   }
   
   func makeSignInView() -> SignInView {
      LogInViewController()
   }
   
   func makeForgotPasswordView(email: String?) -> ForgotPasswordView {
      let formView = OTPRequestFormView()
      formView.email = email
      formView.refreshRegisterButton()
      return UserChangeViewController(formView: formView)
   }
   
   func makeResetPasswordView(email: String?) -> ResetPasswordView {
      let formView = OTPSubmissionFormView()
      formView.email = email
      return UserChangeViewController(formView: formView)
   }
   
   func makeRatingView() -> RatingView {
      RatingController()
   }
}

// MARK: - ProfileModuleFactory

extension ModuleFactory: ProfileModuleFactory {
   
   func makeProfileView(user: User) -> ProfileView {
      UserViewController(user: user)
   }
   
   func makeAddressView(
      address: String?,
      latitude: Double?,
      longitude: Double?
   ) -> AddressChangeView {
      AddressViewController(address: address, latitude: latitude, longitude: longitude)
   }
   
   func makeNameChangeView() -> ProfileChangeView {
      UserChangeViewController(formView: NameChangeFormView())
   }
   
   func makePhoneChangeView() -> ProfileChangeView {
      UserChangeViewController(formView: PhoneChangeFormView())
   }
   
   func makeEmailChangeView() -> ProfileChangeView {
      UserChangeViewController(formView: EmailChangeFormView())
   }
   
   func makePasswordChangeView() -> ProfileChangeView {
      UserChangeViewController(formView: PasswordChangeFormView())
   }
   
   func makeCardListView() -> CardListView {
      CardsViewController()
   }
}

// MARK: - CatalogModuleFactory

extension ModuleFactory: CatalogModuleFactory {
   
   func makeCatalogView() -> CatalogModuleView {
      CatalogViewController()
   }
   
   func makeListingsView(_ listing: Listing) -> ListingsView {
      SublistingsViewController(listing: listing)
   }
}

// MARK: - OrderPlacementModuleFactory

extension ModuleFactory: OrderPlacementModuleFactory {
   
   func makeServiceProviderSelectorView(data: Order.FormData) -> ServiceProviderSelectorView {
      ServiceProvidersViewController(data: data)
   }
   
   func makeOrderFormView(data: Order.FormData) -> OrderFormModuleView {
      OrderFormViewController(data: data)
   }
   
   func makeListingDescriptionView(listing: Listing) -> ListingDescriptionView {
      ListingDescriptionViewController(listing)
   }
   
   func makeDateFormView(data: Order.FormData) -> DateFormModuleView {
      DateFormViewController(data: data)
   }
   
   func makeOrderSummaryView(data: Order.FormData) -> OrderSummaryModuleView {
      OrderSummaryViewController(data: data)
   }
   
   func makeOrderConfirmationView(order: Order) -> OrderConfirmationModuleView {
      OrderConfirmationViewController(order: order)
   }
   
   func makePaymentOTPController(url: URL) -> PaymentOTPView {
      PaymentOTPController(
         url: url,
         successURL: ServerURL.successfulPaymentCallback.url,
         failureURL: ServerURL.failedPaymentCallback.url
      )
   }
}

// MARK: - OrderModuleFactory

extension ModuleFactory: OrderModuleFactory {
   
   func makeOrderDetailView(_ order: Order) -> OrderDetailView {
      OrderViewController(order)
   }
   
   
   func makeOrderListView() -> OrderListView {
      OrdersViewController()
   }
}

// MARK: - CardModuleFactory

extension ModuleFactory: CardModuleFactory {
   
   func makeAddCardView(isForPayment: Bool) -> AddCardView {
      CardFormViewController(purpose: isForPayment ? .pay : .add)
   }
}

// MARK: - SupportModuleFactory

extension ModuleFactory: SupportModuleFactory {
   
   func makeSupportView() -> SupportModuleView {
      SupportViewController()
   }
}
