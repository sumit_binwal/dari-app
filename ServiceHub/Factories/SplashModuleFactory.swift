//
//  SplashModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

// TODO: This is overkill. Find a better way to do it.
protocol SplashModuleFactory {
   func makeSplashView() -> SplashView
}
