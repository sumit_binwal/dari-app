//
//  OrderModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 15/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol OrderModuleFactory {
   func makeOrderListView() -> OrderListView
   func makeOrderDetailView(_ order: Order) -> OrderDetailView
}
