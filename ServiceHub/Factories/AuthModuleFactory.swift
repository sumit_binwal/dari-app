//
//  AuthModuleFactory.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 06/02/2021.
//  Copyright © 2021 Venture X. All rights reserved.
//

protocol AuthModuleFactory {
   func makeDistrictSelectorView() -> DistrictSelectorView
   func makeSignUpView() -> SignUpView
   func makeSignInView() -> SignInView
   func makeForgotPasswordView(email: String?) -> ForgotPasswordView
   func makeResetPasswordView(email: String?) -> ResetPasswordView
   func makeAddressView(address: String?, latitude: Double?, longitude: Double?) -> AddressChangeView
}
