//
//  AppDelegate.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/21/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit
import os.log
import Intercom
import UserNotifications
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   // MARK: - Properties
   
   var window: UIWindow?
   private lazy var appCoordinator: Coordinator & AppCoordinatorInput = makeAppCoordinator()
   
   // MARK: App Life Cycle
   
   func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
   ) -> Bool {
      
      self.performFirstLaunchOperationsIfNecessary()
      self.performUpdateOperationsIfNecessary()
      
      self.configureAppearance()
      self.appCoordinator.start()
      
      TrackingManager.configureTracking(application, launchOptions: launchOptions)
      
      return true
   }
   
   func applicationDidBecomeActive(_ application: UIApplication) {
      EventLogger.appDidBecomeActive(application)
      checkVersion()
   }
   
   func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
   ) {
      if TrackingManager.isTrackingEnabled {
         Intercom.setDeviceToken(deviceToken)
//         Freshchat.sharedInstance().setPushRegistrationToken(deviceToken)
      }
      TrackingManager.sendPushNotificationToken(deviceToken)
   }
   
   func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error
   ) {
      print("[AppDelegate] Failed to register remote notifications: \(error.localizedDescription)")
   }
   
   func application(
      _ app: UIApplication,
      open url: URL,
      options: [UIApplication.OpenURLOptionsKey : Any] = [:]
   ) -> Bool {
      ApplicationDelegate.shared.application(
         app,
         open: url,
         sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
         annotation: options[UIApplication.OpenURLOptionsKey.annotation]
      )
   }
   
   func application(
      _ application: UIApplication,
      didReceiveRemoteNotification userInfo: [AnyHashable : Any],
      fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
   {
       print(userInfo)
       completionHandler(.newData)

//      if Freshchat.sharedInstance().isFreshchatNotification(userInfo) {
//         Freshchat.sharedInstance().handleRemoteNotification(
//            userInfo,
//            andAppstate: application.applicationState
//         )
//      }
   }
   
    
   func applicationDidEnterBackground(_ application: UIApplication) {
      UIApplication.shared.applicationIconBadgeNumber = 0
   }
   
   // MARK: Init
   override init() {
      super.init()
      UIFont.overrideInitialize()
   }
   
   // MARK: - Helpers
   
   private func configureAppearance() {
      window?.tintColor = .tint
      
      UINavigationBar.appearance().barTintColor = .bar
      UINavigationBar.appearance().isTranslucent = true
      UINavigationBar.appearance().titleTextAttributes = [
         .font: UIFont.boldFont(ofSize: 17),
         .foregroundColor: UIColor.text
      ]
      
      UITabBar.appearance().barTintColor = .bar
      UITabBar.appearance().isTranslucent = true
      UITabBar.appearance().unselectedItemTintColor = .unhighlightedBarItem
      [UIControl.State.normal, .selected].forEach {
         UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.regularFont(ofSize: 10)], for: $0)
      }
      
      UIToolbar.appearance().barTintColor = .bar
      UIToolbar.appearance().isTranslucent = true
      
      UITableView.appearance().backgroundColor = .background
      UITableView.appearance().separatorColor = .separator
      UITableViewCell.appearance().backgroundColor = .background
      
      UICollectionView.appearance().backgroundColor = .background
      UICollectionViewCell.appearance().backgroundColor = .background
      
      UITextView.appearance().backgroundColor = .background
      UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
         .defaultTextAttributes = [.foregroundColor: UIColor.prominentText]
      UILabel.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self])
         .textColor = .subtleText
   }
   
   private func makeAppCoordinator() -> Coordinator & AppCoordinatorInput {
      AppCoordinator(
         window: self.window ?? UIWindow(),
         root: RootViewController(),
         coordinatorFactory: CoordinatorFactoryImp()
      )
   }
   
   private func checkVersion() {
      guard
         let buildKey = kCFBundleVersionKey as String?,
         let buildNumber = Bundle.main.object(forInfoDictionaryKey: buildKey) as? String
      else { return }
      
      let url = ServerURL.checkVersion.url
         .appendingPathComponent(buildNumber)
         .appendingPathComponent("ios")
      
      let request = URLRequest(url: url)
      
      Environment.sessionManager.get(request: request) { [weak self] data, response, error in
         guard
            let response = response as? HTTPURLResponse,
            response.statusCode == 200,
            let data = data
         else { return }
         
         struct VersionData: Decodable {
            let isForceUpdate: Bool
            
            enum CodingKeys: String, CodingKey {
               case isForceUpdate = "is_force"
            }
         }
         
         guard let versionData = try? JSONDecoder().decode(VersionData.self, from: data) else {
            return
         }
         
         if versionData.isForceUpdate {
            self?.appCoordinator.showForceUpdate()
         }
      }
   }
}

// MARK: - First Launch and Updates

extension AppDelegate {
   private func performUpdateOperationsIfNecessary() {
      // CHECKLIST: Make sure build number is updated
      var buildNumber = UserDefaults.standard.integer(forKey: "buildNumber")
      
      guard buildNumber < 24 else { return }
      
      if buildNumber < 6 {
         Environment.language = Locale.current.languageCode
         
         buildNumber = 6
      }
      
      if buildNumber < 14 {
         ISOCountry.updateFrom13to14()
         
         District.all = []
         City.all = []
         Country.all = []
         
         try? CodableStorage().deleteData(for: "services")
         try? CodableStorage().deleteData(for: "serviceProviders")
         try? CodableStorage().deleteData(for: "orders")
         
         buildNumber = 14
      }
      
      buildNumber = 24
      
      UserDefaults.standard.set(buildNumber, forKey: "buildNumber")
   }
   
   private func performFirstLaunchOperationsIfNecessary() {
      guard UserDefaults.standard.integer(forKey: "buildNumber") == 0 else { return }
      os_log("[AppDelegate] Performing first launch operations.")
      
      Environment.language = Locale.current.languageCode
      ISOCountry.extractFromFile()
      
      SessionManager.default.setToken(nil)
      SessionManager.default.removeCSRFToken()
      
      // CHECKLIST: Make sure build number is updated
      UserDefaults.standard.set(23, forKey: "buildNumber")
   }
}
