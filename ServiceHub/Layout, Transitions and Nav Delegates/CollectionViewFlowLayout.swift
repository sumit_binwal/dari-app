//
//  CollectionViewFlowLayout.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 5/21/20.
//  Copyright © 2020 Venture X. All rights reserved.
//

import UIKit

class CollectionViewFlowLayout: UICollectionViewFlowLayout {
   override var flipsHorizontallyInOppositeLayoutDirection: Bool { return true }
   
   override init() {
      super.init()
      self.configure()
   }
   
   required init?(coder: NSCoder) {
      super.init(coder: coder)
      self.configure()
   }
   
   private func configure() {
      self.scrollDirection = .horizontal
      
      let leftMargin: CGFloat = self.collectionView?.layoutMargins.left ?? 20
      let rightMargin: CGFloat = self.collectionView?.layoutMargins.right ?? 20
      self.sectionInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
   }
   
   override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool { true }
}
