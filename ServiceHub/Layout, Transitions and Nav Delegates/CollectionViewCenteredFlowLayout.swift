//
//  CollectionViewCenteredFlowLayout.swift
//  ServiceHub
//
//  Created by Ziad Jammal on 9/29/19.
//  Copyright © 2019 Venture X. All rights reserved.
//

import UIKit

class CollectionViewCenteredFlowLayout: UICollectionViewLayout {
   weak var delegate: CollectionViewCenteredFlowLayoutDelegate?
   
   var itemSpacing: CGFloat = 10
   var sectionInsets: UIEdgeInsets = .zero
   
   override var flipsHorizontallyInOppositeLayoutDirection: Bool { return true }
   private var cache: [UICollectionViewLayoutAttributes] = []
   
   private var contentHeight: CGFloat {
      guard let collectionView = collectionView else { return 0 }
      let contentInset = collectionView.contentInset
      return collectionView.bounds.height - contentInset.top - contentInset.bottom
   }
   private var contentWidth: CGFloat = 0
   
   override var collectionViewContentSize: CGSize {
      let insets: UIEdgeInsets = {
         guard let collectionView = self.collectionView,
            let delegate = self.delegate else { return self.sectionInsets }
         return delegate.collectionView(collectionView, insetsForSectionAt: 0)
      }()
      return .init(insets.left + self.contentWidth + insets.right, self.contentHeight)
   }
   
   override func prepare() {
      guard self.cache.isEmpty, let collectionView = self.collectionView else { return }
      
      let spacing = self.itemSpacing
      let numberOfItems: Int = collectionView.numberOfItems(inSection: 0)
      
      let widths: [CGFloat] = (0 ..< numberOfItems).map {
         delegate?.collectionView(
            collectionView, widthForItemAt: IndexPath(item: $0, section: 0)) ?? contentHeight
      }
      
      contentWidth = widths.reduce(-spacing) { $0 + $1 + spacing }
      let inset: UIEdgeInsets =
         self.delegate?.collectionView(collectionView, insetsForSectionAt: 0) ?? self.sectionInsets
      
      var x: CGFloat = {
         guard self.contentWidth < collectionView.bounds.width - inset.left - inset.right else {
            return inset.left
         }
         return (collectionView.bounds.width - contentWidth) / 2 - inset.left
      }()
      
      for item in 0 ..< numberOfItems {
         let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: item, section: 0))
         attributes.frame = CGRect(x, 0, widths[item], contentHeight)
         cache.append(attributes)
         
         x += widths[item] + spacing
      }
   }
   
   override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
      if let collectionView = collectionView, let inset = self.collectionView?.contentInset,
         contentWidth < collectionView.bounds.width - inset.left - inset.right {
         return cache
      }
      return cache.filter { $0.frame.intersects(rect) }
   }
   
   override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
      return cache[indexPath.item]
   }
   
   override func invalidateLayout() {
      super.invalidateLayout()
      cache.removeAll()
   }
}

protocol CollectionViewCenteredFlowLayoutDelegate: AnyObject {
   func collectionView(
      _ collectionView: UICollectionView,
      widthForItemAt indexPath: IndexPath
   ) -> CGFloat
   
   func collectionView(
      _ collectionView: UICollectionView,
      insetsForSectionAt inset: Int
   ) -> UIEdgeInsets
}
